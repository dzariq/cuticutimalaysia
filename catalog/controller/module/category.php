<?php

class ControllerModuleCategory extends Controller {

    protected function index($setting) {
        $this->language->load('module/category');

        $this->data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
        } else {
            $this->data['child_id'] = 0;
        }

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $this->data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        $options = $this->model_catalog_product->getProductOptions();

        $tmp = array();
        foreach ($options as $item) {
            if (!in_array($item['name'], $tmp)) {
                $unique[] = $item;
                $tmp[] = $item['name'];
            }
        }
//print_r($options);die;
        $this->data['options'] = $options;
        $this->data['options_unique'] = $unique;

        foreach ($categories as $category) {
            if ($category['image']) {
                $image = $this->model_tool_image->resize($category['image'], 210, 210);
            } else {
                $image = $this->model_tool_image->resize('/data/default_product.png', 210, 210);
            }
            if (($category['sort_order'] < 0) || ($category['sort_order'] >= 1000)) {
                continue;
            }

            $data = array(
                'filter_category_id' => $category['category_id']
            );

            $products = array();

            $products_details = $this->model_catalog_product->getProducts($data);

            foreach ($products_details as $value) {
                $products[] = array(
                    'product_id' => $value['product_id'],
                    'name' => $value['name'],
                    'image' => $value['image'],
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $value['product_id'])
                );
            }


            $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

            $children_data = array();


            $children = $this->model_catalog_category->getCategories($category['category_id']);

            foreach ($children as $child) {
                $data = array(
                    'filter_category_id' => $child['category_id'],
                    'filter_sub_category' => true
                );

                if ($child['image']) {
                    $c_image = $this->model_tool_image->resize($child['image'], 210, 210);
                } else {
                    $c_image = $this->model_tool_image->resize('/data/default_product.png', 210, 210);
                }

                $product_total = $this->model_catalog_product->getTotalProducts($data);

                $total += $product_total;


                if (!isset($child['linkto']))
                    $child['linkto'] = '';
                if ($child['linkto']) {
                    $link = $child['linkto'];
                } else {
                    if (isset($category['database'])) {
                        $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '&db=' . $category['database']);
                    } else {
                        $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);
                    }
                }

                $children_data[] = array(
                    'category_id' => $child['category_id'],
                    'thumb' => $c_image,
                    'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
                    'href' => $link
                );
            }


            if (!isset($category['linkto']))
                $category['linkto'] = '';
            if ($category['linkto']) {
                $link = $category['linkto'];
            } else {
                if (isset($category['database'])) {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '&db=' . $category['database']);
                } else {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id']);
                }
            }

            $this->data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                'children' => $children_data,
                'products' => $products,
                'thumb' => $image,
                'href' => $link
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/category.tpl';
        } else {
            $this->template = 'default/template/module/category.tpl';
        }

        $this->render();
    }

}

?>