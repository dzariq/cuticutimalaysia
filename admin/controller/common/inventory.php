<?php

class ControllerCommonInventory extends Controller {

    public function index() {
        $this->document->setTitle('Inventory');

        $result_product = $this->db->query('SELECT * FROM product p LEFT JOIN product_description pd ON pd.product_id = p.product_id WHERE p.status = 1');
        $this->data['products'] = array();

        $i = 0;
        foreach ($result_product->rows as $products) {
            $result_category = $this->db->query('SELECT name FROM product_to_category p2c LEFT JOIN category_description c ON c.category_id = p2c.category_id WHERE p2c.product_id = "' . $products['product_id'] . '" ');
            $result_option = $this->db->query('SELECT * FROM product_option_value pov LEFT JOIN option_value_description od ON pov.option_value_id = od.option_value_id WHERE pov.product_id = "' . $products['product_id'] . '" ');

            $this->data['products'][$i]['name'] = $products['name'];
            $this->data['products'][$i]['category'] = $result_category->row['name'];
            $z = 0;
            foreach ($result_option->rows as $option) {
                $this->data['products'][$i]['child'][$z]['option'] = $option['name'];
                $this->data['products'][$i]['child'][$z]['quantity'] = $option['quantity'];
                $z++;
            }
            $i++;
        }
        $this->template = 'common/inventory.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $this->response->setOutput($this->render());
    }

}

?>