<?php

class ControllerSubdomainCreate extends Controller {

    public function index() {
        if (isset($this->request->post['webname']) && $this->request->post['webname'] != '') {
            header('Access-Control-Allow-Origin: *');

            $this->createSubdomain($this->request->post['webname'], $this->request->post['email'], $this->request->post['firstname'], $this->request->post['theme']);

            $subject = "Your web store is ready";
            $message = "Hello,
\n
Your web is ready!.
Click here to access your web : http://" . strtolower($this->request->post['webname']) . ".creativeparttime.com
To upload images and products : http://" . strtolower($this->request->post['webname']) . ".creativeparttime.com/admin
Username: manager
Password: 10kq12yt
\n
Attached with this email is the basic Tutorial for managing your web
Your free web is valid until 30th April 2015
To purchase own domain and full package at only RM 350/year, email to : creativeparttime@gmail.com.
\n
Thank You and good luck!";

            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');
            $mail->setTo($this->request->post['email']);
            $mail->setFrom('no-reply@' . $_SERVER['HTTP_HOST']);
            $mail->setSender($this->config->get('config_name'));
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
            $mail->addAttachment('Tutorial Ecommerce.pdf');
            $mail->send();

// Send to main admin email if new account email is enabled
            $mail->setTo($this->config->get('config_email'));
            $mail->send();

            echo json_encode('success');
        } else {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/subdomain/create.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/subdomain/create.tpl';
            } else {
                $this->template = 'default/template/subdomain/create.tpl';
            }

            $this->children = array(
                'common/column_left',
                'common/column_right',
                'common/content_top',
                'common/content_bottom',
                'common/footer',
                'common/header'
            );

            $this->response->setOutput($this->render());
        }
    }

    public function createSubdomain($my_domain, $email, $firstname, $theme) {

        //register subdomain

        require("xmlapi.php"); // this can be downlaoded

        $temp = explode('.', strtolower($my_domain));

        if (count($temp) == 3 && ($temp[2] == 'my')) {
            $db_name = $temp[0] . $temp[1] . $temp[2];
        } else if (count($temp) == 2) {
            $db_name = $temp[0] . $temp[1];
        } else if (count($temp) == 3) {
            $db_name = $temp[0];
        } else {
            $db_name = $temp[0] . $temp[1];
        }

        if (!file_exists('/home/creativeparttime/public_html/ecommerce/image/data/' . strtolower($this->clean($db_name)) . '/')) {

            mkdir('/home/creativeparttime/public_html/ecommerce/image/data/' . strtolower($this->clean($db_name)) . '/', 0777, true);
        }

        $xmlapi = new xmlapi("localhost");
        $opts['user'] = 'creativeparttime';
        $opts['pass'] = 'dzariqmirza85';


        $xmlapi->set_port(2083);
        $xmlapi->password_auth($opts['user'], $opts['pass']);
        $xmlapi->set_debug(1); //output actions in the error log 1 for true and 0 false


        $cpaneluser = $opts['user'];

        $my_domain = strtolower($this->clean($my_domain));


        $databasename = "creative_ecommerce_" . $db_name;

        $databaseuser = "creative_admin";

        $databasepass = $opts['pass'];

//create database

        $createdb = $xmlapi->api1_query($cpaneluser, "Mysql", "adddb", array($databasename));

//add user to database

        $addusr = $xmlapi->api1_query($cpaneluser, "Mysql", "adduserdb", array($databasename, 'creative_admin', 'all'));

        $mysqli = new mysqli('localhost', 'creative_admin', 'dzariqmirza85', $databasename);


        if (mysqli_connect_error()) {

            die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }


        $sql = file_get_contents('creative_ecommerce_' . $theme . '.sql');

        //store client email as user admin email
        $sql = str_replace('{email}', $email, $sql);
        $sql = str_replace('{name}', $firstname, $sql);
        $sql = str_replace('{domain}', $temp[0], $sql);

        if (!$sql) {

            die('Error opening file');
        }




        mysqli_multi_query($mysqli, $sql);



        $mysqli->close();
    }

    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

}
