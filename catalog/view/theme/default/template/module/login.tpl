<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->
<div class="col-xs-12">
    <section style="height:215px">
        <?php if(!$this->customer->isLogged()){ ?>
        <h2>Members Login Here</h2>
        <input type="text" placeholder="Email" value="" id="front_email" class="form-control" />
        <input type="password" placeholder="Password" id="front_password" style="margin-top:10px" class="form-control" />
        <div class="col-md-3" style="padding-left:0px">
            <input type="button"  value="Login" onclick="front_login_form()" id="front-login" style="background: #5bb75b;color:white;margin-top:10px" class="btn btn-success" />
        </div>
        <div style="margin-top:12px" class="col-md-9">
            <a style="text-decoration: underline;font-size:12px;color:#666" href="index.php?route=account/forgotten">Lupa Password</a>
            <br/>
            <a style="text-decoration: underline;font-size:12px;color:#666" href="register">Be a Member Now</a>
        </div>
        <div style="margin-top:12px;padding:0px" class="col-md-12">
            <div class="error" id="front_message"></div>
        </div>
        <?php }else{ ?>
        <h2 style="text-decoration: none">Welcome <?php echo $this->customer->getFirstName() ?></h2>
        <?php } ?>
    </section>
</div>

<script>
    function front_login_form() {
        var data = {
            'email': $('#front_email').val(),
            'password': $('#front_password').val(),
            'json': 1
        };

        $('#front_message').html('');

        $.ajax({
            url: 'index.php?route=account/login',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json.status == 0) {

                    $('#front_message').html('Invalid Email or Password')
                } else {
                    window.location = "index.php?route=account/account";
                }
            },
        });

    }
</script>
<!-- /Auto Generated Module By HostJars http://opencart.hostjars.com -->