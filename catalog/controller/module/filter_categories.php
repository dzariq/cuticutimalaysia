<?php

################################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com    	   #
################################################################################################

class ControllerModuleFilterCategories extends Controller {

    protected function index($setting) {

        static $module = 0;
        $this->load->model('design/banner');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $this->data['limit'] = $setting['limit'];
        $this->data['scroll'] = $setting['scroll'];
        $this->data['title'] = $setting['title'];
        $this->data['description'] = $setting['description'];
        $this->data['link'] = $setting['link'];
        $this->data['linkname'] = $setting['linkname'];

        $this->data['banners'] = array();

        $results = $this->model_design_banner->getBanner($setting['banner_id']);

        $taggingArray = Array();

        $i = 0;

        $categories = $this->model_catalog_category->getCategories();

        foreach ($categories as $category) {

            if (!isset($result['linkto']))
                $result['linkto'] = '';
            if ($result['linkto']) {
                $link = $result['linkto'];
            } else {
                $link = $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url);
            }

            $data['filter_category_id'] = $category['category_id'];
            $products = $this->model_catalog_product->getProducts($data);

            foreach ($products as $result) {

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float) $result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                $productsArray[] = array(
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'product_id' => $result['product_id'],
                    'image' => $result['image'],
                    'title' => $result['name'],
                    'price' => $price,
                    'special' => $special,
                    'tagging' => $category['name'],
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $result['reviews']),
                    'link' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                );
            }
        }
        foreach ($productsArray as $result) {

            $tagArray = Array();
            if ($result['tagging'] != '') {
                $temp = explode(',', $result['tagging']);
                if (count($temp) != 0) {
                    $z = 0;
                    foreach ($temp as $val) {
                        $tagArray[$z]['tagname'] = $val;
                        $val2 = explode(' ', $val);
                        $taggingArray[$i]['tagname'] = $val;
                        if (count($val2) == 2) {
                            
                            $taggingArray[$i]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0] . '-' . $val2[1]));
                            $tagArray[$z]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0] . '-' . $val2[1]));
                        } else if (count($val2) == 3) {
                            $taggingArray[$i]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0] . '-' . $val2[1]. '-' . $val2[2]));
                            $tagArray[$z]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0] . '-' . $val2[1]. '-' . $val2[2]));
                        } else {
                            $taggingArray[$i]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0]));
                            $tagArray[$z]['tag'] = preg_replace('/[^\p{L}\p{N}\s]/u', '', strtolower($val2[0]));
                        }
                        $z++;
                    }
                }
            }

            $new_date = date('d M Y', strtotime($result['date']));

            if (file_exists(DIR_IMAGE . $result['image'])) {
                $this->data['banners'][$i] = array(
                    'title' => $result['title'],
                    'link' => $result['link'],
                    'date' => $new_date,
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 450) . '..',
                    'link_name' => $result['link_name'],
                    'tagging' => $tagArray,
                    'image' => $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']),
                    'imagelarge' => $this->model_tool_image->resize($result['imagelarge'], 524, 385),
                    'image2' => $this->model_tool_image->resize($result['image2'], $setting['image_width'], $setting['image_height']),
                    'image3' => $this->model_tool_image->resize($result['image3'], $setting['image_width'], $setting['image_height']),
                    'logo' => 'image/' . $result['logo']
                );
            }
            $i++;
        }
//echo '<pre>';
//        print_r($this->data['banners']);
//        echo '</pre>';die;

        $taggingArraynew = $this->superunique($taggingArray);


        $this->data['taggingArray'] = ($taggingArraynew);

        $this->data['module'] = $module++;

        $this->language->load('module/image_filter');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('module/image_filter');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter_categories.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/filter_categories.tpl';
        } else {
            $this->template = 'default/template/module/filter_categories.tpl';
        }

        $this->render();
    }

    protected function superunique($array) {
        $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $result[$key] = $this->superunique($value);
            }
        }

        return $result;
    }

}

?>