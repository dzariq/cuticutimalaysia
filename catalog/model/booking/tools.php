<?php

class ModelBookingTools extends Model {

    public function getBooked($product_id, $room_id, $quantityRequested = 1) {
        //get product quantity

        $product = $this->db->query('SELECT quantity FROM product WHERE product_id = "' . $product_id . '"');

        $results = $this->db->query('SELECT COUNT(*) as qty,date FROM bookings LEFT JOIN `order` ON order.order_id = bookings.order_id WHERE room_id = "' . $room_id . '" AND product_id = "' . $product_id . '"  AND (order.order_status_id = 1 OR order.order_status_id = 19) GROUP BY date ');
        $datesList = array();
        if (count($results->rows) != 0) {
            foreach ($results->rows as $date) {
                $roomLeft = $product->row['quantity'] - $date['qty'];
                if ($roomLeft < $quantityRequested) {
                    $datesList[] = $date['date'];
                }
            }
            return $datesList;
        } else {
            return null;
        }
    }

    public function getRealPrice($data, $date_booked, $product_id, $roomid, $qty) {

        $realprice = 0;

        $this->load->model('catalog/product');

        $holidays = $this->getHolidays($data['mpn']);
        $peaks = $this->getPeaks($product_id);
        $offs = $this->getOffDates($product_id);
        $booked = $this->getBooked($product_id, $roomid, $qty);
        $school = $this->getSchoolHolidays($product_id);

        $returnJson = array();
        foreach ($date_booked as $date) {
            $dayType = $this->checkDayType($date, $data['location'], $holidays, $peaks, $offs, $booked, $school);
//get room price by room id and product id
//            3-peak, 4-schoolbreak, 2-publichol, 1-weekend
            if ($dayType == 3) {
                $curPrice = $this->model_catalog_product->getPrice4($roomid, $product_id);
                $realprice += $curPrice;
            } else if ($dayType == 4) {
                $curPrice = $this->model_catalog_product->getPrice5($roomid, $product_id);
                $realprice += $curPrice;
            } else if ($dayType == 2) {
                $curPrice = $this->model_catalog_product->getPrice3($roomid, $product_id);
                $realprice += $curPrice;
            } else if ($dayType == 1) {
                $curPrice = $this->model_catalog_product->getPrice2($roomid, $product_id);
                $realprice += $curPrice;
            } else if ($dayType == 9) {
                $returnJson['status'] = 0;
                $returnJson['message'] = 'Date Not Available';
                return $returnJson;
            } else if ($dayType == 10) {
                $returnJson['status'] = 3;
                $returnJson['message'] = 'Date Not Available';
                return $returnJson;
            } else {
                $curPrice = $this->model_catalog_product->getPrice1($roomid, $product_id);
                $realprice += $curPrice;
            }
            if ($curPrice == 0) {
                $realprice += $this->model_catalog_product->getPrice1($roomid, $product_id);
            }
        }
        $returnJson['status'] = 1;
        $returnJson['message'] = $realprice;

        return $returnJson;
    }

    public function checkDayType($date, $type, $holidays, $peaks, $offs, $booked, $school) {
        if (in_array($date, $offs)) {
            return 10;
        }
        if (in_array($date, $booked)) {
            return 9;
        }
        if (in_array($date, $peaks)) {
            return 3;
        }
        if (in_array($date, $school)) {
            return 4;
        }
        if (in_array($date, $holidays)) {
            return 2;
        }
        if ($type == 1) {
            if (date('N', strtotime($date)) >= 5 && date('N', strtotime($date)) < 7) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if (date('N', strtotime($date)) >= 4 && date('N', strtotime($date)) < 6) {
                return 1;
            } else {

                return 0;
            }
        }
    }

    public function getHolidays($mpn) {
        $key = 'weight_' . $mpn . '_rate';
        $holidays = $this->db->query("SELECT value FROM setting WHERE `group` = 'weight' AND `key` = '$key' ");

        //got holidays
        if (count($holidays->rows) != 0) {
            $holiday_list = $holidays->row['value'];
            explode(',', $holiday_list);
            $lists = explode(',', $holiday_list);
            $hol = array();
            foreach ($lists as $list) {
                $hol[] = date('Y-m-d', strtotime($list));
            }

            return $hol;
        } else {
            return null;
        }
    }

    public function getPeaks($product_id) {

        $peaks = $this->db->query("SELECT isbn FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['isbn'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function getOffDates($product_id) {

        $peaks = $this->db->query("SELECT weight FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['weight'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function getSchoolHolidays($product_id) {

        $peaks = $this->db->query("SELECT length FROM product WHERE product_id = '$product_id' ");

        //got holidays
        if (count($peaks->rows) != 0) {
            $peaks_list = $peaks->row['length'];
            explode(',', $peaks_list);
            $lists = explode(',', $peaks_list);
            $peakArray = array();
            foreach ($lists as $list) {
                $peakArray[] = date('Y-m-d', strtotime($list));
            }

            return $peakArray;
        } else {
            return null;
        }
    }

    public function createDateRangeArray($strDateFrom, $strDateTo) {

        $strDateFrom = date("Y-m-d", strtotime(str_replace('/', '-', $strDateFrom)));
        $strDateTo = date("Y-m-d", strtotime(str_replace('/', '-', $strDateTo)));

        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.
        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }

        array_pop($aryRange);

        return ($aryRange);
    }

}
