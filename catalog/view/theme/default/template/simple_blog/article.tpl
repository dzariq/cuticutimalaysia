<?php if($this->config->get('blog_manager_custom_theme')) { ?>
<?php echo $blog_header; ?>


<div class="row">
    <div class="col-lg-2 visible-lg"></div>
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">

    </div>
    <div class="col-lg-2 visible-lg"></div>
</div>


<?php echo $blog_footer; ?>	
<?php } else { ?>
<?php echo $header; ?>
<div class='col-md-8 col-xs-12' style="padding:0px">
    <div id="content">
        <?php echo $content_top; ?>


        <?php if(isset($author_information_found)) { ?>
        <div class="author-info">
            <div class="left">
                <img class='img-responsive' src="<?php echo $author_image; ?>" alt="<?php echo $author_name; ?>" style="margin:0 auto;border-radius: 5px; border: 1px solid #eee;" />
            </div>
            <div class="right">
                <h1><?php echo $author_name; ?></h1>
                <?php echo $author_description; ?>
            </div>
        </div>
        <?php } else { ?>
        <h2><?php echo $heading_title; ?></h2>
        <?php } ?>

        <?php if($articles) { ?>
        <?php  foreach($articles as $article) { ?>
        <div class="article-info">
            <div class="article-title">
                <div style="padding:0" class=" col-sm-3 col-xs-7">
                <span class="blog-date "></span><time ><?php echo $article['date_added']; ?></time>
                </div>
                <div style="padding:0"  class="col-sm-9 hidden-xs">
                <span class="blog-author "></span><time style="cursor: pointer;display: inline" onclick="window.location='<?php echo $article['author_href']; ?>'" ><?php echo $article['author_name']; ?></time>
                </div>
                <div class="clearfix"></div>
                <h4><a href="<?php echo $article['href']; ?>"><?php echo $article['article_title']; ?></a><h4>
                        </div>



                        <?php if($article['image']) { ?>
                        <?php if($article['featured_found']) { ?>
                        <div class="article-image">
                            <img style='margin:0 auto' class='img-responsive' src="<?php echo $article['image']; ?>" alt="<?php echo $article['article_title']; ?>"  />
                        </div>
                        <br/>
                        <?php } else { ?>
                        <div class="article-thumbnail-image">
                            <img style='margin:0 auto' class='img-responsive' src="<?php echo $article['image']; ?>" alt="<?php echo $article['article_title']; ?>" height="100" width="100" />
                            <span class="article-description">
                                <?php echo $article['description']; ?>
                            </span>
                        </div>
                        <?php } ?>
                        <?php } ?>

                        <?php if($article['featured_found']) { ?>						
                        <div class="article-description">
                            <?php echo $article['description']; ?>
                        </div>
                        <?php } else { ?>
                        <div class="article-description">
                            <?php echo $article['description']; ?>
                        </div>
                        <?php } ?>

                        <div align="right"><a class="button blog-continue" href="<?php echo $article['href']; ?>"><?php echo $button_continue_reading; ?> &rarr;</a></div>
<div class="clearfix"></div>
<br/>
<br/>
                        <?php if(!$article['featured_found']) { ?>
                        <div class="article-thumbnail-found"></div>
                        <?php } ?>
                        <div class="article-sub-title">
                            <?php if($article['allow_comment']) { ?>
                            <span class="article-comment"><a href="<?php echo $article['comment_href']; ?>#comment-section"><?php echo $article['total_comment']; ?></a></span>
                            <?php } ?>

                        </div>
                        </div>	

                        <?php } ?>
                        <?php } else { ?>
                        <div class="buttons">
                            <div class="center">
                                <?php echo $text_no_found; ?>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="pagination"><?php echo $pagination; ?></div>

                        <?php echo $content_bottom; ?>
                        </div>
                        </div>
                        <div class='col-md-4 col-xs-12'>
                            <?php echo $column_right; ?>
                        </div>

                        <?php echo $footer; ?>	
                        <?php } ?>