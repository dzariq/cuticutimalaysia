<!-- Opencart Mobile Admin 1.0 -->
<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') && strpos($_SERVER['HTTP_USER_AGENT'], 'Opera')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n" ."<!-- This is here for the Old Opera mobile  -->"; ?> 
<!DOCTYPE html>
<!--[if IEMobile 7 ]>    <html class="no-js iem7"> <![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!--> <html> <!--<![endif]--> 
  <head>
    <meta charset="utf-8">
    
    <title><?php echo $title; ?></title>    
    <?php if ($description) { ?><meta name="description" content="<?php echo $description; ?>"><?php } ?>
    <meta name="author" content="">
    
    <base href="<?php echo $base; ?>" >
    
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="apple-touch-fullscreen" content="YES">    
    
    <meta http-equiv="cleartype" content="on">
    
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" >
    <?php } ?>
    <?php if (isset($icon)) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" >
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" >
    <?php } ?>    
    <script>
    window.location.hash = '#m';
    (function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>    
    <?php if (file_exists(DIR_TEMPLATE . $this->config->get('config_admin_template') . '/css/style.css')) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo 'view/theme/' . $this->config->get('config_admin_template') ?>/css/style.css" >
    <?php } else {?>
    <link rel="stylesheet" type="text/css" href="view/theme/oma/css/style.css" >
    <?php } ?>
  </head>
  <body>
    <div id="m">
		<?php if ($logged) { ?>
		<header>
			<a id="logout" href="<?php echo $logout; ?>" class="header-button"><?php echo $text_logout; ?></a>
			<a id="back" href="index.php?route=common/inventory/&token=<?php echo $this->session->data['token'] ?>" class="header-button">Inventory</a>
			<a id="back" href="javascript: history.go(-1)" class="header-button"><?php echo $text_back; ?></a>
			<h2 id="title"></h2>
		</header>
		<?php } ?>