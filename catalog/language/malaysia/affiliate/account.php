<?php
// Heading 
$_['heading_title']        = 'Akaun Afiliasi Saya';

// Text
$_['text_account']         = 'Akaun';
$_['text_my_account']      = 'Akaun Afiliasi Saya';
$_['text_my_tracking']     = 'Maklumat Tracking Saya';
$_['text_my_transactions'] = 'Transaksi Saya';
$_['text_edit']            = 'Ubah informasi Akaun Anda';
$_['text_password']        = 'Ganti password Anda';
$_['text_payment']         = 'Ubah cara pembayaran Anda';
$_['text_tracking']        = 'Kod Tracking Afiliasi Custom';
$_['text_transaction']     = 'Tampilkan Riwayat Transaksi Anda';
?>