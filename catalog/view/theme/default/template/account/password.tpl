<?php echo $header; ?>
<div class="<?php echo layout ?>">
<div class="row">
	<div class="col-md-12">
        <ol class="breadcrumb hidden-xs">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ol>
    </div>
    <div id="content" class="col-md-6">
        <?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
        <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="password">
            <h2><?php echo $text_password; ?></h2>
            <div class="form-group">
                <label for="password" class="required"><?php echo $entry_password; ?></label>
                <input class="form-control" type="password" id="password" name="password" value="<?php echo $password; ?>" autofocus />
                <?php if ($error_password) { ?>
                    <span class="error"><?php echo $error_password; ?></span>
                <?php } ?>
            </div>
			<div class="form-group">
                <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
                <input class="form-control" type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                <?php if ($error_confirm) { ?>
                    <span class="error"><?php echo $error_confirm; ?></span>
                <?php } ?>
            </div>
            <div class="pull-left">
                <a href="<?php echo $back; ?>" class="btn btn-info"><?php echo $button_back; ?></a>
            </div>
            <div class="pull-right">
                <a onclick="$('#password').submit();" class="btn btn-info"><?php echo $button_continue; ?></a>
            </div>
        </form>
        <?php echo $content_bottom; ?>
    </div>
</div>
</div>
<br/>
<br/>
<?php echo $footer; ?>