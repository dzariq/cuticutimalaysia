<?php echo $header ?>
<style>
    .showroomheader{
        font-size:18px;
        text-align: center;
        text-decoration: none;
        color:orange;
        cursor: pointer
    }

    .price-hi{
        padding:3px;
        margin:2px;
        background: #5fa816;
        color:white;
        font-size:14px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
    }

    .input-color {
        position: relative;
    }
    .input-color input {
        padding-left: 22px;
        width: 20%;
        font-size:10px
    }
    .input-color .color-box {
        width: 16px;
        height: 16px;
        display: inline-block;
        background-color: #ccc;
        position: absolute;
        left: 5px;
        top: 5px;
    }
</style>

<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="hotel-details" class="product-info innerpage-section-padding">
        <div class="container">
            <div class="row">        	
                <div class="col-xs-12 col-sm-12 col-md-3 side-bar left-side-bar">

                    <div class="side-bar-block booking-form-block">
                        <h2 class="selected-price"><?php echo $price ?></h2>

                        <div class="booking-form">
                            <h3>Booking Details</h3>
                            <p>Find your dream hotel today</p>

                            <form>
                                <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                                <input type="hidden" name="quantity"  value="<?php echo $minimum; ?>" />
                                <div style='display:none' class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 no-sp-1">
                                        <div class="form-group right-icon">
                                            <select  id='quantity' style='margin-top: 12px;' class="form-control" name="room-duration" id="room-duration" >
                                                <?php for($i=$minimum;$i <= 5 ;$i++){ ?><?php echo $text_quantity ?>
                                                <option <?php echo $text_quantity ?> value="<?php echo $i ?>" ><?php echo $i ?> Room(s)</option>
                                                <?php } ?>
                                            </select>
                                            <i class="fa fa-angle-down"></i>
                                        </div>


                                    </div>

                                </div>
                                <?php if($stockstatus == 'SOLD OUT'){ ?>
                                <button style="visibility:hidden;margin-top:-10px;margin-bottom:10px;color:white;width:60%;border:none;background:#a50025 !important" type="button" class="btn btn-primary" >SOLD OUT</button>
                                <?php }else{ ?>
                                <button style="width:50%;margin-top:0px;height:0px;visibility:hidden" type="button" id="button-cart" class="btn btn-primary" >Book Now</button>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12 no-sp-1">
                                        <div class="form-group right-icon">
                                            <?php if($isbn == ''){ $isbn = 1; } ?>
                                            <select onchange="calculateCheckOut();
                                                    checkRoomAvailable()" style='margin-top: 12px;' class="form-control" name="room-duration" id="room-duration" >
                                                <?php for($i=1;$i <= 5 ;$i++){ ?>
                                                <option value="<?php echo $i ?>" ><?php echo $i ?> Night(s)</option>
                                                <?php } ?>
                                            </select>
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                    </div>
                                </div>
                                <?php include "catalog/view/theme/cutiv6/template/product/product_options.tpl" ?>
                            </form>
                            <div data-scroll-to=".select_room" id='room-available-show' class='text-center'></div>
                        </div><!-- end booking-form -->
                        <div id="review"></div>
                        <div class="booking-form">
                            <h3 id="review-title"><?php echo $text_write; ?></h3>
                            <br/>
                            <br/>
                            <b><?php echo $entry_name; ?></b><br />

                            <input type="text" class="form-control" name="name" value="" />

                            <br />

                            <br />

                            <b><?php echo $entry_review; ?></b>

                            <textarea  class="form-control" name="text" cols="40" rows="8" ></textarea>

                            <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />

                            <br />

                            <b><?php echo $entry_rating; ?></b><br/> <span><?php echo $entry_bad; ?></span>&nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="1" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="2" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="3" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="4" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="5" />

                            &nbsp;<span><?php echo $entry_good; ?></span><br />

                            <br />

                            <b><?php echo $entry_captcha; ?></b><br />

                            <input class="form-control" type="text" name="captcha" value="" />

                            <br />
                            <br />

                            <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />

                            <br />

                            <div class="buttons">

                                <div class="right"><a id="button-review" style="cursor:pointer" class="btn btn-primary"><?php echo $button_continue; ?></a></div>

                            </div>
                        </div>
                    </div><!-- end side-bar-block -->

                </div><!-- end columns -->  
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                    <h2><?php echo $heading_title ?>
                        <?php $this->language->load('module/share_buttons'); $text_share_buttons = $this->language->get('text_share_buttons'); ?>
                       <!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_button_whatsapp"></a>
<a class="a2a_button_google_gmail"></a>
<a class="a2a_button_telegram"></a>
<a class="a2a_button_copy_link"></a>
<a class="a2a_button_wechat"></a>
<a class="a2a_button_sms"></a>
<a class="a2a_button_skype"></a>
<a class="a2a_button_email"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_pinterest"></a>
</div>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->
                    </h2>

                    <div class="detail-slider">
                        <div class="feature-slider">
                            <?php foreach($images as $banner){ ?>
                            <div><img src="<?php echo $banner['popup'] ?>" class="img-responsive" alt="feature-img"/></div>
                            <?php  } ?>
                        </div><!-- end feature-slider -->

                        <div class="feature-slider-nav">
                            <?php foreach($images as $banner){ ?>
                            <div><img src="<?php echo $banner['thumb'] ?>" class="img-responsive" alt="feature-thumb"/></div>
                            <?php  } ?>
                        </div><!-- end feature-slider-nav -->
                    </div>  <!-- end detail-slider -->

                    <div class="detail-tabs">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#hotel-overview" data-toggle="tab">Hotel Overview</a></li>
                            <?php $i=0;foreach($section2_images as $fac){ ?>
                            <li><a href="#fac_<?php echo $i ?>" data-toggle="tab"><?php echo $fac['caption'] ?></a></li>
                            <?php $i++; } ?>
                        </ul>

                        <div class="tab-content">

                            <div id="hotel-overview" class="tab-pane in active">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 tab-img">
                                        <img src="<?php echo $banner['main_thumb'] ?>" class="img-responsive" alt="flight-detail-img" />
                                    </div><!-- end columns -->

                                    <div class="col-sm-8 col-md-8 tab-text">
                                        <h3>Hotel Overview</h3>
                                        <p><?php echo $description ?></p>
                                    </div><!-- end columns -->
                                </div><!-- end row -->
                            </div><!-- end hotel-overview -->

                            <?php $i=0;foreach($section2_images as $fac){ ?>
                            <div id="fac_<?php echo $i ?>" class="tab-pane">
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 tab-img">
                                        <?php if($fac['image'] != 'no_image.jpg'){ ?>
                                        <img style='margin:0 auto' class='col-xs-12 img-responsive' src="<?php echo $fac['main_thumb'] ?>" />
                                        <?php } ?>
                                    </div><!-- end columns -->

                                    <div class="col-sm-8 col-md-8 tab-text">
                                        <h3><?php echo $fac['caption'] ?></h3>
                                        <p><?php echo $fac['description'] ?></p>
                                    </div><!-- end columns -->
                                </div><!-- end row -->
                            </div><!-- end restaurant -->
                            <?php $i++; } ?>

                        </div><!-- end tab-content -->
                    </div><!-- end detail-tabs -->

                    <div class="available-blocks" id="available-rooms">
                        <div class='col-xs-12'>
                            <?php if($this->customer->islogged()){ ?> 
                            <?php if($this->customer->getCustomerGroupId() == 2){ ?> 
                            <div class="input-color">
                                <input type="text" value="Normal" />
                                <div class="color-box" style="background-color: grey;"></div>
                                <!-- Replace "#FF850A" to change the color -->
                            </div>
                            <div class="input-color">
                                <input type="text" value="Weekend" />
                                <div class="color-box" style="background-color: #51a351;"></div>
                                <!-- Replace "navy" to change the color -->
                            </div>
                            <div class="input-color">
                                <input type="text" value="Public Holiday" />
                                <div class="color-box" style="background-color: #F8BB00;"></div>
                                <!-- Replace "navy" to change the color -->
                            </div>
                            <div class="input-color">
                                <input type="text" value="Super Peak" />
                                <div class="color-box" style="background-color: #EE5757;"></div>
                                <!-- Replace "navy" to change the color -->
                            </div>
                            <div class="input-color">
                                <input type="text" value="School Break" />
                                <div class="color-box" style="background-color: #FFD1D1;"></div>
                                <!-- Replace "navy" to change the color -->
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <br/>
                            <br/>
                        </div>
                        <h2 class="select_room" >Select Your Room</h2>
                        <?php foreach($section1_images as $room){ ?>
                        <div class="list-block main-block room-block">
                            <div class="list-content">
                                <div class="main-img list-img room-img">
                                    <?php if($room['image'] != 'no_image.jpg'){ ?>
                                    <a href='<?php echo $room[image] ?>' rel='prettyPhoto' >
                                        <img class='img-responsive' src="<?php echo $room['image'] ?>" />
                                    </a>
                                    <?php } ?> 
                                    <div class="main-mask">
                                        <ul class="list-unstyled list-inline offer-price-1">
                                            <li class="price" id="price-<?php echo $room[room_id] ?>">RM <?php echo $room['description2'] ?></li>

                                        </ul>
                                    </div><!-- end main-mask -->
                                </div><!-- end room-img -->

                                <div class="list-info room-info">
                                    <h3 class="block-title"><?php echo $room['caption'] ?></h3>
                                    <?php if($this->customer->islogged()){ ?> 
                                    <?php if($this->customer->getCustomerGroupId() == 2){ ?> 
                                    <br/>
                                    <table class='table-striped' style="width:100%">
                                        <tr>
                                            <?php if($room['description2'] != 0){ ?>
                                            <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: grey' class='price-hi'><?php echo $room['description2'] ?></span></td>
                                            <?php } ?>
                                            <?php if($room['price2'] != 0){ ?>
                                            <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #51a351' class='price-hi'><?php echo $room['price2'] ?></span></td>
                                            <?php } ?>
                                            <?php if($room['price3'] != 0){ ?>
                                            <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #F8BB00' class='price-hi'><?php echo $room['price3'] ?></span></td>
                                            <?php } ?>
                                            <?php if($room['price4'] != 0){ ?>
                                            <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #EE5757' class='price-hi'><?php echo $room['price4'] ?></span></td>
                                            <?php } ?>
                                            <?php if($room['price5'] != 0){ ?>
                                            <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background:  #FFD1D1' class='price-hi'><?php echo $room['price5'] ?></span></td>
                                            <?php } ?>
                                        </tr>

                                    </table>
                                    <br/>
                                    <?php } ?>
                                    <?php } ?>
                                    <!-- <p class="block-minor">Max Guest:03</p>-->
                                    <p><?php echo $room['description'] ?></p>
                                    <button type="button" onclick="book('<?php echo $room[room_id] ?>', '<?php echo $room[caption] ?>')" class="btn btn-primary" >Book Now</button>
                                    <span id='full-<?php echo $room[room_id] ?>' style='display:none' class="noroom">Fully Booked</span>
                                </div><!-- end room-info -->
                            </div><!-- end list-content -->
                        </div><!-- end room-block -->
                       
                        <?php } ?>
                    </div><!-- end columns -->
                </div><!-- end columns -->



            </div><!-- end row -->
        </div><!-- end container -->

    </div><!-- end hotel-details -->

</section><!-- end innerpage-wrapper -->

<?php  include "catalog/view/theme/cutiv6/template/product/product_footer.tpl" ?>


<?php echo $footer ?>
