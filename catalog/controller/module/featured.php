<?php

class ControllerModuleFeatured extends Controller {

    protected function index($setting) {

        $this->language->load('module/featured');



        $this->data['heading_title'] = $this->language->get('heading_title');



        $this->data['button_cart'] = $this->language->get('button_cart');



        $this->load->model('catalog/product');



        $this->load->model('tool/image');



        $this->data['products'] = array();



        $products = explode(',', $this->config->get('featured_product'));



        if (empty($setting['limit'])) {

            $setting['limit'] = 6;
        }



        $products = array_slice($products, 0, (int) $setting['limit']);


        foreach ($products as $product_id) {

            $product_info = $this->model_catalog_product->getProduct($product_id);



            if ($product_info) {

                if ($product_info['image']) {

                    $image = $this->model_tool_image->resize($product_info['image'],$setting['image_width'], $setting['image_height']);
                    $image2 = $product_info['image'];
                } else {

                    $image = false;
                    $image2 = false;
                }



                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {

                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $price = false;
                }



                if ((float) $product_info['special']) {

                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $special = false;
                }

                //percent discount
                if ($special) {
                    $percent_discount = '-' . round((($product_info['price'] - $product_info['special']) / $product_info['price']) * 100) . '%';
                } else {
                    $percent_discount = false;
                }



                if ($this->config->get('config_review_status')) {

                    $rating = $product_info['rating'];
                } else {

                    $rating = false;
                }

                if (strlen($product_info['name']) > 15) {
                    $product_info['name'] = $product_info['name'];
//                    $product_info['name'] = utf8_substr($product_info['name'], 0, 15) . '..';
                }

                //check if this is new
                $now = time(); // or your date as well
                $your_date = strtotime($product_info['date_added']);
                $datediff = $now - $your_date;
                $datediff = floor($datediff / (60 * 60 * 24));
                
                //1 month considered latest product
                if($datediff <= 30){
                    $latest = true;
                }else{
                    $latest = false;
                }

                $this->data['products'][] = array(
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'product_id' => $product_info['product_id'],
                    'latest' => $latest,
                    'thumb' => $image,
                    'thumb_large' => $image2,
                    'name' => $product_info['name'],
                    'viewed' => $product_info['viewed'],
                    'price' => $price,
                    'special' => $special,
                    'percent_discount' => $percent_discount,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $product_info['reviews']),
                    'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                );
            }
        }



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {

            $this->template = $this->config->get('config_template') . '/template/module/featured.tpl';
        } else {

            $this->template = 'default/template/module/featured.tpl';
        }



        $this->data['button_compare'] = $this->language->get('button_compare');
        $this->data['button_wishlist'] = $this->language->get('button_wishlist');
        $this->data['button_view'] = $this->language->get('button_view');
        $this->data['sidebar'] = ($setting['position'] == 'column_left' || $setting['position'] == 'column_right') ? true : false;
        $this->render();
    }

}

?>