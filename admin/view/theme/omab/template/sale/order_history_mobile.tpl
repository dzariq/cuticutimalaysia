<?php if ($error) { ?>
<div class="warning"><?php echo $error; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($histories) { ?>
<?php $id = 0;?>
<?php foreach ($histories as $history) { ?>
<section id = "history<?php echo $id ?>" class="history">
	<h4><?php echo $history['date_added']; ?>	<?php echo $history['status']; ?></h4>
	<div class="history-content">
		<table>
			<tr>
				<th><?php echo $column_notify; ?></th>
				<td class="left"><?php echo $history['notify']; ?></td>
			</tr>
			<tr>
				<th><?php echo $column_comment; ?></th>
				<td class="left"><?php echo $history['comment']; ?></td>
			</tr>
		</table>
	</div>
</section>
<?php $id++; ?>
<?php } ?>
<?php } else { ?>
<table>
	<tr>
		<td class="center" colspan="2"><?php echo $text_no_results; ?></td>
	</tr>
</table>
<?php } ?>
