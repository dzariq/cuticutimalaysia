<?php if(count($banners) != 0){ ?>
    <!-- Slider navigation -->
    <?php if(count($banners) > 1){ ?>

    <nav class="control slider_nav">
        <a href="#" class="left">&nbsp;</a>
        <a href="#" class="right">&nbsp;</a>
    </nav>

    
    <?php } ?>
    <div class="slider_wrapper nonboxsize">

        <!-- Slider content -->
        <ul class="homepage_slider nonboxsize">

            <!-- First slide -->
            <?php foreach($banners as $banner){ ?>
            <li  class="nonboxsize">
                <img class='img-responsive nonboxsize' style='margin-top:-20px;margin:0 auto' src='<?php echo $banner[image] ?>' />

            </li>
            <?php } ?>


        </ul>

    </div>

<?php } ?>