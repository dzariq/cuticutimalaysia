<?php echo $header; ?>
<section>
    <div class="container">
        <div id="register" class="row">
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger">
                <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <style>
                .error{
                color:red;
                }
            </style>
            <!--===== INNERPAGE-WRAPPER ====-->
            <section class="innerpage-wrapper">
                <div id="registration" class="innerpage-section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="flex-content">
                                    <div class="custom-form custom-form-fields">
                                        <h3>Registration</h3>
                                        <p>Keep up-to-date with latest promotions and new destinations by Registering as CutiCuti Malaysia Member.</p>
                                        <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >


                                            <div class="form-group">
                                                <input type="text" placeholder='Name' class="form-control" id="firstname" name="Name" value="<?php echo $firstname; ?>" autofocus />
                                                <?php if ($error_firstname) { ?>
                                                <span class="error"><?php echo $error_firstname; ?></span>
                                                <?php } ?>
                                                <span><i class="fa fa-user"></i></span>
                                            </div>

                                            <div class="form-group">
                                                <input type="email" placeholder='Email Address' class="form-control" id="email" name="email" value="<?php echo $email; ?>" />
                                                <?php if ($error_email) { ?>
                                                <span class="error"><?php echo $error_email; ?></span>
                                                <?php } ?>
                                                <span><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>">
                                                    <label for="customer_group_id">Account Type</label>
                                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                                    <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input onclick="check_group('<?php echo $customer_group[membership_fee]; ?>', '<?php echo $customer_group[description]; ?>', '<?php echo $customer_group[customer_group_id]; ?>')" type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id" checked="checked" />
                                                            <?php echo $customer_group['name']; ?>
                                                        </label>
                                                    </div>
                                                    <?php } else { ?>
                                                    <div class="radio">
                                                        <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>">
                                                            <input onclick="check_group('<?php echo $customer_group[membership_fee]; ?>', '<?php echo $customer_group[description]; ?>', '<?php echo $customer_group[customer_group_id]; ?>')" type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id" />
                                                            <?php echo $customer_group['name']; ?>
                                                        </label>
                                                    </div>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    <br/>
                                            </div>

                                            <div class="form-group">
                                                <input placeholder='Address' type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1; ?>" />
                                                <?php if ($error_address_1) { ?>
                                                <span class="error"><?php echo $error_address_1; ?></span>
                                                <?php } ?>
                                                 <span><i class="fa fa-address-card"></i></span>
                                            </div>

                                            <div class="form-group">
                                                <input placeholder='City' type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>" />
                                                <?php if ($error_city) { ?>
                                                <span class="error"><?php echo $error_city; ?></span>
                                                <?php } ?>
                                                 <span><i class="fa fa-building"></i></span>
                                            </div>
                                            <div class="form-group">
                                                <input placeholder='Postcode' type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
                                                <?php if ($error_postcode) { ?>
                                                <span class="error"><?php echo $error_postcode; ?></span>
                                                <?php } ?>
                                                 <span><i class="fa fa-code"></i></span>
                                            </div>
                                            <div class="form-group">
                                                <select placeholder='Country' class="form-control" id="country_id" name="country_id">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($countries as $country) { ?>
                                                    <?php if ($country['country_id'] == $country_id) { ?>
                                                    <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                                    <?php } else { ?>
                                                    <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <?php if ($error_country) { ?>
                                                <span class="error"><?php echo $error_country; ?></span>
                                                <?php } ?>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" id="zone" name="zone_id"></select>
                                                <?php if ($error_zone) { ?>
                                                <span class="error"><?php echo $error_zone; ?></span>
                                                <?php } ?>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control" placeholder='Password' type="password" id="password" name="password" value="<?php echo $password; ?>" />
                                                <?php if ($error_password) { ?>
                                                <span class="error"><?php echo $error_password; ?></span>
                                                <?php } ?>
                                                <span><i class="fa fa-lock"></i></span>
                                            </div>

                                            <div class="form-group">
                                                <input class="form-control" placeholder='Password Confirm' type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                                                <?php if ($error_confirm) { ?>
                                                <span class="error"><?php echo $error_confirm; ?></span>
                                                <?php } ?>
                                                <span><i class="fa fa-lock"></i></span>
                                            </div>
                                            
                                            <input type="hidden" id="newsletter" name="newsletter" value="1" />

                                            <?php  if($fee != ''){ ?>
                                            <div id='payment_method_section'>
                                                <?php if ($payment_methods) { ?>
                                                <h2><?php echo $text_payment_method; ?></h2>
                                                <table class="table table-hover">
                                                    <?php foreach ($payment_methods as $payment_method) { ?>
                                                    <tr>
                                                    <div class="form-group">
                                                        <div class="radio">
                                                            <label for="<?php echo $payment_method['code']; ?>">
                                                                <?php if ($payment_method['code'] == $code || !$code) { ?>
                                                                <?php $code = $payment_method['code']; ?>
                                                                <input  type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
                                                                <?php } else { ?>
                                                                <input  type="radio"  name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
                                                                <?php } 
                                                                echo $payment_method['title']; ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    </tr>
                                                    <?php } ?>
                                                </table>
                                                <?php } ?>
                                            </div>
                                            <?php } ?>
                                            <div style='padding:0px' class="col-xs-12 ">
                                                <?php if ($text_agree) {
                                                echo $text_agree;
                                                if ($agree) { ?>
                                                <input style="margin: 4px 15px 0 10px;" type="checkbox" id="agree" name="agree" value="1" checked="checked" />
                                                <?php } else { ?>
                                                <input style="margin: 4px 15px 0 10px;" type="checkbox" id="agree" name="agree" value="1" />
                                                <?php } ?>
                                                <br/>
                                                <input type="hidden" id="warning" />
                                                <br/>
                                                <button type="button" onclick="process_form();" class="btn btn-orange btn-block"> REGISTER</button>
                                                <?php } else { ?>
                                                <button type="button" onclick="process_form()" class="btn btn-orange btn-block"> REGISTER</button>
                                                <?php } ?>
                                            </div>
                                        </form>

                                        <div class="other-links">
                                            <p class="link-line">Already Have An Account ? <a href="login">Login Here</a></p>
                                        </div><!-- end other-links -->
                                    </div><!-- end custom-form -->

                                    <div class="flex-content-img custom-form-img">
                                        <img src="image/login_image.jpg" class="img-responsive" alt="registration-img" />
                                    </div><!-- end custom-form-img -->
                                </div><!-- end form-content -->

                            </div><!-- end columns -->
                        </div><!-- end row -->
                    </div><!-- end container -->         
                </div><!-- end registration -->
            </section><!-- end innerpage-wrapper -->
        </div>
        <div class="row">
            <div id="paymentSection" >
            </div>
        </div>

        <div class="sep-bor"></div>
    </div>
</section>
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').on('change', function () {
        var customer_group = [];
                <?php foreach ($customer_groups as $customer_group) { ?>
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ] = [];
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
                <?php } ?>
                if (customer_group[this.value]) {
            if (customer_group[this.value]['company_id_display'] == '1') {
                $('#company-id-display').show();
            } else {
                $('#company-id-display').hide();
            }

            if (customer_group[this.value]['company_id_required'] == '1') {
                $('#company-id-required').show();
            } else {
                $('#company-id-required').hide();
            }

            if (customer_group[this.value]['tax_id_display'] == '1') {
                $('#tax-id-display').show();
            } else {
                $('#tax-id-display').hide();
            }

            if (customer_group[this.value]['tax_id_required'] == '1') {
                $('#tax-id-required').show();
            } else {
                $('#tax-id-required').hide();
            }
        }
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function () {
        $.ajax({
            url: 'index.php?route=account/register/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function () {
                $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function () {
                $('.wait').remove();
            },
            success: function (json) {
                if (json['postcode_required'] == '1') {
                    $('#postcode-required').show();
                } else {
                    $('#postcode-required').hide();
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--



    function process_form() {
        if ($('#agree').prop('checked') == true) {
            var data = {
                'firstname': $('#firstname').val(),
                'email': $('#email').val(),
                'customer_group_id': $('#customer_group_id').val(),
                'telephone': $('#telephone').val(),
                'address_1': $('#address_1').val(),
                'city': $('#city').val(),
                'postcode': $('#postcode').val(),
                'zone_id': $('#zone').val(),
                'country_id': $('#country_id').val(),
                'password': $('#password').val(),
                'confirm': $('#confirm').val(),
                'agree': 1,
                'newsletter': $('#newsletter').val(),
                'payment_method': $("input[name=payment_method]:checked").val()

            };
        } else {
            var data = {
                'firstname': $('#firstname').val(),
                'email': $('#email').val(),
                'customer_group_id': $('#customer_group_id').val(),
                'telephone': $('#telephone').val(),
                'address_1': $('#address_1').val(),
                'city': $('#city').val(),
                'postcode': $('#postcode').val(),
                'zone_id': $('#zone').val(),
                'country_id': $('#country_id').val(),
                'password': $('#password').val(),
                'confirm': $('#confirm').val(),
                'newsletter': $('#newsletter').val(),
                'payment_method': $("input[name=payment_method]:checked").val()

            };

        }
        console.log(data)
        $('.error').html('')

        $.ajax({
            url: 'index.php?route=account/register',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function (json) {
                console.log(json)
                if (json.status == 0) {
                    $.each(json.remark, function (key, value) {
                        $('#' + key).after('<span class="error">' + value + '</span>')
                        $('#' + key).focus()
                        if(key == 'warning'){
                            alert(value)
                        }
                    });
                } else {
                    $('#register').hide('slow')
                    if (json.redirect == 1) {
                        window.location = "index.php?route=account/success";
                    } else {
                        $('#paymentSection').load('index.php?route=account/register/payment_' + json.payment_method)
                        $('#paymentSection').focus()
                    }

                }
            },
        });


    }



    function check_group(fee, desc, id) {
        if (fee != '') {
            $('#payment_method_section').show('slow')
        } else {
            $('#payment_method_section').hide('slow')
        }
        $('#customer_group_id').val(id)
        $('#group-desc').html(desc)
    }


//--></script> 
<?php echo $footer; ?>