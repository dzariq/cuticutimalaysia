<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->
<style>
    .message-success{
        color: white;
        display:none;
        margin-top: 20px;
        font-size: 18px;
        padding: 10px;
        background: #339933;
    }
    .message-error{
        color: white;
        display:none;
        margin-top: 20px;
        font-size: 18px;
        padding: 10px;
        background: #FF3333;
    }
</style>
<p><?php echo $description ?></p>

<input type="hidden" id="remark" class="row" />
<div class="message-success">

</div>
<div class="message-error">

</div>
<form id="booking-form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="content" style='padding: 10px;
margin-top: 2px;
background: #e0e0e0;'>
        <label><?php echo $entry_name; ?></label><br />
        <input class="form-control" type="text" id="name" value="<?php echo $name; ?>" />
        <br />
        <label><?php echo $entry_email; ?></label><br />
        <input class="form-control" type="text" id="email" value="<?php echo $email; ?>" />
        <br />
        <label><?php  echo $title ?>:</label><br />
        <select id='pakej' class="form-control" >
            <?php foreach($banners as $banner){ ?>
            <option value='<?php echo $banner[title] ?>'><?php echo $banner['title'] ?></option>
            <?php } ?>
        </select>
        <br />
        <label><?php echo $entry_enquiry ?></label><br />
        <textarea class="form-control" id="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>

        <!-- <br />
         <label><?php echo $entry_captcha; ?></label><br />
         <input type="text" id="captcha" value="<?php echo $captcha; ?>" />
         <br />
         <img src="index.php?route=information/contact/captcha" alt="" />-->
    </div>
    <br/>
    <div class="buttons">
        <div class="right"><input type="button" onclick="submitContact()" value="Hantar" class="btn btn-primary" /></div>
    </div>

</form>

<script>

    function submitContact() {

        $('.message-error').hide()
        $('.message-success').hide()
        $('.message-error').html('')
        $('.message-success').html('')

        var data = {
            'name': $('#name').val(),
            'email': $('#email').val(),
            'enquiry': '<?php echo $page ?>:' + $('#pakej').val() + '\n\n' + $('#enquiry').val(),
            'category': $('#category').val(),
            // 'captcha': $('#captcha').val(),
            'json': 1
        }

        console.log(data)
        $.ajax({
            url: $('#booking-form').attr('action'),
            type: "POST",
            dataType: 'json',
            data: data,
            success: function (data) {
                var html = '';
                html += data.remark;

                if (data.status == 1) {

                    $('.message-success').show()
                    $('.message-success').html(html)
                } else {
                    $('.message-error').show()
                    $('.message-error').html(html)
                }
                $('#remark').focus()
                console.log(data);
            }
        });

    }
</script>