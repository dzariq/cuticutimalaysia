<?php echo $header; ?>
<div id="content">
    <?php echo $content_top; ?>

    <div class='container'>
        <div class='col-sm-6 col-xs-12'>
            <?php echo $column_left; ?>
        </div>
        <div class='col-sm-6 col-xs-12'>
            <?php echo $column_right; ?>
        </div>
    </div>

    <!-- CTA Ends -->
    <?php echo $content_bottom; ?>

</div>
<?php echo $footer; ?>