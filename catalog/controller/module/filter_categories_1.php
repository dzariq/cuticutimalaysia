<?php

################################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com    	   #
################################################################################################

class ControllerModuleFilterCategories extends Controller {

    protected function index($setting) {

        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $this->data['limit'] = $setting['limit'];
        $this->data['scroll'] = $setting['scroll'];
        $this->data['title'] = $setting['title'];
        $this->data['description'] = $setting['description'];
        $this->data['link'] = $setting['link'];
        $this->data['linkname'] = $setting['linkname'];

        $this->data['banners'] = array();

        $results = $this->model_design_banner->getBanner($setting['banner_id']);

        $categories = $this->model_catalog_category->getCategories();

        foreach ($categories as $category) {

            if (!isset($result['linkto']))
                $result['linkto'] = '';
            if ($result['linkto']) {
                $link = $result['linkto'];
            } else {
                $link = $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url);
            }

            $data['filter_category_id'] = $category['category_id'];
            $products = $this->model_catalog_product->getProducts($data);

            foreach ($products as $result) {

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float) $result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                $productsArray[] = array(
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'product_id' => $result['product_id'],
                    'image' => $result['image'],
                    'name' => utf8_substr($result['name'], 0, 10) . '..',
                    'price' => $price,
                    'special' => $special,
                    'tagging' => $category['name'],
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $result['reviews']),
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                );
            }
        }
        foreach ($productsArray as $result) {

            $tagArray = Array();

            if ($result['tagging'] != '') {
                $temp = explode(',', $result['tagging']);
                if (count($temp) != 0) {
                    foreach ($temp as $val) {
                        $taggingArray[] = $val;
                        $tagArray[] = $val;
                    }
                }
            }

            if (file_exists(DIR_IMAGE . $result['image'])) {
                $this->data['banners'][] = array(
                    'title' => $result['name'],
                    'link' => $result['href'],
                    'description' => nl2br($result['description']),
                    'link_name' => '',
                    'tagging' => $tagArray,
                    'image' => $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']),
                    'imagelarge' => $this->model_tool_image->resize($result['image'], $setting['image_width'] * 1.5, $setting['image_height'] * 1.5)
                );
            }
        }


        $this->data['taggingArray'] = array_unique($taggingArray);

        $this->data['module'] = $module++;

        $this->language->load('module/featured_category');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('module/featured_category');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured_category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/featured_category.tpl';
        } else {
            $this->template = 'default/template/module/featured_category.tpl';
        }

        $this->render();
    }

}

?>