<?php echo $header; ?>
        <script type="text/javascript" src="view/javascript/jquery/jquery-ui-timepicker-addon.js"></script>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) {
        ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_install) {
    ?>
            <div class="warning"><?php echo $error_install; ?></div>
    <?php } ?>
    <?php if ($error_image) {
    ?>
            <div class="warning"><?php echo $error_image; ?></div>
    <?php } ?>
    <?php if ($error_image_cache) {
    ?>
            <div class="warning"><?php echo $error_image_cache; ?></div>
    <?php } ?>
    <?php if ($error_cache) {
    ?>
            <div class="warning"><?php echo $error_cache; ?></div>
    <?php } ?>
    <?php if ($error_download) {
    ?>
            <div class="warning"><?php echo $error_download; ?></div>
    <?php } ?>
    <?php if ($error_logs) {
    ?>
            <div class="warning"><?php echo $error_logs; ?></div>
    <?php } ?>
        <div class="box">
            <div class="content">
                <div class="overview">
                    <div class="dashboard-heading"><?php echo 'Today\'s Summary'; ?></div>
                    <div class="dashboard-content">
                        <table class="list">
                            <tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('report/sale_order', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_sale; ?></td>
                                <td class="left"><b><font color="green"><?php echo $total_sale; ?></font></b></td>
                            </tr>
                            <tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_order; ?></td>
                                <td class="left"><b><?php echo $total_order; ?></b></td>
                            </tr>
                            <tr style="display:none;cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale/customer', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_customer; ?></td>
                                <td class="left"><b><?php echo $total_customer; ?></b></td>
                            </tr>
                            <!--
                                        <tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'], 'SSL'); ?>'"><td class="right"><?php echo $text_total_affiliate; ?></td><td class="left"><b><?php echo $total_affiliate; ?></b></td>
                                        </tr>





                            -->
                        <?php if ($total_customer_approval) {
                        ?><tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale/customer', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_customer_approval; ?></td>
                                <td class="left"><b><font color="red"><?php echo $total_customer_approval; ?></font></b></td>
                            </tr><?php } ?>
                        <?php if ($total_affiliate_approval) {
                        ?><tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('sale/affiliate', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_affiliate_approval; ?></td>
                                <td class="left"><b><font color="red"><?php echo $total_affiliate_approval; ?></font></b></td>
                            </tr><?php } ?>
                        <?php if ($total_review_approval) {
                        ?><tr style="cursor: pointer;" onclick="location = '<?php echo $this->url->link('catalog/review', 'token=' . $this->session->data['token'], 'SSL'); ?>'">
                                <td class="right"><?php echo $text_total_review_approval; ?></td>
                                <td class="left"><b><font color="red"><?php echo $total_review_approval; ?></font></b></td>
                            </tr><?php } ?>
                    </table>
                </div>




































            </div>
            <!--<div class="news">
                    <div class="dashboard-heading"><?php //echo $dashboard_news;                     ?></div>
                    <div class="dashboard-content" style="height : 180px; overflow : auto;">
            <?php //echo $news;           ?>
                                                                                                                                                                                </div>
                                                                                                                                                                              </div>-->


                        <div style="<?php
                        if ($this->config->get('config_event') == 0) {
                            echo 'display:none;';
                        }
            ?>margin-top:20px" class="latest">
                       <style>    table.calendar    { border-left:1px solid #999;width:100% }tr.calendar-row  {  }td.calendar-day  { vertical-align:top;min-height:80px; font-size:11px; position:relative; } * html div.calendar-day { height:80px; }td.calendar-day:hover  { background:#eceff5; }td.calendar-day-np  { background:#eee; min-height:80px; } * html div.calendar-day-np { height:80px; }td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }div.day-number    { background:#999; padding:5px; color:#fff; font-weight:bold; float:right; margin:-5px -5px 0 0; width:20px; text-align:center; }/* shared */
                           td.calendar-day, td.calendar-day-np {
                               width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999;
                               height:100px
                           }
                       </style>

                       <div  class="dashboard-heading">Create Event</div>
                       <div class="dashboard-content"  style="text-align:center">
                           <table style="margin-top:-25px">
                               <thead>
                                   <tr>&nbsp;</tr>
                               </thead>
                               <tr>
                                   <td>
                                       <div >
                                           <span style="font-weight:bold;cursor:pointer;font-size:20px;" onclick="window.location='index.php?route=common/home&token=<?php echo $this->session->data['token'] ?>&date=<?php echo $calendar_prev ?>&product_id='+$('#product').val()+'&category_id='+$('#category').val()"><img title="Previous Month" src="../image/arrow-left.png"/></span>
                                           <span style="line-height: 0.2;font-size:20px;color:green"><?php echo ' ' . $calendar_month ?>/<?php echo $calendar_year . ' ' ?></span>
                                           <span style="font-weight:bold;cursor:pointer;font-size:20px;" onclick="window.location='index.php?route=common/home&token=<?php echo $this->session->data['token'] ?>&date=<?php echo $calendar_next ?>&product_id='+$('#product').val()+'&category_id='+$('#category').val()"><img title="Next Month" src="../image/arrow-right.png"/></span>
                                       </div>
                                   </td>
                               </tr>
                           </table>
                           <div style="clear:both"></div>

                    <?php echo $calendar ?>
                    </div>
                </div>

                <div class="latest">
                    <div class="dashboard-heading"><?php echo 'To do List'; ?></div>
                    <div class="dashboard-content">
                        <table class="list">
                            <thead>
                                <tr>
                                    <td class="right"><?php echo $column_order; ?></td>
                                    <td class="left"><?php echo $column_customer; ?></td>
                                    <td class="left"><?php echo 'Address'; ?></td>
                                    <td class="left"><?php echo 'Product(Quantity)'; ?></td>
                                    <td class="left"><?php echo $column_status; ?></td>
                                    <td class="left"><?php echo $column_date_added; ?></td>
                                    <td class="right"><?php echo $column_total; ?></td>
                                    <td class="right"><?php echo $column_action; ?></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if ($orders) {
                            ?>
                            <?php foreach ($orders as $order) {
                            ?>
                                    <tr>
                                        <td class="right"><?php echo $order['order_id']; ?></td>
                                        <td class="left"><?php echo $order['customer']; ?></td>
                                        <td class="left"><?php echo $order['address']; ?></td>
                                        <td class="left"><?php echo $order['product']; ?></td>
                                        <td class="left"><?php echo $order['status']; ?></td>
                                        <td class="left"><?php echo $order['date_added']; ?></td>
                                        <td class="right"><?php echo $order['total']; ?></td>
                                        <td class="right"><?php foreach ($order['action'] as $action) { ?>
                                                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                    <?php } ?>
                                    [ <a target='_blank' href="index.php?route=sale/order/invoice&token=<?php echo $this->session->data['token']?>&order_id=<?php echo $order['order_id']; ?>">Print Invoice</a> ]
                                        </td>
                            </tr>
                            <?php } ?>
                            <?php } else {
                            ?>
                                <tr>
                                    <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>



            <div style="display:none" class="statistic">
                <div class="range"><?php echo $entry_range; ?>
                    <select id="range" onchange="getSalesChart(this.value)">
                        <option value="day"><?php echo $text_day; ?></option>
                        <option value="week" selected="true"><?php echo $text_week; ?></option>
                        <option value="month"><?php echo $text_month; ?></option>
                        <option value="year"><?php echo $text_year; ?></option>
                    </select>
                </div>
                <div class="dashboard-heading"><?php echo $text_statistics; ?></div>
                <div class="dashboard-content">
                    <!--          <div id="report" style="width: 390px; height: 170px; margin: auto;"></div>-->
                    <div id="report" style="width: 100%; height: 430px; margin: auto;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--[if IE]>
<script type="text/javascript" src="view/javascript/jquery/flot/excanvas.js"></script>
<![endif]-->
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script>
<script type="text/javascript"><!--

    $(document).ready(function(){

        $( ".calendar-day" ).css('cursor','pointer');

        $( ".calendar-day" ).click(function() {
            var date = $(this).children().text()
                            
            date = parseInt(date.slice(0, 2));
                            
            if(date < 10){
                date = '0'+date;
            }


            var month = '<?php echo $calendar_month ?>';
            var year = '<?php echo $calendar_year ?>';

            var date_string = year+'-'+month+'-'+date;


            $('#product_id').val('')
            $('#event_name').val('')
            $('#event_start').val('')
            $('#event_end').val('')
            $('#event_start_time').val('')
            $('#event_end_time').val('')
            $('#event_location').val('')
            $('#event_trainer').val('')
            $('#event_price').val('')
            $('#event_quantity').val('')
            $('#event_description').val('')
            $('#event_participant').html('')


            $( "#create_event" ).dialog({
                beforeClose: function(event, ui) {
                    $('#delete_event').css('display','none')
                }
            });

            $( "#create_event" ).dialog({ width: 620 });
            $( "#create_event" ).dialog({ height: 640 });
            $( "#create_event" ).dialog( "open" );

            $( "#event_start" ).val(date_string);


        });

    });

    function getFormattedDate() {
        var date = new Date();
        var str = date.getFullYear() + "-" + getFormattedPartTime(date.getMonth()) + "-" + getFormattedPartTime(date.getDate()) + " " +  getFormattedPartTime(date.getHours()) + ":" + getFormattedPartTime(date.getMinutes()) + ":" + getFormattedPartTime(date.getSeconds());

        return str;
    }

    function getFormattedPartTime(partTime){
        if (partTime<10)
            return "0"+partTime;
        return partTime;
    }

    function getSalesChart(range) {
        $.ajax({
            type: 'get',
            url: 'index.php?route=common/home/chart&token=<?php echo $token; ?>&range=' + range,
            dataType: 'json',
            async: false,
            success: function(json) {
                var option = {
                    shadowSize: 0,
                    lines: {
                        show: true,
                        fill: true,
                        lineWidth: 1
                    },
                    grid: {
                        backgroundColor: '#FFFFFF'
                    },
                    xaxis: {
                        ticks: json.xaxis
                    }
                }

                $.plot($('#report'), [json.order, json.customer], option);
            }
        });
    }

    function search_calendar(){
        if($('#category').val() != '' && $('#product').val() == ''){
            alert('Please select a product');
        }else{
            window.location='index.php?route=common/home&token=<?php echo $this->session->data['token'] ?>&date=<?php echo $calendar_now ?>&product_id='+$('#product').val()+'&category_id='+$('#category').val()
        }
    }



    function open_dialog(product_id){
        var data = {
            'product_id' : product_id
        }

        $("#create_event").mask("Loading...");

        $.ajax({
            type: 'post',
            url: 'index.php?route=catalog/product/getEvent&token=<?php echo $token; ?>',
            dataType: 'json',
            data: data,
            success: function(json) {
                $('#product_id').val(json['product_id'])
                $('#event_name').val(json['name'])
                $('#event_start').val(json['start'])
                $('#event_end').val(json['end'])
                $('#event_start_time').val(json['jan'])
                $('#event_end_time').val(json['isbn'])
                $('#event_location').val(json['mpn'])
                $('#event_trainer').val(json['sku'])
                $('#event_price').val(json['price'])
                $('#event_quantity').val(json['maximum'])
                $('#event_description').val(json['description'])
                $('#event_participant').html(json['participant'])

                $( "#delete_event" ).css('display','block');

                $("#create_event").unmask();


                $( "#create_event" ).dialog( "open" );


            }
        });


    }

    function close_dialog(id){
        $( "#"+id ).dialog( "close");

    }

    function confirm_delete(){
        var data = {
            'selected': {
                0 :  $('#product_id').val()
            }
        };
        $("#create_event").mask("Loading...");

        var url_post = 'index.php?route=catalog/product/delete&token=<?php echo $token; ?>'

        $.ajax({
            type: 'post',
            url: url_post,
            dataType: 'text',
            data: data,
            async: false,
            success: function(json) {
                $( "#message_warning" ).dialog({ width: 400 });
                $( "#message_warning" ).dialog({ height: 140 });
                $( "#message_warning" ).dialog( "open" );
                $('#warning_content').html('Event Successfully Deleted')
                $( "#create_event" ).dialog( "close");
                window.location.href=location.href;
            }
        });
    }

    function delete_event(){

        $( "#confirm_delete" ).dialog({ width: 400 });
        $( "#confirm_delete" ).dialog({ height: 140 });
        $( "#confirm_delete" ).dialog( "open" );

    
    }

    function create_my_event(){
        if($('#event_name').val().length < 1 || $('#event_name').val().length > 100){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Event Name must contain 1 to 100 Characters')
            return false;
        }

        if($('#event_start').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please specify your event Start Date')
            return false;
        }

        if($('#event_end').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please specify your event End Date')

            return false;
        }
                        
        if($('#event_end').val() < $('#event_start').val() ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Event End Date cannot be earlier than Start Date')
            return false;
        }


        if($('#event_start_time').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please specify your event Start Time')
            return false;
        }

        if($('#event_end_time').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please specify your event End Time')
            return false;
        }
        if($('#event_trainer').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please key in Event Trainer')
            return false;
        }
        if($('#event_location').val().length < 1 ){
            $( "#message_warning" ).dialog({ width: 400 });
            $( "#message_warning" ).dialog({ height: 140 });
            $( "#message_warning" ).dialog( "open" );
            $('#warning_content').html('Please specify Event Location')
            return false;
        }
                    

        $("#create_event").mask("Loading...");


        var data = {
            'product_description': {
                '1' :  {
                    'name' : $('#event_name').val(),
                    'description' : $('#event_description').val()
                },
                '3' :  {
                    'name' : $('#event_name').val(),
                    'description' : $('#event_description').val()
                },
                '4' :  {
                    'name' : $('#event_name').val(),
                    'description' : $('#event_description').val()
                }
            },
            'sku' : $('#event_trainer').val(),
            'model' : 'event',
            'price' : $('#event_price').val(),
            'quantity' : $('#event_quantity').val(),
            'status' : '1',
            'subtract' : '1',
            'minimum' : $('#event_minimum').val(),
            'stock_status_id' : '5',
            'date_available' : '2013-10-21',
            'upc' : $('#event_start').val(),
            'ean' : $('#event_end').val(),
            'jan' : $('#event_start_time').val(),
            'isbn' : $('#event_end_time').val(),
            'mpn' : $('#event_location').val(),
            'product_layout' : '0',
            'product_store' :  {
                '0' : '0'
            }
        };

        if($('#product_id').val() == ''){
            var url_post = 'index.php?route=catalog/product/insert&token=<?php echo $token; ?>'
        }else{
            var url_post = 'index.php?route=catalog/product/update&token=<?php echo $token; ?>&product_id='+$('#product_id').val()
        }

        $.ajax({
            type: 'post',
            url: url_post,
            dataType: 'text',
            data: data,
            async: false,
            success: function(json) {
                if($('#product_id').val() == ''){
                    $( "#message_warning" ).dialog({ width: 400 });
                    $( "#message_warning" ).dialog({ height: 140 });
                    $( "#message_warning" ).dialog( "open" );
                    $('#warning_content').html('Event Successfully Created')
                }else{
                    $( "#message_warning" ).dialog({ width: 400 });
                    $( "#message_warning" ).dialog({ height: 140 });
                    $( "#message_warning" ).dialog( "open" );
                    $('#warning_content').html('Event Successfully Updated')
                }
                $( "#create_event" ).dialog( "close");
                window.location.href=location.href;
            }
        });
    }


    getSalesChart($('#range').val());
    //--></script>
<style>
    .dialog_table{
        width:100%
    }
    .dialog_table td{
        padding:5px;
        background: white;
        border:1px solid #e0e0e0;
    }
    #bar_cal:hover{
        background:  #e0e0e0;
        color:black
    }
      #bar_cal{
                                font-size:13px;max-width:180px;float:left;text-align: left;margin-top:2px;color: white;cursor: pointer;padding: 4px;
                             background: #3f4c6b; /* Old browsers */
background: -moz-linear-gradient(top,  #3f4c6b 0%, #3f4c6b 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#3f4c6b), color-stop(100%,#3f4c6b)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #3f4c6b 0%,#3f4c6b 100%); /* IE10+ */
background: linear-gradient(to bottom,  #3f4c6b 0%,#3f4c6b 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3f4c6b', endColorstr='#3f4c6b',GradientType=0 ); /* IE6-9 */

                            }
    .my_table input[type="text"]{
        width: 260px;
    }
    .my_table td{
        border:1px solid #eaeaea
    }
</style>
<?php if ($this->config->get('config_event') != 0) {
?>
                                <div style="display:none" id="create_event" title="Event Details">
                                    <div id="error_event"></div>
                                    <input id="product_id" type="hidden" value="" />
                                    <table class="my_table" style="width:100%">
                                        <tr>
                                            <td style="font-weight:bold;">Event Name<em style="color:red">*</em></td>
                                            <td><textarea style="width: 260px;height: 40px;"  id="event_name"></textarea></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Date<em style="color:red">*</em></td>
                                            <td><input style="width:80px" readonly="readonly" id="event_start"  type="text" /> to <input style="width:80px" id="event_end" class="date" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Time<em style="color:red">*</em></td>
                                            <td><input style="width:80px" id="event_start_time" class="time" type="text" /> to <input style="width:80px" id="event_end_time" class="time" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Trainer Name<em style="color:red">*</em></td>
                                            <td><input id="event_trainer" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Location<em style="color:red">*</em></td>
                                            <td><input id="event_location" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Price</td>
                                            <td><input value="0"  id="event_price" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Maximum Seat</td>
                                            <td><input  id="event_quantity" type="text" /></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Description</td>
                                            <td><textarea style="width: 260px;height: 100px;"  id="event_description"></textarea></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:bold;">Participants List:</td>
                                            <td><span id="event_participant"></span></td>
                                        </tr>
                                    </table>
                                    <input style="float:right;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="close_dialog('create_event')" value="Close" />
                                    <input style="float:right;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;margin-right:10px;color:white;width:80px;height:30px;" type="button" onclick="create_my_event()" value="Submit" />
                                    <input id="delete_event" style="display:none;float:left;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:120px;height:30px;" type="button" onclick="delete_event()" value="Delete Event" />
                                </div>
<?php } ?>
                            <script>
                                $('.date').datepicker({dateFormat: 'yy-mm-dd'});
                                $('.time').timepicker({timeFormat: 'h:mm'});

                            </script>
                            <div style="background:white;text-align:center;display:none" id="message_warning"  title="Message">
                                <span id="warning_content" ></span>
                                <br/>
                                <input style="margin-top:20px;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="close_dialog('message_warning')" value="Close" />
                            </div>
                            <div style="background:white;text-align:center;display:none" id="confirm_delete"  title="Message">
                                Are you sure you want to delete this event?
                                <br/>
                                <input style="float:left;margin-top:20px;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="close_dialog('confirm_delete')" value="Cancel" />
                                <input style="float:right;margin-top:20px;background-color:#f7b64a;border:0px;font-size:14px;cursor:pointer;color:white;width:80px;height:30px;" type="button" onclick="confirm_delete()" value="Confirm" />
                            </div>
<?php echo $footer; ?>