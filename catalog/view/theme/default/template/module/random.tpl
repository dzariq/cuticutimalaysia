
<div id="notification"></div>

<h2 style="text-align: center;color:#444444" class="category_title"><?php echo $heading_title; ?></h2>
<?php if ($thumb || $description) {
 ?>
    <div class="category-info">
<?php if ($thumb) { ?>
        <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) {
 ?>
<?php echo $description; ?>
<?php } ?>
    </div>
<?php } ?>

<?php if ($products) { ?>
    <div style="background:#180000 " class="options">
        <div class="display">
            <a class="grid" onclick="display('grid');"><span><?php echo $text_grid; ?></span></a><div class="list"><span><?php echo $text_list; ?></span></div>
        </div>

        <div class="product-compare <?php echo isset($this->session->data['compare']) && count($this->session->data['compare']) > 0 ? '' : 'empty' ?>">
            <a href="<?php echo $compare; ?>" id="compare-total"></a>
        </div>

        <div class="show"><span><?php echo $text_limit; ?></span>
        <select onchange="location = this.value;">
<?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) {
 ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else {
 ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
<?php } ?>
<?php } ?>
        </select>
    </div>
    <div class="sort"><span class="sort-by"><?php echo $text_sort; ?></span>
        <select onchange="location = this.value;">
<?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) {
 ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
<?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
<?php } ?>
    <?php } ?>
            </select>
        </div>
    </div><!-- .options -->
    <br/>
            <div class="pagination">
<?php
            echo strtr($pagination, array(
                '|&lt;' => '<span class="first">|&lt;</span>',
                '&gt;|' => '<span class="last">&gt;|</span>',
                '&lt;' => '<span class="prev">←</span>',
                '&gt;' => '<span class="next">→</span>')); ?>
            </div>
    <div class="product-list">
<?php foreach ($products as $product) { ?>
    <div>
<?php if ($product['thumb']) { ?>
            <div class="prev">
<?php if ($product['special']) { ?>
                        <div class="sale-label"></div>
        <?php } ?>
                    <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
<?php } ?>
            <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            </div>
             <div class="description"><?php echo $product['description']; ?></div>
      
                <?php if ($product['price']) {
 ?>
            <div style="background:  #e9322d;text-align: center !important;font-size:14px;color:white;" class="price">
                <div class="vert">
<?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else {
 ?>

                        <div class="price_new"><?php echo $product['special']; ?></div>
                        <div class="price_old"><?php echo $product['price']; ?></div>

        <?php } ?>
<?php if ($product['tax']) { ?>
                                <div class="price-tax"><span><?php echo $text_tax; ?></span> <?php echo $product['tax']; ?></div>
<?php } ?>
                        </div>
                    </div>
<?php } ?>
<?php if ($product['rating']) { ?>
                    <div class="rating"><img src="catalog/view/theme/<?php echo $this->config->get('config_template') ?>/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
    <?php } ?>
                    <div class="bay">
                        <input style="display:none" class="add-cart" type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" />
                    </div>
                    <div class="wishlist"><a style="display:none" onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
                    <div class="compare"><a style="display:none" onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
                </div>
    <?php } ?>
            <div class="clear"></div>
        </div>
        <div class="pagination">
<?php
            echo strtr($pagination, array(
                '|&lt;' => '<span class="first">|&lt;</span>',
                '&gt;|' => '<span class="last">&gt;|</span>',
                '&lt;' => '<span class="prev">←</span>',
                '&gt;' => '<span class="next">→</span>')); ?>
            </div>
<?php } ?>
<?php if (!$categories && !$products) { ?>
            <div class="content"><?php echo $text_empty; ?></div>
            <div class="buttons">
                <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
            </div>
<?php } ?>



        <script type="text/javascript"><!--
            function display(view) {
                if (view == 'list') {
                    $('.product-grid').attr('class', 'product-list');

                    $('.product-list > div').each(function(index, element) {

                        html = '<div class="product_li">';

                        html += '<div class="grid_3">';

                        var image = $(element).find('.prev').html();

                        if (image != null) {
                            html += '<div class="prev">' + image + '</div>';
                        } else {
                            html += '<div class="prev"></div>';
                        }

                        html += '</div>';

                        html += '<div class="grid_4">';
                        html += '<div class="entry_content">';

                        html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

                        var rating = $(element).find('.rating').html();

                        if (rating != null) {
                            html += '<div class="rating">' + rating + '</div>';
                        }

                        html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

                        html += '</div>';//<!-- .entry_content -->
                        html += '</div>';//<!-- .grid_4 -->

                        html += '<div class="grid_2">';
                        html += '<div class="cart">';
                        var price = $(element).find('.price').html();

                        if (price != null) {
                            html += '<div style="background:  #e9322d;text-align: center !important;font-size:14px;color:white;" class="price">' + price  + '</div>';
                        }
                        html += '  <div class="bay">' + $(element).find('.bay').html() + '</div>';

                        html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
                        html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';

                        html += '</div>';//<!-- .cart -->
                        html += '</div>';//<!-- .grid_2 -->

                        html += '</div>';//<!-- .product_li -->

                        $(element).html(html);
                    });

                    $('.display').html('<a class="grid" onclick="display(\'grid\');"><span><?php echo $text_grid; ?></span></a><div class="list"><span><?php echo $text_list; ?></span></div>');

                    $.totalStorage('display', 'list');
                } else {
                    $('.product-list').attr('class', 'product-grid');

                    $('.product-grid > div').each(function(index, element) {
                        html = '<div class="grid_3 product">';

                        var image = $(element).find('.prev').html();

                        if (image != null) {
                            html += '<div class="prev">' + image + '</div>';
                        } else {
                            html += '<div class="prev"></div>';
                        }

                        html += '<div class="name">' + $(element).find('.name').html() + '</div>';
                        html += '<div class="description">' + $(element).find('.description').html() + '</div>';

                        html += '<div class="cart">';

                        var price = $(element).find('.price').html();

                        if (price != null) {
                            html += '<div style="background:  #e9322d;text-align: center !important;font-size:14px;color:white;" class="price">' + price  + '</div>';
                        }

                        var rating = $(element).find('.rating').html();

                        if (rating != null) {
                            html += '<div class="rating">' + rating + '</div>';
                        }

                        html += '<div class="bay">' + $(element).find('.bay').html() + '</div>';
                        html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
                        html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';

                        html += '</div>';//<!-- .cart -->

                        html += '</div>';

                        $(element).html(html);
                    });

                    $('.display').html('<div class="grid"><span><?php echo $text_grid; ?></span></div><a class="list" onclick="display(\'list\');"><span><?php echo $text_list; ?></span></a>');

            $.totalStorage('display', 'grid');
        }
    }

    //    view = $.totalStorage('display');

    //    if (view) {
    display('grid');
    //    } else {
    //            display('list');
    //    }
    //--></script>
<div class="clear"></div>