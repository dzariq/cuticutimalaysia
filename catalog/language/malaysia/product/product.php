<?php
// Text
$_['text_search']       = 'Cari';
$_['text_brand']        = 'Jenama';
$_['text_manufacturer'] = 'Jenama:';
$_['text_model']        = 'Kod Produk:';
$_['text_reward']       = 'Point Reward:'; 
$_['text_points']       = 'Harga dalam Point Reward:';
$_['text_stock']        = 'Bilangan Sedia Ada:';
$_['text_instock']      = 'Tersedia';
$_['text_price']        = 'Harga:'; 
$_['text_tax']          = 'Tanpa Cukai:';
$_['text_discount']     = '%s atau lebih %s';
$_['text_option']       = 'Pilihan yang tersedia';
$_['text_qty']          = 'Jml:';
$_['text_minimum']      = 'Produk ini memiliki jumlah minimum %s';
$_['text_or']           = '- ATAU -';
$_['text_reviews']      = '%s review'; 
$_['text_quantity']      = 'Kuantiti:'; 
$_['text_write']        = 'Tulis review';
$_['text_no_reviews']   = 'Tidak ada review untuk produk ini.';
$_['text_on']           = ' dalam ';
$_['text_note']         = '<span style="color: #FF0000;">Note:</span> HTML tidak diterjemahkan!';
$_['text_share']        = 'Kongsi';
$_['text_success']      = 'Terima kasih atas review Anda.';
$_['text_upload']       = 'File Anda berjaya diupload!';
$_['text_wait']         = 'Sila Tunggu!';
$_['text_tags']         = 'Tags:';
$_['text_error']        = 'Produk tidak ditemukan!';

// Entry
$_['entry_name']        = 'Nama Anda:';
$_['entry_review']      = 'Review Anda:';
$_['entry_rating']      = 'Rating:';
$_['entry_good']        = 'Bagus';
$_['entry_bad']         = 'Tidak Bagus';
$_['entry_captcha']     = 'Masukkan kod verifikasi berikut:';

// Tabs
$_['tab_description']   = 'Keterangan';
$_['tab_attribute']     = 'Spesifikasi';
$_['tab_review']        = 'Review (%s)';
$_['tab_related']       = 'Produk berkaitan';

// Error
$_['error_name']        = 'Peringatan: Nama penulis review harus terdiri dari 3 hingga 25 karakter!';
$_['error_text']        = 'Peringatan: Review harus terdiri dari 25 hingga 1000 karakter!';
$_['error_rating']      = 'Peringatan: Sila pilih rating review!';
$_['error_captcha']     = 'Peringatan: Kod verifikasi tidak sesuai gambar!';
$_['error_upload']      = 'Upload diperlukan!';
$_['error_filename']    = 'Nama file harus terdiri dari 3 hingga 64 karakter!';
$_['error_filetype']    = 'Tipe file tidak valid!';
?>