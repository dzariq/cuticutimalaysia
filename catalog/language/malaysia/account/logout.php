<?php
// Heading 
$_['heading_title'] = 'Logout Akaun';

// Text
$_['text_message']  = '<p>Anda telah berjaya keluar/logout dari Akaun Anda.</p><p>Belanjaan Anda sudah tersimpan, item barang akan ditampilkan kembali sewaktu Anda login ke Akaun Anda.</p>';
$_['text_account']  = 'Akaun';
$_['text_logout']   = 'Logout';
?>