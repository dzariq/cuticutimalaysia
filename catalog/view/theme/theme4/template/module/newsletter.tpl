<div style="margin-bottom:20px">
    <h3><?php echo $title ?></h3>
    <p class="caption"><?php echo $description ?></p>

    <div class="row">
        <div class="col-xs-9">
            <input placeholder="email" id="emailnewsletter" type="text" class="form-control" /> 
        </div>
        <div class="right col-xs-3">
            <input  type="button" onclick="submitNewsletter()" value="Send" class="btn btn-primary" />
        </div>
        <div class="col-xs-12">
            <div tabindex="1" class="message-success">
            </div>
            <div tabindex="1" class="message-error">
            </div>
        </div>
    </div>

    <script>


        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (!emailReg.test($email)) {
                alert('Masukkan alamat email yang sah');
                return false;
            } else {
                return true;
            }
        }

        function submitNewsletter() {

            var status = true;

            status = validateEmail($('#emailnewsletter').val());

            $('.message-error').hide()
            $('.message-success').hide()
            $('.message-error').html('')
            $('.message-success').html('');

            if (status) {

                var data = {
                    'name': 'Newsletter',
                    'email': $('#emailnewsletter').val(),
                    'category': 'newsletter',
                    'json': 1
                }

                console.log(data)
                $.ajax({
                    url: 'index.php?route=information/contact',
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        var html = '';
                        html += data.remark;

                        if (data.status == 1) {

                            $('.message-success').show()
                            $('.message-success').html(html)
                        } else {
                            $('.message-error').show()
                            $('.message-error').html(html)
                        }
                        $('#remark').focus()
                        console.log(data);
                    }
                });

            }

        }
    </script>
</div>