<?php
// Heading 
$_['heading_title']    = 'Akaun';

// Text
$_['text_register']    = 'Daftar';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Lupa Password';
$_['text_account']     = 'Akaun Saya';
$_['text_edit']        = 'Ubah Akaun';
$_['text_password']    = 'Password';
$_['text_wishlist']    = 'Wish List';
$_['text_order']       = 'Sejarah Pesanan';
$_['text_download']    = 'Download';
$_['text_return']      = 'Retur';
$_['text_transaction'] = 'Transaksi';
$_['text_newsletter']  = 'Berita';
?>
