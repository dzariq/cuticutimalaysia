<?php  
class ControllerModuleFftheme extends Controller {
	protected function index($setting) {
		static $module = 0;

		$this->language->load('myoc/fftheme');

		$this->data['text_buynow'] = $this->language->get('text_buynow');
		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$this->data['slideshow_height'] = $setting['height'];
		
		$this->data['slideshow_images'] = array();

		$slideshow_images = $this->config->get('ffslideshow_image');
		
		if ($slideshow_images) {			  
			foreach ($slideshow_images as $slideshow_image) {
				$product = 
				$price = 
				$special = false;
				if($slideshow_image['product_id']) {
					$product = $this->model_catalog_product->getProduct($slideshow_image['product_id']);
					
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
					}
					
					if ((float)$product['special']) {
						$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')));
					}
				}

				$this->data['slideshow_images'][] = array(
					'image' 		=> ($slideshow_image['image'] && file_exists(DIR_IMAGE . $slideshow_image['image'])) ? HTTP_SERVER . 'image/' . $slideshow_image['image'] : false,
					'price'			=> $price,
					'special'		=> $special,
					'title' 		=> $slideshow_image['slideshow_image_description'][$this->config->get('config_language_id')]['title'] ? $slideshow_image['slideshow_image_description'][$this->config->get('config_language_id')]['title'] : ($product ? $product['name'] : false),
					'description'	=> $slideshow_image['slideshow_image_description'][$this->config->get('config_language_id')]['description'] ? html_entity_decode($slideshow_image['slideshow_image_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8') : ($product ? strip_tags(html_entity_decode($product['description'])) : false),
					'text_bg'		=> $slideshow_image['text_bg'],
					'href'  		=> $slideshow_image['link'] ? $slideshow_image['link'] : ($product ? $this->url->link('product/product', 'product_id=' . $product['product_id']) : false),
					'position'  	=> $slideshow_image['position'],
					'sort_order'  	=> $slideshow_image['sort_order'],
				);
			}
		}

		usort($this->data['slideshow_images'], array(get_class($this), 'sortByOrder'));
		
		$this->data['module'] = $module++;
						
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/ff_slideshow.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/ff_slideshow.tpl';
		} else {
			return false;
		}
		
		$this->render();
	}

	private static function sortByOrder($a, $b) {
	    return $a['sort_order'] - $b['sort_order'];
	}
}
?>