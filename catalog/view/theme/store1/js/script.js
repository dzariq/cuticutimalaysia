$(document).ready(function () {
    
    $("table").addClass("table-responsive");
    $("table td").css("background",'white');
    $("table th").css("background",'#333');
    $("table td").css("font-family",'verdana');
    $("table th").css("font-family",'verdana');
    $("table th").css("color",'#FFFFFF');
    $("table th span").css("color",'#FFFFFF');

    /* Add border to images */
    $('article a img, .gallery .slider_content img, .results a img, img.add_border, .description .main_image img, .similar_hotels .thumb img, .results_wide .thumb img').each(function () {
        $(this).wrap('<span class="with_border" />');
        $(this).before(
                '<span></span>'
                );
    });

    /* Search form selection */
    $('.search h2 span').click(function () {
        $('.search h2 span').removeClass('selected');
        $(this).addClass('selected')
        $('.search form').css('display', 'none');
        $('.search form[data-form="' + $(this).attr('data-form') + '"]').css('display', 'block');
    });

    /* Slider navigation */
    $('.slider_nav a').click(function (e) {
        e.preventDefault();
    });

    

    /* Gallery slider */
    $('.gallery').each(function () {

        /* Functions */
        function resetInterval() {
            clearInterval(gallerySliderInterval);
            gallerySliderInterval = setInterval(next, 8000);
        }
        function next() {
            $('.gallery .slider_content').animate({left: '-150px'}, 1500, function () {
                $('.gallery .slider_content').css({left: '0px'}).children(':first').remove().appendTo($('.gallery .slider_content'));
                $('.gallery .slider_content a').fancybox(); // Reinitialize Fancybox
            });
        }
        function previous() {
            $('.gallery .slider_content').css({left: '-=150px'}).children(':last').remove().prependTo($('.gallery .slider_content'));
            $('.gallery .slider_content a').fancybox(); // Reinitialize Fancybox
            $('.gallery .slider_content').stop().animate({left: '0px'}, 1500);
        }

        /* Initialize */
        var gallerySliderInterval;
        resetInterval();

        /* Controls */
        $('.gallery .left').click(function () {
            previous();
            resetInterval();
        });
        $('.gallery .right').click(function () {
            next();
            resetInterval();
        });

    });

    /* Homepage slider */
    $('.homepage_slider').each(function () {

        /* Functions */
        function resetInterval() {
            clearInterval(gallerySliderInterval);
            gallerySliderInterval = setInterval(next, 8000);
        }
        function next() {
            $('.homepage_slider').animate({left: '-620px'}, 1000, function () {
                $('.homepage_slider').css({left: '0px'}).children(':first').remove().appendTo($('.homepage_slider'));
            });
        }
        function previous() {
            $('.homepage_slider').css({left: '-620px'}).children(':last').remove().prependTo($('.homepage_slider'));
            $('.homepage_slider').animate({left: '0px'}, 1000);
        }

        /* Initialize */
        var gallerySliderInterval;
        resetInterval();

        /* Controls */
        $('.control  .left').click(function () {
            previous();
            resetInterval();
        });
        $('.control  .right').click(function () {
            next();
            resetInterval();
        });

    });

    /* Fancybox */
    $('.gallery .slider_content a').fancybox();
    $('.image_slider .slides a').fancybox();
    $('.image_thumbnail a').fancybox();
    $('a.fancybox').fancybox();

    /* Datepicker */
    $('input.date').datepicker();

    /* Autocomplete */
    $('input[name="destination"]').autocomplete({
        source: [
         
        ]
    });
    $('input[name="transportation"]').autocomplete({
        source: [
           
        ]
    });


    /* Contact form */
    $('#contact_form').submit(function () {
        $.ajax({
            type: 'POST',
            url: 'contact.php',
            data: {
                name: $('#contact_form input[type=text]').val(),
                email: $("#contact_form input[type=email]").val(),
                text: $("#contact_form textarea").val()
            },
            success: function (data) {
                if (data == 'sent') {
                    $('#contact_form .status').html('E-mail has been sent.');
                } else if (data == 'invalid') {
                    $('#contact_form .status').html('Your name, email or message is invalid.');
                } else {
                    $('#contact_form .status').html('E-mail could not be sent.');
                }
            },
            error: function () {
                $('#contact_form .status').html('E-mail could not be sent.');
            }
        });
        return false;
    });

    $body = $("body");

    $(document).on({
        ajaxStart: function () {
            $body.addClass("loading");
        },
        ajaxStop: function () {
            $body.removeClass("loading");
        }
    });

    /* Contact form */
    $('.searchajax').click(function () {
        event.preventDefault();
        var formnumber = $(this).attr('formnumber');
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'index.php?route=module/searchajax/api',
            data: {
                searchall: $('#searchform'+formnumber+' input[name=name]').val(),
                category: $('#searchform'+formnumber+' select[name=category]').val(),
                manufacturer: $('#searchform'+formnumber+'select[name=manufacturer]').val(),
            },
            success: function (data) {
                var html = "";
                html += '<tr class="header">';
                html += '<th >Name</td>';
                html += '<th >Description</th>';
                html += '<th >Price</th>';
                html += '</tr >';
                if (data.status == 1) {

                    $.each(data.result, function (i, item) {
                        html += '<tr onclick="window.location=\'index.php?route=product/product&product_id=' + item.product_id + '\'" style="cursor:pointer">';
                        html += '<td >' + item.name + '</td>';
                        html += '<td >' + item.description + '</td>';
                        html += '<td >' + item.price + '</td>';
                        html += '</tr>';
                    });
                    $('#ajaxsearch-result').html(html)
                } else {
                    html += '<tr >';
                    html += '<td colspan="4">No result found</td>';
                    html += '</tr>';
                    $('#ajaxsearch-result').html(html)

                }
            },
            error: function () {
                // $('#contact_form .status').html('E-mail could not be sent.');
            }
        });
        return false;
    });
    
    

});