<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="alert alert-success">
    	<?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>

         <!-- Page title -->
      <div class="page-title">
         <div class="<?php echo layout ?>">
            <h2><i class="icon-desktop color"></i> <?php echo $heading_title ?></h2>
            <hr />
         </div>
      </div>
      <!-- Page title -->
	    <!-- Page content -->

      <div class="account-content">
         <div class="<?php echo layout ?>">

            <div class="row">
               <div class="col-md-3">
                  <?php echo $column_left ?>
               </div>
               <div class="col-md-9">
                  <h3><i class="icon-user color"></i> &nbsp;<?php echo $heading_title ?></h3>
                  <!-- Your details -->
                   <div class="address">
                     <address>
                       <!-- Your name -->
                       <strong><?php echo $name ?></strong><br>
                       <!-- Address -->
                       <?php echo $my_address['address_1'] ?>,<br>
                       <?php if($my_address['address_2'] != ''){echo $my_address['address_2'].','; } ?> <br>
                       <?php if($my_address['postcode'] != ''){echo $my_address['postcode'].','; } ?> <br>
                       <?php echo $my_address['city'] ?>,<br>
                       <?php echo $my_address['zone'] ?>,<br>
                       <?php echo $my_address['country'] ?>,<br>


                       <!-- Phone number -->
                       <abbr title="Phone">P:</abbr> <?php print_r($telephone) ?><br />
                       <a href="mailto:<?php print_r($email) ?>"><?php print_r($email) ?></a>
                     </address>
                   </div>
               </div>
            </div>

            <div class="sep-bor"></div>
         </div>
      </div>
<?php echo $footer; ?> 