<div class='clearfix'></div>
<div class="col-sm-12 wow fadeInDown" style="margin-top:5px;padding-left:5px;padding-right:5px">
    <div class="accordion">
        <div class="panel-group" id="accordion1">
            <?php $i=0; foreach($banners as $banner){ ?>
            <div class="panel panel-default">
                <div class="panel-heading <?php if($i==0){ ?> active <?php } ?>">
                    <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne<?php echo $i+1 ?>">
                            <?php echo $banner['title'] ?>
                            <i class="fa fa-angle-right pull-right"></i>
                        </a>
                    </h3>
                </div>

                <div id="collapseOne<?php echo $i+1 ?>" class="panel-collapse collapse <?php if($i==0){ ?> in <?php } ?>">
                    <div class="panel-body">
                        <div class="media accordion-inner">
                            <?php if($banner[image] != ''){ ?>
                            <div class="col-xs-4 ">
                                <img class="img-responsive" src="<?php echo $banner['image'] ?>">
                            </div>
                            <?php } ?>
                            <div class="col-xs-8 media-body">
                                <h4><?php echo $banner['title'] ?></h4>
                                <p><?php echo $banner['description'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $i++; } ?>

        </div>
    </div>
</div>