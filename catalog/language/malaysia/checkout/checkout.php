<?php
// Heading
$_['heading_title']                  = 'Checkout';

// Text

$_['text_cart']                      = 'Bakul Belanja';

$_['text_back']                      = 'Kembali';
$_['text_checkout_option']           = 'Login/Daftar';
$_['text_checkout_account']          = 'Maklumat Anda';
$_['text_checkout_payment_address']  = 'Maklumat Anda';
$_['text_checkout_shipping_address'] = 'Langkah 3: Maklumat Pengiriman';
$_['text_checkout_shipping_method']  = 'Langkah 4: Kaedah Pengiriman';
$_['text_checkout_payment_method']   = 'Langkah 5: Kaedah Pembayaran';
$_['text_checkout_confirm']          = 'Langkah 6: Selesai';
$_['text_checkout_payment_method_2']   = 'Langkah 3: Kaedah Pembayaran';
$_['text_checkout_confirm_2']          = 'Langkah 4: Selesai';
$_['text_modify']                    = 'Ubah &raquo;';
$_['text_new_customer']              = 'Pelanggan Baru';
$_['text_returning_customer']        = 'Pelanggan Sedia Ada';
$_['text_checkout']                  = 'Pilihan Checkout:';
$_['text_i_am_returning_customer']   = 'Saya telah pernah mendaftar';
$_['text_register']                  = 'Daftar Akaun';
$_['text_guest']                     = 'Belanja Tanpa Menjadi Member';
$_['text_register_account']          = 'Sila klik <b>Teruskan</b> untuk daftar sebagai <b>Pelanggan Baru</b>.<br/>Anda hanya perlu mendaftar <b>sekali sahaja</b>.';
$_['text_forgotten']                 = 'Klik di sini jika anda terlupa Password';
$_['text_your_details']              = 'Detail Pribadi Anda';
$_['text_your_address']              = 'Alamat Anda';
$_['text_your_password']             = 'Password Anda';
$_['text_agree']                     = 'Saya telah membaca dan setuju dengan <a class="colorbox" href="%s" alt="%s"><b>%s</b></a>';
$_['text_address_new']               = 'Saya ingin menggunakan alamat baru';
$_['text_address_existing']          = 'Saya ingin menggunakan alamat yang sudah ada';
$_['text_shipping_method']           = 'Sila pilih kaedah pengiriman yang disukai untuk pengiriman pesanan Anda.';
$_['text_payment_method']            = 'Sila pilih kaedah pembayaran yang disukai untuk membayar pesanan Anda.';
$_['text_comments']                  = 'Tambahkan komen mengenai pesanan Anda';

// Column
$_['column_name']                    = 'Nama Produk';
$_['column_model']                   = 'Model';
$_['column_quantity']                = 'Kuantiti';
$_['column_price']                   = 'Harga';
$_['column_total']                   = 'Total';

// Entry
$_['entry_email_address']            = 'Alamat Email:';
$_['entry_email']                    = 'Email:';
$_['entry_password']                 = 'Password:';
$_['entry_confirm']                  = 'Pengesahan Password:';
$_['entry_firstname']                = 'Nama:';
$_['entry_lastname']                 = 'Nama Belakang:';
$_['entry_telephone']                = 'No telefon:';
$_['entry_fax']                      = 'Fax:';
$_['entry_account']                  = 'Akaun:';
$_['entry_company']                  = 'Syarikat:';
$_['entry_company_id']               = 'ID Syarikat:';
$_['entry_tax_id']                   = 'No Cukai:';
$_['entry_address_1']                = 'Alamat:';
$_['entry_address_2']                = 'Alamat 2:';
$_['entry_postcode']                 = 'Poskod:';
$_['entry_city']                     = 'Bandar:';
$_['entry_country']                  = 'Negara:';
$_['entry_zone']                     = 'Negeri:';
$_['entry_newsletter']               = 'Saya ingin melanggan %s berita/promosi terkini.';
$_['entry_shipping'] 	             = 'Alamat pengiriman dan alamat bil saya adalah sama.';

// Error
$_['error_Peringatan']                  = 'Terdapat kesalahan dalam pesanan Anda! Jika masalah berlanjut, sila cuba pilih kaedah pembayaran yang berbeza atau sila <a href="%s">hubungi kami</a>.';
$_['error_login']                    = 'Peringatan: Alamat Email dan/atau Password tidak ditemukan.';
$_['error_approved']                 = 'Peringatan: Akaun Anda memerlukan persetujuan sebelum Anda dapat login.';
$_['error_exists']                   = 'Peringatan: Alamat Email telah terdaftar!';
$_['error_firstname']                = 'Nama Hadapan harus terdiri dari 1 s/d 32 karakter!';
$_['error_lastname']                 = 'Nama Belakang harus terdiri dari 1 s/d 32 karakter!';
$_['error_email']                    = 'Alamat Email tidak valid!';
$_['error_telephone']                = 'No Telefon harus terdiri dari 3 s/d 32 karakter!';
$_['error_password']                 = 'Password harus terdiri dari 3 s/d 20 karakter!';
$_['error_confirm']                  = 'Pengesahan password tidak padan!';
$_['error_company_id']               = 'ID Syarikat diperlukan!';
$_['error_tax_id']                   = 'No Cukai diperlukan!';
$_['error_vat']                      = 'Nomor VAT tidak valid!';
$_['error_address_1']                = 'Alamat 1 harus terdiri dari 3 s/d 128 karakter!';
$_['error_city']                     = 'Bandar harus terdiri dari 2 s/d 128 karakter!';
$_['error_postcode']                 = 'Poskod harus terdiri dari 2 s/d 10 karakter!';
$_['error_country']                  = 'Sila pilih Negara!';
$_['error_zone']                     = 'Sila pilih Negeri!';
$_['error_agree']                    = 'Peringatan: Anda harus setuju dengan %s!';
$_['error_address']                  = 'Peringatan: Anda harus memilih alamat!';
$_['error_shipping']                 = 'Peringatan: Kaedah pengiriman diperlukan!';
$_['error_no_shipping']              = 'Peringatan: Tidak ada pilihan pengiriman yang tersedia. Sila <a href="%s">hubungi kami</a>!';
$_['error_payment']                  = 'Peringatan: Kaedah pambayaran diperlukan!';
$_['error_no_payment']               = 'Peringatan: Tidak ada pilihan pembayaran yang tersedia. Sila <a href="%s">hubungi kami</a>!';
?>