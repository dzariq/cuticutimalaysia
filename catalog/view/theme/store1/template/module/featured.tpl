<!-- Recommended trips -->
<ul class="results">
    <?php foreach($products as $key=>$banner){ ?>
    <li class="short col-xs-6">
        <a href="<?php echo $banner['href'] ?>"><img src="<?php echo $banner['thumb'] ?>" alt="" /></a>
        <h3><a href="<?php echo $banner['href'] ?>"><?php echo $banner['name'] ?></a></h3>
        <span class="stars">
            <?php echo $banner['title'] ?>
            <img src="catalog/view/theme/default/image/stars-<?php echo $banner['rating'] ?>.png" alt="rating" />
        </span>
        <div>
                    <span><a href="<?php echo $banner['manufacturerlink'] ?>"><?php echo $banner['manufacturer'] ?></a></span>
            <span><a href="<?php echo $banner['href'] ?>"><?php echo $banner['category'] ?></a></span>
            <span><strong><?php echo $banner['price'] ?></strong></span>
        </div>
    </li>
    <?php } ?>
</ul>