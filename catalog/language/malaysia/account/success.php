<?php
// Heading
$_['heading_title'] = 'Akaun Anda berjaya didaftar!';

// Text
$_['text_message']  = '<p>Terima Kasih! Akaun Anda telah berjaya didaftar! </p><p>Jika Anda memiliki pertanyaan, sila hubungi kami.</p> <p>Kami juga telah menghantar email pengesahan ke alamat Email yang anda gunakan semasa mendaftar akaun. Jika Anda belum menerimanya, sila <a href="%s">hubungi kami</a>.</p>';
$_['text_approval'] = '<p>Terima kasih telah mendaftar di %s!</p><p>Anda akan mendapatkan Email pengesahan setelah akaun Anda diaktifkan.</p><p>Jika Anda mempunyai pertanyaan, silah <a href="%s">hubungi kami</a>.</p>';
$_['text_account']  = 'Akaun';
$_['text_success']  = 'berjaya';
?>