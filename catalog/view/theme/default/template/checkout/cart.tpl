<?php echo $header ?>
<?php if ($attention) { ?>
    <div class="alert alert-info">
    	<?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<?php if ($success) { ?>
    <div class="alert alert-success">
    	<?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="alert alert-danger">
    	<?php echo $error_warning; ?>
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<!-- Page title -->
      <div class="page-title">
            <h2> <?php echo $heading_title ?> </h2>
      </div>
      <!-- Page title -->

      <!-- Page content -->

      <div class="view-cart blocky">
            <div class="row">
               <div class="col-md-12">

                  <!-- Table -->
                    <table class="table ">
                      <thead>
                        <tr>
                        <th class="image"><?php echo $column_image; ?></th>
                        <th class="name"><?php echo $column_name; ?></th>
                        <th class="quantity"><?php echo $column_quantity; ?></th>
                        <th class="price"><?php echo $column_price; ?></th>
                        <th class="total"><?php echo $column_total; ?></th>
                      </tr>
                      </thead>
                      <tbody>
                                 <?php foreach ($products as $product) { ?>
                          <tr>
                            <td class="image"><?php if ($product['thumb']) { ?>
                              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                              <?php } ?></td>
                            <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                              <?php if (!$product['stock']) { ?>
                              <span class="stock">***</span>
                              <?php } ?>
                              <div>
                                <?php foreach ($product['option'] as $option) { ?>
                                - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                                <?php } ?>
                              </div>
                              <?php if ($product['reward']) { ?>
                              <small><?php echo $product['reward']; ?></small>
                              <?php } ?></td>
                            <td class="quantity">
                                <div class="input-group">
                                    <form action="" class="form-inline"  method="post">
                                  <input style="float:left;width:37px" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" type="text" class="form-control" placeholder="1">
                                  <span style="float:left" class="input-group-btn">
                                    <button class="btn btn-danger"  type="submit"><i class="icon-refresh"></i></button>
                                    <a href="<?php echo $product['remove']; ?>"><button class="btn btn-danger" type="button"><i class="icon-remove"></i></button></a>
                                  </span>
                                  <div style="clear:both"></div>
                                    </form>
                                </div>
                            </td>
                            <td class="price"><?php echo $product['price']; ?></td>
                            <td class="total"><?php echo $product['total']; ?></td>
                          </tr>
                        <?php } ?>
                        
                      </tbody>
                    </table>
                   <div class="row">
                       <div class="pull-right">
                            <table class='table' id="total">
                              <?php foreach ($totals as $total) { ?>
                              <tr>
                                <td class="right"><b><?php echo $total['title']; ?>:</b></td>
                                <td class="right"><?php echo $total['text']; ?></td>
                              </tr>
                              <?php } ?>
                            </table>
                        </div>
                   </div>
                     <div class="sep-bor"></div>
                     <!-- Discount Coupen -->
                      <h5 class="title">Coupon</h5>
                      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-inline" role="form">
                         <div class="form-group">
                           <input type="hidden" name="next" value="coupon" />
                           <input id="use_coupon" type="discount" class="form-control" name="coupon" value="<?php echo $coupon; ?>" />
                         </div>
                         <button type="submit" class="btn btn-info"><?php echo $button_coupon; ?></button>
                      </form>

                     <!-- Button s-->
                    <div class="row">
                      <div class="span4 offset8">
                        <div class="pull-right">
                          <a href="<?php echo $continue; ?>" class="btn btn-info"><?php echo $button_shopping; ?></a>
                          <a href="<?php echo $checkout; ?>" class="btn btn-info"><?php echo $button_checkout; ?></a>
                        </div>
                      </div>
                    </div>


               </div>
            </div>
         </div>
       <?php echo $footer ?>