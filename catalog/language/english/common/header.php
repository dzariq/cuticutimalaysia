<?php
// Text
$_['text_home']           = 'Home';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_shopping_cart']  = 'Booking';
$_['text_search']         = 'Search';

				$_['text_blog_search'] = 'Search Article';
			
$_['text_welcome']        = 'Welcome visitor you can <a href="%s">login</a> or <a href="%s">create an account</a>.';
$_['text_logged']         = 'You are logged in as <a href="%s">%s</a> <b>(</b> <a href="%s">Logout</a> <b>)</b>';
$_['text_account']        = 'My Account';
$_['text_contact']        = 'Contact Us';
$_['text_checkout']       = 'Checkout';
$_['text_special']       = 'Special';
$_['text_categories']       = 'Categories';
$_['text_blogs']           = 'Blogs';
$_['text_login']        = 'Login';
$_['text_logout']        = 'Logout';
$_['text_register']        = 'Sign Up';
$_['text_blog_search'] = 'Search Article';
?>