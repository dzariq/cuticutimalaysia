<?php  
class ControllerMyocFftheme extends Controller {
	protected function navigation() {
		$this->language->load('myoc/fftheme');

		$this->data['text_all_categories'] = $this->language->get('text_all_categories');
		$this->data['text_inside'] = $this->language->get('text_inside');
		$this->data['text_special'] = $this->language->get('text_special');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_sitemap'] = $this->language->get('text_sitemap');
		$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$this->data['text_shop_manufacturer'] = $this->language->get('text_shop_manufacturer');
		$this->data['text_all_manufacturer'] = $this->language->get('text_all_manufacturer');

		$this->data['special'] = $this->url->link('product/special');
		$this->data['contact'] = $this->url->link('information/contact');
    	$this->data['sitemap'] = $this->url->link('information/sitemap');
		$this->data['manufacturer'] = $this->url->link('product/manufacturer');

		$this->data['ff_base_color'] = $this->config->get('ffbase_color');
		$this->data['ff_menulink_color'] = $this->config->get('ffmenulink_color');

		$this->load->model('myoc/fftheme');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$this->data['is_special'] = $route == 'product/special' ? true : false;
		$this->data['is_contact'] = $route == 'information/contact' ? true : false;
		$this->data['is_information'] = $route == 'information/information' ? $this->request->get['information_id'] : false;
		
		$this->data['categories'] = array();
					
		$categories = $this->model_catalog_category->getCategories(0);
		
		foreach ($categories as $category) {
			if ($category['top']) {
				$children_data = array();
				
				$children = $this->model_catalog_category->getCategories($category['category_id']);
				
				$manufacturer_data = array();
				$category_ids = array($category['category_id']);

				foreach ($children as $child) {
					$gchildren_data = array();

					$gchildren = $this->model_catalog_category->getCategories($child['category_id']);

					foreach ($gchildren as $gchild) {
						$data = array(
							'filter_category_id'  => $gchild['category_id'],
							'filter_sub_category' => true
						);
						
						$product_total = $this->model_catalog_product->getTotalProducts($data);
										
						$gchildren_data[] = array(
							'name'  => $gchild['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
							'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $gchild['category_id'])	
						);
					}

					$data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);
					
					$product_total = $this->model_catalog_product->getTotalProducts($data);
									
					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
						'children' => $gchildren_data,
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
					);

					$category_ids[] = $child['category_id'];
				}
				$data = array(
					'filter_category_id'  => $category['category_id'],
					'filter_sub_category' => true
				);

				$product_total = $this->model_catalog_product->getTotalProducts($data);

				$category_manufacturers = $this->model_myoc_fftheme->getManufacturersByCategoryIds($category_ids);
				
				foreach ($category_manufacturers as $category_manufacturer) {
					$manufacturer_data[] = array(
						'name' => $category_manufacturer['name'],
						'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $category_manufacturer['manufacturer_id'])
					);
				}

				$this->data['categories'][] = array(
					'name'     		=> $category['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'children' 		=> $children_data,
					'column'   		=> $category['column'] ? $category['column'] : 1,
					'href'     		=> $this->url->link('product/category', 'path=' . $category['category_id']),
					'manufacturers' => $manufacturer_data,
					'image' 		=> $category['image'] ? $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height')) : false,
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		$this->data['manufacturers'] = array();

		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();

		foreach ($manufacturers as $manufacturer) {			
			$this->data['manufacturers'][] = array(
				'name' => $manufacturer['name'],
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
			);
		}

		$this->load->model('catalog/information');
		
		$this->data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			$this->data['informations'][] = array(
				'information_id' => $result['information_id'],
				'title' => $result['title'],
				'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
			);
    	}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/navigation.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/navigation.tpl';
		} else {
			return false;
		}
								
		$this->render();
	}

	protected function extend() {
		$this->language->load('myoc/fftheme');

		$this->data['text_extend_contact'] = $this->language->get('text_extend_contact');
		$this->data['text_extend_tweet'] = $this->language->get('text_extend_tweet');
		$this->data['text_extend_fblike'] = $this->language->get('text_extend_fblike');

		$footer_description = $this->config->get('fffooter_description');
		$this->data['footer_title'] = html_entity_decode($footer_description[$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');;
		$this->data['footer_description'] = html_entity_decode($footer_description[$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');;

		$this->data['contact_phone1'] = $this->config->get('ffcontact_phone1');
		$this->data['contact_phone2'] = $this->config->get('ffcontact_phone2');
		$this->data['contact_fax1'] = $this->config->get('ffcontact_fax1');
		$this->data['contact_fax2'] = $this->config->get('ffcontact_fax2');
		$this->data['contact_email1'] = $this->config->get('ffcontact_email1');
		$this->data['contact_email2'] = $this->config->get('ffcontact_email2');

		$this->data['facebook_fanpage'] = $this->config->get('fffacebook_fanpage');
		$this->data['twitter_username'] = $this->config->get('fftwitter_username');
		$this->data['tweet_no'] = $this->config->get('fftweet_no');

		$this->load->library('user');
		
		$this->user = new User($this->registry);

		$this->data['control'] = $this->user->isLogged() ? true : false;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/extend.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/extend.tpl';
		} else {
			return false;
		}
								
		$this->render();
	}

	protected function copyright() {
		$this->language->load('myoc/fftheme');

		$this->data['text_copyright'] = $this->language->get('text_copyright');
		$this->data['text_stay'] = $this->language->get('text_stay');

		$this->data['sm_fb'] = $this->config->get('ff_sm_fb');
		$this->data['sm_tw'] = $this->config->get('ff_sm_tw');
		$this->data['sm_gp'] = $this->config->get('ff_sm_gp');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/copyright.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/copyright.tpl';
		} else {
			return false;
		}
								
		$this->render();
	}

	public function getSettings() {
		$json = array();

		$json['ff_scheme'] = $this->config->get('ffscheme');
		$json['ff_header_color'] = $this->config->get('ffheader_color');

		$json['ff_categorybg_color'] = $this->config->get('ffcategorybg_color');
		$json['ff_categorybg_font'] = $this->config->get('ffcategorybg_font');

		$this->response->setOutput(json_encode($json));
	}

	public function saveSettings() {
		$this->load->library('user');
		
		$this->user = new User($this->registry);

		$json = array();

		$this->language->load('myoc/fftheme');

		if (!$this->user->isLogged() || !$this->user->hasPermission('modify', 'module/fftheme')) {
			$json['error'] = $this->language->get('error_save');
		} else {
			$this->load->model('myoc/fftheme');

			$ff_categorybg_color = $this->config->get('ffcategorybg_color');
			$ff_categorybg_font = $this->config->get('ffcategorybg_font');

			$ff_categorybg_color[$this->request->post['category_id']] = $this->request->post['categorybg_color'];
			$ff_categorybg_font[$this->request->post['category_id']] = $this->request->post['white_font'];

			$data = array(
				'ffscheme' => $this->request->post['color_scheme'],
				'ffheader_color' => $this->request->post['header_color'],
				'ffcategorybg_color' => $ff_categorybg_color,
				'ffcategorybg_font' => $ff_categorybg_font,
			);

			$this->model_myoc_fftheme->saveSettings($data);

			$json['success'] = $this->language->get('text_success');
			$json['data'] = $data;
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>