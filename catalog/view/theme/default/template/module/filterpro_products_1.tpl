<?php foreach($products as $product){ ?>
                        <!-- Item #1 -->
                        <div style="cursor:pointer" onclick="window.location='<?php echo $product["href"] ?>'" class="col-md-4 col-sm-4 col-xs-12">
                            <div  class="item" >
                                  <!-- Item image -->
                                  <ul style="padding-left:0px" class="thumbnails" id="hover-cap-4col">
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <div style="text-align: center" class=" <?php if($product['stock'] == 'SOLD OUT'){ echo 'captionnohide'; }else{ echo 'caption'; } ?> ?>">
                                                <?php if($product['stock'] != 'SOLD OUT'){  ?>
                                                <h3><?php echo $product['name'] ?></h3>
                                                    <!-- Price -->
                                    <div class="item-price  "  style="margin:0;text-align: center;color:#FAFAFA !important;background:transparent !important;width:100%;font-size:16px;">
                                        <?php if (!$product['special']) { ?><?php echo $product['price'] ?><?php }else{ ?><span style="text-decoration:line-through"><?php echo $product['price']; ?></span>&nbsp;&nbsp;<?php echo $product['special']; ?><?php } ?>
                                    </div>
                                                <?php }else{ ?>
                                                <h2 align='center' class="sold-out" style='font-weight:bold;line-height:4.4;color:white !Important'>SOLD OUT</h2>
                                                <?php } ?>
                                            </div>
                                            <?php if ($product['special']) { ?>
                                            <span class="sale_product"></span>
                                            <?php } ?>
                                            <img  style="height:auto" src="<?php echo $product['thumb'] ?>" alt="<?php echo $product['name'] ?>" />
                                        </div>
                                    </li>

                                </ul>
                               
                            </div>
                        </div>
                        <?php } ?>
                        
                        <script>
                             $('#hover-cap-4col .thumbnail').hover(
                function() {
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },
                function() {
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                }
        );
                            
                            </script>