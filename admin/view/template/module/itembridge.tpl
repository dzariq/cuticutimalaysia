<?php echo $header ?>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb): ?>
      <?php echo $breadcrumb['separator'] ?><a href="<?php echo $breadcrumb['href'] ?>"><?php echo $breadcrumb['text'] ?></a>
    <?php endforeach ?>
  </div>

  <?php if ($error_warning): ?>
    <div class="warning"><?php echo $error_warning ?></div>
  <?php endif ?>

  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title ?></h1>
      <div class="buttons"><a onclick="$('#form').submit()" class="button"><span><?php echo $button_save ?></span></a><a onclick="location.href = '<?php echo $cancel ?>'" class="button"><span><?php echo $button_cancel ?></span></a></div>
    </div>

    <div class="content">
      <form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" id="form">

        <div class="htabs">
          <a href="#home-page-tab">Home Page</a>
<!--          <a href="#footer-tab">Footer</a>
          <a href="#colors-tab">Colors</a>-->
        </div>

        <div id="home-page-tab">
          <div class="htabs">
            <?php foreach ($languages as $language): ?>
            <a href="#home-language-<?php echo $language['code'] ?>"><img src="view/image/flags/<?php echo $language['image'] ?>" title="<?php echo $language['name'] ?>" /> <?php echo $language['name'] ?></a>
            <?php endforeach ?>
          </div>

          <?php foreach ($languages as $language): ?>
          <div id="home-language-<?php echo $language['code'] ?>">
            <table class="form">
              <tr>
                <td><?php echo $home_about ?></td>
                <td><textarea name="itembridge_home_about_<?php echo $language['code'] ?>" id="itembridge_home_about_<?php echo $language['code'] ?>"><?php echo ${"itembridge_home_about_$language[code]"} ?></textarea></td>
              </tr>

              <tr>
                <td><?php echo $home_latest_news ?></td>
                <td><textarea name="itembridge_home_latest_news_<?php echo $language['code'] ?>" id="itembridge_home_latest_news_<?php echo $language['code'] ?>"><?php echo ${"itembridge_home_latest_news_$language[code]"} ?></textarea></td>
              </tr>
              
              <tr  style="display:none">
                <td><?php echo $home_widget ?></td>
                <td><textarea style="height: 200px; width: 100%;" name="itembridge_home_widget_<?php echo $language['code'] ?>" id="itembridge_home_widget_<?php echo $language['code'] ?>"><?php echo ${"itembridge_home_widget_$language[code]"} ?></textarea></td>
              </tr>
            </table>
          </div>
          <?php endforeach ?>
        </div>


        <div style="display:none" id="footer-tab">
          <div class="htabs">
            <?php foreach ($languages as $language): ?>
            <a href="#footer-language-<?php echo $language['code'] ?>"><img src="view/image/flags/<?php echo $language['image'] ?>" title="<?php echo $language['name'] ?>" /> <?php echo $language['name'] ?></a>
            <?php endforeach ?>
          </div>

          <?php foreach ($languages as $language): ?>
          <div id="footer-language-<?php echo $language['code'] ?>">
            <table class="form">
              <tr>
                <td><?php echo $footer_social ?></td>
                <td><textarea name="itembridge_footer_social_<?php echo $language['code'] ?>" id="itembridge_footer_social_<?php echo $language['code'] ?>"><?php echo ${"itembridge_footer_social_$language[code]"} ?></textarea></td>
              </tr>
            </table>
          </div>
          <?php endforeach ?>
        </div>

        <div style="display:none" id="colors-tab">
            <input type="hidden" name="itembridge_colors_version" value="<?php echo $itembridge_colors_version ?>">

            <table class="form">
              <tr>
                <th><?php echo $general_group ?></th>
                <th></th>
              </tr>
              <tr>
                <td><?php echo $page_color ?></td>
                <td><input type="color" name="itembridge_page_color" value="<?php echo $itembridge_page_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $text_color ?></td>
                <td><input type="color" name="itembridge_text_color" value="<?php echo $itembridge_text_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $link_color ?></td>
                <td><input type="color" name="itembridge_link_color" value="<?php echo $itembridge_link_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $title_color ?></td>
                <td><input type="color" name="itembridge_title_color" value="<?php echo $itembridge_title_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $price_color ?></td>
                <td><input type="color" name="itembridge_price_color" value="<?php echo $itembridge_price_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $old_price_color ?></td>
                <td><input type="color" name="itembridge_old_price_color" value="<?php echo $itembridge_old_price_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $input_border_color ?></td>
                <td><input type="color" name="itembridge_input_border_color" value="<?php echo $itembridge_input_border_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $focused_input_border_color ?></td>
                <td><input type="color" name="itembridge_focused_input_border_color" value="<?php echo $itembridge_focused_input_border_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $button_color ?></td>
                <td><input type="color" name="itembridge_button_color" value="<?php echo $itembridge_button_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hover_button_color ?></td>
                <td><input type="color" name="itembridge_hover_button_color" value="<?php echo $itembridge_hover_button_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $pressed_button_color ?></td>
                <td><input type="color" name="itembridge_pressed_button_color" value="<?php echo $itembridge_pressed_button_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $line_color ?></td>
                <td><input type="color" name="itembridge_line_color" value="<?php echo $itembridge_line_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $table_header_color ?></td>
                <td><input type="color" name="itembridge_table_header_color" value="<?php echo $itembridge_table_header_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $bottom_block_color ?></td>
                <td><input type="color" name="itembridge_bottom_block_color" value="<?php echo $itembridge_bottom_block_color ?>"></td>
              </tr>
              
              <tr>
                <th><br><?php echo $header_group ?></th>
                <th></th>
              </tr>
              <tr>
                <td><?php echo $header_ribbon_color1 ?></td>
                <td><input type="color" name="itembridge_header_ribbon_color1" value="<?php echo $itembridge_header_ribbon_color1 ?>"></td>
              </tr>
              <tr>
                <td><?php echo $header_ribbon_color2 ?></td>
                <td><input type="color" name="itembridge_header_ribbon_color2" value="<?php echo $itembridge_header_ribbon_color2 ?>"></td>
              </tr>
              <tr>
                <td><?php echo $header_ribbon_color3 ?></td>
                <td><input type="color" name="itembridge_header_ribbon_color3" value="<?php echo $itembridge_header_ribbon_color3 ?>"></td>
              </tr>
              <tr>
                <td><?php echo $header_color ?></td>
                <td><input type="color" name="itembridge_header_color" value="<?php echo $itembridge_header_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $welcome_text_color ?></td>
                <td><input type="color" name="itembridge_welcome_text_color" value="<?php echo $itembridge_welcome_text_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hovered_header_nav_links ?></td>
                <td><input type="color" name="itembridge_hovered_header_nav_links" value="<?php echo $itembridge_hovered_header_nav_links ?>"></td>
              </tr>
              <tr>
                <td><?php echo $primary_nav ?></td>
                <td><input type="color" name="itembridge_primary_nav" value="<?php echo $itembridge_primary_nav ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hover_primary_nav ?></td>
                <td><input type="color" name="itembridge_hover_primary_nav" value="<?php echo $itembridge_hover_primary_nav ?>"></td>
              </tr>
              <tr>
                <td><?php echo $primary_nav_sub ?></td>
                <td><input type="color" name="itembridge_primary_nav_sub" value="<?php echo $itembridge_primary_nav_sub ?>"></td>
              </tr>
              <tr>
                <td><?php echo $primary_nav_sub_hover ?></td>
                <td><input type="color" name="itembridge_primary_nav_sub_hover" value="<?php echo $itembridge_primary_nav_sub_hover ?>"></td>
              </tr>
              
              <tr>
                <th><br><?php echo $slider_group ?></th>
                <th></th>
              </tr>
              <tr>
                <td><?php echo $next_prev ?></td>
                <td><input type="color" name="itembridge_next_prev" value="<?php echo $itembridge_next_prev ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hover_next_prev ?></td>
                <td><input type="color" name="itembridge_hover_next_prev" value="<?php echo $itembridge_hover_next_prev ?>"></td>
              </tr>
              
              <tr>
                <th><br><?php echo $product_group ?></th>
                <th></th>
              </tr>
              <tr>
              <tr>
                <td><?php echo $hover_produck_border ?></td>
                <td><input type="color" name="itembridge_hover_produck_border" value="<?php echo $itembridge_hover_produck_border ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hover_action_button ?></td>
                <td><input type="color" name="itembridge_hover_action_button" value="<?php echo $itembridge_hover_action_button ?>"></td>
              </tr>
              <tr>
                <td><?php echo $add_cart ?></td>
                <td><input type="color" name="itembridge_add_cart" value="<?php echo $itembridge_add_cart ?>"></td>
              </tr>
              <tr>
                <td><?php echo $tab_border ?></td>
                <td><input type="color" name="itembridge_tab_border" value="<?php echo $itembridge_tab_border ?>"></td>
              </tr>
              <tr>
                <td><?php echo $tab_color ?></td>
                <td><input type="color" name="itembridge_tab_color" value="<?php echo $itembridge_tab_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $hover_tab_color ?></td>
                <td><input type="color" name="itembridge_hover_tab_color" value="<?php echo $itembridge_hover_tab_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $current_tab_color ?></td>
                <td><input type="color" name="itembridge_current_tab_color" value="<?php echo $itembridge_current_tab_color ?>"></td>
              </tr>
              
              <tr>
                <th><br><?php echo $footer_group ?></th>
                <th></th>
              </tr>
              <tr>
                <td><?php echo $footer_color ?></td>
                <td><input type="color" name="itembridge_footer_color" value="<?php echo $itembridge_footer_color ?>"></td>
              </tr>
              <tr>
                <td><?php echo $footer_links ?></td>
                <td><input type="color" name="itembridge_footer_links" value="<?php echo $itembridge_footer_links ?>"></td>
              </tr>
              <tr>
                <td><?php echo $footer_links_hover ?></td>
                <td><input type="color" name="itembridge_footer_links_hover" value="<?php echo $itembridge_footer_links_hover ?>"></td>
              </tr>
            </table>
        </div>

      </form>
    </div>
  </div>
</div>

<?php echo $footer ?>


<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>

<script type="text/javascript">//<!--
$('.htabs').each(function () {
	$(this).find('a').tabs();
});
//--></script>

<script type="text/javascript">//<!--

<?php foreach ($languages as $language): ?>
  setWysiwyg('itembridge_home_about_<?php echo $language["code"] ?>');
  setWysiwyg('itembridge_home_latest_news_<?php echo $language["code"] ?>');

  setWysiwyg('itembridge_footer_social_<?php echo $language["code"] ?>');
<?php endforeach ?>

function setWysiwyg(id)
{
  var file_browser_url = 'index.php?route=common/filemanager&token=<?php echo $token ?>';
  CKEDITOR.replace(id, {
    filebrowserBrowseUrl: file_browser_url,
    filebrowserImageBrowseUrl: file_browser_url,
    filebrowserFlashBrowseUrl: file_browser_url,
    filebrowserUploadUrl: file_browser_url,
    filebrowserImageUploadUrl: file_browser_url,
    filebrowserFlashUploadUrl: file_browser_url
  });
}
//--></script>
