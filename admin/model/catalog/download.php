<?php
class ModelCatalogDownload extends Model {
	public function addDownload($data,$product_id=0) {
        if (!(isset($data['remaining']) and $data['remaining'])) $data['remaining']=0;
      	$this->db->query("INSERT INTO " . DB_PREFIX . "download SET filename = '" . $this->db->escape($data['filename']) . "', mask = '" . $this->db->escape($data['mask']) . "', remaining = '" . (int)$data['remaining'] . "', date_added = NOW()");

      	$download_id = $this->db->getLastId(); 

      	foreach ($data['download_description'] as $language_id => $value) {
        	$this->db->query("INSERT INTO " . DB_PREFIX . "download_description SET download_id = '" . (int)$download_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
      	}	
if ($product_id) $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET download_id = '" . (int)$download_id . "', product_id='".$product_id."'");
	}
	
	public function editDownload($download_id, $data) {
if (!(isset($data['remaining']) and $data['remaining'])) $data['remaining']=0;
		if (!empty($data['update'])) {
			$download_info = $this->getDownload($download_id);
        	
			if ($download_info) {
      			$this->db->query("UPDATE " . DB_PREFIX . "order_download SET `filename` = '" . $this->db->escape($data['filename']) . "', mask = '" . $this->db->escape($data['mask']) . "', remaining = '" . (int)$data['remaining'] . "' WHERE `filename` = '" . $this->db->escape($download_info['filename']) . "'");
			}
		}		
		
        $this->db->query("UPDATE " . DB_PREFIX . "download SET filename = '" . $this->db->escape($data['filename']) . "', mask = '" . $this->db->escape($data['mask']) . "', remaining = '" . (int)$data['remaining'] . "' WHERE download_id = '" . (int)$download_id . "'");

      	$this->db->query("DELETE FROM " . DB_PREFIX . "download_description WHERE download_id = '" . (int)$download_id . "'");

      	foreach ($data['download_description'] as $language_id => $value) {
        	$this->db->query("INSERT INTO " . DB_PREFIX . "download_description SET download_id = '" . (int)$download_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
      	}
	}
	
	public function deleteDownload($download_id) {
      	$this->db->query("DELETE FROM " . DB_PREFIX . "download WHERE download_id = '" . (int)$download_id . "'");
	  	$this->db->query("DELETE FROM " . DB_PREFIX . "download_description WHERE download_id = '" . (int)$download_id . "'");	
$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");
	}	

	public function getDownload($download_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "download WHERE download_id = '" . (int)$download_id . "'");
		
		return $query->row;
	}

	public function getDownloads($data = array(),$product_id=0) {

		$sql = "SELECT * FROM " . DB_PREFIX . "download d LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) ";

        if ($product_id) $sql = $sql . "LEFT JOIN " . DB_PREFIX . "product_to_download p2d ON (d.download_id = p2d.download_id) ";
        $sql = $sql . "WHERE dd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
        if ($product_id) $sql = $sql . " AND product_id='" . $product_id . "'";

	
		$sort_data = array(
			'dd.name',
			'd.remaining'
		);
	
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY d.download_id";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql)->rows;
        $result = array();

        foreach ($query as $download) {
            // --- Check file existance and size
            $size = false;
            if (file_exists(DIR_DOWNLOAD . $download['filename'])) {
                $size = filesize(DIR_DOWNLOAD . $download['filename']);
                $i = 0;
                $suffix = array('bytes','Kb','Mb','Gb','Tb');
                while (($size / 1024) > 1) {
                    $size = $size / 1024;
                    $i++;
                }
                $size = '('.round(substr($size, 0, strpos($size, '.') + 4), 2) ." ". $suffix[$i].')';
            }

            // ---------- File Icon ------------
            if ($download['mask']) $ext=strtolower(substr(strrchr($download['mask'],'.'),1));
               else $ext=strtolower(substr(strrchr($download['filename'],'.'),1));
            if (file_exists(DIR_DOWNLOAD . 'icons/'.$ext.'.png'))
                $icon='../download/icons/'.$ext.'.png';
            else $icon=false;

            $result[] = array(
                'download_id' => $download['download_id'],
                'filename'    => $download['filename'],
                'mask'        => $download['mask'],
                'remaining'   => $download['remaining'],
                'date_added'  => $download['date_added'],
                'name'        => $download['name'],
                'size'        => $size,
                'icon'        => $icon
            );
        }

		return $result;


	}
	
	public function getDownloadDescriptions($download_id) {
		$download_description_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "download_description WHERE download_id = '" . (int)$download_id . "'");
		
		foreach ($query->rows as $result) {
			$download_description_data[$result['language_id']] = array('name' => $result['name']);
		}
		
		return $download_description_data;
	}
	
	public function getTotalDownloads() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "download");
		
		return $query->row['total'];
	}	
}
?>