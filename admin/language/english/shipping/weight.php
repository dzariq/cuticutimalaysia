<?php
// Heading
$_['heading_title']    = 'Holidays Setting';

// Text
$_['text_shipping']    = 'Holidays';
$_['text_success']     = 'Success: You have modified holidays!';

// Entry
$_['entry_rate']       = 'Dates:<br /><span class="help">Example: 29-06-2015,06-07-2015</span>';
$_['entry_tax_class']  = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify this!';
?>