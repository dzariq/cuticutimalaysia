<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="alert alert-danger">
    <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<!-- Page content -->

<div class="blocky">
    <div class="<?php echo layout ?>">
        <div class="row">

            <div class="col-md-12">
                <a href="register" class="btn btn-info" >Back</a>
            </div>
            <div class="col-md-12">
                <div class="register-login">
                    <?php echo $this->getChild('payment/pp_standard'); ?>
                </div>
            </div>
        </div>
        <div class="sep-bor"></div>
    </div>
</div>

<?php echo $footer; ?>