<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <div  id="content" class="col-md-12">
                <?php echo $content_top; ?>
                <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="contact">
                    <h2>Contact Us</h2>
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-md-4">
                            <strong><?php echo $text_address; ?></strong>
                            <br />
                            <?php echo $store; ?>
                            <br />
                            <address><?php echo $address; ?></address>
                        </div>
                        <div class="col-md-4">
                            <?php if ($telephone) { ?>
                            <strong><?php echo $text_telephone; ?></strong>
                            <br />
                            <?php echo $telephone; ?>
                            <br />
                            <br />
                            <?php } ?>
                            <?php if ($fax) { ?>
                            <strong><?php echo $text_fax; ?></strong>
                            <br />
                            <?php echo $fax; ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-4">
                            <strong>Email:</strong>
                            <br />
                            <?php echo $this->config->get('config_email') ?>
                        </div>
                    </div>
                </form>
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>