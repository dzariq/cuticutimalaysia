<?php
// Text
$_['text_home']           = 'Utama';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_shopping_cart']  = 'Bakul Belanja';
$_['text_search']         = 'Cari';
$_['text_welcome']        = 'Selamat datang, silakan <a href="%s">login</a> atau <a href="%s">daftar</a>.';
$_['text_logged']         = 'Anda login sebagai <a href="%s">%s</a> <b>(</b> <a href="%s">Logout</a> <b>)</b>';
$_['text_account']        = 'Akaun Saya';
$_['text_checkout']       = 'Checkout';
$_['text_register']       = 'Daftar';
$_['text_special']       = 'Istimewa';
$_['text_categories']       = 'Kategori';
$_['text_contact']        = 'Hubungi Kami';


?>