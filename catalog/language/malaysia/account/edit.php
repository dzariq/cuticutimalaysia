<?php
// Heading 
$_['heading_title']     = 'Informasi Akaun Saya';

// Text
$_['text_account']      = 'Akaun';
$_['text_edit']         = 'Ubah Informasi';
$_['text_your_details'] = 'Detail Pribadi Anda';
$_['text_success']      = 'Berhasil: Akaun Anda berhasil diperbaharui.';

// Entry
$_['entry_firstname']  = 'Nama Hadapan:';
$_['entry_lastname']   = 'Nama Belakang:';
$_['entry_email']      = 'Email:';
$_['entry_telephone']  = 'Telepon:';
$_['entry_fax']        = 'Fax:';

// Error
$_['error_exists']     = 'Peringatan: Alamat Email telah terdaftar sebelumnya!';
$_['error_firstname']  = 'Nama Hadapan harus terdiri dari 1 hingga 32 karakter!';
$_['error_lastname']   = 'Nama Belakang harus terdiri dari 1 hingga 32 karakter!';
$_['error_email']      = 'Alamat Email tidak valid!';
$_['error_telephone']  = 'No Telefon harus terdiri dari 3 hingga 32 karakter!';
?>