
$(document).on({
    ajaxStart: function () {
        $('.homepage').addClass("loading");
    },
    ajaxStop: function () {
        $('.homepage').removeClass("loading");
    }
});
jQuery(function ($) {


    //#main-slider
    $('.carousel').carousel({
        interval: 3000
    });
    // accordian
    $('.accordion-toggle').on('click', function () {
        $(this).closest('.panel-group').children().each(function () {
            $(this).find('>.panel-heading').removeClass('active');
        });
        $(this).closest('.panel-heading').toggleClass('active');
    });
    $('img').bind('contextmenu', function (e) {
        return false;
    });
    //Initiat WOW JS
    new WOW().init();
    // portfolio filter
    $(window).load(function () {
        'use strict';
        var $portfolio_selectors = $('.portfolio-filter >li>a');
        var $portfolio = $('.portfolio-items');
        $portfolio.isotope({
            itemSelector: '.portfolio-item',
            layoutMode: 'fitRows'
        });
        $portfolio_selectors.on('click', function () {
            $portfolio_selectors.removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $portfolio.isotope({filter: selector});
            return false;
        });
    });
    // Contact form
    var form = $('#main-contact-form');
    form.submit(function (event) {
        event.preventDefault();
        var form_status = $('<div class="form_status"></div>');
        $.ajax({
            url: $(this).attr('action'),
            beforeSend: function () {
                form.prepend(form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn());
            }
        }).done(function (data) {
            form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
        });
    });
    //goto top
   
    //Pretty Photo
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false
    });
    $('img').bind('contextmenu', function (e) {
        return false;
    });
});
var option_quantity = '';
var option_required = false;
$(document).ready(function () {
    $(".tile").height($("#tile1").width());
    $(".carousel").height($("#tile1").width() * 3);
    $(".itemtile").height($("#tile1").width());
    $(".item").height("320");
    $(window).resize(function () {
        if (this.resizeTO)
            clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 10);
    });
    $(window).bind('resizeEnd', function () {
        $(".tile").height($("#tile1").width());
        $(".carousel").height($("#tile1").width() * 3);
        $(".itemtile").height($("#tile1").width());
        $(".itemt").height("575");
    });
});
$(document).ready(function () {

    $('#blog-landing').pinterest_grid({
        no_columns: 3,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
    $('#video-landing').pinterest_grid({
        no_columns: 2,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });
    /*
     * Author:      Marco Kuiper (http://www.marcofolio.net/)
     */


    // When everything has loaded, place all polaroids on a random position	
    $(".polaroid").each(function (i) {
        var tempVal = Math.round(Math.random());
        if (tempVal == 1) {
            var rotDegrees = randomXToY(330, 360); // rotate left
        } else {
            var rotDegrees = randomXToY(0, 30); // rotate right
        }

// Internet Explorer doesn't have the "window.innerWidth" and "window.innerHeight" properties
        if (window.innerWidth == undefined) {
            var wiw = 1000;
            var wih = 700;
        } else {
            var wiw = window.innerWidth;
            var wih = window.innerHeight;
        }

        var cssObj = {'left': Math.random() * (wiw - 400),
            'top': Math.random() * (wih - 400),
            '-webkit-transform': 'rotate(' + rotDegrees + 'deg)', // safari only
            'transform': 'rotate(' + rotDegrees + 'deg)'}; // added in case CSS3 is standard
        $(this).css(cssObj);
    });
    // Set the Z-Index (used to display images on top while dragging)
    var zindexnr = 1;
    // boolean to check if the user is dragging
    var dragging = false;
    // Show the polaroid on top when clicked on
    $(".polaroid").mouseup(function (e) {
        if (!dragging) {
// Bring polaroid to the foreground
            zindexnr++;
            var cssObj = {'z-index': zindexnr,
                'transform': 'rotate(0deg)', // added in case CSS3 is standard
                '-webkit-transform': 'rotate(0deg)'}; // safari only
            $(this).css(cssObj);
        }
    });
    // Make the polaroid draggable & display a shadow when dragging
    $(".polaroid").draggable({
        cursor: 'crosshair',
        start: function (event, ui) {
            dragging = true;
            zindexnr++;
            var cssObj = {'box-shadow': '#888 5px 10px 10px', // added in case CSS3 is standard
                '-webkit-box-shadow': '#888 5px 10px 10px', // safari only
                'margin-left': '-10px',
                'margin-top': '-10px',
                'z-index': zindexnr};
            $(this).css(cssObj);
        },
        stop: function (event, ui) {
            var tempVal = Math.round(Math.random());
            if (tempVal == 1) {
                var rotDegrees = randomXToY(330, 360); // rotate left
            } else {
                var rotDegrees = randomXToY(0, 30); // rotate right
            }
            var cssObj = {'box-shadow': '', // added in case CSS3 is standard
                '-webkit-box-shadow': '', // safari only
                'transform': 'rotate(' + rotDegrees + 'deg)', // added in case CSS3 is standard
                '-webkit-transform': 'rotate(' + rotDegrees + 'deg)', // safari only
                'margin-left': '0px',
                'margin-top': '0px'};
            $(this).css(cssObj);
            dragging = false;
        }
    });
    // Function to get random number upto m
    // http://roshanbh.com.np/2008/09/get-random-number-range-two-numbers-javascript.html
    function randomXToY(minVal, maxVal, floatVal) {
        var randVal = minVal + (Math.random() * (maxVal - minVal));
        return typeof floatVal == 'undefined' ? Math.round(randVal) : randVal.toFixed(floatVal);
    }


    $('.book-room').on('click', function () {
        var name = $(this).attr('name');
        var room_id = $(this).attr('roomid');
        $(".room-type").val(name);
        $(".room-id").val(room_id);
        $("#button-cart").trigger("click");
    });



});




