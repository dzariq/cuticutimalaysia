<?php

class ControllerInformationTest extends Controller {

    public function index() {
        
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->hostname = $this->config->get('config_smtp_host');
        $mail->username = $this->config->get('config_smtp_username');
        $mail->password = $this->config->get('config_smtp_password');
        $mail->port = $this->config->get('config_smtp_port');
        $mail->timeout = $this->config->get('config_smtp_timeout');
        $mail->setTo('creativeparttime@gmail.com');
//        $mail->setFrom($this->config->get('config_email'));
        $mail->setFrom('no-reply@'.$_SERVER['HTTP_HOST']);
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject(html_entity_decode('testmail', ENT_QUOTES, 'UTF-8'));
        $mail->setText(html_entity_decode('test message', ENT_QUOTES, 'UTF-8'));
        $mail->send();
    }

}
