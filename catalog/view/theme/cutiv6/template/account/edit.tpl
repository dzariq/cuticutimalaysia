<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li class="active"><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
                                <h2 class="dash-content-title">My Profile</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h4>Edit Profile</h4></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="edit">
                                                <div class="form-group">
                                                    <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                                                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                                                    <?php if ($error_firstname) { ?>
                                                    <span class="error"><?php echo $error_firstname; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="required"><?php echo $entry_email; ?></label>
                                                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" />
                                                    <?php if ($error_email) { ?>
                                                    <span class="error"><?php echo $error_email; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="telephone" class="required"><?php echo $entry_telephone; ?></label>
                                                    <input type="tel" class="form-control" id="telephone" name="telephone" value="<?php echo $telephone; ?>" />
                                                    <?php if ($error_telephone) { ?>
                                                    <span class="error"><?php echo $error_telephone; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="pull-left">
                                                    <a href="<?php echo $back; ?>" ><button class='btn btn-orange'>BACK</button></a>
                                                </div>
                                                <div class="pull-right">
                                                    <a onclick="$('#edit').submit();" ><button class='btn btn-orange'>SAVE</button></a>
                                                </div>
                                            </form>
                                        </div><!-- end row -->

                                    </div><!-- end panel-body -->
                                </div><!-- end panel-detault -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 