<?php
// Heading
$_['heading_title'] = 'Akaun Afiliasi Anda telah dibuat!';

// Text 
$_['text_approval'] = '<p>Terima kasih telah mendaftar Akaun Afiliasi dengan %s!</p><p>Anda akan diberitahu melalui Email begitu Akaun Anda diaktifkan.</p><p>Jika Anda memiliki pertanyaan seputar program affiliate ini, silahkan <a href="%s">hubungi kami</a>.</p>';
$_['text_account']  = 'Akaun';
$_['text_success']  = 'Berjaya';
?>