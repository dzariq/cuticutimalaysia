<?php

class ControllerInformationAutoworldnews extends Controller {

    private $error = array();

    public function index() {

        $this->document->setTitle('Autoworld News');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/autoworldnews.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/information/autoworldnews.tpl';
        } else {
            $this->template = 'default/template/information/autoworldnews.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    }

}

?>