<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <h1><?php echo $heading_title; ?></h1>
            <div id="content" class="col-md-12">
                <?php echo $content_top; ?>
                <section>
                    <?php echo $text_error; ?>
                </section>
                <div class="pull-right">
                    <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                </div>
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>