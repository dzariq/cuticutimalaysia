<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li ><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li class="active"><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content booking-trips">
                                <h2 class="dash-content-title">Trips You have Booked!</h2>
                                <div class="dashboard-listing booking-listing">


                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tbody>
                                                <?php if ($orders) { ?>
                                                <?php $i=0; foreach ($orders as $order) { ?>
                                                <tr>
                                                    <td class="dash-list-icon booking-list-date"><div class="b-date"><h3><?php echo date('d',strtotime($order['date_added'])); ?></h3><p><?php echo date('M',strtotime($order['date_added'])); ?></p></div></td>
                                                    <td class="dash-list-text booking-list-detail">
                                                        <h3>BOOKING ID: #<?php echo $order['order_id']; ?> - <?php echo $order['product_details']['name']; ?></h3>
                                                        <ul class="list-unstyled booking-info">
                                                            <li><span>Booking Date:</span> <?php echo $order['date_added']; ?></li>
                                                            <li><span>Booking Total:</span> <?php echo $order['total']; ?></li>
                                                        </ul>
                                                        <?php if($order['status'] == 'Paid'){ ?>
                                                        <a  ><button class="btn btn-orange"><?php echo $order['status']; ?> - Waiting Hotel Confirmation</button></a>
                                                        <?php }else if($order['status'] == 'Complete'){ ?>
                                                        <a  ><button class="btn btn-green">Confirmed</button></a>
                                                        <?php }else{ ?>
                                                        <a  ><button class="btn btn-orange"><?php echo $order['status']; ?></button></a>
                                                        <?php } ?>
                                                        <?php if($order['tracking_number']){ ?>
                                                        <a href='index.php?route=sale/order/invoice&order_id=<?php echo $order["order_id"] ?>&pdf=true' target='_blank' ><button class="btn btn-orange">Download Booking Voucher</button></a>
                                                        <?php } ?>
                                                    </td>
                                                    <td class="dash-list-btn">
                                                        <a href="<?php echo $order['href']; ?>" ><button class="btn btn-orange">VIEW DETAILS</button></a>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                                <?php }else{ ?>
                                                <tr>
                                                    <td>
                                                        <h3>You have not made any bookings with Cuti Cuti Malaysia. Choose your dream hotel today!</h3>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div><!-- end table-responsive -->
                                </div><!-- end booking-listings -->

                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 