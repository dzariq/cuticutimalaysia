<?php
// Heading 
$_['heading_title']                = 'Login Akaun';

// Text
$_['text_what_dropship']                 = 'Apa itu DROPSHIP ?';
$_['text_account']                 = 'Akaun';
$_['text_login']                   = 'Login';
$_['text_new_customer']            = 'Daftar Akaun';
$_['text_register']                = 'Daftar Akaun';
$_['text_register_account']        = 'Dengan membuat Akaun, Anda dapat berbelanja lebih cepat, selalu update dengan status pesanan dan dapat melihat sejarah pesanan yang pernah Anda lakukan sebelumnya.';
$_['text_returning_customer']      = 'Member Login';
$_['text_i_am_returning_customer'] = 'Bagi yang sudah memiliki Akaun, sila login disini.';
$_['text_forgotten']               = 'Lupa password anda?';

// Entry
$_['entry_email']                  = 'Alamat Email:';
$_['entry_password']               = 'Password:';

// Error
$_['error_login']                  = 'Peringatan: Alamat Email dan/atau password tidak sesuai.';
$_['error_approved']               = 'Peringatan: Akaun Anda memerlukan persetujuan Admin untuk diaktifkan terlebih dahulu.';
?>