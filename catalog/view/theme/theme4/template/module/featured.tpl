<div class="col-sm-8 col-xs-8">
    <div id="tile4" class="tile">
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div style='background:url("<?php echo $this->model_tool_image->resize($products[0][thumb_large], 380, 185); ?>") no-repeat;background-size:100% 100%' class="item active">
                </div>
                <div style='background:url("<?php echo $this->model_tool_image->resize($products[1][thumb_large], 380, 185); ?>") no-repeat;background-size:100% 100%' class="item ">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile5" class="tile">
      <img src="<?php echo $products[2][thumb] ?>" class="img-responsive"  />
    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile6" class="tile">
      <img src="<?php echo $products[3][thumb] ?>" class="img-responsive" />
    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile7" class="tile">
      <img src="<?php echo $products[4][thumb] ?>" class="img-responsive" />
    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile8" class="tile">
      <img src="<?php echo $products[5][thumb] ?>" class="img-responsive" />
    </div>
</div>