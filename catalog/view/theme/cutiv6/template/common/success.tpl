<?php echo $header; ?>
<section class="innerpage-wrapper">
    <div id="payment-success" style='background: #e0e0e0;' class="section-padding">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="payment-success-text">
                        
                        <?php echo $text_message; ?>

                        <span><i class="fa fa-check-circle"></i></span>
                        
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo $continue; ?>" ><button class="btn btn-orange"><?php echo $button_continue; ?></button></a>
                    </div>
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end coming-soon-text -->
</section><!-- end innerpage-wrapper -->
<?php echo $footer; ?>