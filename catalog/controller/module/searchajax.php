<?php

################################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com    	   #
################################################################################################

class ControllerModuleSearchajax extends Controller {

    protected function index($setting) {


        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $this->data['categories'] = $this->model_catalog_category->getCategories(0);
        $this->data['childCategories'] = array();

        $this->data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();
        $this->data['manucat'] = array();
        $this->data['action'] = "index.php?route=product/search";

        foreach ($this->data['categories'] as $category) {
            //get products with this manu
            foreach ($this->data['manufacturers'] as $keym => $manufacturer) {
                $criteria = array(
                    'filter_category_id' => $category['category_id'],
                    'filter_manufacturer_id' => $manufacturer['manufacturer_id'],
                );

                $products = $this->model_catalog_product->getProducts($criteria);
                if (count($products) != 0) {
                    $this->data['manucat'][$category['category_id']][] = $manufacturer;
                }
            }

            $this->data['childCategories'][$category['category_id']] = $this->model_catalog_category->getCategories($category['category_id']);
        }


        static $module = 0;

        $this->data['title'] = $setting['title'];
        $this->data['description'] = $setting['description'];
        $this->data['link'] = $setting['link'];
        $this->data['linkname'] = $setting['linkname'];
        $this->data['module'] = $module++;


        $this->load->model('module/searchajax');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/searchajax.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/searchajax.tpl';
        } else {
            $this->template = 'default/template/module/searchajax.tpl';
        }

        $this->render();
    }

    public function api() {

        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        if ($this->request->post['searchall'] == '' && $this->request->post['category'] == '' && $this->request->post['manufacturer'] == '') {
            $data = array(
                'status' => 0,
                'result' => '',
            );


            echo json_encode($data);
            die;
        }

        //get data
        $criteria = array(
            'filter_name' => $this->request->post['searchall'],
            'filter_category_id' => $this->request->post['category'],
            'filter_manufacturer_id' => $this->request->post['manufacturer'],
        );

        $category = $this->model_catalog_category->getCategory($this->request->post['category']);

        $products = $this->model_catalog_product->getProducts($criteria);

        foreach ($products as $key => $product) {
            $products[$key]['price'] = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            $products[$key]['description'] = utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, 50) . '..';
            $products[$key]['category'] = $category['name'];
        }
        $status = 1;

        if (count($products) == 0) {
            $status = 0;
        }

//        echo '<pre>';
//        print_r($products);
//        echo '</pre>';
        $data = array(
            'status' => $status,
            'result' => $products,
        );


        echo json_encode($data);
    }

}

?>