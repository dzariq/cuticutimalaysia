<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="alert alert-danger">
    <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<!-- Page content -->
<style>
    .register-login{
        margin:10px auto !important;
    }
</style>
<div class="blocky">
    <div class="<?php echo layout ?>">
        <div id="register" class="row">
                        <h3><?php echo $heading_title ?></h3>

            <div class=" col-md-4 register-login">
                <div class="cool-block">
                    <div class="cool-block-bor">

                        <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
                            <h2><?php echo $text_your_details; ?></h2>
                            <div class="form-group">
                                <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                                <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                                <?php if ($error_firstname) { ?>
                                <span class="error"><?php echo $error_firstname; ?></span>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="email" class="required"><?php echo $entry_email; ?></label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" />
                                <?php if ($error_email) { ?>
                                <span class="error"><?php echo $error_email; ?></span>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="required"><?php echo $entry_telephone; ?></label>
                                <input type="tel" class="form-control" id="telephone" name="telephone" value="<?php echo $telephone; ?>" />
                                <?php if ($error_telephone) { ?>
                                <span class="error"><?php echo $error_telephone; ?></span>
                                <?php } ?>
                            </div>

                            <div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;">
                                <div class="form-group">
                                    <label for="customer_group_id"><?php echo $entry_customer_group; ?></label>
                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                    <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
                                   <div class="radio">
                                            <label>
                                                <input onclick="check_group('<?php echo $customer_group[membership_fee]; ?>','<?php echo $customer_group[description]; ?>', '<?php echo $customer_group[customer_group_id]; ?>')" type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
                                                <?php echo $customer_group['name']; ?>
                                            </label>
                                        </div>
                                        <?php } else { ?>
                                        <div class="radio">
                                            <label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>">
                                                <input onclick="check_group('<?php echo $customer_group[membership_fee]; ?>','<?php echo $customer_group[description]; ?>', '<?php echo $customer_group[customer_group_id]; ?>')" type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
                                                <?php echo $customer_group['name']; ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>

<input type="hidden" value="<?php echo $customer_group_id; ?>" id="customer_group_id" />


                    </div>
                </div>
                <div id="group-desc" style="padding:20px;font-size:25px;text-align: center;line-height:1.3">
                    <?php echo $group_desc ?>
                </div>
            </div>
            <div class=" col-md-4 register-login">
                <div class="cool-block">
                    <div class="cool-block-bor">
                        <h2><?php echo $text_your_address; ?> </h2>

                        <div class="form-group">
                            <label for="address_1" class="required"><?php echo $entry_address_1; ?></label>
                            <input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1; ?>" />
                            <?php if ($error_address_1) { ?>
                            <span class="error"><?php echo $error_address_1; ?></span>
                            <?php } ?>
                        </div>

                        <div class="form-group">
                            <label for="city" class="required"><?php echo $entry_city; ?></label>
                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>" />
                            <?php if ($error_city) { ?>
                            <span class="error"><?php echo $error_city; ?></span>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label  for="postcode" class="required"><?php echo $entry_postcode; ?></label>
                            <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
                            <?php if ($error_postcode) { ?>
                            <span class="error"><?php echo $error_postcode; ?></span>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label for="country_id" class="required"><?php echo $entry_country; ?></label>
                            <select class="form-control" id="country_id" name="country_id">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($countries as $country) { ?>
                                <?php if ($country['country_id'] == $country_id) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if ($error_country) { ?>
                            <span class="error"><?php echo $error_country; ?></span>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
                            <select class="form-control" id="zone" name="zone_id"></select>
                            <?php if ($error_zone) { ?>
                            <span class="error"><?php echo $error_zone; ?></span>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        <div class=" col-md-4 register-login">
            <div class="cool-block">
                <div class="cool-block-bor">
                    <h2><?php echo $text_your_password; ?></h2>
                    <div class="form-group">
                        <label for="password" class="required"><?php echo $entry_password; ?></label>
                        <input class="form-control" type="password" id="password" name="password" value="<?php echo $password; ?>" />
                        <?php if ($error_password) { ?>
                        <span class="error"><?php echo $error_password; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
                        <input class="form-control" type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                        <?php if ($error_confirm) { ?>
                        <span class="error"><?php echo $error_confirm; ?></span>
                        <?php } ?>
                    </div>
                 
                        <input type="hidden" id="newsletter" name="newsletter" value="1" />
                     
                    <?php  if($fee != ''){ ?>
                    <div id='payment_method_section'>
                        <?php if ($payment_methods) { ?>
                        <h2><?php echo $text_payment_method; ?></h2>
                        <table class="table table-hover">
                            <?php foreach ($payment_methods as $payment_method) { ?>
                            <tr>
                            <div class="form-group">
                                <div class="radio">
                                    <label for="<?php echo $payment_method['code']; ?>">
                                        <?php if ($payment_method['code'] == $code || !$code) { ?>
                                        <?php $code = $payment_method['code']; ?>
                                        <input  type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
                                        <?php } else { ?>
                                        <input  type="radio"  name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
                                        <?php } 
                                        echo $payment_method['title']; ?>
                                    </label>
                                </div>
                            </div>
                            </tr>
                            <?php } ?>
                        </table>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div style='padding:0px' class="col-xs-12 ">
                        <?php if ($text_agree) {
                        echo $text_agree;
                        if ($agree) { ?>
                        <input style="margin: 4px 15px 0 10px;" type="checkbox" id="agree" name="agree" value="1" checked="checked" />
                        <?php } else { ?>
                        <input style="margin: 4px 15px 0 10px;" type="checkbox" id="agree" name="agree" value="1" />
                        <?php } ?>
                        <br/>
                        <input type="hidden" id="warning" />
                        <br/>
                        <a onclick="process_form();" class="btn btn-info"> <?php echo $button_continue; ?></a>
                        <?php } else { ?>
                        <a onclick="process_form()" class="btn btn-info"> <?php echo $button_continue; ?></a>
                        <?php } ?>
                    </div>

                    </form>
                    <br/>
                 
                </div>
            </div>
        </div>
        </div>
        <div class="row">
               <div id="paymentSection" >
               </div>
        </div>

        <div class="sep-bor"></div>
    </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').on('change', function() {
        var customer_group = [];
                <?php foreach ($customer_groups as $customer_group) { ?>
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ] = [];
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
                customer_group[ <?php echo $customer_group['customer_group_id']; ?> ]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
                <?php } ?>
                if (customer_group[this.value]) {
            if (customer_group[this.value]['company_id_display'] == '1') {
                $('#company-id-display').show();
            } else {
                $('#company-id-display').hide();
            }

            if (customer_group[this.value]['company_id_required'] == '1') {
                $('#company-id-required').show();
            } else {
                $('#company-id-required').hide();
            }

            if (customer_group[this.value]['tax_id_display'] == '1') {
                $('#tax-id-display').show();
            } else {
                $('#tax-id-display').hide();
            }

            if (customer_group[this.value]['tax_id_required'] == '1') {
                $('#tax-id-required').show();
            } else {
                $('#tax-id-required').hide();
            }
        }
    });

    $('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('select[name=\'country_id\']').bind('change', function() {
        $.ajax({
            url: 'index.php?route=account/register/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function() {
                $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
            },
            complete: function() {
                $('.wait').remove();
            },
            success: function(json) {
                if (json['postcode_required'] == '1') {
                    $('#postcode-required').show();
                } else {
                    $('#postcode-required').hide();
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('select[name=\'zone_id\']').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('select[name=\'country_id\']').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
        $('.colorbox').colorbox({
            overlayClose: true,
            opacity: 0.5,
            width: "80%", height: "80%",
            rel: "colorbox"
        });
    });


    function process_form() {
        if ($('#agree').prop('checked') == true) {
            var data = {
                'firstname': $('#firstname').val(),
                'email': $('#email').val(),
                'customer_group_id': $('#customer_group_id').val(),
                'telephone': $('#telephone').val(),
                'address_1': $('#address_1').val(),
                'city': $('#city').val(),
                'postcode': $('#postcode').val(),
                'zone_id': $('#zone').val(),
                'country_id': $('#country_id').val(),
                'password': $('#password').val(),
                'confirm': $('#confirm').val(),
                'agree': 1,
                'newsletter': $('#newsletter').val(),
                               'payment_method': $("input[name=payment_method]:checked").val()

            };
        } else {
            var data = {
                'firstname': $('#firstname').val(),
                'email': $('#email').val(),
                'customer_group_id': $('#customer_group_id').val(),
                'telephone': $('#telephone').val(),
                'address_1': $('#address_1').val(),
                'city': $('#city').val(),
                'postcode': $('#postcode').val(),
                'zone_id': $('#zone').val(),
                'country_id': $('#country_id').val(),
                'password': $('#password').val(),
                'confirm': $('#confirm').val(),
                'newsletter': $('#newsletter').val(),
                                'payment_method': $("input[name=payment_method]:checked").val()

            };

        }
        console.log(data)
        $('.error').html('')

        $.ajax({
            url: 'index.php?route=account/register',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(json) {
                if (json.status == 0) {
                    $.each(json.remark, function(key, value) {
                        $('#' + key).after('<span class="error">' + value + '</span>')
                        $('#' + key).focus()
                    });
                } else {
                    $('#register').hide('slow')
                    if(json.redirect == 1){
                        window.location="index.php?route=account/success";
                    }else{
                         $('#paymentSection').load('index.php?route=account/register/payment_' + json.payment_method)
                         $('#paymentSection').focus()
                    }

                }
            },
        });


    }



        function check_group(fee, desc,id) {
            if (fee != '') {
                $('#payment_method_section').show('slow')
            } else {
                $('#payment_method_section').hide('slow')
            }
            $('#customer_group_id').val(id)
            $('#group-desc').html(desc)
        }
  
    
//--></script> 
<?php echo $footer; ?>