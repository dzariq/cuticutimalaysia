<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->
<div style="margin-top:20px" class="row">
<h3 class="text-left" ><?php echo $title ?></h3>
<p ><?php echo $description ?></p>
<?php if($banners){ ?>
<style>
    .featureddesc{
        font-size:12px
    }
</style>
<div class="carousel slide" id="myCarousel">
    <div class="control-box">                            
        <a data-slide="prev" href="#myCarousel" class="carousel-control left">‹</a>
        <a data-slide="next" href="#myCarousel" class="carousel-control right">›</a>
    </div><!-- /.control-box --> 
    <br/>
    <div class="carousel-inner">
        <div class="item active">
            <ul class="thumbnails">
                <?php foreach($banners as $key=>$banner){ ?>
                    <?php if($key < 4){ ?>
                        <li class="col-sm-3">
                            <div >
                                <a href="<?php echo $banner['link'] ?>"><img style="margin:0 auto" class='img-responsive' src="<?php echo $banner['image'] ?>" alt=""></a>
                            </div>
                            <div class="caption">
                                <h4><?php echo $banner['title'] ?></h4>
                                <p class='featureddesc'><?php echo $banner['description'] ?></p>
                                <a class="btn btn-mini" href="<?php echo $banner['link'] ?>">&raquo; Read More</a>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div><!-- /Slide1 --> 
        <div class="item">
            <ul class="thumbnails">
                <?php foreach($banners as $key=>$banner){ ?>
                    <?php if($key > 3 && $key < 8){ ?>
                        <li class="col-sm-3">
                            <div >
                                <a href="<?php echo $banner['link'] ?>"><img style="margin:0 auto" class='img-responsive' src="<?php echo $banner['image'] ?>" alt=""></a>
                            </div>
                            <div class="caption">
                                <h4><?php echo $banner['title'] ?></h4>
                                <p class='featureddesc'><?php echo $banner['description'] ?></p>
                                <a class="btn btn-mini" href="<?php echo $banner['link'] ?>">&raquo; Read More</a>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div><!-- /Slide1 --> 
</div>
</div><!-- /#myCarousel -->
<?php } ?>
</div>
<!-- /Auto Generated Module By HostJars http://opencart.hostjars.com -->
<script>
    // Carousel Auto-Cycle
  $(document).ready(function() {
        $('#myCarousel').carousel({
        interval: 10000
        })
    
    $('#myCarousel').on('slid.bs.carousel', function() {
        //alert("slid");
        });
    
    
});


</script>