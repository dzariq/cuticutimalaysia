<?php echo $header; ?>
<div class="container-fluid">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="alert alert-error"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="alert alert-success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><i class="hicon-banner"></i><?php echo $heading_title; ?></h1>
			<div class="buttons"><a href="<?php echo $insert; ?>" class="btn btn-info"><i class="icon-white icon-plus"></i><span class="hidden-phone"> <?php echo $button_insert; ?></span></a><a onclick="$('form').submit();" class="btn btn-danger"><i class="icon-white icon-trash"></i><span class="hidden-phone"> <?php echo $button_delete; ?></span></a></div>
		</div>
		<div class="content">
			<form class="form-inline" action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="1" class="center"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked',this.checked);"></th>
							<td><?php if ($sort == 'name'){ ?>
								<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
								<?php } ?></td>
							<td class="hidden-phone"><?php if ($sort == 'status') { ?>
								<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
								<?php } ?></td>
							<th class="right"><span class="hidden-phone"><?php echo $column_action; ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php if ($banners) { ?>
						<?php foreach ($banners as $banner) { ?>
						<tr>
							<td class="center"><?php if ($banner['selected']) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $banner['banner_id']; ?>" checked="">
								<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $banner['banner_id']; ?>">
								<?php } ?></td>
							<td><?php echo $banner['name']; ?></td>
							<td class="hidden-phone"><?php echo $banner['status']; ?></td>
							<td class="right"><?php foreach ($banner['action'] as $action) { ?>
								<span class="bracket"><a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a></span>
								<?php } ?></td>
						</tr>
						<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="4"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</form>
			<div class="pagination"><?php echo str_replace('....','',$pagination); ?></div>
		</div>
	</div>
</div>
<?php echo $footer; ?>