<?php echo $header; ?>
<div class="<?php echo layout ?>">
   <!-- Page title -->
      <div class="page-title">
         <div class="<?php echo layout ?>">
            <h2><?php echo $heading_title ?></h2>
            <hr />
         </div>
      </div>
      <!-- Page title -->

      <!-- Page content -->

      <div class="shop-items">
         <div class="<?php echo layout ?>">

            <div class="row">

               <div class="col-md-12 ">

              
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>
        <div style='margin-bottom:50px' id="content" class="col-md-8">
            <?php echo $content_top; ?>
            <br/>
            <section>
                <?php echo $description; ?>
            </section>
        </div>
            <?php echo $content_bottom; ?>
    </div>
</div>
<?php echo $footer; ?>