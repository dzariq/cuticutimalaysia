<?php
// Text
$_['text_title']       = 'Online/Manual Bank Transfer';
$_['text_instruction'] = 'Bank Transfer Instructions';
$_['text_description'] = 'Please transfer the total amount to the following bank account.';
$_['text_payment']     = 'Your order will not ship until we receive payment.';
?>