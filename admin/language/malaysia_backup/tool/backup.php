<?php
// Heading
$_['heading_title']    = 'Backup / Restore';

// Text
$_['text_backup']      = 'Download Backup';
$_['text_success']     = 'Sukses: Anda telah berhasil meng-impor database anda!';

// Entry
$_['entry_restore']    = 'Restore Backup:';
$_['entry_backup']     = 'Backup:';

// Error
$_['error_permission'] = 'Perhatian: Anda tidak memiliki izin untuk memodifikasi backup!';
$_['error_empty']      = 'Perhatian: File yang anda unggah kosong!';
?>