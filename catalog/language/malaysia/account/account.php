<?php
// Heading 
$_['heading_title']      = 'Akaun Saya';

// Text
$_['text_account']       = 'Akaun';
$_['text_account_number']       = '<b>Nombor Keahlian Ahli: </b>';
$_['text_account_date']       = '<b>Tempoh Langganan: </b>';
$_['text_my_account']    = 'Akaun  Saya';
$_['text_my_orders']     = 'Pesanan Saya';
$_['text_renew']     = 'Perbaharui Akaun Saya';

$_['text_my_newsletter'] = 'Berita';
$_['text_edit']          = 'Ubah informasi Akaun Anda';
$_['text_password']      = 'Tukar password Anda';
$_['text_address']       = 'Ubah daftar buku alamat Anda';
$_['text_wishlist']      = 'Ubah wish list Anda';
$_['text_order']         = 'Tampilkan sejarah pesanan Anda';
$_['text_download']      = 'Download';
$_['text_reward']        = 'Point Reward Anda'; 
$_['text_return']        = 'Tampilkan permintaan pulangan Anda';
$_['text_transaction']   = 'Transaksi Anda'; 
$_['text_newsletter']    = 'Langganan Promosi / Henti Langgan';
?>