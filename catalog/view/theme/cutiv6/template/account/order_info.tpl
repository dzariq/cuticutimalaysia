<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li ><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li class="active"><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content booking-trips">
                               <h2 class="dash-content-title">Booking Details - Booking ID: #<?php echo $order_id; ?></h2>
                                <table class="table table-responsive table-striped">
                                    <thead>
                                        <tr>
                                            <td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="left" style="width: 50%;">
                                                <?php if ($invoice_no) { ?>
                                                <strong><?php echo $text_invoice_no; ?></strong>
                                                <?php echo $invoice_no; ?>
                                                <br />
                                                <?php } ?>
                                                <strong><?php echo $text_order_id; ?></strong> 
                                                #<?php echo $order_id; ?>
                                                <br />
                                                <strong><?php echo $text_date_added; ?></strong> 
                                                <?php echo $date_added; ?>
                                            </td>
                                            <td class="left" style="width: 50%;">
                                                <?php if ($payment_method) { ?>
                                                <strong><?php echo $text_payment_method; ?></strong> 
                                                <?php echo $payment_method; ?>
                                                <br />
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <td class="left"><?php echo $text_payment_address; ?></td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="left"><?php echo $payment_address; ?></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <td class="left"><?php echo $column_name; ?></td>
                                                <td class="right"><?php echo $column_quantity; ?></td>
                                                <td class="right"><?php echo $column_price; ?></td>
                                                <td class="right"><?php echo $column_total; ?></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($products as $product) { ?>
                                            <tr>
                                                <td class="left">
                                                    <?php echo $product['name']; ?>
                                                    <?php foreach ($product['option'] as $option) { ?>
                                                    <br />
                                                    &nbsp;
                                                    <small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                                    <?php } ?>
                                                </td>
                                                <td class="right"><?php echo $product['quantity']; ?></td>
                                                <td class="right"><?php echo $product['price']; ?></td>
                                                <td class="right"><?php echo $product['total']; ?></td>
                                            </tr>
                                            <?php } ?>
                                            <?php foreach ($vouchers as $voucher) { ?>
                                            <tr>
                                                <td class="left"><?php echo $voucher['description']; ?></td>
                                                <td class="left"></td>
                                                <td class="right">1</td>
                                                <td class="right"><?php echo $voucher['amount']; ?></td>
                                                <td class="right"><?php echo $voucher['amount']; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <?php foreach ($totals as $total) { ?>
                                            <tr>
                                                <td colspan="2"></td>
                                                <td class="right"><strong><?php echo $total['title']; ?>:</strong></td>
                                                <td class="right"><?php echo $total['text']; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tfoot>
                                    </table>
                                    <?php if ($comment) { ?>
                                    <table class="table table-responsive table-striped">
                                        <thead>
                                            <tr>
                                                <td class="left"><?php echo $text_comment; ?></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="left"><?php echo $comment; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                    <div class="pull-right">
                                        <a href="index.php?route=account/order" ><button class='btn btn-orange'>BACK</button></a>
                                    </div>

                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 