<script type="text/javascript">
  $(function() {
    $( ".date" ).datepicker({
        'dateFormat' : 'd/mm/yy'
    });
  });
  
            $('#button-cart').bind('click', function() {
    $.ajax({
    url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
            dataType: 'json',
            success: function(json) {
            $('.success, .warning, .attention, information, .error').remove();
                    if (json['error']) {
            if (json['error']['option']) {
            for (i in json['error']['option']) {
            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
            }
            }
            }

            if (json['success']) {
            window.location = "index.php?route=checkout/cart";
            }
            }
    });
    });
//--></script>

<script type="text/javascript"><!--
$('#review .pagination a').bind('click', function() {
    $('#review').fadeOut('slow');
            $('#review').load(this.href);
            $('#review').fadeIn('slow');
            return false;
    });
            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
            $('#button-review').bind('click', function() {
    $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function() {
            $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
            $('#button-review').attr('disabled', false);
                    $('.attention').remove();
            },
            success: function(data) {
            if (data['error']) {
            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
            }

            if (data['success']) {
            $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
            }
            }
    });
    });
            function radio_click(qty, name, id,image,popup){
                
               if(image != ''){
                    $('#image').attr('src',image)
                    $('#main-popup').attr('href',popup)
            }
                
            option_quantity = qty;
                    if ($('#quantity').val() > option_quantity){
                        var total_dec = $('#quantity').val() - option_quantity;
                        for(i=0;i<total_dec;i++){
                            decrement();
                        }
            }
            $(".radio-box").removeClass("active");
                    $("#" + id).addClass("active");
                    $("#option-value-" + id).prop('checked', true);
            }

    $('#resta1').click(function() {
    decrement();
    });
            $('#suma1').click(function() {
    increment();
    });
            function increment() {
            if (option_required == true && option_quantity == ''){
            alert('<?php echo $text_require_option ?>');
                    return 0;
            }
            if (option_quantity != ''){
            var max_qty = option_quantity
            } else{
            var max_qty = <?php echo $quantity ?> ;
            }

            $('#quantity').val(function(i, oldval) {
            var old = oldval;
                    var newval = ++oldval;
                    if (oldval > max_qty){
            return old;
            } else{
            return newval;
            }
            });
                    $('#number1').html($('#quantity').val())
            }

    function decrement() {
    $('#quantity').val(function(i, oldval) {
    var old = oldval;
            var newval = --oldval;
            if (oldval < <?php echo $minimum ?> ){
    return old;
    } else{
    return newval;
    }
    });
            $('#number1').html($('#quantity').val())

    }
//--></script>
<?php if (isset($pmLocations) && !empty($pmLocations)) { ?>
  				<div id="tab-maps" class="tab-content">
					<style>
						#pmFrom {
							display: block;
							width: 90%;
							margin: 0 auto;
						}

						#pmDirectionsBtn {
							display: block;
							margin: 10px auto;
						}

						#pmDirWarning {
							display: block;
							margin: 5px auto;
							width: 81%;
						}

						#pmDirDiv {
							display: inline;
							width: 60%;
							float: left;
						}

						#pmMapDiv {
							display: inline;
							width: 40%;
							float: left;
						}
					</style>

					<?php 
						echo "<div id='pmDirDiv'>";
						echo "<input id='pmTo' value='{$pmLocations[0]['address']}' type='hidden' />";
						echo "<input id='pmFrom' value='Chinatown, New York' type='text' />";
						echo "<input id='pmDirectionsBtn' type='button' value='$button_get_directions'>";
						echo "<div id='pmDirectionsList'></div>";
						echo "</div>";
						echo "<div id='pmMapDiv'>";
						echo "<div style='margin: 0 auto; width:{$pmLocations[0]['width']}px; height:{$pmLocations[0]['height']}px;' id=\"pmMap{$pmLocations[0]['id']}\"></div>";
						echo "</div>";
					?>

					<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

					<script type="text/javascript">
						$(function() {
							function createMap(e,lat,lng,zoom,width,height) {
						        var gmLatlng = new google.maps.LatLng(lat, lng);
						        var gmOpt = {
						            zoom: zoom,
						            center: gmLatlng,
						            mapTypeId: google.maps.MapTypeId.ROADMAP
						        }
						        var map = new google.maps.Map(document.getElementById(e), gmOpt);
						        var marker = new google.maps.Marker({
						            position: gmLatlng,
						            map: map,
						            draggable:true
						        });

								return map;
							}


							function generateRoute(map) {
								$("#pmDirWarning").remove();
								$("#pmDirectionsList").empty();
								var directionsService = new google.maps.DirectionsService();
								var directionsDisplay = new google.maps.DirectionsRenderer();

								var start = document.getElementById("pmFrom").value;
								var end = document.getElementById("pmTo").value;

								var request = {
									origin:start,
									destination:end,
									travelMode: google.maps.TravelMode.DRIVING
								};

								directionsService.route(request, function(result, status) {
									if (status == google.maps.DirectionsStatus.OK) {
										directionsDisplay.setMap(map);
										directionsDisplay.setPanel(document.getElementById("pmDirectionsList"));
									  	directionsDisplay.setDirections(result);
									} else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) { 
										$("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_route_not_found; ?></div>');
									} else if (status == google.maps.DirectionsStatus.NOT_FOUND) { 
										$("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_address_not_found; ?></div>');
									} else {
										$("#pmFrom").before('<div class="warning" id="pmDirWarning">' + status + '</div>');
									}
								});
							}

							$('#pmDirectionsBtn').bind('click', function(event, ui) {
							<?php 
									echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
							?>
								generateRoute(map);
							});

							$('#tab-maps-load').bind('click', function(event, ui) {
							<?php 
									echo "createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
							?>
							});

						});
                                              
					</script>
				</div>
				<?php } ?>