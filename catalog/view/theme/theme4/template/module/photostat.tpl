<div id="photostat-option" class="wow fadeInDown row-centered">
    <div id="upload-doc" class="delivery col-xs-4 col-centered">
        <i class="fa fa-upload bigicon" ></i>
        <h2>upload documents</h2>
    </div>
    <div id="delivery-doc" class="upload col-xs-4 col-centered">
        <i class="fa fa-truck bigicon" ></i>
        <h2>we pickup</h2>
    </div>
</div>

<div id="upload-modal" class="modal fade">
    <input id='book-pid' type='hidden' value='' />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Upload your Documents</h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-sm-6'>
                        <label>Upload your documents file</label>
                        <input class="form-control" type="file" name="attachment1" id='attachment1' />
                        <br/>
                        <label>name</label>
                        <input placeholder="name" id="name" type="text" class="form-control" /> 
                        <br/>
                        <label>Message</label>
                        <br/>
                        <textarea  style='resize: none;'class="form-control"   id="message" rows="2" ></textarea>
                        <br/>
                        <label>phone</label>
                        <br/>
                        <input  placeholder="phone" id="phone" type="phone" class="form-control" /> 
                        <br/>
                    </div>
                    <div class='col-sm-6'>
                        <label>email</label>
                        <br/>
                        <input  placeholder="email" id="emailnewsletter" type="email" class="form-control" /> 
                        <br/>
                        <label>address</label>
                        <br/>
                        <textarea style='resize: none;' class="form-control"  id="address" rows="7" value="" ></textarea>
                        <br/>
                        <label>Upload payment slip</label>
                        <input class="form-control" type="file" name="attachment2" id='attachment2' />
                        <br/>
                    </div>
                </div>



                <div class="col-xs-12">
                    <div tabindex="1" class="message-success">
                    </div>
                    <div tabindex="1" class="message-error">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
                <button style='margin-top:0px' onclick="submitUpload()" type="button" class="btn btn-primary">Submit</button>

            </div>
        </div>
    </div>
</div>

<div id="delivery-modal" class="modal fade">
    <input id='book-pid' type='hidden' value='' />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">We pickup your documents</h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-sm-6'>
                        <label>name</label>
                        <br/>
                        <input placeholder="name" id="name-delivery" type="text" class="form-control" /> 
                        <br/>
                        <label>message</label>
                        <br/>
                        <textarea style='resize: none;' class="form-control"  placeholder="message" id="message-delivery" rows="2" ></textarea>
                        <br/>
                        <label>email</label>
                        <br/>
                        <input  placeholder="email" id="emailnewsletter-delivery" type="email" class="form-control" /> 
                    </div>
                    <div class='col-sm-6'>
                        <label>phone</label>
                        <br/>
                        <input  placeholder="phone" id="phone-delivery" type="phone" class="form-control" /> 
                        <br/>
                        <label>address</label>
                        <br/>
                        <textarea style='resize: none;' class="form-control" placeholder="address" id="address-delivery" rows="2" ></textarea>
                        <br/>
                        <label>Upload payment slip</label>
                        <input class="form-control" type="file" name="attachment3" id='attachment3' />
                    </div>
                </div>
                <div class="col-xs-12">
                    <div tabindex="1" class="message-success">
                    </div>
                    <div tabindex="1" class="message-error">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
                <button style='margin-top:0px' onclick="submitDelivery()" type="button" class="btn btn-primary">Submit</button>

            </div>
        </div>
    </div>
</div>

<script>
    $("#upload-doc").click(function () {
        $("#upload-modal").modal('show');
    });
    $("#delivery-doc").click(function () {
        $("#delivery-modal").modal('show');
    });

    function validateEmail($email) {
        if ($('#attachment1').val().length == 0) {
            alert('Please upload your document(s)');
            return false;
        }
        if ($('#attachment2').val().length == 0) {
            alert('Please upload your bank payment slip');
            return false;
        }

        if ($('#phone').val().length < 9) {
            alert('Please insert your phone number');
            return false;
        }

        if ($('#address').val().trim().length < 6) {
            alert('Please insert your address');
            return false;
        }

        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            alert('Email address not valid');
            return false;
        } else {
            return true;
        }


    }
    function validateEmail2($email) {
        if ($('#phone-delivery').val().length < 9) {
            alert('Please insert your phone number');
            return false;
        }

        if ($('#attachment3').val().length == 0) {
            alert('Please upload your bank payment slip');
            return false;
        }

        if ($('#address-delivery').val().trim().length < 6) {
            alert('Please insert your address');
            return false;
        }


        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($email)) {
            alert('Email address not valid');
            return false;
        } else {
            return true;
        }


    }

    function submitUpload() {


        var status = true;

        status = validateEmail($('#emailnewsletter').val());

        $('.message-error').hide()
        $('.message-success').hide()
        $('.message-error').html('')
        $('.message-success').html('');

        if (status) {
            var data = new FormData();

            data.append('name', $('#name').val()),
                    data.append('details_phone_text', $('#phone').val()),
                    data.append('message', $('#message').val()),
                    data.append('details_address_text', $('#address').val()),
                    data.append('email', $('#emailnewsletter').val()),
                    data.append('category', 'Photostat-Upload');
            data.append('attachment1', $('input[name=attachment1]')[0].files[0]);
            data.append('attachment2', $('input[name=attachment2]')[0].files[0]);
            data.append('json', 1);



            $.ajax({
                url: 'index.php?route=information/contact',
                type: "POST",
                processData: false,
                headers: {
                    accept: "image/png",
                },
                contentType: false,
                dataType: 'json',
                data: data,
                success: function (data) {
                    var html = '';
                    html += data.remark;

                    if (data.status == 1) {

                        $('.message-success').show()
                        $('.message-success').html(html);
                        $("#upload-modal").modal('hide');
                        alert('Your submission is successful')
                    } else {
                        $('.message-error').show()
                        $('.message-error').html(html)
                    }
                    $('#remark').focus()
                    console.log(data);
                }
            });

        }

    }

    function submitDelivery() {


        var status = true;

        status = validateEmail2($('#emailnewsletter').val());

        $('.message-error').hide()
        $('.message-success').hide()
        $('.message-error').html('')
        $('.message-success').html('');

        if (status) {
            var data = new FormData();


            data.append('name', $('#name-delivery').val()),
                    data.append('details_phone_text', $('#phone-delivery').val()),
                    data.append('message', $('#message-delivery').val()),
                    data.append('details_address_text', $('#address-delivery').val()),
                    data.append('email', $('#emailnewsletter-delivery').val()),
                    data.append('category', 'Photostat-Delivery');
            data.append('attachment1', $('input[name=attachment3]')[0].files[0]);
            data.append('json', 1);

            $.ajax({
                url: 'index.php?route=information/contact',
                type: "POST",
                processData: false,
                headers: {
                    accept: "image/png",
                },
                contentType: false,
                dataType: 'json',
                data: data,
                success: function (data) {
                    var html = '';
                    html += data.remark;

                    if (data.status == 1) {

                        $('.message-success').show()
                        $('.message-success').html(html);
                        $("#delivery-modal").modal('hide');
                                                alert('Your submission is successful')

                    } else {
                        $('.message-error').show()
                        $('.message-error').html(html)
                    }
                    $('#remark').focus()
                    console.log(data);
                }
            });

        }

    }


</script>