<div id='searchengine' class="col-sm-12"   >
    <div class="inner-search col-sm-12"   >
        <div style="padding-left:0px;margin-bottom:26px"  class="col-md-12">
            <h4 >Accomodation</h4>
            <select onchange="javascript:$('#name-search').val('')" class="form-control" style='width:100%' id="category-search" name='category-search' >
                <?php foreach($categories as $category){ ?>
                <?php if($this->request->get['filter_category_id'] == $category[category_id]){ ?>
                <option selected="selected" value='<?php echo $category[category_id] ?>'><?php echo $category[name] ?></option>
                <?php }else{ ?>
                <option value='<?php echo $category[category_id] ?>'><?php echo $category[name] ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
        <div class='clearfix'></div>
        <h4 >Hotel Search</h4>
        <div style="padding-left:0px;margin-bottom:26px" class="col-md-12 ">
            <input placeholder='Keyword' type="text" id="name-search" class="form-control" name="name" value="" placeholder="Search" />
            <input type="hidden" id="redirect-hidden" value=""  />
        </div>
        <div class='clearfix'></div>
        <div style="padding-left:0px;margin-bottom:26px"  class="col-md-6">
            <h4 >Check in</h4>
            <input type='text' value='<?php echo $this->request->get[checkin] ?>' id='maindate' class='date form-control' name='date' />
        </div>
        <div style="padding-left:0px;margin-bottom:26px"  class="col-md-6">
            <h4 >Nights(s)</h4>
            <select class="form-control" style='width:100%' id="nights" name='nights' >
                <?php for($i=1;$i<6;$i++){ ?>
                <?php if($this->request->get['nights'] == $i){ ?>
                <option selected='selected' value='<?php echo $i ?>'><?php echo $i ?></option>
                <?php }else{ ?>
                <option  value='<?php echo $i ?>'><?php echo $i ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>

        <button id="searchengine_submit" style="margin-top:20px" class="btn btn-primary"  >Search</button>
    </div>
</div>
<style type="text/css">
    .ui-menu-item{
        text-align:left;
        z-index: 160;
    }
    .ui-autocomplete{
        text-align:left;
        z-index: 160;
    }
</style>
<script type="text/javascript"><!--
//########################################################################
// Module: Search Autocomplete
//########################################################################
    $(document).ready(function () {
        $('#name-search').autocomplete({
            delay: 500,
            source: function (request, response) {
                $.ajax({
                    url: "<?php echo $search_json;?>",
                    dataType: "json",
                    data: {
                        keyword: request.term,
                        category_id: $('#category-search').val()

                    },
                    success: function (data) {
                        response($.map(data.result, function (item) {
                            return {
                                label: item.name,
                                type: item.type,
                                value: item.href
                            }
                        }));
                    }
                });
            },
            minLength: 3,
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                if (ui.item.value == "") {
                    return false;
                } else {
                    if(ui.item.type == 'more'){
                     location.href=ui.item.value;
                    }
                    $('#name-search').val(ui.item.label)
                    $('#redirect-hidden').val(ui.item.value)
                    return false;
                }
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
                $('.ui-autocomplete').css("z-index", "99");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
        };
    })
//########################################################################
// Module: Search Autocomplete
//########################################################################
//--></script>