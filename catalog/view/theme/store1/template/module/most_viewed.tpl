<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->

<?php if($products){ ?>
<style>
    .featureddesc{
        font-size:12px
    }
    .mostviewed_bottom{
        padding: 0px;
    }
    .carousel .name{
        margin-top:7px;float:right;color:#FF2626;font-size:16px;
    }
    .carousel .viewed{
        margin-top:7px;float:left;color:#27DE55;font-size:16px;
    }
    .carousel ul{
        list-style:none
    }
    .mostviewed-title  {
        z-index: 99999;
        position: absolute;
        font-size: 22px;
        text-align: center;
        width: 100%;
        color: white;
        font-family: Ranga;
        margin-top: -55px;
        padding: 10px;
        background: rgba(0,0,0,0.4);
    }
</style>
<div class="carousel slide" id="myCarousel" >
    <div class="control-box">                            
        <a data-slide="prev" href="#myCarousel" class="carousel-control left">‹</a>
        <a data-slide="next" href="#myCarousel" class="carousel-control right">›</a>
    </div><!-- /.control-box --> 
    <div class="carousel-inner">
        <?php $i=0; foreach($products as $key=>$banner){ ?>
        <div class="item <?php if($i==0){ ?> active <?php } ?>">
            <ul style='padding:0px' class="thumbnails">
                <li >
                    <div style="background:#2f2f2f" >
                        <a href="<?php echo $banner['href'] ?>"><img style="margin:0 auto" class='img-responsive' src="<?php echo $banner['thumb'] ?>" alt=""></a>
                    </div>
                    <div class="caption">
                        <h3 class='mostviewed-title'><?php echo $banner['name'] ?></h3>
                    </div>
                    <div style="display:none" class='mostviewed_bottom'>
                        <a  class="name btn btn-mini" href="<?php echo $banner['href'] ?>">Book Now</a>
                        <a  class="viewed btn btn-mini" ><?php echo $banner['viewed'] ?> Viewed</a>
                    </div>
                    <div class='clearfix'></div>
                </li>
            </ul>
        </div><!-- /Slide1 --> 
        <?php $i++; } ?>
    </div>
</div><!-- /#myCarousel -->
<?php } ?>
<h2 style="margin-top:16px;text-align:center;color:#eb750e;display:block;font-weight:bold;font-size:21px">
    Top 5 Popular Destinations
</h2>
<!-- /Auto Generated Module By HostJars http://opencart.hostjars.com -->
<script>
    // Carousel Auto-Cycle
    $(document).ready(function () {
        $('#myCarousel').carousel({
            interval: 10000
        })

        $('#myCarousel').on('slid.bs.carousel', function () {
            //alert("slid");
        });


    });


</script>