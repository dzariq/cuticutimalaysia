<!-- Search form and last minute -->
<section class="search_lm " style="margin-top:-148px">

    
    <!-- Ticket search form -->
    <section class="search sidebar"  >
       <!-- <h2 style="  color:white;text-shadow:none;font-size:21px;margin-bottom:0px;text-align:center">Find your dream vacation here</h2>
-->
        <h2 style="   font-size:14px; text-shadow: none;color:white">
            <?php $i=0; foreach($categories as $category){ ?>
            <span data-form="find_<?php echo $category['name'] ?>" <?php if($i==0){ ?> class="selected" <?php } ?> ><?php echo $category['name'] ?></span>
            <?php $i++; } ?>
        </h2>
        <div id="searchajax-criteria">
            <?php $i=0; foreach($categories as $category){ ?>

            <!-- Find a hotel form -->
            <form id='searchform<?php echo $i ?>' data-form="find_<?php echo $category['name'] ?>" class="black" <?php if($i != 0){ ?> style="display:none" <?php } ?> >
                  <div class="col-xs-4 ">
                    <label>Keyword</label>
                    <input type="text" style='background:white;color:black !Important' name="name" value="" placeholder="Search" />
                </div>
                <div class="col-xs-4">
                    <label>Location</label>
                    <select style='background:white;color:black' name='category' >
                        <option value=''>Choose</option>
                        <?php foreach($childCategories[$category['category_id']] as $child){ ?>
                        <option value='<?php echo $child[category_id] ?>'><?php echo $child['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <label>Accomodation</label>
                    <select style='background:white;color:black' name='manufacturer' >
                        <option value=''>Choose</option>
                        <?php foreach($manucat[$category['category_id']] as $manufacturer){ ?>
                        <option value='<?php echo $manufacturer[manufacturer_id] ?>'><?php echo $manufacturer['name'] ?></option>
                        <?php } ?>
                    </select>	
                </div>
                <div style="margin-bottom:20px" class="col-xs-4 col-xs-offset-4">
                    <label>&nbsp;</label>
                    <input style="background:#eb750e;font-size:16px;line-height:0" formnumber='<?php echo $i ?>' type="submit" class='searchajax' value="Find your dream vacation here">
                </div>
            </form>
            <?php $i++; } ?>
        </div>

    </section>

    <section class="last_minute">
        <table style="width:100% !important" id="ajaxsearch-result">
        </table>
    </section>

</section>