<?php
/*

This file is auto generated from a template file

*/
?>
<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        {HJ_FIELDS}
        <?php if (FALSE) { ?>
        <!-- {HJ_PRODUCT_SELECTOR}<tr>
          <tr>
          <td><?php //echo $entry_product; ?></td>
          <td><input type="text" name="auto-product" value="" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><div class="scrollbox" id="auto-product">
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
              <div id="auto-product<?php echo $product['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product['name']; ?> <img src="view/image/delete.png" />
                <input type="hidden" value="<?php echo $product['product_id']; ?>" />
              </div>
              <?php } ?>
            </div>
            <input type="hidden" name="auto_product" value="<?php echo $auto_product; ?>" /></td>
        </tr>
        {/HJ_PRODUCT_SELECTOR} -->
        <?php } ?>
        {HJ_CATEGORY_SELECTOR}
        <tr>
              <td><?php echo $entry_category; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'odd'; ?>
                  <?php foreach ($categories as $category) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <input type="checkbox" name="category[]" value="<?php echo $category['category_id']; ?>" />
                    <?php echo $category['name']; ?> </div>
                  <?php } ?>
                </div></td>
            </tr>
        {/HJ_CATEGORY_SELECTOR}
        {HJ_MANUFACTURER_SELECTOR}
        <tr>
          <td><?php echo $entry_manufacturer; ?></td>
          <td><div class="scrollbox">
            <?php $class = 'odd'; ?>
            <?php foreach ($manufacturers as $manufacturer) { ?>
            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
            <div class="<?php echo $class; ?>">
              <input type="checkbox" name="manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
              <?php echo $manufacturer['name']; ?>
            </div>
            <?php } ?>
          </div></td>
        </tr>
        {/HJ_MANUFACTURER_SELECTOR}
      </table>
      <table id="module" class="list">
        <thead>
          <tr>
            {HJ_REMOVE_LIMIT}<td class="left"><?php echo $entry_limit; ?></td>{/HJ_REMOVE_LIMIT}
            {HJ_REMOVE_BANNER}<td class="left">Banner</td>{/HJ_REMOVE_BANNER}
            {HJ_REMOVE_TITLE}<td class="left">Title</td>{/HJ_REMOVE_TITLE}
            {HJ_REMOVE_DESCRIPTION}<td class="left">Description</td>{/HJ_REMOVE_DESCRIPTION}
            {HJ_REMOVE_LINK}<td class="left">Link</td>{/HJ_REMOVE_LINK}
            {HJ_REMOVE_LINKNAME}<td class="left">Link Name (Button)</td>{/HJ_REMOVE_LINKNAME}
            {HJ_REMOVE_IMAGE}<td class="left"><?php echo $entry_image; ?></td>{/HJ_REMOVE_IMAGE}
            {HJ_REMOVE_LAYOUT}<td class="left"><?php echo $entry_layout; ?></td>{/HJ_REMOVE_LAYOUT}
            {HJ_REMOVE_POSITION}<td class="left"><?php echo $entry_position; ?></td>{/HJ_REMOVE_POSITION}
            {HJ_REMOVE_STATUS}<td class="left"><?php echo $entry_status; ?></td>{/HJ_REMOVE_STATUS}
            {HJ_REMOVE_SORT_ORDER}<td class="right"><?php echo $entry_sort_order; ?></td>{/HJ_REMOVE_SORT_ORDER}
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
            {HJ_REMOVE_LIMIT}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" /></td>{/HJ_REMOVE_LIMIT}
            {HJ_REMOVE_BANNER}<td><select name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][banner_id]" class="span2">
								<?php foreach ($banners as $banner) { ?>
								<?php if ($banner['banner_id'] == $module['banner_id']) { ?>
								<option value="<?php echo $banner['banner_id']; ?>" selected=""><?php echo $banner['name']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $banner['banner_id']; ?>"><?php echo $banner['name']; ?></option>
								<?php } ?>
								<?php } ?>
							</select></td>{/HJ_REMOVE_BANNER}
            {HJ_REMOVE_TITLE}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][title]" value="<?php echo $module['title']; ?>" size="20" /></td>{/HJ_REMOVE_TITLE}
            {HJ_REMOVE_DESCRIPTION}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][description]" value="<?php echo $module['description']; ?>" size="50" /></td>{/HJ_REMOVE_DESCRIPTION}
            {HJ_REMOVE_LINK}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][link]" value="<?php echo $module['link']; ?>" size="10" /></td>{/HJ_REMOVE_LINK}
            {HJ_REMOVE_LINKNAME}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][linkname]" value="<?php echo $module['linkname']; ?>" size="20" /></td>{/HJ_REMOVE_LINKNAME}
            {HJ_REMOVE_IMAGE}<td class="left"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
              <input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>{/HJ_REMOVE_IMAGE}
            {HJ_REMOVE_LAYOUT}<td class="left"><select name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>{/HJ_REMOVE_LAYOUT}
            {HJ_REMOVE_POSITION}<td class="left"><select name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>{/HJ_REMOVE_POSITION}
            {HJ_REMOVE_STATUS}<td class="left"><select name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>{/HJ_REMOVE_STATUS}
            {HJ_REMOVE_SORT_ORDER}<td class="right"><input type="text" name="{HJ_MOD_NAME}_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>{HJ_REMOVE_SORT_ORDER}
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="6"></td>
            <td class="left"><a onclick="addModule();" class="button"><span><?php echo $button_add_module; ?></span></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
<?php if (FALSE) { ?>
{HJ_PRODUCT_SELECTOR}
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
		
	}, 
	select: function(event, ui) {
		$('#auto-product' + ui.item.value).remove();
		
		$('#auto-product').append('<div id="auto-product' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#auto-product div:odd').attr('class', 'odd');
		$('#auto-product div:even').attr('class', 'even');
		
		data = $.map($('#auto-product input'), function(element){
			return $(element).attr('value');
		});
						
		$('input[name=\'auto_product\']').attr('value', data.join());
					
		return false;
	}
});

$('#auto-product div img').live('click', function() {
	$(this).parent().remove();
	
	$('#auto-product div:odd').attr('class', 'odd');
	$('#auto-product div:even').attr('class', 'even');

	data = $.map($('#auto-product input'), function(element){
		return $(element).attr('value');
	});
					
	$('input[name=\'auto_product\']').attr('value', data.join());	
});
//--></script> 
{/HJ_PRODUCT_SELECTOR}
<?php } ?>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	{HJ_REMOVE_LIMIT}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][limit]" value="5" size="1" /></td>';{/HJ_REMOVE_LIMIT}
	{HJ_REMOVE_BANNER}html += '<td><select name="{HJ_MOD_NAME}_module[' + module_row + '][banner_id]" class="span2">';
	<?php foreach ($banners as $banner) { ?>
	html += '<option value="<?php echo $banner['banner_id']; ?>"><?php echo addslashes($banner['name']); ?></option>';
	<?php } ?>
	html += '</select></td>';{/HJ_REMOVE_BANNER}
	{HJ_REMOVE_TITLE}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][title]" value="" size="20" /></td>';{/HJ_REMOVE_TITLE}
	{HJ_REMOVE_DESCRIPTION}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][description]" value="" size="50" /></td>';{/HJ_REMOVE_DESCRIPTION}
	{HJ_REMOVE_LINK}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][link]" value="" size="10" /></td>';{/HJ_REMOVE_LINK}
	{HJ_REMOVE_LINKNAME}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][linkname]" value="" size="20" /></td>';{/HJ_REMOVE_LINKNAME}
	{HJ_REMOVE_IMAGE}html += '    <td class="left"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][image_height]" value="80" size="3" /></td>';{/HJ_REMOVE_IMAGE}		
	{HJ_REMOVE_LAYOUT}html += '    <td class="left"><select name="{HJ_MOD_NAME}_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';{/HJ_REMOVE_LAYOUT}
	{HJ_REMOVE_POSITION}html += '    <td class="left"><select name="{HJ_MOD_NAME}_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';{/HJ_REMOVE_POSITION}
	{HJ_REMOVE_STATUS}html += '    <td class="left"><select name="{HJ_MOD_NAME}_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';{/HJ_REMOVE_STATUS}
	{HJ_REMOVE_SORT_ORDER}html += '    <td class="right"><input type="text" name="{HJ_MOD_NAME}_module[' + module_row + '][sort_order]" value="" size="3" /></td>';{/HJ_REMOVE_SORT_ORDER}
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<?php echo $footer; ?>