<?php if (isset($_SERVER['HTTP_USER_AGENT']) && !strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6')) echo '<?xml version="1.0" encoding="UTF-8"?>'. "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php echo $google_analytics; ?>
<style type="text/css">
    html {
		overflow: -moz-scrollbars-vertical;
		margin: 0;
		padding: 0;
	}
	body {
		background-color: #ffffff;
		color: #000000;
		/* font-family: Arial, Helvetica, sans-serif; */
		font-family: "Times New Roman", Times, serif;
		margin: 0px;
		padding: 0px;
	}
	body, td, th, a {
		font-size: 12px;
	}
    .fleft {
        float: left;
    }
    .fright {
        float: right;
    }
    .clear {
        clear: both;
    }
    .tright {
        text-align: right;
    }
    .tcenter {
        text-align: center;
    }
    .pricelist {
        overflow: auto;
    }
    .pricelist table {
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #333;
        margin-bottom: 10px;
    }
    .pricelist table th, .pricelist table td {
        border: 1px solid #333;
        border-width: 1px;
    }
    .pricelist table th {
        padding: 7px;
        background-color: #ccc;
    }
    .pricelist table td {
        padding: 5px;
    }
    .pricelist table th.image {
        width: 100px;
        text-align: center;
    }
    .pricelist table th.qty {
        width: 60px;
    }
    .pricelist table td.nowrap {
        white-space: nowrap;
    }
    .pricelist table td .price {
        font-weight: bold;
    }
    .pricelist table td .price-old {
        text-decoration: line-through;
    }
    .pricelist table td .price-new {
        color: #f00;
        font-weight: bold;
    }
    .pricelist table td .discount {
        margin-top: 10px;
        color: #666;
    }
    .pricelist table td .nostock {
        color: #c00;
    }
</style>
</head>
<body>
<div id="container">
	<div id="header">
		<div class="fleft">
		  <?php if ($logo) { ?>
		  <div id="logo"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></div>
		  <?php } ?>
		  <p><?php echo $store_title; ?></p>
		  <p><?php echo $store_url; ?></p>
		</div>
		<div class="fright">
		  <p><?php echo $store_address; ?></p>
		  <p><?php echo $store_email; ?></p>
		  <p><?php echo $store_telephone; ?></p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="pricelist">
      <table>
        <thead>
          <tr>
            <th><?php echo $column_number; ?></th>
            <th class="image"><?php echo $column_image; ?></th>
            <th><?php echo $column_name; ?></th>
            <th><?php echo $column_rating; ?></th>
            <th><?php echo $column_price; ?></th>
            <th><?php echo $column_stock; ?></th>
            <th class="qty"><?php echo $column_qty; ?></th>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($products)) { ?>
          <?php $count = 0; ?>
          <?php foreach($products as $product_id => $product) { ?>
            <tr>
            	<td><?php echo ++$count; ?>.</td>
                <td class="tcenter image"><img src="<?php echo $product['image']; ?>" alt="no_image" title="<?php echo $product['name']; ?>" /></td>
                <td><strong><?php echo $product['name']; ?></strong><br /><?php echo $product['description']; ?></td>
             
                <td><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['rating']; ?> *" /></td>
                <td class="tright nowrap"><?php if($product['price']) { if(!$product['special']) { ?><span class="price"><?php echo $product['price']; ?></span><?php } else { ?><span class="price-old"><?php echo $product['price']; ?></span><br /><span class="price-new"><?php echo $product['special']; ?></span><?php } ?>
                    <?php if ($product['discounts'] && !$product['special']) { ?>
                    <br />
                    <div class="discount">
                      <?php foreach ($product['discounts'] as $discount) { ?>
                      <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br /><br />
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php } else { ?>-<?php } ?>
                </td>
                <td class="tright"><span class="<?php if(!$product['quantity']) { ?>nostock<?php } ?>"><?php echo $product['quantity']; ?></span></td>
                <td class="tright">&nbsp;</td>
            </tr>
          <?php } ?>
          <?php } else { ?>
            <tr><td colspan="8" class="tcenter"><?php echo $text_empty; ?></td></tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
</div>
<script type="text/javascript">
window.print();
</script>
</body>
</html>