<style>
    .thumbnail{
        background: transparent;
    }

    .editor{
        font-family:"Droid Serif" !Important;
        font-size:18px !Important  ;
        color:#222222 !Important
    }


    .caption p{
        text-align: justify
    }
</style>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 editor">
                <?php echo $message; ?>
            </div>
        </div>
    </div>
</section>
<script>
    $(".editor img").addClass("img-responsive");
    $(".editor img").css("margin", "0 auto");
</script>
