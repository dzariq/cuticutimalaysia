<?php echo $header; ?>
<div class="container-fluid">  
    <div class="breadcrumb">   
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>       
        <?php echo $breadcrumb['separator']; ?>
        <a href="<?php echo $breadcrumb['href']; ?>">
            <?php echo $breadcrumb['text']; ?>
        </a>       
        <?php } ?>  
    </div>  
    <?php if ($error_warning) { ?>  
    <div class="alert alert-error">
        <?php echo $error_warning; ?></div> 
    <?php } ?>    <div class="box">      
        <div class="heading">         
            <h1><i class="hicon-banner"
                   ></i><?php echo $heading_title; ?></h1>           
            <div class="buttons">
                <a class="btn btn-primary" onclick="$('#form').submit();">
                    <?php echo $button_save; ?>
                </a>
                <a class="btn" href="<?php echo $cancel; ?>">
                    <?php echo $button_cancel; ?>
                </a>
            </div>  
        </div>     
        <div class="content">    
            <form class="form-horizontal" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">                <div class="control-group">              
                    <label class="control-label"><span class="required">*</span> 
                        <?php echo $entry_name; ?>
                    </label>                 
                    <div class="controls">   
                        <input type="text" name="name" value="<?php echo $name; ?>" class="input-xxlarge">       
                        <?php if ($error_name) { ?>                  
                        <div class="help-block error">
                            <?php echo $error_name; ?>
                        </div>     
                        <?php } ?>              
                    </div>                  
                    <label style='margin-top:10px' class="control-label">
                        <?php echo $entry_status; ?>
                    </label>              
                    <div class="controls">   
                        <select style='margin-top:10px' name="status" class="input-medium">  
                            <?php if ($status) { ?>      
                            <option value="1" selected=""><?php echo $text_enabled; ?></option>  
                            <option value="0"><?php echo $text_disabled; ?></option>    
                            <?php } else { ?>                    
                            <option value="1"><?php echo $text_enabled; ?></option>      
                            <option value="0" selected=""><?php echo $text_disabled; ?></option>   
                            <?php } ?>                        
                        </select>               
                    </div>  
                </div>              
                <table id="images" class="table table-bordered table-striped">              
                    <thead>                     
                        <tr>                       
                            <th><?php echo $entry_title; ?></th>    
                            <td class="left">Description</td>     
                            <td class="left">Link Name (Button)</td>    
                            <th>Category</th>                       
                            <th><?php echo $entry_link; ?></th>     
                            <th><?php echo $entry_image; ?></th>   
                            <!--<th>Image Large</th>-->            
                            <th></th>                     
                        </tr>       
                    </thead>        
                    <tbody>               
                        <?php $image_row = 0; ?>    
                        <?php foreach ($banner_images as $banner_image) { ?>      
                        <tr id="image-row<?php echo $image_row; ?>">        
                            <td><?php foreach ($languages as $language) { ?>  
                                <p><input style="width:80px" type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['title'] :''; ?>" class="input-large">       
                                    <i class="lang-<?php echo str_replace('.png','', $language['image']); ?>" title="<?php echo $language['name']; ?>"></i>                                    <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>               
                                <div class="text-error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></div>                                <?php } ?></p>                                <?php } ?></td>                         
                            <td class="left"><?php foreach ($languages as $language) {  ?>           
                                <textarea style="height:120px" type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][description]"  >                                  
  <?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['description'] : ''; ?>                                </textarea>       
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />                                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>                      
                                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?>
                                </span>       
                                <?php } ?>                             
                                <?php } ?>
                            </td>      
                            <td class="left"><?php foreach ($languages as $language) { ?>               
                                <input style="width:100px" type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][link_name]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['link_name'] : ''; ?>" />                               
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />                                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>                           
                                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></span>           
                                <?php } ?>                        
                                <?php } ?>
                            </td>       
                            <td class="left">
                                <?php foreach ($languages as $language) { ?>        
                                <input style="width:100px" type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][tagging]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['tagging'] : ''; ?>" />                 
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />

                                <br />                    
                                <?php if (isset($error_banner_image[$image_row][$language['language_id']])) { ?>       
                                <span class="error"><?php echo $error_banner_image[$image_row][$language['language_id']]; ?></span>    
                                <?php } ?>                            
                                <?php } ?>
                            </td>               
                            <td><input style="width:100px" type="text" name="banner_image[<?php echo $image_row; ?>][link]" value="<?php echo $banner_image['link']; ?>" class="input-large">
                            </td>                          
                            <td>
                                <div class="media">                       
                                    <a class="pull-left" onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');">
                                        <img class="thumbnail" src="<?php echo $banner_image['thumb']; ?>" width="100" height="100" alt="" id="thumb<?php echo $image_row; ?>"></a>                                    <input type="hidden" name="banner_image[<?php echo $image_row; ?>][image]" value="<?php echo $banner_image['image']; ?>" id="image<?php echo $image_row; ?>">      
                                    <div class="media-body hidden-phone">                             
                                        <a class="btn" onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');">
                                            <?php echo $text_browse; ?></a>&nbsp;                             


                                        <a class="btn" onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>');
                                                $('#image<?php echo $image_row; ?>').val('');"><?php echo $text_clear; ?></a>         
                                    </div>                              
                                </div>
                            </td>      
                            <td style='display:none'>
                                <div class="media">  
                                    <a class="pull-left" onclick="image_upload('imagelarge<?php echo $image_row; ?>', 'thumblarge<?php echo $image_row; ?>');">
                                        <img class="thumbnail" src="<?php echo $banner_image['thumblarge']; ?>" width="100" height="100" alt="" id="thumblarge<?php echo $image_row; ?>"></a>                                   
                                    <input type="hidden" name="banner_image[<?php echo $image_row; ?>][imagelarge]" value="<?php echo $banner_image['imagelarge']; ?>" id="imagelarge<?php echo $image_row; ?>">                               
                                    <div class="media-body hidden-phone">                       
                                        <a class="btn" onclick="image_upload('imagelarge<?php echo $image_row; ?>', 'thumblarge<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;                                        <a class="btn" onclick="$('#thumblarge<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>');
                                                $('#imagelarge<?php echo $image_row; ?>').val('');">
                                            <?php echo $text_clear; ?>
                                        </a>                              
                                    </div>                             
                                </div>
                            </td>    
                            <td>
                                <a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="btn btn-danger"><i class="icon-white icon-trash"></i><span class="hidden-phone"> <?php echo $button_remove; ?></span></a></td>    
                        </tr>                      
                        <?php $image_row++; ?>              
                        <?php } ?>                 
                    </tbody>                
                    <tfoot>               
                        <tr>          
                            <td colspan="6">

                            </td>           
                            <td>
                                <a onclick="addImage();" class="btn btn-info">
                                    <i class="icon-white icon-plus"></i><span class="hidden-phone"> 
                                        <?php echo $button_add_banner; ?>
                                    </span>
                                </a>
                            </td>   
                        </tr>             
                    </tfoot>              
                </table>           
            </form>      
        </div>   
    </div>
</div>

<script>
    var image_row = <?php echo $image_row; ?> ; function addImage() {
        html = '<tr id="image-row' + image_row + '">';
        html += '<td>';
                <?php foreach ($languages as $language) { ?> html += '<p><input style="width:80px" type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="" class="input-large"> <i class="lang-<?php echo str_replace('.png','', $language['image']); ?>" title="<?php echo $language['name']; ?>"></i></p>'; <?php } ?> html += '</td>'; html += '<td>';
                <?php foreach ($languages as $language) { ?> html += '<p><textarea style="height:120px"  type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][description]"  ></textarea> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></p>'; <?php } ?> html += '</td>'; html += '<td >';
                <?php foreach ($languages as $language) { ?> html += '<p><input  style="width:100px" type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][link_name]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></p>'; <?php } ?> html += '</td>'; html += '<td >';
                <?php foreach ($languages as $language) { ?> html += '<p><input  style="width:100px" type="text" name="banner_image[' + image_row + '][banner_image_description][<?php echo $language['language_id']; ?>][tagging]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></p>'; <?php } ?> html += '</td>'; html += '<td><input style="width:100px" type="text" name="banner_image[' + image_row + '][link]" value="" class="input-large"></td>';
        html += '<td><div class="media"><a class="pull-left" onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><img class="thumbnail" src="<?php echo $no_image; ?>" width="100" height="100" alt="" id="thumb' + image_row + '"></a>';
        html += '<input type="hidden" name="banner_image[' + image_row + '][image]" value="" id="image' + image_row + '">';
        html += '<div class="media-body hidden-phone">';
        html += '<a class="btn" onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;';
        html += '<a class="btn" onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
        html += '</div></div></td>';
        html += '<td style="display:none"><div class="media"><a class="pull-left" onclick="image_upload(\'imagelarge' + image_row + '\', \'thumblarge' + image_row + '\');"><img class="thumbnail" src="<?php echo $no_image; ?>" width="100" height="100" alt="" id="thumblarge' + image_row + '"></a>';
        html += '<input type="hidden" name="banner_image[' + image_row + '][imagelarge]" value="" id="imagelarge' + image_row + '">';
        html += '<div class="media-body hidden-phone">';
        html += '<a class="btn" onclick="image_upload(\'imagelarge' + image_row + '\', \'thumblarge' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;';
        html += '<a class="btn" onclick="$(\'#thumblarge' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#imagelarge' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a>';
        html += '</div></div></td>';
        html += '<td><a onclick="$(\'#image-row' + image_row + '\').remove();" class="btn btn-danger"><i class="icon-white icon-trash"></i><span class="hidden-phone"> <?php echo $button_remove; ?></span></a></td>';
        html += '</tr>';
        $('#images tbody').append(html);
        image_row++;
    }</script><script>    function image_upload(field, thumb) {
            $('#modal').remove();
            html = '<div style="width: 52%;left: 42%;" id="modal" class="modal modal-upload hide fade" tabindex="-1" role="dialog">';
            html += '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4><?php echo $text_image_manager; ?></h4></div>';
            html += '<div class="modal-upload-body"><iframe style="height: 365px;width: 700px;" src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" frameborder="no" scrolling="auto"></iframe></div>';
            html += '<div class="modal-footer"><a class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $button_cancel; ?></a></div>';
            html += '</div>';
            $('body').prepend(html);
            $('#modal').on('hidden', function (event, ui) {
                if ($('#' + field).val()) {
                $.ajax({url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()), dataType: 'text', success: function (text) {
                                                                                            $('#' + thumb).replaceWith('<img class="thumbnail" src="' + text + '" width="100" height="100" alt="" id="' + thumb + '">');
    }});
                }
            }).modal();
        }
        ;</script><?php echo $footer; ?>