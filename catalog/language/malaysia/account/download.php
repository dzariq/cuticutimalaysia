<?php
// Heading 
$_['heading_title']   = 'Ruangan Download';

// Text
$_['text_account']    = 'Akaun';
$_['text_downloads']  = 'Download';
$_['text_order']      = 'No. Pesan:';
$_['text_date_added'] = 'Tanggal Pesan:';
$_['text_name']       = 'Nama:';
$_['text_remaining']  = 'Sisa:';
$_['text_size']       = 'Ukuran:';
$_['text_empty']      = 'Anda tidak memiliki pesanan file yang bisa di download!';
?>