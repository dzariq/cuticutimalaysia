<?php
// Heading
$_['heading_title']    = 'IP Blacklist Pelanggan';

// Text
$_['text_success']     = 'Sukses: Anda telah memodifikasi IP blacklist pelanggan!';

// Column
$_['column_ip']        = 'IP';
$_['column_customer']  = 'Pelanggan';
$_['column_action']    = 'Aksi';

// Entry
$_['entry_ip']         = 'IP:';

// Error
$_['error_permission'] = 'Peringatan: Anda tidak memiliki izin untuk memodifikasi IP blacklist pelanggan!';
$_['error_ip']         = 'IP harus diantara 1 - 15 karakter!';
?>