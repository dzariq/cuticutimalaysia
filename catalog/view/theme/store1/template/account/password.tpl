<?php echo $header; ?>
<div class="col-xs-3">
    <?php echo $column_left ?>
</div>
<div class="grid_9">
    <section>
        <?php echo $content_top; ?>
        <h2><?php echo $heading_title; ?></h2>
        <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="password">
            <div class="form-group">
                <label for="password" class="required"><?php echo $entry_password; ?></label>
                <input class="form-control" type="password" id="password" name="password" value="<?php echo $password; ?>" autofocus />
                <?php if ($error_password) { ?>
                <span class="error"><?php echo $error_password; ?></span>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
                <input class="form-control" type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                <?php if ($error_confirm) { ?>
                <span class="error"><?php echo $error_confirm; ?></span>
                <?php } ?>
            </div>
            <div class="pull-left">
                <a href="<?php echo $back; ?>" class="mybutton"><?php echo $button_back; ?></a>
            </div>
            <div class="pull-right">
                <a onclick="$('#password').submit();" class="mybutton"><?php echo $button_continue; ?></a>
            </div>
        </form>
        <?php echo $content_bottom; ?>
    </section>
</div>
</div>
<?php echo $footer; ?>