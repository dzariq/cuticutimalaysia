<?php echo $header; ?>
<section class="innerpage-wrapper">
    <div id="about-content-2" class="innerpage-section-padding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                    <div id="abt-cnt-2-img">
                        <img src="image/login_image.jpg" class="img-responsive" alt="about-img" />
                    </div><!-- end abt-cnt-2-img -->
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8">
                    <div id="abt-cnt-2-text">
                        <h2><?php echo $heading_title ?></h2>
                        <p>
                            <?php echo $description; ?>
                        </p>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="abt-cnt-2-ftr">
                                    <span><i class="fa fa-diamond"></i></span>
                                    <h4>Best Service</h4>
                                </div><!-- end abt-cnt-2-ftr -->
                            </div><!-- end columns -->

                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="abt-cnt-2-ftr">
                                    <span><i class="fa fa-clock-o"></i></span>
                                    <h4>24/7 Availability</h4>
                                </div><!-- end abt-cnt-2-ftr -->
                            </div><!-- end columns -->

                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="abt-cnt-2-ftr">
                                    <span><i class="fa fa-star"></i></span>
                                    <h4>5 Star Rating</h4>
                                </div><!-- end abt-cnt-2-ftr -->
                            </div><!-- end columns -->
                        </div><!-- end row -->
                    </div><!-- end abt-cnt-2-text -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end about-content-2 -->
</section><!-- end innerpage-wrapper -->
<?php echo $footer; ?>