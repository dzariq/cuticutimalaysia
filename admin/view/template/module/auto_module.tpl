<?php
#########################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com  #
#########################################################################################
?>
<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
  <div id="tabs" class="htabs"><a href="#tab-module"><?php echo $tab_module; ?></a><a style='visibility:hidden' href="#tab-fields"><?php echo $tab_fields; ?></a><a href="#tab-layout"><?php echo $tab_layout; ?></a></div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <!-- MODULE SETTINGS -->
      <div id="tab-module"><table class="form">
        <tr><td colspan="2"><?php echo $text_module_settings; ?></td></tr>
        <tr>
          <td><?php echo $entry_mod_name; ?></td>
          <td><input type="text" name="mod_name"></td>
        </tr>
        <tr style='visibility:hidden'>
          <td><?php echo $entry_backend_model; ?></td>
          <td><select name="backend_model">
          	<option value="1"><?php echo $text_yes; ?></option>
          	<option value="0"><?php echo $text_no; ?></option>
          </select></td>
        </tr>
        <tr style='visibility:hidden'>
          <td><?php echo $entry_frontend_model; ?></td>
          <td><select name="frontend_model">
          	<option value="1"><?php echo $text_yes; ?></option>
          	<option value="0"><?php echo $text_no; ?></option>
          </select></td>
        </tr>
      </table></div>
      <!-- CONFIG SETTINGS -->
      <div style='visibility:hidden' id="tab-fields"><table class="form">
        <tr><td colspan="2"><?php echo $text_config_settings; ?></td></tr>
        <tr class="field_options"><td><select name="fields[]">
        	<option>--Select--</option>
        <?php foreach ($field_options as $type=>$has_options) { ?>
	        <option value="<?php echo $type; ?>" <?php if ($has_options) echo 'class="has_options"'; ?>><?php echo ucwords($type); ?></option>
        <?php } //end foreach ?>
        </select></td></tr>
       
      </table></div>
      <!-- LAYOUT SETTINGS -->
      <div id="tab-layout"><table class="form">
        <tr><td colspan="2"><?php echo $text_layout_settings; ?></td></tr>
        <tr>
        	<td><?php echo $entry_image_w_h; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="image" checked="checked"></td>
        </tr>
        <tr>
        	<td>Banner</td>
        	<td><input type="checkbox" name="layout_data[]" value="banner" checked="checked"></td>
        </tr>
        <tr>
        	<td>Title</td>
        	<td><input type="checkbox" name="layout_data[]" value="title" checked="checked"></td>
        </tr>
        <tr>
        	<td>Description</td>
        	<td><input type="checkbox" name="layout_data[]" value="description" checked="checked"></td>
        </tr>
        <tr>
        	<td><?php echo $entry_position; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="position" checked="checked"></td>
        </tr>
        <tr>
        	<td><?php echo $entry_layout; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="layout" checked="checked"></td>
        </tr>
        <tr>
        	<td><?php echo $entry_status; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="status" checked="checked"></td>
        </tr>
        <tr>
        	<td><?php echo $entry_sort_order; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="sort_order" checked="checked"></td>
        </tr>
        <tr>
        	<td><?php echo $entry_limit; ?></td>
        	<td><input type="checkbox" name="layout_data[]" value="limit" checked="checked"></td>
        </tr>
      </table></div>
      
    </form>
  </div>
</div>
</div>
<script type="text/javascript"><!--
var num_multis = -1;
function addMulti(el) {
	html = 'Option: <input type="text" name="field_multi_val['+num_multis+'][]">';
	$(el).before(html);
}
function addFields() {
	$this = $(this);
	multi = ($this.val() == 'checkbox' || $this.val() == 'select' || $this.val() == 'radio');
	if (multi) {
		num_multis += 1;
		html = '<td>Label: <input type="text" name="field_' + $this.val() + '[]">';
		html += ' Option: <input type="text" name="field_multi_val['+num_multis+'][]"><a class="button" onclick="addMulti(this);"><span>Add</span></a></td>';
	} else {
		html = '<td>Label: <input type="text" name="field_' + $this.val() + '[]"></td>';
	}
	$grandpa = $this.parent().parent('.field_options');
	$clone = $grandpa.clone();
	$grandpa.append(html);
	$grandpa.after($clone);
	$("#tab-fields select").change(addFields);
}
$("#tab-fields select").change(addFields);
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>
<?php echo $footer; ?>