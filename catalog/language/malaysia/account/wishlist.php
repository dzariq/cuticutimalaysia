<?php
// Heading 
$_['heading_title'] = 'Wish List Saya';

// Text
$_['text_account']  = 'Akaun';
$_['text_instock']  = 'Stok Ada';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login']    = 'Anda harus <a href="%s">login</a> atau <a href="%s">membuat Akaun</a> untuk menyimpan <a href="%s">%s</a> kedalam <a href="%s">Wish List</a>!';
$_['text_success']  = 'Berhasil: Anda telah menambahkan <a href="%s">%s</a> kedalam <a href="%s">Wish List</a>!';
$_['text_remove']   = 'Berhasil: Anda berhasil mengubah Wish List!';
$_['text_empty']    = 'Wish List Anda kosong.';

// Column
$_['column_image']  = 'Gambar';
$_['column_name']   = 'Nama Produk';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stok';
$_['column_price']  = 'Harga per Unit';
$_['column_action'] = 'Aksi';
?>