<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li class="active"><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
                                <h2 class="dash-content-title">My Profile</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h4>Edit Address</h4></div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="address">
                                                <div class="form-group">
                                                    <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                                                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                                                    <?php if ($error_firstname) { ?>
                                                    <span class="error"><?php echo $error_firstname; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname" class="required"><?php echo $entry_lastname; ?></label>
                                                    <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $lastname; ?>" />
                                                    <?php if ($error_lastname) { ?>
                                                    <span class="error"><?php echo $error_lastname; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="company"><?php echo $entry_company; ?></label>
                                                    <input type="text" class="form-control" id="company" name="company" value="<?php echo $company; ?>" />
                                                </div> 
                                                <?php if ($company_id_display) { ?>
                                                <div id="company-id-display" class="form-group">      
                                                    <label for="company_id"><span id="company-id-required" class="required"></span><?php echo $entry_company_id; ?></label>
                                                    <input type="text" class="form-control" id="company_id" name="company_id" value="<?php echo $company_id; ?>" />
                                                    <?php if ($error_company_id) { ?>
                                                    <span class="error"><?php echo $error_company_id; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                                <?php if ($tax_id_display) { ?>
                                                <div id="tax-id-display" class="form-group">
                                                    <label for="tax_id"><span id="tax-id-required" class="required"></span><?php echo $entry_tax_id; ?></label>
                                                    <input type="text" class="form-control" id="tax_id" name="tax_id" value="<?php echo $tax_id; ?>" />
                                                    <?php if ($error_tax_id) { ?>
                                                    <span class="error"><?php echo $error_tax_id; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                                <div class="form-group">
                                                    <label for="address_1" class="required"><?php echo $entry_address_1; ?></label>
                                                    <input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1; ?>" />
                                                    <?php if ($error_address_1) { ?>
                                                    <span class="error"><?php echo $error_address_1; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="address_2"><?php echo $entry_address_2; ?></label>
                                                    <input type="text" class="form-control" id="address_2" name="address_2" value="<?php echo $address_2; ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="city" class="required"><?php echo $entry_city; ?></label>
                                                    <input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>" />
                                                    <?php if ($error_city) { ?>
                                                    <span class="error"><?php echo $error_city; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label id="postcode-required" for="postcode" class="required"><?php echo $entry_postcode; ?></label>
                                                    <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
                                                    <?php if ($error_postcode) { ?>
                                                    <span class="error"><?php echo $error_postcode; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="countr_id" class="required"><?php echo $entry_country; ?></label>
                                                    <select class="form-control" id="country_id" name="country_id">
                                                        <option value=""><?php echo $text_select; ?></option>
                                                        <?php foreach ($countries as $country) { ?>
                                                        <?php if ($country['country_id'] == $country_id) { ?>
                                                        <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                                        <?php } else { ?>
                                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                                        <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                    <?php if ($error_country) { ?>
                                                    <span class="error"><?php echo $error_country; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
                                                    <select class="form-control" id="zone_id" name="zone_id"></select>
                                                    <?php if ($error_zone) { ?>
                                                    <span class="error"><?php echo $error_zone; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="default"><?php echo $entry_default; ?></label>
                                                    <?php if ($default) { ?>
                                                    <input type="radio" id="default" name="default" value="1" checked="checked" />
                                                    <?php echo $text_yes; ?>
                                                    <input type="radio" id="default" name="default" value="0" />
                                                    <?php echo $text_no; ?>
                                                    <?php } else { ?>
                                                    <input type="radio" id="default" name="default" value="1" />
                                                    <?php echo $text_yes; ?>
                                                    <input type="radio" id="default" name="default" value="0" checked="checked" />
                                                    <?php echo $text_no; ?>
                                                    <?php } ?>
                                                </div>
                                                <div class="pull-left">
                                                    <a href="index.php?route=account/account" ><button class="btn btn-orange">BACK</button></a>
                                                </div>
                                                <div class="pull-right">
                                                    <a onclick="$('#address').submit();" ><button class="btn btn-orange">SAVE</button></a>
                                                </div>
                                            </form>

                                        </div><!-- end row -->

                                    </div><!-- end panel-body -->
                                </div><!-- end panel-detault -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 