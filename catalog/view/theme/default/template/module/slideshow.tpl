
<div class="clearfix"></div>
<style>
    .bx-wrapper .bx-viewport{
        box-shadow:  none
    }
    
    .bx-viewport{
        position: initial !Important
    }

    @media(max-width:530px){
      
        .downbtn{
            margin:75% auto !Important;
            width:20%
        }
        .desc{
            width:80%;
            margin:-10% auto !Important
        }
        .logotop{
            width:30%;
            margin:20% auto !Important
        }
    }
    
    .bx-pager-item{
        display:none !Important;
    }

</style>
<?php if(count($banners) != 0){ ?>
<ul class=" bxslider" style="display:none">
    <?php foreach($banners as $banner){ ?>
   <li class='fit-height banners' >
       <a href="<?php echo $banner[link] ?>"><img  style="margin:0 auto;margin-top:10px" class="img-responsive" src="<?php echo $banner[image] ?>" /></a>
    </li>
    <?php } ?>

</ul>
<?php } ?>
<script>
    $(document).ready(function() {
        $('.bxslider').show().bxSlider({
            onSlideBefore: function() {
            },
		onSlideAfter: function() {
                },
            preloadImages: 'all',
            auto:true,
            pause: 5000
        });
    });
</script>