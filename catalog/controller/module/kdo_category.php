<?php

class ControllerModuleKDOCategory extends Controller {

    protected function index($setting) {
        $this->language->load('module/category');
        $this->language->load('module/kdo_category');

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['browse_search'] = $this->language->get('browse_search');

        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $this->data['category_id'] = $parts[0];
        } else {
            $this->data['category_id'] = 0;
        }

        if (isset($parts[1])) {
            $this->data['child_id'] = $parts[1];
        } else {
            $this->data['child_id'] = 0;
        }

        if (isset($parts[2])) {
            $this->data['sisters_id'] = $parts[2];
        } else {
            $this->data['sisters_id'] = 0;
        }

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->data['categories'] = array();
        $this->data['database'] = DB_DATABASE;

        if (DB_DATABASE == 'oneweb_shop') {
            $categories = $this->model_catalog_product->getRCategories();
        } else {
            $categories = $this->model_catalog_category->getCategories(0);
        }

        if (DB_DATABASE == 'oneweb_shop') {
            
            $this->data['categories'][] = array(
                'category_id' => '',
                'name' => 'All',
                'children' => '',
                'sister' => '',
                'href' => $this->url->link('common/home')
            );
        }

        foreach ($categories as $category) {
            $total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));

            $children_data = array();

            $sister_data = array();


            if (DB_DATABASE == 'oneweb_shop') {
                $children = '';
            } else {
                $children = $this->model_catalog_category->getCategories($category['category_id']);
            }
            foreach ($children as $child) {
                $sister_data = array();
                $sisters = $this->model_catalog_category->getCategories($child['category_id']);
                if ($sisters) {
                    foreach ($sisters as $sisterMember) {
                        $sister_data[] = array(
                            'category_id' => $sisterMember['category_id'],
                            'name' => $sisterMember['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $sisterMember['category_id'])
                        );
                    }
                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'sister_id' => $sister_data,
                        'name' => $child['name'],
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                } else {
                    $children_data[] = array(
                        'category_id' => $child['category_id'],
                        'sister_id' => '',
                        'name' => $child['name'],
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }
            }

            if (DB_DATABASE == 'oneweb_shop') {

                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }
                if (isset($this->request->get['filter_name'])) {
                    $url .= '&filter_name=' . $this->request->get['filter_name'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }

                if (isset($this->request->get['filter_name'])) {
                    $this->data['filter_name'] = $this->request->get['filter_name'];
                }else{
                    $this->data['filter_name'] = 'Search';
                }

                $this->data['url_action'] = $this->url->link('common/home', $url . '&category=' . $category['name']);



                $this->data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                    'children' => $children_data,
                    'sister' => $sister_data,
                    'href' => $this->url->link('common/home', $url . '&category=' . $category['name'])
                );
            } else {
                $this->data['categories'][] = array(
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $total . ')' : ''),
                    'children' => $children_data,
                    'sister' => $sister_data,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/kdo_category.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/kdo_category.tpl';
        } else {
            $this->template = 'default/template/module/kdo_category.tpl';
        }

        $this->render();
    }

}

?>