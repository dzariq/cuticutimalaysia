<?php
// Heading 
$_['heading_title']                 = 'Program Afiliasi';

// Text
$_['text_account']                  = 'Akaun';
$_['text_login']                    = 'Login';
$_['text_description']              = '<p>%s program Afiliasi adalah untuk mendapatkan komisen dengan menempatkan link pada halaman web mereka yang mengiklankan %s atau produk tertentu didalamnya. Setiap pembelian yang dilakukan customer dengan mengakses link tersebut akan mendapatkan komisen Afiliasi.</p><p>Untuk maklunmat lebih lanjut, kunjungi halaman FAQ atau lihat syarat & terma Afiliasi &amp; kami.</p>';
$_['text_new_affiliate']            = 'Afiliasi Baru';
$_['text_register_account']         = '<p>Saya belum menjadi Afiliasi.</p><p>Klik Teruskan untuk mendaftar Akaun Afiliasi baru. Akaun ini tidak sama dengan akaun pada web utama.</p>';
$_['text_returning_affiliate']      = 'Login Afiliasi';
$_['text_i_am_returning_affiliate'] = 'Saya adalah member Afiliasi.';
$_['text_forgotten']                = 'Terlupa Password';

// Entry
$_['entry_email']                   = 'Email Afiliasi:';
$_['entry_password']                = 'Password:';

// Error
$_['error_login']                   = 'Peringatan: Alamat Email dan/atau Password tidak dijumpai.';
$_['error_approved']                = 'Peringatan: Akaun Anda harus disetujui terlebih dulu sebelum Anda dapat login.';
?>