<?php

class ControllerCommonHeader extends Controller {

    protected function index() {
        $this->data['title'] = $this->document->getTitle();

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $this->data['base'] = $this->config->get('config_ssl');
        } else {
            $this->data['base'] = $this->config->get('config_url');
        }

        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";



        $nowww = ereg_replace('www\.', '', $url);

        $domain = parse_url($nowww);

        if (!empty($domain["host"])) {

            $this->data['domain_name'] = $domain["host"];
        } else {

            $this->data['domain_name'] = $domain["path"];
        }

        $this->data['main_theme'] = $this->config->get('config_main_theme');
        
        
        //compulsory styles !!!!!!!!!!!!!!!!!!!!!!!!!!!
       // $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/carousel.css');

        //compulsory styles !!!!!!!!!!!!!!!!!!!!!!!!!!!
        

        $this->data['description'] = $this->document->getDescription();
        $this->data['keywords'] = $this->document->getKeywords();
        $this->data['links'] = $this->document->getLinks();
        $this->data['styles'] = $this->document->getStyles();
        $this->data['scripts'] = $this->document->getScripts();
        $this->data['lang'] = $this->language->get('code');
        $this->data['direction'] = $this->language->get('direction');
        $this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');

        
 
        
        
        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
        }

        $this->language->load('common/header');

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $server = HTTPS_IMAGE;
        } else {
            $server = HTTP_IMAGE;
        }

        if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->data['icon'] = $server . $this->config->get('config_icon');
        } else {
            $this->data['icon'] = '';
        }

        $this->data['name'] = $this->config->get('config_name');
        
                $this->load->model('tool/image');

        if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
//            $this->data['logo'] = $server . $this->config->get('config_logo');
            $this->data['logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 160, 50);
        } else {
            $this->data['logo'] = '';
        }
        $this->load->model('localisation/currency');
        $this->load->model('localisation/language');



        $this->data['languages'] = array();

        $results = $this->model_localisation_language->getLanguages();

        foreach ($results as $result) {
            if ($result['status']) {
                $this->data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code'],
                    'image' => $result['image']
                );
            }
        }

        $this->data['currencies'] = array();

        $results = $this->model_localisation_currency->getCurrencies();

        foreach ($results as $result) {
            if ($result['status']) {
                $this->data['currencies'][] = array(
                    'title' => $result['title'],
                    'code' => $result['code'],
                    'symbol_left' => $result['symbol_left'],
                    'symbol_right' => $result['symbol_right']
                );
            }
        }

        $this->data['text_about_us'] = $this->language->get('text_about_us');
        $this->data['text_categories'] = $this->language->get('text_categories');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_home'] = $this->language->get('text_home');
        $this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        $this->data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $this->data['text_search'] = $this->language->get('text_search');
        $this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
        $this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
        $this->data['text_account'] = $this->language->get('text_account');
        $this->data['text_special'] = $this->language->get('text_special');
        $this->data['text_login'] = $this->language->get('text_login');
        $this->data['text_logout'] = $this->language->get('text_logout');
        $this->data['text_register'] = $this->language->get('text_register');

        $this->data['text_checkout'] = $this->language->get('text_checkout');

        $this->data['home'] = $this->url->link('common/home');
        $this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $this->data['logged'] = $this->customer->isLogged();
        $this->data['account'] = $this->url->link('account/account', '', 'SSL');
        $this->data['shopping_cart'] = $this->url->link('checkout/cart');
        $this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

        if (isset($this->request->get['filter_name'])) {
            $this->data['filter_name'] = $this->request->get['filter_name'];
        } else {
            $this->data['filter_name'] = $this->language->get('text_search');
        }

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);
        foreach ($categories as $category) {
            if ($category['top']) {
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);
                foreach ($children as $child) {
                    $data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $product_total = $this->model_catalog_product->getTotalProducts($data);

                    if (!isset($child['linkto']))
                        $child['linkto'] = '';
                    if ($child['linkto']) {
                        $link = $child['linkto'];
                    } else {
                        if (isset($child['database'])) {

                            $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '&db=' . $child['database']);
                        } else {
                            $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']);
                        }
                    }

                    $children_data[] = array(
                        'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
                        'href' => $link,
                        'image' => $child['image'],
                        'description' => $child['description'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($child['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    );
                }

                // Level 1

                if (!isset($category['linkto']))
                    $category['linkto'] = '';
                if ($category['linkto']) {
                    $link = $category['linkto'];
                } else
                if (isset($category['database'])) {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id'] . '&db=' . $category['database']);
                } else {
                    $link = $this->url->link('product/category', 'path=' . $category['category_id']);
                }
            }

            $this->data['categories'][] = array(
                'name' => $category['name'],
                'children' => $children_data,
                'column' => $category['column'] ? $category['column'] : 1,
                'href' => $link,
                'image' => $category['image'],
                'description' => utf8_substr(strip_tags(html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8')), 0, 150),
            );
        }


        $this->load->model('catalog/information');

        $this->data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            $this->data['informations'][] = array(
                'name' => $result['title'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 150),
                'children' => '',
                'column' => 1,
                'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
        }

        $this->children = array(
            'module/language',
            'module/currency',
            'module/cart'
        );

        $this->data['theme'] = $this->config->get('config_template');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/header.tpl';
        } else {
            $this->template = 'default/template/common/header.tpl';
        }


$this->data['ff_navigation'] = $this->getChild('myoc/fftheme/navigation');
                $this->data['ff_header_color'] = $this->config->get('ffheader_color');
                $this->data['ff_scheme'] = $this->config->get('ffscheme');
        $this->render();
    }

}

?>