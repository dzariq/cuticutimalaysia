<?php
// Heading 
$_['heading_title']      = 'Pulangan Produk';

// Text
$_['text_account']       = 'Akaun';
$_['text_return']        = 'Informasi Pulangan';
$_['text_return_detail'] = 'Maklumat Pulangan';
$_['text_description']   = '<p>Untuk mendapatkan nomor RMA, silahkan lengkapi form dibawah ini.</p>';
$_['text_order']         = 'Informasi Pesanan';
$_['text_product']       = 'Informasi Produk &amp; Alasan untuk Pulangan';
$_['text_message']       = '<p>Terima kasih telah mengirimkan permintaan pulangan. Permintaan Anda telah disampaikan ke departemen yang bersangkutan untuk diproses.</p><p> Anda akan diberitahu mengenai status pulangan melalui Email.</p>';
$_['text_return_id']     = 'No. Pulangan:';
$_['text_order_id']      = 'No. Pesan:';
$_['text_date_ordered']  = 'Tarikh Pesanan:';
$_['text_status']        = 'Status:';
$_['text_date_added']    = 'Tarikh Pesanan:';
$_['text_customer']      = 'Pelanggan:';
$_['text_comment']       = 'Komen Pulangan';
$_['text_history']       = 'Riwayat Pulangan';
$_['text_empty']         = 'Anda belum pernah melakukan pulangan produk!';
$_['text_error']         = 'Pulangan yang Anda minta tidak dapat ditemukan!';

// Column
$_['column_product']     = 'Nama Produk';
$_['column_model']       = 'Model';
$_['column_quantity']    = 'Jumlah';
$_['column_price']       = 'Harga';
$_['column_opened']      = 'Terbuka';
$_['column_comment']     = 'Komen';
$_['column_reason']      = 'Alasan';
$_['column_action']      = 'Aksi';
$_['column_date_added']  = 'Tarikh Pesan';
$_['column_status']      = 'Status';

// Entry
$_['entry_order_id']     = 'No. Pesan:';
$_['entry_date_ordered'] = 'Tarikh Pesan:';
$_['entry_firstname']    = 'Nama Depan:';
$_['entry_lastname']     = 'Nama Belakang:';
$_['entry_email']        = 'Email:';
$_['entry_telephone']    = 'Telepon:';
$_['entry_product']      = 'Nama Produk:';
$_['entry_model']        = 'Kod Produk:';
$_['entry_quantity']     = 'Jumlah:';
$_['entry_reason']       = 'Alasan untuk Pulangan:';
$_['entry_opened']       = 'Produk telah dibuka:';
$_['entry_fault_detail'] = 'Kerosakan atau maklumat lain:';
$_['entry_captcha']      = 'Masukkan kod verifikasi berikut:';

// Error
$_['error_order_id']     = 'No. Pesan diperlukan!';
$_['error_firstname']    = 'Nama Depan harus terdiri dari 1 s/d 32 karakter!';
$_['error_lastname']     = 'Nama Belakang harus terdiri dari 1 s/d 32 karakter!';
$_['error_email']        = 'Alamat Email tidak valid!';
$_['error_telephone']    = 'Telefon harus terdiri dari 3 s/d 32 karakter!';
$_['error_product']      = 'Nama Produk harus lebih besar dari 3 dan kurang dari 255 karakter!';
$_['error_model']        = 'Model Produk harus lebih besar dari 3 dan kurang dari 64 karakter!';
$_['error_reason']       = 'Anda harus memilih alasan pulangan produk!';
$_['error_captcha']      = 'Kod verifikasi tidak sesuai gambar!';
?>