<?php
// Heading 
$_['heading_title']     = 'Informasi Akaun Saya';

// Text
$_['text_account']      = 'Akaun';
$_['text_edit']         = 'Ubah Informasi';
$_['text_your_details'] = 'Detail Pribadi Anda';
$_['text_your_address'] = 'Alamat Anda';
$_['text_success']      = 'Berjaya: Akaun Anda berjaya diperbaharui.';

// Entry
$_['entry_firstname']   = 'Nama Hadapan:';
$_['entry_lastname']    = 'Nama Belakang:';
$_['entry_email']       = 'Email:';
$_['entry_telephone']   = 'No Telefon:';
$_['entry_fax']         = 'Fax:';
$_['entry_company']     = 'Nama Syarikat:';
$_['entry_website']     = 'Website:';
$_['entry_address_1']   = 'Alamat 1:';
$_['entry_address_2']   = 'Alamat 2:';
$_['entry_postcode']    = 'Poskod:';
$_['entry_city']        = 'Bandar:';
$_['entry_country']     = 'Negara:';
$_['entry_zone']        = 'Negeri:';

// Error
$_['error_exists']      = 'Peringatan: Alamat Email sudah terdaftar!';
$_['error_firstname']   = 'Nama Hadapan harus terdiri dari 1 hingga 32 karakter!';
$_['error_lastname']    = 'Nama Belakang harus terdiri dari 1 hingga 32 karakter!';
$_['error_email']       = 'Alamat Email tidak valid!';
$_['error_telephone']   = 'No Telefon harus terdiri dari 3 hingga 32 karakter!';
$_['error_address_1']   = 'Alamat 1 harus terdiri dari 3 hingga 128 karakter!';
$_['error_city']        = 'Bandar harus terdiri dari 2 hingga 128 karakter!';
$_['error_country']     = 'Sila pilih Negara!';
$_['error_zone']        = 'Sila pilih Negeri!';
$_['error_postcode']    = 'Poskod harus terdiri dari 2 hingga 10 karakter!';
?>