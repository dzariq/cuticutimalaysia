<?php
class ModelMyocFftheme extends Model {
    public function getManufacturersByCategoryIds($category_ids = array())
    {
        $query = $this->db->query("SELECT DISTINCT(m.manufacturer_id), m.* FROM " . DB_PREFIX . "manufacturer m LEFT JOIN "  . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) INNER JOIN " . DB_PREFIX . "product AS p ON m.manufacturer_id = p.manufacturer_id INNER JOIN " . DB_PREFIX . "product_to_category AS p2c ON p.product_id = p2c.product_id WHERE m2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND p2c.category_id IN (" . implode(',', $category_ids) . ") ORDER BY m.sort_order, LCASE(m.name) ASC");
        
        return $query->rows;
    }

    public function saveSettings($data = array())
    {
    	foreach ($data as $key => $value) {
    		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$this->config->get('config_store_id') . "' AND `group` = 'fftheme' AND `key` = '" . $this->db->escape($key) . "'");
    		if (!is_array($value)) {
    			$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$this->config->get('config_store_id') . "', `group` = 'fftheme', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
    		} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$this->config->get('config_store_id') . "', `group` = 'fftheme', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'");
			}
    	}
    }
}
?>