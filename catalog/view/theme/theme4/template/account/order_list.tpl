<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <h2><?php echo $heading_title ?></h2>
            <div class="account-content">
                <div class="col-md-3">
                    <?php echo $column_left ?>
                </div>
                <div class="col-md-9">
                    <h3><i class="icon-user color"></i> <?php echo $heading_title ?></h3>
                    <table class="table table-striped tcart">
                        <thead>
                            <tr>
                                <th><?php echo $text_order_id; ?></th>
                                <th><?php echo $text_status; ?></th>
                                <th><?php echo $text_date_added; ?></th>
                                <th><?php echo $text_products; ?></th>
                                <th><?php echo $text_total; ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($orders) { ?>
                            <?php $i=0; foreach ($orders as $order) { ?>
                            <tr>
                                <td>#<?php echo $order['order_id']; ?></td>
                                <td><?php echo $order['status']; ?></td>
                                <td><?php echo $order['date_added']; ?></td>
                                <td><?php echo $order['products']; ?></td>
                                <td><?php echo $order['total']; ?></td>
                                <td><a href="<?php echo $order['href']; ?>" style="color:white !important" class="btn btn-primary" data-toggle="tooltip" title="<?php echo $button_view; ?>">
                                        <?php echo $button_view; ?>
                                    </a>
                                </td>
                            </tr>
                            <?php $i++; } } ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="sep-bor"></div>
        </div>
    </div>
</section>

<?php echo $footer; ?>