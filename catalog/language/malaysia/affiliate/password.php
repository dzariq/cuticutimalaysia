<?php
// Heading 
$_['heading_title']  = 'Ubah Password';

// Text
$_['text_account']   = 'Akaun';
$_['text_password']  = 'Password Anda';
$_['text_success']   = 'Berjaya: Password Anda berjaya diperbaharui.';

// Entry
$_['entry_password'] = 'Password:';
$_['entry_confirm']  = 'Pengesahan Password:';

// Error
$_['error_password'] = 'Password harus terdiri dari 4 hingga 20 karakter!';
$_['error_confirm']  = 'Pengesahan Password tidak padan!';
?>