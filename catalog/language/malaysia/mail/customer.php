<?php
// Text
$_['text_subject']  = '%s - Terima kasih telah mendaftar';
$_['text_login_number']  = 'Your Membership Number is: %s. You can login using your email or this number.';
$_['text_welcome']  = 'Selamat datang dan terima kasih kerana mendaftar di %s!';
$_['text_login']    = 'Akaun Anda telah didaftar. Anda boleh login menggunakan alamat Email dan passsword Anda dengan mengunjungi website kami atau pada URL berikut:';
$_['text_approval'] = 'Akaun Anda harus disetujui terlebih dahulu sebelum Anda dapat login. Setelah disetujui, Anda dapat login menggunakan alamat Email dan passsword Anda dengan mengunjungi website kami atau pada URL berikut:';
$_['text_services'] = 'Setelah login, Anda dapat melihat servis lain termasuk sejarah pesanan, mencetak invoice dan mengubah maklumat Akaun Anda.';
$_['text_thanks']   = 'Terima kasih,';
?>