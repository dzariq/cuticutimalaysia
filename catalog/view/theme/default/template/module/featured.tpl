<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->
<div style="margin-top:20px">
<h3 class="text-left" ><?php echo $heading_title ?></h3>
<?php if($products){ ?>
<style>
    .featureddesc{
        font-size:12px
    }
</style>
<div class="carousel slide" id="myCarousel">
    <div class="control-box">                            
        <a data-slide="prev" href="#myCarousel" class="carousel-control left">‹</a>
        <a data-slide="next" href="#myCarousel" class="carousel-control right">›</a>
    </div><!-- /.control-box --> 
    <br/>
    <div class="carousel-inner">
        <div class="item active">
            <ul class="thumbnails">
                <?php foreach($products as $key=>$banner){ ?>
                    <?php if($key < 3){ ?>
                        <li class="col-sm-4">
                            <div >
                                <a href="<?php echo $banner['href'] ?>"><img style="margin:0 auto" class='img-responsive' src="<?php echo $banner['thumb'] ?>" alt=""></a>
                            </div>
                            <div class="caption">
                                <h6><?php echo $banner['name'] ?></h6>
                                <a class="btn btn-mini" href="<?php echo $banner['href'] ?>">&raquo; Go</a>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div><!-- /Slide1 --> 
        <div class="item">
            <ul class="thumbnails">
                <?php foreach($products as $key=>$banner){ ?>
                    <?php if($key > 2 && $key < 6){ ?>
                        <li class="col-sm-4">
                            <div >
                                <a href="<?php echo $banner['link'] ?>"><img style="margin:0 auto" class='img-responsive' src="<?php echo $banner['thumb'] ?>" alt=""></a>
                            </div>
                            <div class="caption">
                                <h6><?php echo $banner['name'] ?></h6>
                                <a class="btn btn-mini" href="<?php echo $banner['link'] ?>">&raquo; Go</a>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div><!-- /Slide1 --> 
</div>
</div><!-- /#myCarousel -->
<?php } ?>
</div>
<!-- /Auto Generated Module By HostJars http://opencart.hostjars.com -->
<script>
    // Carousel Auto-Cycle
  $(document).ready(function() {
        $('#myCarousel').carousel({
        interval: 10000
        })
    
    $('#myCarousel').on('slid.bs.carousel', function() {
        //alert("slid");
        });
    
    
});


</script>