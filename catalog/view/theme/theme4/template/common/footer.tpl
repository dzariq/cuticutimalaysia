<section id="bottom">
    <div class="container wow fadeInDown" >
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3 style='margin-top:15px'>Information</h3>
                    <ul>
                        <?php foreach($informations as $info) { ?>
                        <li><a href="<?php echo $info[href] ?>"><?php echo $info[title] ?></a></li>
                        <?php } ?>
                    </ul>
                    <div>
                        <i class="fa fa-truck fa-lg"></i>
                        <?php echo $this->config->get('config_address') ?>
                    </div>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3 style='margin-top:15px'>Category</h3>
                    <ul>
                        <?php foreach($categories as $info) { ?>
                        <li><a href="<?php echo $info[href] ?>"><?php echo $info[name] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3 style='margin-top:15px'><?php echo $text_customer_service ?></h3>
                    <?php if($this->config->get('config_fb') != ''){ ?>
                    <a href='<?php echo $this->config->get("config_fb") ?>'>
                        <i class="fa fa-lg fa-facebook"></i>
                    </a>
                    <?php } ?>
                    <?php if($this->config->get('config_twitter') != ''){ ?>
                    <a style="margin-left:10px" href='<?php echo $this->config->get("config_twitter") ?>'>
                        <i class="fa fa-lg fa-twitter"></i>
                    </a>
                    <?php } ?>
                    <?php if($this->config->get('config_insta') != ''){ ?>
                    <a  style="margin-left:10px" href='<?php echo $this->config->get("config_insta") ?>'>
                        <i class="fa fa-lg fa-instagram"></i>
                    </a>
                    <?php } ?>
                    <div style='margin-top:6px' class="address">
                        <i class="fa fa-lg fa-phone"></i>
                        <span style='margin-top:5px;margin-left:4px;'><?php echo $this->config->get('config_telephone') ?></span>
                    </div>
                    <div style='clear:both'></div>
                    <div style='margin-top:6px' class="address">
                        <i class="fa fa-lg fa-envelope"></i>
                        <span style='margin-top:5px;margin-left:4px;'><a  style="margin-left:0px;" href="mailto:<?php echo $this->config->get('config_email') ?>"><?php echo $this->config->get('config_email') ?></a></span>
                    </div>
                    <div style='clear:both'></div>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3 style='margin-top:15px'><?php echo $text_about ?></h3>
                    <p  ><?php echo $this->config->get('config_meta_description') ?></p>

                </div>    
            </div><!--/.col-md-3-->
        </div>
    </div>
</section><!--/#bottom-->

<footer id="footer" class="midnight-blue">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <?php echo $this->config->get('config_name') ?> <?php echo date('Y') ?> &copy; - <a   target="_blank" title="Malaysia Hotels, Themeparks & Resorts Booking" href="https://goholidaymalaysia.com">Partnered with GO Holiday Malaysia</a>
            </div>
        </div>
    </div>
</footer><!--/#footer-->
<div class='modalajax' >
</div>
<div id="loader-icon">
    
        </div>
</body>
</html>