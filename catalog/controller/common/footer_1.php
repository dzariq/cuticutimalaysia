<?php

class ControllerCommonFooter extends Controller {

    protected function index() {
        $this->language->load('common/footer');

        $this->data['text_information'] = $this->language->get('text_information');
        $this->data['text_service'] = $this->language->get('text_service');
        $this->data['text_extra'] = $this->language->get('text_extra');
        $this->data['text_contact'] = $this->language->get('text_contact');
        $this->data['text_customer_service'] = $this->language->get('text_customer_service');
        $this->data['text_about'] = $this->language->get('text_about');
        $this->data['text_information'] = $this->language->get('text_information');
        $this->data['text_return'] = $this->language->get('text_return');
        $this->data['text_sitemap'] = $this->language->get('text_sitemap');
        $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $this->data['text_voucher'] = $this->language->get('text_voucher');
        $this->data['text_affiliate'] = $this->language->get('text_affiliate');
        $this->data['text_special'] = $this->language->get('text_special');
        $this->data['text_account'] = $this->language->get('text_account');
        $this->data['text_order'] = $this->language->get('text_order');
        $this->data['text_wishlist'] = $this->language->get('text_wishlist');
        $this->data['text_newsletter'] = $this->language->get('text_newsletter');
        $this->data['text_about_us'] = $this->language->get('text_about_us');

        $this->load->model('catalog/information');

        $this->data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
//			if ($result['bottom']) {

            if (($result['sort_order'] < 0) || ($result['sort_order'] >= 1000)) {
                continue;
            }

            $this->data['informations'][] = array(
                'title' => $result['title'],
                'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
//			}
        }


				if($this->config->get('simple_blog_status')) {
				
					if($this->config->get('blog_footer_heading')) {
						$this->data['text_simple_blog'] = $this->config->get('blog_footer_heading');
					} else {
						$this->data['text_simple_blog'] = $this->language->get('text_simple_blog');
					}
				
					$this->data['simple_blog']	= $this->url->link('simple_blog/article');
				}
			
        $this->data['contact'] = $this->url->link('information/contact');
        $this->data['return'] = $this->url->link('account/return/insert', '', 'SSL');
        $this->data['sitemap'] = $this->url->link('information/sitemap');
        $this->data['manufacturer'] = $this->url->link('product/manufacturer');
        $this->data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $this->data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $this->data['special'] = $this->url->link('product/special');
        $this->data['account'] = $this->url->link('account/account', '', 'SSL');
        $this->data['order'] = $this->url->link('account/order', '', 'SSL');
        $this->data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $this->data['powered'] = $this->config->get('config_name') . ' &copy; ' . date('Y', time()); // = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/common/footer.tpl';
        } else {
            $this->template = 'default/template/common/footer.tpl';
        }

        $this->data['text_stay'] = $this->language->get('text_stay');
        $this->data['sm_fb'] = $this->config->get('ffsm_fb') ? 'http://www.facebook.com/' . $this->config->get('ffsm_fb') : false;
        $this->data['sm_tw'] = $this->config->get('ffsm_tw') ? 'http://www.twitter.com/' . $this->config->get('ffsm_tw') : false;
        $this->data['sm_gp'] = $this->config->get('ffsm_gp') ? 'http://plus.google.com/' . $this->config->get('ffsm_gp') : false;
        $this->data['sm_yt'] = $this->config->get('ffsm_yt') ? 'http://youtube.com/' . $this->config->get('ffsm_yt') : false;
        $this->data['sm_pin'] = $this->config->get('ffsm_pin') ? 'http://pinterest.com/' . $this->config->get('ffsm_pin') : false;
        $this->render();
    }

}

?>