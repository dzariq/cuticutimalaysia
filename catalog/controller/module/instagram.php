<?php  
class ControllerModuleInstagram extends Controller {
	protected function index() {
		$this->language->load('module/instagram');
		
        	$this->data['heading_title'] = $this->language->get('heading_title');
    			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
                        $this->document->addScript('catalog/view/javascript/jquery/jquery.cycle.js');
			
                $this->load->model('setting/setting');
                $this->data['instagram_account']=$this->config->get('instagram_account');
                $instagram_account=$this->config->get('instagram_account');
                $access_token=$instagram_account['client_access_token'];
                $this->data['images']=array();
                $this->data['message']=$this->config->get('message');
                $this->data['next']=$this->config->get('next');
                $this->data['prev']=$this->config->get('prev');
                
                if(!empty($access_token)){
                $url="https://api.instagram.com/v1/media/popular?access_token=".$access_token;

     
                $process = curl_init($url); 
                curl_setopt($process, CURLOPT_TIMEOUT, 300); 
                curl_setopt($process, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($process, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($process, CURLOPT_HTTPGET, true); 
                curl_setopt($process, CURLOPT_SSL_VERIFYPEER, FALSE);  
                $return = curl_exec($process);
                curl_close($process); 


                if($return){
                   $response = json_decode($return, true);
                   $this->data['images']=$response['data'];
                }                  
                }
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/instagram.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/instagram.tpl';
		} else {
			$this->template = 'default/template/module/instagram.tpl';
		}
		
		$this->render();
	}
        

}
?>