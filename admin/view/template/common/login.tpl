<?php echo $header; ?>
<style> #content-body { margin-left: 0px; } </style>
<div id="content">
  <div class="box" style="width: 400px; min-height: 300px; margin-top: 70px; margin-left: auto; margin-right: auto;">
    <div class="content" style="min-height: 250px; margin-top: 30px;">

      <?php if ($success) { ?>
      <div class="success"><?php echo $success; ?></div>
      <?php } ?>
      <?php if ($error_warning) { ?>
      <div class="warning"><?php echo $error_warning; ?></div>
      <?php } ?>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

		<div id="login-container">

			<div id="login-content" class="clearfix">

				<fieldset>
					<div class="control-group">
						<label class="control-label" for="username"><?php echo $entry_username; ?></label>
						<div class="controls">
							<input type="text" class="" name="username" id="username">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="password"><?php echo $entry_password; ?></label>
						<div class="controls">
							<input type="password" name="password" class="" id="password">
						</div>
					</div>
                                    <?php if(isset($this->request->get['firsttime'])){ ?>
					<div class="control-group">
						<div style="color:#0077f1" class="controls">
                                                    ** Check your email for your Website's Control Panel username & password.
						</div>
					</div>
                                    <?php } ?>
				</fieldset>
                                    <?php if(!isset($this->request->get['firsttime'])){ ?>

				<a href="<?php echo $forgotten; ?>">Forgot Password</a>
<?php } ?>
				<div class="pull-right">
					<a onclick="$('#form').submit();" class="button"><?php echo $button_login; ?></a>
				</div>

				<?php if ($redirect) { ?>
  				    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
   	 			<?php } ?>

			</div> <!-- /login-content -->

		</div> <!-- /login-wrapper -->

       </form>

    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#form').submit();
	}
});
$('#username').select();
//--></script>
</body>
</html>
<?php // echo $footer; ?>