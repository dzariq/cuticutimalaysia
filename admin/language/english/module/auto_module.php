<?php
#########################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com  #
#########################################################################################

// Heading Goes here:
$_['heading_title']    				= 'Auto Module';

// Tabs:
$_['tab_module']					= 'Module';
$_['tab_fields']					= 'Fields';
$_['tab_layout']					= 'Layout';

//Entry headings:
$_['entry_mod_name'] 				= 'New Module Name';
$_['entry_backend_model']			= 'Create Backend Model';
$_['entry_frontend_model'] 			= 'Create Frontend Model';
$_['entry_radio_option']			= 'Radio Field';
$_['entry_text_option']				= 'Text Field';
$_['entry_checkbox_option']			= 'Checkbox Field';
$_['entry_select_option']			= 'Select Field';
$_['entry_file_option']				= 'File Upload Field';
$_['entry_product_option']			= 'Product Selector';
$_['entry_category_option']			= 'Category Selector';
$_['entry_manufacturer_option']		= 'Manufacturer Selector';
$_['entry_limit']					= 'Limit';
$_['entry_image_w_h']				= 'Image (W x H)';
$_['entry_layout']					= 'Layout';
$_['entry_position']				= 'Position';
$_['entry_status']					= 'Status';
$_['entry_sort_order']				= 'Sort Order';

//Text items:
$_['text_yes']						= 'Yes';
$_['text_no']						= 'No';
$_['text_module_settings']			= 'Module Settings';
$_['text_config_settings']			= 'Choose Config Options';
$_['text_layout_settings']			= 'Choose Layout Settings';
$_['text_success']					= 'Congratulations, you created a new module!';

?>