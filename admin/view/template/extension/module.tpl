<?php echo $header; ?>
<div class="container-fluid">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($success) { ?>
	<div class="alert alert-success"><?php echo $success; ?></div>
	<?php } ?>
	<?php if ($error) { ?>
	<div class="alert alert-error"><?php echo $error; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><i class="hicon-module"></i><?php echo $heading_title; ?></h1>
		</div>
		<div class="content">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th><?php echo $column_name; ?></th>
						<th class="right"><span class="hidden-phone"><?php echo $column_action; ?></span></th>
					</tr>
				</thead>
				<tbody>
					<?php if ($extensions) { ?>
					<?php foreach ($extensions as $extension) { ?>
					<tr>
						<td><?php echo $extension['name']; ?></td>
						<td class="right"><?php foreach ($extension['action'] as $action) { ?>
							<span class="bracket"><a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a></span>
						<?php } ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
						<td class="center" colspan="8"><?php echo $text_no_results; ?></td>
					</tr>
                                        <?php } ?>
                                         <?php  if ($disabled) { ?>
          <?php foreach ($disabled as $extension) { ?>
<?php  if($user_type == 1 ){ ?>
          <tr onclick="location='<?php echo $extension['action'][0]['href']; ?>'" style="cursor: pointer;">
              <td class="left"><font color="Grey"><?php echo $extension['name']; ?></font></td>
          </tr>
          <?php } ?>
                      </tr>

          <?php } ?>
          <?php } ?>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php echo $footer; ?>