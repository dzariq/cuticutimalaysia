<?php
// Heading
$_['heading_title']    = 'Peta Situs';
 
// Text
$_['text_special']     = 'Penawaran Spesial';
$_['text_account']     = 'Akaun Saya';
$_['text_edit']        = 'Informasi Akaun';
$_['text_password']    = 'Password';
$_['text_address']     = 'Buku Alamat';
$_['text_history']     = 'Sejarah Pesanan';
$_['text_download']    = 'Download';
$_['text_cart']        = 'Bakul Belanja';
$_['text_checkout']    = 'Checkout';
$_['text_search']      = 'Cari';
$_['text_information'] = 'Informasi';
$_['text_contact']     = 'Hubungi Kami';
?>
