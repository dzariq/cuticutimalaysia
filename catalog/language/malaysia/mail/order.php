<?php
// Text
$_['text_new_subject']          = '%s - Pesanan %s';
$_['text_new_greeting']         = 'Terima kasih kerana membeli di %s. Pesanan Anda telah kami terima dan akan diproses setelah pembayaran Anda disahkan.';
$_['text_new_received']         = 'Anda menerima pesanan baru.';
$_['text_new_link']             = 'Untuk melihat pesanan, klik link dibawah ini:';
$_['text_new_order_detail']     = 'Maklumat Pesanan';
$_['text_new_instruction']      = 'Arahan';
$_['text_new_order_id']         = 'No. Pesanan:';
$_['text_new_date_added']       = 'Tarikh Pesan:';
$_['text_new_order_status']     = 'Status Pesanan:';
$_['text_new_payment_method']   = 'Kaedah Pembayaran:';
$_['text_new_shipping_method']  = 'Kaedah Pengiriman:';
$_['text_new_email']  			= 'Email:';
$_['text_new_telephone']  		= 'No Telefon:';
$_['text_new_ip']  				= 'Nombor IP:';
$_['text_new_payment_address']  = 'Alamat Bil';
$_['text_new_shipping_address'] = 'Alamat Pengiriman';
$_['text_new_products']         = 'Produk';
$_['text_new_product']          = 'Produk';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Jumlah';
$_['text_new_price']            = 'Harga';
$_['text_new_order_total']      = 'Total Pesanan';	
$_['text_new_total']            = 'Total';	
$_['text_new_download']         = 'Setelah pembayaran Anda disahkan, Anda dapat mengakses link dibawah ini untuk mendownload file:';
$_['text_new_comment']          = 'Komen:';
$_['text_new_footer']           = 'Sila balas Email ini jika Anda mempunyai pertanyaan';
$_['text_new_powered']          = '';
//$_['text_new_powered']          = 'Powered By <a href="http://www.opencart.com">OpenCart</a>.';
$_['text_update_subject']       = '%s - Update Pesanan %s';
$_['text_update_order']         = 'No. Pesanan:';
$_['text_update_date_added']    = 'Tarikh Pesan:';
$_['text_update_order_status']  = 'Pesanan Anda telah diupdate dengan status berikut:';
$_['text_update_comment']       = 'Komen:';
$_['text_update_link']          = 'Untuk melihat pesanan Anda klik link berikut:';
$_['text_update_footer']        = 'Sila balas Email ini jika Anda mempunyai pertanyaan.';
?>