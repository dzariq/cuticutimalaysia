<div class="form-group">
    <?php if ($options) { ?>
    <div class="options">
                <input class="form-control" type="hidden" name="location" value="<?php echo $location ?>" />
        <input class="form-control" type="hidden" name="mpn" value="<?php echo $release ?>" />
        <?php  foreach ($options as $option) { ?>

        <?php if ( $option['type'] == 'select' || $option['type'] == 'radio') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">

            <?php if ($option['required']) { ?>
            <script>
                var option_required = true;</script>
            <span class="required">*</span>
            <?php } ?><?php echo $option['name']; ?><br />
            <ul class="sizes">
                <?php  foreach ($option['option_value'] as $option_value) { ?>
                <li class="radio-box <?php echo $option['product_option_id']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" onclick="radio_click( < ?php echo $option_value['quantity'] ? > , < ?php echo $option['product_option_id']; ? > , < ?php echo $option_value['product_option_value_id']; ? > , '<?php echo $option_value[image_option_value]; ?>', '<?php echo $option_value[image_option_value_popup]; ?>')" value="<?php echo $option_value['product_option_value_id']; ?>">
                    <?php echo $option_value['name']; ?> 
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) 
                    <?php } ?>
                </li>
                <?php } ?>    
            </ul>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <input type="radio" style="visibility: hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
            <?php } ?>
        </div>

        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <input  class="room-type form-control" readonly='true' type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
        </div>

        <?php } ?>

        <?php if ($option['type'] == 'textarea') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <?php if ($option['required']) { ?>
            <span class="required">*</span>
            <?php } ?>
            <b><?php echo $option['name']; ?>:</b><br />
            <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
        </div>

        <?php } ?>


        <?php if ($option['type'] == 'date') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" style="padding:0px" class="pull-left col-xs-4 option">
            <input placeholder="Check-in: " style="width:90%;" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $this->request->get['checkin']; ?>" class="date checkin form-control" />
        </div>
        <?php } ?>

        <?php if ($option['type'] == 'date2') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" style="padding:0px" class="pull-left col-xs-4  option">
            <input placeholder="Checkout: " style="margin-top:10px;width:90%;" readonly="true" type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="" class=" checkout form-control" />
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'room_id') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" class=" room-id form-control" />
        </div>
        <?php } ?>
        <?php } ?>
    </div>
    <?php } ?>
</div>
