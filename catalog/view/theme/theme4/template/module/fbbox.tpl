<div class="col-sm-4 col-xs-4">
    <div onclick='window.location = "photostats"' id="tile20" class="tile">
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-info-circle bigicon"></span>
                    </div>
                    <div class="icontext">
                        About Us
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-8 col-xs-8" id='fb-box'>
    <div onclick='window.location = "about-us"' id="tile21" class="tile">
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <?php  if($this->config->get('config_fb') != ''){ ?>
                    <div class="fb-page" data-href="<?php echo ($this->config->get('config_fb')) ?>" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo ($this->config->get('config_fb')) ?>"><a href="<?php echo ($this->config->get('config_fb')) ?>"></a></blockquote></div></div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="col-sm-4 col-xs-4">
    <div id="tile22" class="tile" onclick='window.location = "index.php?route=information/contact"'>
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-phone-square bigicon"></span>
                    </div>
                    <div class="icontext">
                        Contact Us
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile23" class="tile" onclick='window.location = "<?php echo ($this->config->get(config_fb)) ?>"'>
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-facebook-square bigicon"></span>
                    </div>
                  <div class="icontext">
                        Facebook
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-4 col-xs-4">
    <div id="tile24" class="tile" onclick='window.location = "<?php echo ($this->config->get(config_twitter)) ?>"'>
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-twitter-square bigicon"></span>
                    </div>
                   <div class="icontext">
                        Twitter
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

