<?php
#########################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com  #
#########################################################################################
class ControllerModuleAutoModule extends Controller {
	
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/auto_module');

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->load->model('module/auto_module');
			$this->model_module_auto_module->createModule($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		
		$text_strings = array(
			'heading_title',
			'tab_module',
			'tab_fields',
			'tab_layout',
			'text_module_settings',
			'text_config_settings',
			'text_layout_settings',
			'text_yes',
			'text_no',
			'entry_mod_name',
			'entry_backend_model',
			'entry_frontend_model',
			'entry_radio_option',
			'entry_text_option',
			'entry_checkbox_option',
			'entry_select_option',
			'entry_file_option',
			'entry_product_option',
			'entry_category_option',
			'entry_manufacturer_option',
			'entry_image_w_h',
			'entry_limit',
			'entry_layout',
			'entry_position',
			'entry_status',
			'entry_sort_order',
			'button_save',
			'button_cancel',
			'button_add',
			'button_remove',
		);
		
		foreach ($text_strings as $text) {
			$this->data[$text] = $this->language->get($text);
		}
		
// 		$config_data = array(
		
// 		);
		
// 		foreach ($config_data as $conf) {
// 			if (isset($this->request->post[$conf])) {
// 				$this->data[$conf] = $this->request->post[$conf];
// 			} else {
// 				$this->data[$conf] = $this->config->get($conf);
// 			}
// 		}

		$this->data['field_options'] = array(
			'text' => false,
			'textarea' => false,
			'file' => false,
 			'checkbox' => true,
			'select' => true,
			'radio' => true,
		);
		
		
		
	
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/auto_module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/auto_module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

	
		$this->data['modules'] = array();
		
		if (isset($this->request->post['auto_module_module'])) {
			$this->data['modules'] = $this->request->post['auto_module_module'];
		} elseif ($this->config->get('auto_module_module')) { 
			$this->data['modules'] = $this->config->get('auto_module_module');
		}		

		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/auto_module.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/auto_module')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return (!$this->error);
	}


}
?>