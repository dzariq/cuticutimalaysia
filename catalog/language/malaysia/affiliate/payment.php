<?php
// Heading 
$_['heading_title']             = 'Kaedah Payment';

// Text
$_['text_account']              = 'Akaun';
$_['text_payment']              = 'Pembayaran';
$_['text_your_payment']         = 'Informasi Pembayaran';
$_['text_your_password']        = 'Password Anda';
$_['text_cheque']               = 'Cek';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Transfer Bank';
$_['text_success']              = 'Berjaya: Akaun Anda berjaya diperbaharui.';

// Entry
$_['entry_tax']                 = 'Cukai:';
$_['entry_payment']             = 'Kaedah Pembayaran:';
$_['entry_cheque']              = 'Nama Penerima Cek:';
$_['entry_paypal']              = 'Email Akaun Paypal:';
$_['entry_bank_name']           = 'Nama Bank:';
$_['entry_bank_branch_number']  = 'Nomor ABA/BSB (Nomor Cabang):';
$_['entry_bank_swift_code']     = 'Kode SWIFT:';
$_['entry_bank_account_name']   = 'Nama Akaun:';
$_['entry_bank_account_number'] = 'Nomor Akaun:';
?>