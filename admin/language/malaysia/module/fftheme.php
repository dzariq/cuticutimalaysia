<?php
// Heading
$_['heading_title'] 		= 'FF Theme<span style="float:right;">[ <a href="http://opencart.my/" target="_blank">Opencart.my</a> ]</span>';
$_['common_title'] 			= 'FF Theme';

// Text
$_['text_module']       	= 'Modules';
$_['text_success']      	= 'Success: You have modified FF Theme module!';
$_['text_default']      	= 'Default';
$_['text_delete']     		= 'Delete';
$_['text_empty']			= 'Upload is empty.';
$_['text_content_top']		= 'Content Top';
$_['text_content_bottom']	= 'Content Bottom';
$_['text_column_left']		= 'Content Left';
$_['text_column_right']		= 'Content Right';
$_['text_column_right']		= 'Content Right';
$_['text_browse']			= 'Browse Image';
$_['text_clear']			= 'Clear Image';
$_['text_white']			= 'White Panel';
$_['text_transparent']		= 'Transparent';
$_['text_top_left'] 		= 'Top Left';
$_['text_top_right'] 		= 'Top Right';
$_['text_bottom_right'] 	= 'Bottom Right';
$_['text_bottom_left'] 		= 'Bottom Left';
$_['text_image_manager'] 	= 'Image Manager';

$_['button_add_slideshow']  = 'Add Slideshow Image';

$_['tab_theme']      		= 'Theme';
$_['tab_setting']     		= 'Settings';
$_['tab_categorybg']     	= 'Category Background Color';
$_['tab_slideshow']      	= 'Slideshow';
$_['tab_module']      		= 'Module';
$_['tab_banner']      		= 'Slideshow Images';
$_['tab_footer']      		= 'Footer';
$_['tab_description']      	= 'Description';
$_['tab_contact']      		= 'Contact Details';
$_['tab_twitter']      		= 'Twitter';
$_['tab_facebook']      	= 'Facebook Like Box';
$_['tab_social']      		= 'Social Media';

$_['entry_scheme']      	= 'Color Scheme:';
$_['entry_base_color']      = 'Base Color:';
$_['entry_header_color']    = 'Header Color:';
$_['entry_menu_link']      	= 'Navigation Links Color:';
$_['entry_layout']      	= 'Layout:';
$_['entry_position']      	= 'Position:';
$_['entry_height']      	= 'Height (px):';
$_['entry_status']      	= 'Status:';
$_['entry_sort_order']      = 'Sort Order:';
$_['entry_image']      		= 'Image:';
$_['entry_product']      	= 'Product:';
$_['entry_content']      	= 'Content:';
$_['entry_title']      		= 'Title:';
$_['entry_description']     = 'Description:';
$_['entry_link']      		= 'Link:';
$_['entry_setting']      	= 'Settings:';
$_['entry_text_bg']      	= 'Text Background:';
$_['entry_phone1']      	= 'Phone Number 1:';
$_['entry_phone2']      	= 'Phone Number 2:';
$_['entry_fax1']      		= 'Fax Number 1:';
$_['entry_fax2']      		= 'Fax Number 2:';
$_['entry_email1']      	= 'Email Address 1:';
$_['entry_email2']      	= 'Email Address 2:';
$_['entry_gmap']      		= 'Google Map Code:<br /><span class="help">Click <a href="http://maps.google.com/help/maps/getmaps/plot-one.html" target="_blank">here</a> for instruction on getting Google Maps embedded code for your address.</span>';
$_['entry_twitter_username']= 'Twitter Username:';
$_['entry_tweet_no']		= 'Number of Tweets:';
$_['entry_facebook_fanpage']= 'Facebook Page URL:';
$_['entry_sm_facebook']     = 'Facebook Page URL:';
$_['entry_sm_facebookid']   = 'Facebook Page Admin ID:<br /><span class="help">A comma-separated list of either the Facebook IDs of page administrators or a Facebook Platform application ID. For use in facebook Like button.</span>';
$_['entry_sm_twitter']     	= 'Twitter username:';
$_['entry_sm_gplus']     	= 'Google+ page ID:';
$_['entry_sm_youtube']     	= 'YouTube username:';
$_['entry_sm_pinterest']    = 'Pinterest username:';

$_['column_category']    	= 'Category';
$_['column_color']    		= 'Color';
$_['column_whitefont']    	= 'Use White Font';

// Error
$_['error_permission']  	= 'Warning: You do not have permission to modify FF Theme module!';

$_['myoc_copyright']		= '<p>Copyright &copy; 2013 Opencart.my. All rights reserved.<br />Version 1.1</p>';
?>