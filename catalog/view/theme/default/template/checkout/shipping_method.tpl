<?php if ($error_warning) { ?>
    <div class="alert alert-danger">
    	<?php echo $error_warning; ?>
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<form role="form">
    <?php if ($shipping_methods) { ?>
        <p><?php echo $text_shipping_method; ?></p>
        <table class="table table-hover">
            <?php foreach ($shipping_methods as $shipping_method) { ?>
                <tr class="active">
                    <td colspan="3"><strong><?php echo $shipping_method['title']; ?></strong></td>
                </tr>
                <?php if (!$shipping_method['error']) {
                    foreach ($shipping_method['quote'] as $quote) { ?>
                        <tr>
                            <td>
                            	<div class="form-group">
                                    <div class="radio">
                                        <label for="<?php echo $quote['code']; ?>">
                                            <?php if ($quote['code'] == $code || !$code) { ?>
                                                <?php $code = $quote['code']; ?>
                                                <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
                                            <?php } else { ?>
                                                <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
                                            <?php } 
                                            echo $quote['title']; ?>
                                        </label>
                                    </div>
                                </div>
                            </td>
                            <td style="text-align: right; vertical-align: bottom;">
                            	<div class="form-group">
                                    <label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label>
                                </div>
                            </td>
                        </tr>
                    <?php }
                } else { ?>
                    <tr>
                        <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
                    </tr>
            <?php } } ?>
        </table>
    <?php } ?>
	<div class="form-group">
        <label for="comment"><?php echo $text_comments; ?></label>
        <textarea class="form-control" id="comment" name="comment" rows="5"><?php echo $comment; ?></textarea>
    </div>
    <div class="pull-right">
        <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="btn btn-info" />
    </div>
</form>