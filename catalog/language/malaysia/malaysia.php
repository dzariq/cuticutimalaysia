<?php
// Locale
$_['code']                  = 'my';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['decimal_point']         = '.';
$_['thousand_point']        = '.';

// Text
$_['text_close']             = 'Tutup';
$_['text_join_event']             = 'Klik untuk mendaftar';

$_['text_home']             = 'Utama';
$_['text_enter_website']             = 'Kategori';
$_['text_yes']              = 'Ya';
$_['text_no']               = 'Tidak';
$_['text_none']             = ' --- Tidak Ada --- ';
$_['text_select']           = ' --- Sila Pilih --- ';
$_['text_all_zones']        = 'Semua Zone';
$_['text_pagination']       = 'Menampilkan {start} hingga {end} dari {total} ({pages} Hal)';
$_['text_separator']        = ' &raquo; ';
$_['text_calendar_info']        = 'Klik pada seminar di kalendar(di bawah) untuk mendaftar';

// Buttons
$_['button_add_address']    = 'Tambah Alamat';
$_['button_back']           = 'Kembali';
$_['button_continue']       = 'Teruskan';
$_['button_cart']           = 'Beli';
$_['button_compare']        = 'Bandingkan';
$_['button_wishlist']       = 'Tambah ke Wish List';
$_['button_checkout']       = 'Checkout';
$_['button_confirm']        = 'Teruskan';
$_['button_coupon']         = 'Gunakan Kupon';
$_['button_delete']         = 'Hapus';
$_['button_download']       = 'Download';
$_['button_edit']           = 'Ubah';
$_['button_new_address']    = 'Alamat Baru';
$_['button_change_address'] = 'Ubah Alamat';
$_['button_reviews']        = 'Review';
$_['button_write']          = 'Tulis Review';
$_['button_login']          = 'Login';
$_['button_update']         = 'Perbaharui';
$_['button_remove']         = 'Keluarkan';
$_['button_reorder']        = 'Reorder';
$_['button_return']         = 'Pulangan';
$_['button_shopping']       = 'Sambung Berbelanja';
$_['button_search']         = 'Cari';
$_['button_shipping']       = 'Pilih Pengiriman';
$_['button_guest']          = 'Belanja Tanpa Mendaftar';
$_['button_view']           = 'Lihat';
$_['button_voucher']        = 'Gunakan Voucher';
$_['button_upload']         = 'Upload File';
$_['button_reward']         = 'Gunakan Point';
$_['button_quote']          = 'Dapatkan Quote';

// Error
$_['error_upload_1']        = 'Peringatan: File yang di upload melebihi ukuran maksima yang ditentukan pada php.ini!';
$_['error_upload_2']        = 'Peringatan: File yang di upload melebihi UKURAN MAKSIMAL yang ditentukan pada form HTML!';
$_['error_upload_3']        = 'Peringatan: Hanya sebagian file yang ter upload!';
$_['error_upload_4']        = 'Peringatan: Tidak ada file yang di upload!';
$_['error_upload_6']        = 'Peringatan: Kehilangan folder sementara!';
$_['error_upload_7']        = 'Peringatan: Gagal menulis pada disk!';
$_['error_upload_8']        = 'Peringatan: File yang di upload dihentikan oleh extensi!';
$_['error_upload_999']      = 'Peringatan: Tidak ada kode Error yang tersedia!';
$_['error_name']      = 'Peringatan: Nama anda harus antara 3 hingga 50 Karakter';
$_['error_email']      = 'Peringatan: Sila masukkan email yang sah!';
$_['existing_email']      = 'Peringatan: Anda sudah daftar dengan email ini';
$_['marketing_success']      = 'Terima Kasih atas minat anda!. Kami telah email kod kupon untuk seminar';
$_['button_send_code']      = 'Dapatkan Kod Promo';
$_['button_resend_code']      = 'Re-Send Kod Promo';
$_['seminar_text']      = 'Klik di sini untuk daftar seminar';
$_['mail_seminar_code_subject']      = 'Sertai Seminar Percuma';
$_['mail_seminar_code_message']      = 'Terima Kasih. Kod Kupon anda: %s. Klik pada link ini untuk mendaftar seminar: %s';

?>