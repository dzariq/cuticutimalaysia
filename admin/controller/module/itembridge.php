<?php

class ControllerModuleItemBridge extends Controller
{
	protected $error;


	public function index()
	{
		$this->load->language('module/itembridge');

		$this->load->model('setting/setting');

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_setting_setting->editSetting('itembridge', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['error_warning'] = isset($this->error) ? $this->error : '';


		$this->document->setTitle($this->language->get('heading_title'));

		$this->assignTextStringsTo($this->data, array(
			'heading_title',
			'button_save',
			'button_cancel',

			'home_widget',
			'home_about',
			'home_latest_news',

			'footer_social',

			'general_group',
                        'page_color',
                        'text_color',
                        'link_color',
                        'title_color',
                        'price_color',
                        'old_price_color',
                        'input_border_color',
                        'focused_input_border_color',
                        'button_color',
                        'hover_button_color',
                        'pressed_button_color',
                        'line_color',
                        'table_header_color',
                        'bottom_block_color',
                    
                        'header_group',
                        'header_ribbon_color1',
                        'header_ribbon_color2',
                        'header_ribbon_color3',
                        'header_color',
                        'welcome_text_color',
                        'hovered_header_nav_links',
                        'primary_nav',
                        'hover_primary_nav',
                        'current_primary_nav',
                        'primary_nav_sub',
                        'primary_nav_sub_hover',
                    
                        'slider_group',
                        'next_prev',
                        'hover_next_prev',
                    
                        'product_group',
                        'hover_produck_border',
                        'hover_action_button',
                        'add_cart',
                        'tab_border',
                        'tab_color',
                        'hover_tab_color',
                        'current_tab_color',
                    
                        'footer_group',
                        'footer_color',
                        'footer_links',
                        'footer_links_hover',
		));

		$this->data['breadcrumbs'] = $this->getBaseBreadcrumbs();

		$this->data['action'] = $this->url->link('module/itembridge', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');


		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();
                
		$this->assignConfigTo($this->data, array(
			'itembridge_example'                    => '',
			'itembridge_module'                     => array(),

                        'itembridge_colors_version'             => '1',

			'itembridge_general_group'              => '',
                        'itembridge_page_color'                 => '#ffffff',
                        'itembridge_text_color'                 => '#444444',
                        'itembridge_link_color'                 => '#59b7c2',
                        'itembridge_title_color'                => '#444444',
                        'itembridge_price_color'                => '#2e8f9a',
                        'itembridge_old_price_color'            => '#777777',
                        'itembridge_input_border_color'         => '#cccccc',
                        'itembridge_focused_input_border_color' => '#59b7c2',
                        'itembridge_button_color'               => '#59b7c2',
                        'itembridge_hover_button_color'         => '#2ab4c4',
                        'itembridge_pressed_button_color'       => '#2baab9',
                        'itembridge_line_color'                 => '#e0e0e0',
                        'itembridge_table_header_color'         => '#a8dade',
                        'itembridge_bottom_block_color'         => '#f5f7f9',
                    
                        'itembridge_header_group'               => '',
                        'itembridge_header_ribbon_color1'       => '#59b7c2',
                        'itembridge_header_ribbon_color2'       => '#7fcbce',
                        'itembridge_header_ribbon_color3'       => '#aadce0',
                        'itembridge_header_color'               => '#f5f7f9',
                        'itembridge_welcome_text_color'         => '#777777',
                        'itembridge_hovered_header_nav_links'   => '#59b7c2',
                        'itembridge_primary_nav'                => '#f5f7f9',
                        'itembridge_hover_primary_nav'          => '#d5e4f1',
                        'itembridge_primary_nav_sub'            => '#ffffff',
                        'itembridge_primary_nav_sub_hover'      => '#f5f7f9',
                    
                        'itembridge_slider_group'               => '',
                        'itembridge_next_prev'                  => '#ffffff',
                        'itembridge_hover_next_prev'            => '#444444',
                    
                        'itembridge_product_group'              => '',
                        'itembridge_hover_produck_border'       => '#59b7c2',
                        'itembridge_hover_action_button'        => '#eff7ff',
                        'itembridge_add_cart'                   => '#f5f7f9',
                        'itembridge_tab_border'                 => '#e0e0e0',
                        'itembridge_tab_color'                  => '#f1f3f5',
                        'itembridge_hover_tab_color'            => '#f7f7f7',
                        'itembridge_current_tab_color'          => '#ffffff',
                    
                        'itembridge_footer_group'               => '',
                        'itembridge_footer_color'               => '#f9fbfc',
                        'itembridge_footer_links'               => '#777777',
                        'itembridge_footer_links_hover'         => '#777777',
		));

		$this->assignI18nConfigTo($this->data, $this->data['languages'], array(
			'itembridge_home_widget' => '',


			'itembridge_home_about' => '
				<h3>About Us</h3>
				<p>A block of text is a stack of line boxes. In the case of left, right and center, this property specifies how the inline-level boxes within each line box align with respect to the line box</p>
                                <p>Alignment is not with respect to the viewport. In the case of justify, this property specifies that the inline-level boxes are to be made flush with both sides of the line box if possible.</p>
                                <p>by expanding or contracting the contents of inline boxes, else aligned as for the initial value.</p>
			',


			'itembridge_home_latest_news' => '
				<h3>News</h3>

				<ul>
					<li><time datetime="2012-02-01">01 January 2012</time>

					<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</a>
					</li>
					<li><time datetime="2012-02-01">01 January 2012</time>
					<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</a>
					</li>
					<li><time datetime="2012-02-01">01 January 2012</time>
					<a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</a>
					</li>
				</ul>
			',


			'itembridge_footer_social' => '
                            <div>
				<a class="google" href="#">Google</a>
                                <a class="twitter" href="#">Twitter</a>
                                <a class="facebook" href="#">Facebook</a>
                            </div>
			',
		));

		/*
		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		*/


		$this->template = 'module/itembridge.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);

		$this->response->setOutput($this->render());
	}


	protected function validate()
	{
		if (!$this->user->hasPermission('modify', 'module/itembridge'))
			$this->error = $this->language->get('error_permission');

		return !isset($this->error);
	}


	protected function getBaseBreadcrumbs()
	{
		return array(
			array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ''
			),
			array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			),
			array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/itembridge', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			)
		);
	}


	protected function assignTextStringsTo(&$var, array $text_strings)
	{
		foreach ($text_strings as $text)
			$var[$text] = $this->language->get($text);
	}


	protected function assignConfigTo(&$var, array $config_data)
	{
		foreach ($config_data as $conf => $default) {
			$var[$conf] = $this->getConfigByKey($conf, $default);
		}
	}


	protected function assignI18nConfigTo(&$var, array $languages, array $config_data)
	{
		foreach ($config_data as $conf => $default) {
			foreach ($languages as $language) {
				$k = $conf . '_' . $language['code'];
				$var[$k] = $this->getConfigByKey($k, $default);
			}
		}
	}


	protected function getConfigByKey($key, $default)
	{
		$config_value = $this->config->get($key);
		return isset($this->request->post[$key]) ? $this->request->post[$key] : (($config_value !== null) ? $config_value : (is_string($default) ? htmlspecialchars($default) : $default));
	}


}
