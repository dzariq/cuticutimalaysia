<script type="text/javascript">
  $(function() {
      
      var dates = '<?php echo $holidays ?>';
      var peaks = '<?php echo $peaks ?>';
      var offs = '<?php echo $offs ?>';
      var weekend = <?php echo $weekends ?>;
      var school = '<?php echo $school ?>';

      $('#room-duration').val(<?php echo $this->request->get[nights] ?>);
      $('#number1').html($('#quantity').val())
   
     $('.checkin').datepicker({
                minDate: "<?php echo date('d/m/Y', strtotime(' +5 day')) ?>" ,
                dateFormat: 'dd/mm/yy',
                 onSelect: function(dateText, inst) {
                     calculateCheckOut();
                     checkRoomAvailable();
                 },
                 beforeShowDay : function(date){
                     
                    var y = date.getFullYear().toString(); // get full year
                    var m = (date.getMonth() + 1).toString(); // get month.
                    var d = date.getDate().toString(); // get Day
                    if(m.length == 1){ m = '0' + m; } // append zero(0) if single digit
                    if(d.length == 1){ d = '0' + d; } // append zero(0) if single digit
                    var currDate = y+'-'+m+'-'+d;
                    if(dates.indexOf(currDate) >= 0){
                    return [true, "ui-highlight"];
                    }else if(peaks.indexOf(currDate) >= 0){
                    return [true, "ui-highlightpeak"];
                    }else if(offs.indexOf(currDate) >= 0){
                    return false
                    }else if(school.indexOf(currDate) >= 0){
                    return [true, "ui-highlightschool"];
                    }else{
                        if(weekend == 2){
                              if(date.getDay() == 4 || date.getDay() == 5){
                                  return [true, "ui-highlightweekend"];
                              }
                          }else{
                              if(date.getDay() == 5 || date.getDay() == 6){
                                  return [true, "ui-highlightweekend"];
                              }
                        }
                    }
                    return [true];
                    }
     });
     
     
      function highlightDays(date) {
        for (var i = 0; i < dates.length; i++) {
            if (new Date(dates[i]).toString() == date.toString()) {              
                return [true, 'highlight', tips[i]];
            }
        }
        return [true, ''];
     } 
    
    calculateCheckOut();
    checkRoomAvailable();
    
    
    
  });
  
  function calculateCheckOut(){
      if($('.checkin').val() != ''){
                var temp_checkin = $('.checkin').val().split('/');
                var checkin_real = temp_checkin[2]+'-'+temp_checkin[1]+'-'+temp_checkin[0]
                var checkin = new Date(checkin_real);
                var checkout = checkin;
                checkout.setDate(checkin.getDate() + parseInt($('#room-duration').val()));
                var checkout_real = ('0' + checkout.getDate()).slice(-2)+'/'+('0' + (checkout.getMonth()+1)).slice(-2)+'/'+checkout.getFullYear()
                $('.checkout').val(checkout_real);
         }else{
                

          }
}
  
            $('#button-cart').bind('click', function() {
    $.ajax({
    url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
            dataType: 'json',
            success: function(json) {
            $('.success, .warning, .attention, information, .error').remove();
                    if (json['error']) {
            if (json['error']['option']) {
            for (i in json['error']['option']) {
                alert(json['error']['option'][i]);
            $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
            }
            }else if(json['error']){
                alert(json['error'])
            }else{
                alert(json['error2'])
                
            }
            }

            if (json['success']) {
            window.location = "index.php?route=checkout/cart";
            }
            }
    });
    });
//--></script>

<script type="text/javascript"><!--
$('#review .pagination a').bind('click', function() {
    $('#review').fadeOut('slow');
            $('#review').load(this.href);
            $('#review').fadeIn('slow');
            return false;
    });
            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
            $('#button-review').bind('click', function() {
    $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function() {
            $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
            $('#button-review').attr('disabled', false);
                    $('.attention').remove();
            },
            success: function(data) {
            if (data['error']) {
            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
            }

            if (data['success']) {
            $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
            }
            }
    });
    });
            function radio_click(qty, name, id,image,popup){
                
               if(image != ''){
                    $('#image').attr('src',image)
                    $('#main-popup').attr('href',popup)
            }
                
            option_quantity = qty;
                    if ($('#quantity').val() > option_quantity){
                        var total_dec = $('#quantity').val() - option_quantity;
                        for(i=0;i<total_dec;i++){
                            decrement();
                        }
            }
            $(".radio-box").removeClass("active");
                    $("#" + id).addClass("active");
                    $("#option-value-" + id).prop('checked', true);
            }

    $('#resta1').click(function() {
    decrement();
    checkRoomAvailable()
    });
            $('#suma1').click(function() {
    increment();
    checkRoomAvailable()
    });
            function increment() {
                
            if (option_required == true && option_quantity == ''){
            alert('<?php echo $text_require_option ?>');
                    return 0;
            }
            if (option_quantity != ''){
            var max_qty = option_quantity
            } else{
            var max_qty = <?php echo $quantity ?> ;
            }
            $('#quantity').val(function(i, oldval) {
            var old = oldval;
                    var newval = ++oldval;
                    if (oldval > max_qty){
            return old;
            } else{
            return newval;
            }
            });
                    $('#number1').html($('#quantity').val())
            }

    function decrement() {
    $('#quantity').val(function(i, oldval) {
    var old = oldval;
            var newval = --oldval;
            if (oldval < <?php echo $minimum ?> ){
    return old;
    } else{
    return newval;
    }
    });
            $('#number1').html($('#quantity').val())

    }
    
    function checkRoomAvailable(){
        var data = {
            'product_id' : '<?php echo $product_id ?>',
            'checkin' : $('.checkin').val(),
            'checkout' : $('.checkout').val(),
            'quantity' : $('#quantity').val()
        }
        
        $.ajax({
             url: 'index.php?route=product/product/checkRoomAvailable',
            type: 'post',
            dataType: 'json',
            data: data,
            success: function(data) {
                $.each( data, function( key, value ) {
                    if(value == false){
                     $("#full-"+key).show('slow')
                     $( "input[roomid='"+key+"']" ).css('display','none');
                    }else{
                      $("#full-"+key).hide('slow')
                        $( "input[roomid='"+key+"']" ).css('display','block');
                    }
                });
            }
    });
    }
   
    
    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
            $('#button-review').bind('click', function() {
    $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function() {
            $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
            $('#button-review').attr('disabled', false);
                    $('.attention').remove();
            },
            success: function(data) {
            if (data['error']) {
            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
            }

            if (data['success']) {
            $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
            }
            }
    });
    });

//--></script>
<div class='clearfix'></div>
