<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <h2><?php echo $heading_title; ?></h2>
            <div class="col-md-3">
                <?php echo $column_left ?>
            </div>
            <div id="content" class="col-md-9">
                <?php echo $content_top; ?>
                <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="address">
                    <h2><?php echo $text_edit_address; ?></h2>
                    <div class="form-group">
                        <label for="firstname" class="required"><?php echo $entry_firstname; ?></label>
                        <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $firstname; ?>" autofocus />
                        <?php if ($error_firstname) { ?>
                        <span class="error"><?php echo $error_firstname; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="required"><?php echo $entry_lastname; ?></label>
                        <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $lastname; ?>" />
                        <?php if ($error_lastname) { ?>
                        <span class="error"><?php echo $error_lastname; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group"> 
                        <label for="company"><?php echo $entry_company; ?></label>
                        <input type="text" class="form-control" id="company" name="company" value="<?php echo $company; ?>" />
                    </div> 
                    <?php if ($company_id_display) { ?>
                    <div id="company-id-display" class="form-group">      
                        <label for="company_id"><span id="company-id-required" class="required"></span><?php echo $entry_company_id; ?></label>
                        <input type="text" class="form-control" id="company_id" name="company_id" value="<?php echo $company_id; ?>" />
                        <?php if ($error_company_id) { ?>
                        <span class="error"><?php echo $error_company_id; ?></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if ($tax_id_display) { ?>
                    <div id="tax-id-display" class="form-group">
                        <label for="tax_id"><span id="tax-id-required" class="required"></span><?php echo $entry_tax_id; ?></label>
                        <input type="text" class="form-control" id="tax_id" name="tax_id" value="<?php echo $tax_id; ?>" />
                        <?php if ($error_tax_id) { ?>
                        <span class="error"><?php echo $error_tax_id; ?></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="address_1" class="required"><?php echo $entry_address_1; ?></label>
                        <input type="text" class="form-control" id="address_1" name="address_1" value="<?php echo $address_1; ?>" />
                        <?php if ($error_address_1) { ?>
                        <span class="error"><?php echo $error_address_1; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="address_2"><?php echo $entry_address_2; ?></label>
                        <input type="text" class="form-control" id="address_2" name="address_2" value="<?php echo $address_2; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="city" class="required"><?php echo $entry_city; ?></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $city; ?>" />
                        <?php if ($error_city) { ?>
                        <span class="error"><?php echo $error_city; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label id="postcode-required" for="postcode" class="required"><?php echo $entry_postcode; ?></label>
                        <input type="text" class="form-control" id="postcode" name="postcode" value="<?php echo $postcode; ?>" />
                        <?php if ($error_postcode) { ?>
                        <span class="error"><?php echo $error_postcode; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="countr_id" class="required"><?php echo $entry_country; ?></label>
                        <select class="form-control" id="country_id" name="country_id">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        <?php if ($error_country) { ?>
                        <span class="error"><?php echo $error_country; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="zone_id" class="required"><?php echo $entry_zone; ?></label>
                        <select class="form-control" id="zone_id" name="zone_id"></select>
                        <?php if ($error_zone) { ?>
                        <span class="error"><?php echo $error_zone; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="default"><?php echo $entry_default; ?></label>
                        <?php if ($default) { ?>
                        <input type="radio" id="default" name="default" value="1" checked="checked" />
                        <?php echo $text_yes; ?>
                        <input type="radio" id="default" name="default" value="0" />
                        <?php echo $text_no; ?>
                        <?php } else { ?>
                        <input type="radio" id="default" name="default" value="1" />
                        <?php echo $text_yes; ?>
                        <input type="radio" id="default" name="default" value="0" checked="checked" />
                        <?php echo $text_no; ?>
                        <?php } ?>
                    </div>
                    <div class="pull-left">
                        <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                    </div>
                    <div class="pull-right">
                        <a onclick="$('#address').submit();" class="btn btn-primary"><?php echo $button_continue; ?></a>
                    </div>
                </form>
                <?php echo $content_bottom; ?>
            </div>
        </div>
        <script type="text/javascript"><!--
        $('select[name=\'country_id\']').bind('change', function () {
                $.ajax({
                    url: 'index.php?route=account/address/country&country_id=' + this.value,
                    dataType: 'json',
                    beforeSend: function () {
                        $('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                    },
                    complete: function () {
                        $('.wait').remove();
                    },
                    success: function (json) {
                        if (json['postcode_required'] == '1') {
                            $('#postcode-required').show();
                        } else {
                            $('#postcode-required').hide();
                        }

                        html = '<option value=""><?php echo $text_select; ?></option>';

                        if (json['zone'] != '') {
                            for (i = 0; i < json['zone'].length; i++) {
                                html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                                if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                                    html += ' selected="selected"';
                                }

                                html += '>' + json['zone'][i]['name'] + '</option>';
                            }
                        } else {
                            html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                        }

                        $('select[name=\'zone_id\']').html(html);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            });

            $('select[name=\'country_id\']').trigger('change');
            //--></script>
    </div>
</section>
<?php echo $footer; ?>