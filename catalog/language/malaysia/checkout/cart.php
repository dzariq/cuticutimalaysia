<?php
// Heading  
$_['heading_title']          = 'Bakul Belanja';

// Text
$_['text_success']           = 'Berjaya: Anda telah menambahkan <a href="%s">%s</a> kedalam <a href="%s">bakul belanja</a>!';
$_['text_remove']            = 'Berjaya: Anda telah mengubah isi bakul belanja Anda!';
$_['text_coupon']            = 'Berjaya: Kupon diskon Anda Berjaya digunakan!';
$_['text_voucher']           = 'Berjaya: Voucher Hadiah Anda Berjaya digunakan!';
$_['text_reward']            = 'Berjaya: Point Reward Anda Berjaya digunakan!';
$_['text_shipping']          = 'Berjaya: Anggaran bayaran ditetapkan!';
$_['text_login']             = 'Perhatian: Anda harus <a href="%s">login</a> atau <a href="%s">membuat Akaun</a> untuk melihat harga!';
$_['text_points']            = 'Point Hadiah: %s';
$_['text_items']             = '%s item(s) - %s';
$_['text_next']              = 'Apa yang akan Anda lakukan selanjutnya?';
$_['text_next_choice']       = 'Pilih jika Anda memiliki Kupon Diskaun atau ingin mengira cas pengiriman.';
$_['text_use_coupon']        = 'Gunakan Kupon Diskaun';
$_['text_use_voucher']       = 'Gunakan Voucher Hadiah';
$_['text_use_reward']        = 'Gunakan Point Reward (Tersedia %s)';
$_['text_shipping_estimate'] = 'Anggaran cas pengiriman';
$_['text_shipping_detail']   = 'Masukkan kawasan anda untuk mendapatkan anggaran cas pengiriman.';
$_['text_shipping_method']   = 'Sila pilih cara pengiriman yang anda sukai.';
$_['text_empty']             = 'Bakul Belanja Anda kosong!';

// Column
$_['column_image']           = 'Gambar';
$_['column_name']            = 'Nama Produk';
$_['column_model']           = 'Model';
$_['column_quantity']        = 'Kuantiti';
$_['column_price']           = 'Harga per Unit';
$_['column_total']           = 'Total';

// Entry
$_['entry_coupon']           = 'Masukkan kupon Anda disini:';
$_['entry_voucher']          = 'Masukkan Voucher Hadiah Anda disini:';
$_['entry_reward']           = 'Point yang akan digunakan (Maks %s):';
$_['entry_country']          = 'Negara:';
$_['entry_zone']             = 'Negeri:';
$_['entry_postcode']         = 'Poskod:';

// Error
$_['error_stock']            = 'Produk yang ditandai dengan *** sedang tidak ada stok!';
$_['error_minimum']          = 'Jumlah pesanan minimum untuk %s adalah %s!';	
$_['error_required']         = '%s diperlukan!';	
$_['error_product']          = 'Peringatan: Tidak ada produk dalam bakul Anda!';
$_['error_coupon']           = 'Peringatan: Kupon tidak valid, atau telah mencapai limit penggunaan!';
$_['error_voucher']          = 'Peringatan: Voucher Hadiah tidak valid atau telah habis digunakan!';
$_['error_reward']           = 'Peringatan: Silah masukkan jumlah Point Reward yang akan digunakan!';	
$_['error_points']           = 'Peringatan: Anda tidak memiliki Point Reward!';
$_['error_maximum']          = 'Peringatan: Jumlah mata maksimum yang dapat dipakai adalah %s!';
$_['error_postcode']         = 'Poskod harus terdiri dari 2 hingga 10 karakter!';
$_['error_country']          = 'Silah pilih Negara!';
$_['error_zone']             = 'Silah pilih Negeri!';
$_['error_shipping']         = 'Peringatan: Kaedah pengiriman diperlukan!';
$_['error_no_shipping']      = 'Peringatan: Tidak ada opsyen pengiriman yang tersedia. Silah <a href="%s">hubungi kami</a>!';
?>