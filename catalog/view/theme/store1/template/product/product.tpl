<?php echo $header ?>
<!-- Description -->
<div class="productpage">
    <section class="description results_wide grid_12">

        <a href="<?php echo $thumbpopup ?>" class="main_image fancybox"><img src="<?php echo $thumb ?>" alt="" /></a>

        <div class="nonboxsize">
            <span>
                <span class="boxed"><?php echo $manufacturer ?></span>
            </span>
            <span>
                <span class="stars boxed">
                    <img src="catalog/view/theme/default/image/stars-<?php echo $rating.'.png'; ?>" alt="<?php echo $review['reviews']; ?>" />
                </span>
            </span>

        </div>
        <div style="padding:10px">
            <?php echo $description ?>

        </div>

    </section>

    <!-- Image gallery -->
    <section class="gallery grid_12">

        <!-- Slider navigation -->
        <nav class="slider_nav">
            <a href="#" class="left">&nbsp;</a>
            <a href="#" class="right">&nbsp;</a>
        </nav>

        <!-- Slider -->
        <div class="slider_wrapper">

            <!-- Slider content -->
            <div class="slider_content">
                <?php foreach ($images as $image) { ?>
                <a href="<?php echo $image['popup'] ?>">
                    <img src="<?php echo $image['thumb'] ?>" alt="" />
                </a>
                <?php } ?>
            </div>

        </div>

    </section>

    <div class="clearfix"></div>

    <?php  if($section1 != ''){ ?>
    <div class="grid_12">
        <h1 style="color:#333">Room Rates</h1>
    </div>
    <!-- Simple text -->
    <section class="text grid_12">
        <?php echo $section1 ?>
    </section>

    <div class="clearfix"></div>

    <?php } ?>

    <!-- Simple text -->

    <?php if(count($section2_images) != 0){ ?>
    <div class="grid_12">
        <h1 style="color:#333">Galleries</h1>
    </div>
    <div class="grid_12">
        <?php foreach($section2_images as $image){ ?>
        <div class="col-xs-3  image_thumbnail">
            <a href="<?php echo $image[popup] ?>">
                <img class="img-responsive thumbnail" src="<?php echo $image[main_thumb] ?>" alt="" />
                <h4 style="font-weight: bold;color:#333;height:25px;text-align: center"><?php echo $image[caption] ?></h4>
            </a>
        </div>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
    <?php } ?>


    <?php if(count($section3_images) != 0){ ?>
    <div class="grid_12">
        <h1 style="color:#333">Facilities</h1>
    </div>


    <div id="slider2" class="image_slider image_sliderslide grid_4">
        <!-- Navigation -->
        <?php if(count($section3_images) > 1){ ?>
        <nav class="slider_nav">
            <a href="#" class="left">&nbsp;</a>
            <a href="#" class="right">&nbsp;</a>
        </nav>
        <?php } ?>
        <!-- Content -->
        <div class="slides">
            <?php foreach($section3_images as $image){ ?>
            <a href="<?php echo $image[popup] ?>">
                <img class="img-responsive" src="<?php echo $image[main_thumb] ?>" alt="" />
            </a>
            <?php } ?>
        </div>
    </div>
    <div class="grid_8" >
        <?php echo $section3 ?>
    </div>
    <div class="clearfix"></div>
    <?php } ?>


    <?php  if($section4 != ''){ ?>
    <section class="faq grid_12">
        <div class="left">
            <h2 class="section_heading">Itenararies</h2>
            <ul>
                <?php $i=1; foreach($section4_images as $image){ ?>

                <li>
                    <h3><span class="number"><?php echo $i ?></span> <?php echo $image['caption'] ?></h3>
                    <p><?php echo $image['description'] ?></p>
                </li>
                <?php $i++; } ?>
            </ul>

        </div>
        <div class="right">
            <div class="cool-block">
                <?php echo $section4 ?>
            </div>
        </div>
    </section>

    <?php } ?>
    
    
    <?php if(count($section5_images) != 0 && $section5 != ''){ ?>
    <div class="grid_12">
        <h1 style="color:#333">Packages</h1>
    </div>
    <div class="  col-xs-6">
        <ul class=" bxslider" style="display:none">
            <?php foreach($section5_images as $banner){ ?>
           <li class='fit-height banners' >
               <a class="fancybox" href="<?php echo $banner[popup] ?>">
                   <img  style="margin:0 auto;" class="img-responsive" src="<?php echo $banner[main_thumb] ?>" />
               </a>
               <h3 style='text-align: center;
  z-index: 99999;
  position: absolute;
  margin-top: -68px;
  font-size: 22px;
  padding: 12px;
  color: white;
  background: rgba(0,0,0,0.3);'><?php echo $banner['caption'] ?></h3>
           </li>
            <?php } ?>
        </ul>
    </div>
    <div class="col-xs-6" >
        <?php echo $section5 ?>
    </div>
    <div class="clearfix"></div>
    <?php } ?>



    <div class="grid_12">
        <h1 style="color:#333">What Other People Say</h1>
    </div>

    <div id="review"></div>

    <div class="clearfix"></div>
        <?php if (isset($pmLocations) && !empty($pmLocations)) { ?>
    <div class="grid_12">
        <h1 style="color:#333">Location Map</h1>
    </div>
    <section class="contact_map grid_12">
        <div >
            <style>
                #pmFrom {
                    display: block;
                    width: 90%;
                    margin: 0 auto;
                }

                #pmDirectionsBtn {
                    display: block;
                    margin: 10px auto;
                }

                #pmDirWarning {
                    display: block;
                    margin: 5px auto;
                    width: 81%;
                }

                #pmDirDiv {
                    display: inline;
                    width: 40%;
                    padding:5px;
                    float: left;
                }

                #pmMapDiv {
                    display: inline;
                    width: 60%;
                    padding:5px;
                    float: left;
                }
            </style>

            <?php 
            echo "<div id='pmDirDiv'>";
            echo "<input style='text-align:center' id='pmTo' value='{$pmLocations[0]['address']}' type='hidden' />";
            echo "<input style='width:200px' id='pmFrom' placeholder='Enter your start location here' type='text' />";
            echo "<input style='width:100px;line-height:0px' class='mybutton' id='pmDirectionsBtn' type='button' value='$button_get_directions'>";
            echo "<div id='pmDirectionsList'></div>";
            echo "</div>";
            echo "<div id='pmMapDiv'>";
            echo "<div style='margin: 0 auto; width:{$pmLocations[0]['width']}px; height:{$pmLocations[0]['height']}px;' id=\"pmMap{$pmLocations[0]['id']}\"></div>";
            echo "</div>";
            ?>

            <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

            <script type="text/javascript">
                $(function() {
                function createMap(e, lat, lng, zoom, width, height) {
                var gmLatlng = new google.maps.LatLng(lat, lng);
                        var gmOpt = {
                        zoom: zoom,
                                center: gmLatlng,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                        }
                var map = new google.maps.Map(document.getElementById(e), gmOpt);
                        var marker = new google.maps.Marker({
                        position: gmLatlng,
                                map: map,
                                draggable:true
                        });
                        return map;
                }


                function generateRoute(map) {
                $("#pmDirWarning").remove();
                        $("#pmDirectionsList").empty();
                        var directionsService = new google.maps.DirectionsService();
                        var directionsDisplay = new google.maps.DirectionsRenderer();
                        var start = document.getElementById("pmFrom").value;
                        var end = document.getElementById("pmTo").value;
                        var request = {
                        origin:start,
                                destination:end,
                                travelMode: google.maps.TravelMode.DRIVING
                        };
                        directionsService.route(request, function(result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setMap(map);
                                directionsDisplay.setPanel(document.getElementById("pmDirectionsList"));
                                directionsDisplay.setDirections(result);
                        } else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
                        $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_route_not_found; ?></div>');
                        } else if (status == google.maps.DirectionsStatus.NOT_FOUND) {
                        $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_address_not_found; ?></div>');
                        } else {
                        $("#pmFrom").before('<div class="warning" id="pmDirWarning">' + status + '</div>');
                        }
                        });
                }

                $('#pmDirectionsBtn').bind('click', function(event, ui) {
                    
                <?php
                        echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                        ?>
                        generateRoute(map);
                });
                    <?php
                        echo "createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                  ?>
                });            </script>
        </div>
    </section>
    <?php } ?>
    <br/>
    <br/>
    <!-- Reservation form -->
    <section class="search_box sidebar grid_4">
        <h2>For Reservation</h2>
        <form id="booking-form" action="index.php?route=information/contact" method="post" enctype="multipart/form-data">
            <div class="content" >
                <label><span class="required">*</span>Name</label><br />
                <input class="form-control" type="text" id="name" value="<?php echo $this->customer->getFirstName() ?>" />
                <br />
                <label><span class="required">*</span>Email</label><br />
                <?php if($this->customer->getEmail() != ''){ ?>
                <input class="form-control" readonly='readonly' type="text" id="email" value="<?php echo $this->customer->getEmail() ?>" />
                <?php }else{ ?>
                <input class="form-control" type="text" id="email" value="" />
                <?php } ?>
                <br />
                <label><span class="required">*</span>Address</label><br />
                <?php if($getAddress != ''){ ?>
                <textarea style="height:120px" id="address">
                    <?php echo $getAddress ?>
                </textarea>
                <?php }else{ ?>
                 <textarea style="height:120px" id="address">
                </textarea>
                <?php } ?>
                <br />
                <label><span class="required">*</span>Phone</label><br />
                <input class="form-control" type="text" id="phone" value="<?php echo $this->customer->getTelephone() ?>" />
                <br />
                <label><span class="required">*</span>Hotel/Resort Preferred</label><br />
                <input class="form-control" type="text" id="destinasi" value="<?php echo $heading_title ?>" />
                <br />
                <label>Room Type</label><br />
                <input class="form-control" type="text" id="roomtype" value="" />
                <br />
                <div class="half">
                    <label>Chhek-in Date</label><br />
                    <input class="form-control datep " type="text" id="tarikhmula" value="" />
                </div>
                <div class="half last">
                    <label>Check-out Date</label><br />
                    <input class="form-control datep " type="text" id="tarikhtamat" value="" />
                </div>
                <div class="half last">
                    <label>No. of pax (child below 3 is free)</label>
                    <select name="pax" id="pax">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <label>Message</label><br />
                <textarea class="form-control" id="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
            </div>
            <br/>
            <div class="buttons">
                <div tabindex="1" class="message-success">
                </div>
                <div tabindex="1" class="message-error">
                </div>
                <div class="right"><input style="margin-top:20px;color:#333" type="button" onclick="submitContact()" value="Submit" class="btn btn-primary" /></div>
            </div>

        </form>

    </section>
    <section class="grid_4">
        <h2>Terms & Conditions</h2>
        <br/>
        <br/>
        <p style="text-align:justify">
            1. FREE BOOKINGS FOR RESERVATIONS MADE ONE MONTH OR EARLIER
            <br/>
            <br/>

            2. DEPOSIT RM50.00 IS REQUIRED FOR RESERVATIONS WITHIN ONE MONTH BOOKING DATES AND FULL PAYMENT IS REQUIRED FOR RESERVATIONS WITHIN 3 WORKING DAYS.
         <br/>
            <br/>
            3. WE CANNOT GUARANTEE SUCCESSFUL RESERVATIONS DURING PEAK, SUPERPEAK OR BLOCKED OUT PERIOD, HOWEVER WE SHALL REFUND EACH UNSUCCESSFUL RESERVATIONS WITHIN 3 WORKING DAYS IN FULL.
        <br/>
        <br/>
        4. YOU WILL BE NOTIFIED via EMAIL, PHONE CALLS OR MESSAGES YOUR BOOKING STATUS AND YOUR RESERVATIONS WE WILL TREAT AT OUR BEST INTEREST WITHOUT ANY LEGAL OBLIGATIONS SINCE WE WILL NOT HOLDING ANY MONIES EXCEPT FOR OUR PAYMENT PURPOSES FOR ANY RESERVATIONS MADE.
         <br/>
            <br/>
            5. THERE WOULD BE NO REFUND DEPOSITS FOR ANY LAST MINUTE CHANGES WITHIN 3 WORKING DAYS AND NO REFUND FOR NO SHOW OR CANCELLATION FOR ANY GIVEN REASON WITHOUT ANY NOTIFICATION TO US.
        </p>
        <br/>
        <p style="text-align:justify">
           <!-- *Please be patience.. Do not worry, your bookings will reach us very soon…
            <br/>
            <br/>-->
            *WE WILL CONTACT YOU IF YOU REQUEST FOR PACKAGE OR EVENTS
        </p
        <p>
            <h1 style="  text-align: center;
  line-height: 1.3;
  color: black;
  font-size: 24px;">For Online Booking Only</h1>
          <img class="img-responsive" style="margin:0 auto" src="catalog/view/image/rhb-logo.png" />
        <h4 style="  text-align: center;"><b>AMK LOGO RESOURCES</b></h4>

        <h4 style="  text-align: center;"><b>ACC NO: 26225 0000 17980</b></h4>
        <br/>
        <br/>
        <h1 style="  text-align: center;
  line-height: 1.3;
  color: black;
  font-size: 24px;">For Corporate and Events Only</h1>
            <img class="img-responsive" style="margin:0 auto" src="catalog/view/image/m2u-logo.png" />
        <h4 style="  text-align: center;"><b>A & R HOLIDAYS EVENT & TRAVEL SDN BHD</b></h4>

        <h4 style="  text-align: center;"><b>ACC NO: 56227 260 8140</b></h4>
        </p>
    </section>
    <section class="grid_4 grey">
        <div id="tab-review" class="tab-content">



            <h2 id="review-title"><?php echo $text_write; ?></h2>
            <br/>
            <br/>
            <b><?php echo $entry_name; ?></b><br />

            <input type="text" name="name" value="" />

            <br />

            <br />

            <b><?php echo $entry_review; ?></b>

            <textarea  class="grey" name="text" cols="40" rows="8" style="padding:8px;background:#f4f4f4;width: 98%;"></textarea>

            <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />

            <br />

            <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;

            <input class="ratingbox" type="radio" name="rating" value="1" />

            &nbsp;

            <input class="ratingbox" type="radio" name="rating" value="2" />

            &nbsp;

            <input class="ratingbox" type="radio" name="rating" value="3" />

            &nbsp;

            <input class="ratingbox" type="radio" name="rating" value="4" />

            &nbsp;

            <input class="ratingbox" type="radio" name="rating" value="5" />

            &nbsp;<span><?php echo $entry_good; ?></span><br />

            <br />

            <b><?php echo $entry_captcha; ?></b><br />

            <input type="text" name="captcha" value="" />

            <br />

            <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />

            <br />

            <div class="buttons">

                <div class="right"><a id="button-review" style="cursor:pointer" class="mybutton"><?php echo $button_continue; ?></a></div>

            </div>

        </div>
    </section>

    <div class="clearfix"></div>


</div> 
</div> 


<?php echo $footer ?>

<script>
            $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');
            $('#button-review').bind('click', function() {
    $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
            beforeSend: function() {
            $('.success, .warning').remove();
                    $('#button-review').attr('disabled', true);
                    $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
            },
            complete: function() {
            $('#button-review').attr('disabled', false);
                    $('.attention').remove();
            },
            success: function(data) {
            if (data['error']) {
            $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
            }

            if (data['success']) {
            $('#review-title').after('<div class="success">' + data['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');
                    $('input[name=\'captcha\']').val('');
            }
            }
    });
    });
            function radio_click(qty, name, id, image, popup){

            if (image != ''){
            $('#image').attr('src', image)
                    $('#main-popup').attr('href', popup)
            }

            option_quantity = qty;
                    if ($('#quantity').val() > option_quantity){
            var total_dec = $('#quantity').val() - option_quantity;
                    for (i = 0; i < total_dec; i++){
            decrement();
            }
            }
            $(".radio-box").removeClass("active");
                    $("#" + id).addClass("active");
                    $("#option-value-" + id).prop('checked', true);
            }

    $('#resta1').click(function() {
    decrement();
    });
            $('#suma1').click(function() {
    increment();
    });
            function increment() {
            if (option_required == true && option_quantity == ''){
            alert('<?php echo $text_require_option ?>');
                    return 0;
            }
            if (option_quantity != ''){
            var max_qty = option_quantity
            } else{
            var max_qty = <?php echo $quantity ?> ;
            }

            $('#quantity').val(function(i, oldval) {
            var old = oldval;
                    var newval = ++oldval;
                    if (oldval > max_qty){
            return old;
            } else{
            return newval;
            }
            });
                    $('#number1').html($('#quantity').val())
            }

    function decrement() {
    $('#quantity').val(function(i, oldval) {
    var old = oldval;
            var newval = --oldval;
            if (oldval < <?php echo $minimum ?> ){
    return old;
    } else{
    return newval;
    }
    });
            $('#number1').html($('#quantity').val())

    }

    $(function () {

    $("#tarikhmula").datepicker({dateFormat: 'dd-mm-yy'});
            $("#tarikhtamat").datepicker({dateFormat: 'dd-mm-yy'});
    });
            function validateForm() {

            if ($('#name').val().length < 3 || $('#name').val().length > 32) {
            alert('Please key in your name between 2 to 32 characters');
                    return false;
            }
            if ($('#email').val().length < 3 || $('#email').val().length > 32) {
            alert('Please key in your valid email address');
                    return false;
            }
            if ($('#phone').val().length < 3 || $('#phone').val().length > 32) {
            alert('Please key in your valid phone number');
                    return false;
            }
            if ($('#destinasi').val().length < 3 || $('#destinasi').val().length > 100) {
            alert('Please key in your destination');
                    return false;
            }
            if ($('#address').val().length < 3 || $('#address').val().length > 100) {
            alert('Please key in your address');
                    return false;
            }

            return true;
            }

    function submitContact() {

    var status = true;
            status = validateForm();
            $('.message-error').hide()
            $('.message-success').hide()
            $('.message-error').html('')
            $('.message-success').html('');
            if (status) {

    var data = {
    'name': $('#name').val(),
            'email': $('#email').val(),
            'details_phone_text': $('#phone').val(),
            'details_address_text': $('#address').val(),
            'details_destination_text': $('#destinasi').val(),
            'details_roomtype_text': $('#roomtype').val(),
            'details_pax_int': $('#pax').val(),
            'details_checkin_text': $('#tarikhmula').val(),
            'details_checkout_text': $('#tarikhtamat').val(),
            'enquiry': $('#enquiry').val(),
            'category': 'booking',
            'json': 1
    }

    console.log(data)
            $.ajax({
            url: $('#booking-form').attr('action'),
                    type: "POST",
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                    var html = '';
                            html += data.remark;
                            if (data.status == 1) {

                    $('.message-success').show()
                            $('.message-success').html(html)
                    } else {
                    $('.message-error').show()
                            $('.message-error').html(html)
                    }
                    $('#remark').focus()
                            console.log(data);
                    }
            });
    }

    }

    /* Sidebar image slider */
    $('#slider1').each(function () {

     /* Functions */
    function resetInterval() {
    clearInterval(imageSliderInterval);
            imageSliderInterval = setInterval(next, 8000);
    }
  
    function next() {
    $('#slider1 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider1 .slides').fadeIn(1);
    });
    }
    function previous() {
    $('#slider1 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider1 .slides').fadeIn(1);
    });
    }

    /* Initialize */
    var imageSliderInterval;
            resetInterval();
            /* Controls */
            $('#slider1 .left').click(function () {
    next();
            resetInterval();
    });
            $('#slider1 .right').click(function () {
    previous();
            resetInterval();
    });
    });
            $('#slider2').each(function () {

    /* Functions */
    function resetInterval() {
    clearInterval(imageSliderInterval);
            imageSliderInterval = setInterval(next, 8000);
    }
    function next() {
    $('#slider2 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider2 .slides').fadeIn(1);
    });
    }
    function previous() {
    $('#slider2 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider2 .slides').fadeIn(1);
    });
    }

    /* Initialize */
    var imageSliderInterval;
            resetInterval();
            /* Controls */
            $('#slider2 .left').click(function () {
    next();
            resetInterval();
    });
            $('#slider2 .right').click(function () {
    previous();
            resetInterval();
    });
    });
    
    
    
      $('#slider5').each(function () {

    /* Functions */
    function resetInterval() {
    clearInterval(imageSliderInterval);
            imageSliderInterval = setInterval(next, 8000);
    }
    function next() {
    $('#slider5 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider5 .slides').fadeIn(1);
    });
    }
    function previous() {
    $('#slider5 .slides').children(':last').fadeOut(1000, function () {
    $(this).prependTo('#slider5 .slides').fadeIn(1);
    });
    }

    /* Initialize */
    var imageSliderInterval;
            resetInterval();
            /* Controls */
            $('#slider5 .left').click(function () {
    next();
            resetInterval();
    });
            $('#slider5 .right').click(function () {
    previous();
            resetInterval();
    });
    });
    $(document).ready(function() {
        $('.bxslider').show().bxSlider({
            onSlideBefore: function() {
            },
		onSlideAfter: function() {
                },
            preloadImages: 'all',
            auto:true,
            pause: 5000
        });
    });
</script>

