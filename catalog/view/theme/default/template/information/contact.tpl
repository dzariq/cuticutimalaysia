<?php echo $header; ?>
<div class="<?php echo layout ?>">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb hidden-xs">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ol>
        </div>
        <?php echo $column_left; ?>
        <?php echo $column_right; ?>
        <div id="content" class="col-md-8">
            <?php echo $content_top; ?>
            <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="contact">
                <h2><?php echo $text_location; ?></h2>
                <div class="row">
                    <div class="col-md-6">
                        <strong><?php echo $text_address; ?></strong>
                        <br />
                        <?php echo $store; ?>
                        <br />
                        <address><?php echo $address; ?></address>
                    </div>
                    <div class="col-md-6">
                        <?php if ($telephone) { ?>
                        <strong><?php echo $text_telephone; ?></strong>
                        <br />
                        <?php echo $telephone; ?>
                        <br />
                        <br />
                        <?php } ?>
                        <?php if ($fax) { ?>
                        <strong><?php echo $text_fax; ?></strong>
                        <br />
                        <?php echo $fax; ?>
                        <?php } ?>
                    </div>
                </div>
                <h2><?php echo $text_contact; ?></h2>
                <div class="form-group">
                    <label for="name"><strong><?php echo $entry_name; ?></strong></label>
                    <input type="text" class="form-control" placeholder="Enter your name" name="name" value="<?php echo $name; ?>" />
                    <?php if ($error_name) { ?>
                    <span class="error"><?php echo $error_name; ?></span>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label for="email"><strong><?php echo $entry_email; ?></strong></label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email" value="<?php echo $email; ?>" />
                    <?php if ($error_email) { ?>
                    <span class="error"><?php echo $error_email; ?></span>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label for="enquiry"><strong><?php echo $entry_enquiry; ?></strong></label>
                    <textarea name="enquiry" class="form-control" placeholder="Write your enquiry here" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
                    <?php if ($error_enquiry) { ?>
                    <span class="error"><?php echo $error_enquiry; ?></span>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <label for="captcha"><strong><?php echo $entry_captcha; ?></strong></label>
                    <input type="text" class="form-control" name="captcha" value="<?php echo $captcha; ?>" />
                    <br />
                    <img src="index.php?route=information/contact/captcha" alt="" />
                    <?php if ($error_captcha) { ?>
                    <span class="error"><?php echo $error_captcha; ?></span>
                    <?php } ?>
                </div>
                <div class="pull-right">
                    <a onclick="$('#contact').submit();" class="btn btn-info"><?php echo $button_continue; ?></a>
                </div>
            </form>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<br/>
<?php echo $footer; ?>