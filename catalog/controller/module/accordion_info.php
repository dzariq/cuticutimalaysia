<?php
################################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com    	   #
################################################################################################
class ControllerModuleAccordionInfo extends Controller {
	protected function index($setting) {

static $module = 0;
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
					
		$this->data['limit'] = $setting['limit'];
		$this->data['scroll'] = $setting['scroll'];
		$this->data['title'] = $setting['title'];
		$this->data['description'] = $setting['description'];
		$this->data['link'] = $setting['link'];
		$this->data['linkname'] = $setting['linkname'];
				
		$this->data['banners'] = array();
		
		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		  
	foreach ($results as $result) {
            
            $tagArray = Array();

            if ($result['tagging'] != '') {
                $temp = explode(',', $result['tagging']);
                if (count($temp) != 0) {
                    foreach ($temp as $val) {
                        $taggingArray[] = $val;
                        $tagArray[] = $val;
                    }
                }
            }
            
            if($result['image'] != '' && $result['image'] != 'no_image.jpg'){
                $image = 'image/'.$result['image'];
            }else{
                $image = '';
            }
          
         

            if (file_exists(DIR_IMAGE . $result['image'])) {
                $this->data['banners'][] = array(
                    'title' => $result['title'],
                    'link' => $result['link'],
                    'description' => nl2br($result['description']),
                    'link_name' => $result['link_name'],
                    'tagging' => $tagArray,
                    'image' => $image,
                    'imagelarge' => 'image/'.$result['imagelarge']
                );
            }
        }
        
        $this->data['taggingArray'] = array_unique($taggingArray);
		
		$this->data['module'] = $module++; 
	
		$this->language->load('module/accordion_info');

      	$this->data['heading_title'] = $this->language->get('heading_title');
              	$this->data['text_programs'] = $this->language->get('text_programs');


		$this->load->model('module/accordion_info');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/accordion_info.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/accordion_info.tpl';
		} else {
			$this->template = 'default/template/module/accordion_info.tpl';
		}

		$this->render();
	}
}
?>