<?php

class ModelAccountMembershipTransaction extends Model {

    public function addMembershipTransaction($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "membership_transaction SET customer_id = '" . (int) $data['customer_id'] . "', customer_group_id = '" . $this->db->escape($data['customer_group_id']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', status = '".$data['status']."', date_added = NOW(),"
                . " comment = '".$data['comment']."',total = '" . $data['total'] . "',currency_id = '" . $this->currency->getId() . "',currency_code = '" . $this->currency->getCode() . "',"
                . "currency_value = '" . $this->currency->getValue() . "',address1 = '".$data['address1']."',"
                . "city = '" . $data['city'] . "',postcode = '".$data['postcode']."',"
                . "country_id = '" . $data['country_id'] . "',zone_id = '".$data['zone_id']."',"
                . "salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' ");

                return $this->db->getLastId();
    }

    public function getTransaction($trans_id) {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "membership_transaction WHERE membership_transaction_id = '".$trans_id."' ");
        return $result->row;
    }
    public function update($trans_id,$status) {
        $this->db->query("UPDATE " . DB_PREFIX . "membership_transaction SET status = '".$status."' WHERE membership_transaction_id = '".$trans_id."' ");
    }


}

?>