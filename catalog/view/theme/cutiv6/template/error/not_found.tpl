<?php echo $header; ?>
<section class="innerpage-wrapper">
    <div id="payment-success" style='background: #e0e0e0;' class="section-padding">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">

                    <h2><?php echo $heading_title; ?></h2>
                    <div id="content" class="col-md-12">
                        <?php echo $content_top; ?>
                        <section>
                            <?php echo $text_error; ?>
                        </section>
                        <div class="pull-right">
                            <a href="<?php echo $continue; ?>" ><button class="btn btn-orange"><?php echo $button_continue; ?></button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>