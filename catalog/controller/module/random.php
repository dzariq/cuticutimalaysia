<?php

class ControllerModuleRandom extends Controller {

    protected function index($setting) {
        $this->language->load('module/random');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->data['text_empty'] = $this->language->get('text_empty');
        $this->data['button_continue'] = $this->language->get('button_continue');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['category'])) {
            $category = $this->request->get['category'];
            $this->data['heading_title'] = $category;
        } else {
            $category = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = 9;
        }
        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
            $this->data['heading_title'] = $this->language->get('search_text') . ' ' . $this->request->get['filter_name'];
        } else {
            $filter_name = '';
        }

        $this->data['text_sort'] = $this->language->get('text_sort');

        $this->data['products'] = array();


        $data = array(
            'sort' => $sort,
            'order' => $order,
            'filter_name' => $filter_name,
            'category' => $category,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        );

        if (DB_DATABASE == 'oneweb_shop') {
            $results = $this->model_catalog_product->getRProducts($data);
        } else {
            $results = $this->model_catalog_product->getProducts($data);
        }


        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
            } else {
                $image = false;
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float) $result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = $result['rating'];
            } else {
                $rating = false;
            }
            if (isset($result['database'])) {
                   

                    $temp = explode('_', $result['database']);
                    $domain = $temp[1];

                    $this->data['products'][] = array(
                        'product_id' => $result['product_id'],
                        'thumb' => $image,
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
                        'name' => $result['name'],
                        'price' => $price,
                        'special' => $special,
                        'rating' => $rating,
                        'reviews' => sprintf($this->language->get('text_reviews'), (int) $result['reviews']),
                        'href' => 'http://' . $domain . $result['domain'] . '/index.php?route=product/product&product_id=' . $result['product_id'],
                    );
            } else {
                $this->data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'price' => $price,
                    'special' => $special,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $result['reviews']),
                    'href' => $this->url->link('common/home', 'product_id=' . $result['product_id']),
                );
            }
        }

        $url = '';

        if (isset($this->request->get['limit'])) {
            $url .= '&limit=' . $this->request->get['limit'];
        }
        if (isset($this->request->get['category'])) {
            $url .= '&category=' . $this->request->get['category'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }

        $this->data['sorts'] = array();

        $this->data['sorts'][] = array(
            'text' => $this->language->get('text_default'),
            'value' => 'p.sort_order-ASC',
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
        );


        $this->data['sorts'][] = array(
            'text' => $this->language->get('text_name_asc'),
            'value' => 'pd.name-ASC',
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
        );

        $this->data['sorts'][] = array(
            'text' => $this->language->get('text_name_desc'),
            'value' => 'pd.name-DESC',
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
        );

        $this->data['sorts'][] = array(
            'text' => $this->language->get('text_price_asc'),
            'value' => 'p.price-ASC',
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
        );

        $this->data['sorts'][] = array(
            'text' => $this->language->get('text_price_desc'),
            'value' => 'p.price-DESC',
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
        );

        if ($this->config->get('config_review_status')) {
            $this->data['sorts'][] = array(
                'text' => $this->language->get('text_rating_desc'),
                'value' => 'rating-DESC',
                'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
            );

            $this->data['sorts'][] = array(
                'text' => $this->language->get('text_rating_asc'),
                'value' => 'rating-ASC',
                'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
            );
        }

//        $this->data['sorts'][] = array(
//            'text' => $this->language->get('text_model_asc'),
//            'value' => 'p.model-ASC',
//            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
//        );
//
//        $this->data['sorts'][] = array(
//            'text' => $this->language->get('text_model_desc'),
//            'value' => 'p.model-DESC',
//            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
//        );

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['category'])) {
            $url .= '&category=' . $this->request->get['category'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }


        $this->data['limits'] = array();

        $this->data['limits'][] = array(
            'text' => $this->config->get('config_catalog_limit'),
            'value' => $this->config->get('config_catalog_limit'),
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&limit=' . $this->config->get('config_catalog_limit'))
        );

        $this->data['limits'][] = array(
            'text' => 25,
            'value' => 25,
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&limit=25')
        );

        $this->data['limits'][] = array(
            'text' => 50,
            'value' => 50,
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&limit=50')
        );

        $this->data['limits'][] = array(
            'text' => 75,
            'value' => 75,
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&limit=75')
        );

        $this->data['limits'][] = array(
            'text' => 100,
            'value' => 100,
            'href' => $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&limit=100')
        );

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }
        if (isset($this->request->get['category'])) {
            $url .= '&category=' . $this->request->get['category'];
        }
        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . $this->request->get['filter_name'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['limit'])) {
            $url .= '&limit=' . $this->request->get['limit'];
        }


        if (DB_DATABASE == 'oneweb_shop') {

            $product_total = $this->model_catalog_product->getRProductsTotal($data);
        } else {
            $product_total = $this->model_catalog_product->getTotalProducts($data);
        }

        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->text = $this->language->get('text_pagination');
        $pagination->url = $this->url->link('common/home', 'path=' . $this->request->get['path'] . $url . '&page={page}');

        $this->data['pagination'] = $pagination->render();

        $this->data['sort'] = $sort;
        $this->data['order'] = $order;
        $this->data['limit'] = $limit;


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/random.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/random.tpl';
        } else {
            $this->template = 'default/template/module/random.tpl';
        }

        $this->render();
    }

}

?>