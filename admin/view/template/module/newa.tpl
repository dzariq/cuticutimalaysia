<?php echo $header; ?>

<?php 
	$bgpatterns = array(
		'pattern1' 		=> 'Pattern 1',
		'pattern2' 		=> 'Pattern 2',
		'pattern3' 		=> 'Pattern 3',
		'pattern4' 		=> 'Pattern 4',
		'pattern5' 		=> 'Pattern 5',
		'pattern6' 		=> 'Pattern 6',
		'pattern7' 		=> 'Pattern 7',
		'pattern8' 		=> 'Pattern 8',
		'pattern9' 		=> 'Pattern 9',
		'pattern10' 	=> 'Pattern 10',
		'pattern11' 	=> 'Pattern 11',
		'pattern12' 	=> 'Pattern 12',
		'pattern13' 	=> 'Pattern 13',
		'pattern14' 	=> 'Pattern 14',
		'pattern15' 	=> 'Pattern 15',
		'pattern16' 	=> 'Pattern 16',
		'pattern17' 	=> 'Pattern 17',
		'pattern18' 	=> 'Pattern 18',
		'pattern19' 	=> 'Pattern 19',
		'pattern20' 	=> 'Pattern 20',
		'pattern21' 	=> 'Pattern 21',
		'pattern22' 	=> 'Pattern 22',
		'pattern23' 	=> 'Pattern 23',
		'pattern24' 	=> 'Pattern 24',
		'pattern25' 	=> 'Pattern 25',
		'pattern26' 	=> 'Pattern 26',
		'pattern27' 	=> 'Pattern 27'

	);
	
	$colorschemes = array(
		'theme1' 		=> 'Theme 1',
		'theme2' 		=> 'Theme 2',
		'theme3' 		=> 'Theme 3',
		'theme4' 		=> 'Theme 4',
		'theme5' 		=> 'Theme 5',
		'theme6' 		=> 'Theme 6',
		'theme7' 		=> 'Theme 7',
		'theme8' 		=> 'Theme 8',
		'theme9' 		=> 'Theme 9',
		'theme10' 		=> 'Theme 10',
		'theme11' 		=> 'Theme 11',
		'theme12' 		=> 'Theme 12',
		'theme13' 		=> 'Theme 13',
		'theme14' 		=> 'Theme 14',
		'theme15' 		=> 'Theme 15',
		'theme16' 		=> 'Theme 16',
		'theme17' 		=> 'Theme 17',
		'theme18' 		=> 'Theme 18',
		'theme19' 		=> 'Theme 19',
		'theme20' 		=> 'Theme 20',
		'theme21' 		=> 'Theme 21',
		'theme22' 		=> 'Theme 22',
		'theme23' 		=> 'Theme 23',
		'theme24' 		=> 'Theme 24',
		'theme25' 		=> 'Theme 25',
		'theme26' 		=> 'Theme 26',
		'theme27' 		=> 'Theme 27'

	);
	
	$bgcolors = array(
		'col1' 			=> 'Color 1',
		'col2' 			=> 'Color 2',
		'col3' 			=> 'Color 3',
		'col4' 			=> 'Color 4',
		'col5' 			=> 'Color 5',
		'col6' 			=> 'Color 6',
		'col7' 			=> 'Color 7',
		'col8' 			=> 'Color 8',
		'col9' 			=> 'Color 9',
		'col10' 		=> 'Color 10',
		'col11' 		=> 'Color 11',
		'col12' 		=> 'Color 12',
		'col13' 		=> 'Color 13',
		'col14' 		=> 'Color 14',
		'col15' 		=> 'Color 15',
		'col16' 		=> 'Color 16',
		'col17' 		=> 'Color 17',
		'col18' 		=> 'Color 18',
		'col19' 		=> 'Color 19',
		'col20' 		=> 'Color 20',
		'col21' 		=> 'Color 21',
		'col22' 		=> 'Color 22',
		'col23' 		=> 'Color 23',
		'col24' 		=> 'Color 24',
		'col25' 		=> 'Color 25',
		'col26' 		=> 'Color 26',
		'col27' 		=> 'Color 27'

	);
	
	$s_links = array(
		'facebook' 				=> 'Facebook',
		'twitter' 				=> 'Twitter',
		'googleplus' 			=> 'Google+',
		'pinterest' 			=> 'Pinterest',
		'dribbble' 				=> 'Dribbble',
		'rss' 					=> 'RSS',
		'flickr' 				=> 'Flickr',
		'linkedin' 				=> 'Linkedin',
		'skype' 				=> 'Skype',
		'vimeo' 				=> 'Vimeo',
		'tumblr' 				=> 'Tumblr',
		'behance' 				=> 'Behance',
		'youtube' 				=> 'Youtube',
		'yahoo' 				=> 'Yahoo',
		'stumbleupon' 			=> 'Stumbleupon',
		'forrst' 				=> 'Forrst',
		'instagram' 			=> 'Instagram',
		'amazon' 				=> 'Amazon',
		'lastfm' 				=> 'Lastfm',
		'picasa' 				=> 'Picasa',
		'paypal' 				=> 'Paypal',
		'deviantart' 			=> 'Deviantart',
		'digg' 					=> 'Digg',
		'blogger' 				=> 'Blogger',
		'wordpress' 			=> 'Wordpress',
		'github' 				=> 'Github',
		'spotify' 				=> 'Spotify',
		'dropbox' 				=> 'Dropbox',
		'evernote' 				=> 'Evernote'

	);
?>

<style type="text/css">
	.pclass { color: #666; font-size:0.9em;width: 100%;display: inline; }
	.backgrouns {width:32px; display: inline-block; text-align: center;}
</style>

<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>

<div class="box">

	<div class="heading">
		<h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
		<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
	</div>

	<div class="content">

		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

			<div id="tabs" class="htabs clearfix">
				<a href="#general_settings">General</a>
				<a href="#footer_settings">Footer</a>
				<a href="#social_links">Social Links</a>
				<a href="#custom_css_settings">Custom CSS</a>
			</div>
			
			<div id="general_settings" class="divtab">
			
				<div id="general_settings_tabs" class="vtabs">
					<a href="#options" class="selected">Options</a>
					<a href="#backgroundStyle" style="">Bacgrounds</a>
					<a href="#functions" style="">Functions</a>
				</div>
				
				<div id="options" class="vtabs-content" style="">
					<table class="form">
						<h2>Options</h2>
						<tr>
							<td>Theme Panel:<br /><span class="help">Note: Disable it for display changes. It's just for live preview.</span></td>
							<td>
								<select name="newa[themePanel]">
									<option value="disable" <?php if(isset($newa['themePanel']) && $newa['themePanel']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['themePanel']) && $newa['themePanel']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Layout Style:</td>
							<td>
								<select name="newa[layoutStyle]">
									<option value="boxed" <?php if(isset($newa['layoutStyle']) && $newa['layoutStyle']=='boxed'){ echo 'selected="selected"'; }?>>Boxed</option>
									<option value="wide" <?php if(isset($newa['layoutStyle']) && $newa['layoutStyle']=='wide'){ echo 'selected="selected"'; }?>>Wide</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Color Schemes</td>
							<td>
								<select name="newa[color_schemes]" onchange="$('#scheme-preview').removeClass().addClass('col' + (this.selectedIndex+1));">
									<?php foreach ($colorschemes as $bv => $bc) { ?>
										<option value="<?php echo $bv; ?>" <?php if(isset($newa['color_schemes']) && $newa['color_schemes']==$bv){ echo 'selected="selected"'; }?>><?php echo $bc; ?></option>	
									<?php } ?>
								</select>
								
								<div id="scheme-preview" style="width: 150px;height: 50px;margin: 3px;"></div>
								
							</td>
						</tr>
					</table>
				</div>
				
				<div id="backgroundStyle" class="vtabs-content" style="">
				
					<table class="form">
						<h2>Bacgrounds</h2>
						<tr>
							<td>Background Style:</td>
							<td>
								<select name="newa[backgroundStyle]">
									<option value="image" <?php if(isset($newa['backgroundStyle']) && $newa['backgroundStyle']=='image'){ echo 'selected="selected"'; }?>>Image</option>
									<option value="color" <?php if(isset($newa['backgroundStyle']) && $newa['backgroundStyle']=='color'){ echo 'selected="selected"'; }?>>Color</option>
								</select>
							</td>
						</tr>
					</table>
					<table class="form">	
						<tr>
							<td>Background Color <br /><span class="help">Note: If you are going to use custom color, the "Background Style" field must be "Color".</span></td>
							<td style="width: 110px;">
								<select name="newa[background_color]" onchange="$('#color-preview').removeClass().addClass(this.value);">
									<?php foreach ($bgcolors as $bv => $bc) { ?>
										<option value="<?php echo $bv; ?>" <?php if(isset($newa['background_color']) && $newa['background_color']==$bv){ echo 'selected="selected"'; }?>><?php echo $bc; ?></option>	
									<?php } ?>
								</select>
								
								<div id="color-preview" style="width: 150px;height: 50px;margin: 3px;"></div>
							</td>
							<td style="text-align:center;width:20%;">
							<strong>OR</strong>
							</td>
							<td>Custom Color (Chose your color) <br /><span class="help">Note: If you are going to use a color from list, the "Custom Color" field must be blank.</span></td>
							<td>
								<input type="text" name="newa[custom_background_color]" value="<?php if(isset($newa['custom_background_color'])){ echo $newa['custom_background_color']; } ?>" size="6" class="color {required:false,hash:true}"  />
							</td>
						</tr>
					</table>
					
					<table class="form">
						<tr>
							<td>Background Pattern<br />
								<span class="help">Note: If you are going to use a pattern image, the "Background Style" field must be "Image".</span>
							</td>
							<td style="width: 110px;">
								<select name="newa[background_pattern]" onchange="$('#pattern-preview').removeClass().addClass(this.value);">
									<?php foreach ($bgpatterns as $bv => $bc) { ?>
										<option value="<?php echo $bv; ?>" <?php if(isset($newa['background_pattern']) && $newa['background_pattern']==$bv){ echo 'selected="selected"'; }?>><?php echo $bc; ?></option>	
									<?php } ?>
								</select>
								
								<div id="pattern-preview" style="width: 150px;height: 50px;margin: 3px;"></div>
							</td>
							<td style="text-align:center;width:20%;">
							<strong>OR</strong>
							</td>
							<td>
								Custom Pattern (Upload your pattern) <br /><span class="help">Note: If you are going to use a pattern from list, the "Custom Pattern" field must be blank.</span>
							</td>
							<td>
								<input type="hidden" name="newa[custom_background_pattern]" value="<?php echo $newa['custom_background_pattern']; ?>" id="custom_background_pattern" />
								<img src="<?php if(isset($newa['custom_background_pattern']) && $newa['custom_background_pattern']!=''){ echo HTTP_IMAGE . $newa['custom_background_pattern']; } else {  echo $no_image; }?>" alt="" id="newa_image_preview" width="70" height="70" />
								<br /><a onclick="image_upload('custom_background_pattern', 'newa_image_preview');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#newa_image_preview').attr('src', '<?php echo $no_image; ?>'); $('#custom_background_pattern').attr('value', '');"><?php echo $button_remove; ?></a>
							</td>
					   </tr>
					</table>
				</div>
				
				<div id="functions" class="vtabs-content" style="">
					<table class="form">
						<tr>
							<td>Offer Icon:</td>
							<td>
								<select name="newa[offerIconEnabled]">
									<option value="disable" <?php if(isset($newa['offerIconEnabled']) && $newa['offerIconEnabled']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['offerIconEnabled']) && $newa['offerIconEnabled']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Top Header Search Autocomplete:</td>
							<td>
								<select name="newa[autocompleteEnable]">
									<option value="disable" <?php if(isset($newa['autocompleteEnable']) && $newa['autocompleteEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['autocompleteEnable']) && $newa['autocompleteEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Responsive Design:<br /><span class="help">Optimizing for mobile devices.</span></td>
							<td>
								<select name="newa[responsiveEnable]">
									<option value="disable" <?php if(isset($newa['responsiveEnable']) && $newa['responsiveEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['responsiveEnable']) && $newa['responsiveEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>CloudZoom Plugin:<br /><span class="help">Product image zoom on product details page.</span></td>
							<td>
								<select name="newa[cloudzoomEnable]">
									<option value="disable" <?php if(isset($newa['cloudzoomEnable']) && $newa['cloudzoomEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['cloudzoomEnable']) && $newa['cloudzoomEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td>Product Titles Min. Height:<br /><span class="help">Example: 45px</span></td>
							<td>
							   <input type="text" name="newa[productMinHeight]" value="<?php if(isset($newa['productMinHeight'])){ echo $newa['productMinHeight']; } ?>" size="2"/>px
							</td>
						</tr>
					</table>
				</div>

			</div>

			<div id="footer_settings" class="divtab">
			
				<div id="footer_settings_tabs" class="vtabs">
					<a href="#information" class="selected">Information Module</a>
					<a href="#facebook" style="">Facebook Module</a>
					<a href="#twitter" style="">Twitter Module</a>
					<a href="#custom" style="">Custom Module</a>
					<a href="#others" style="">Others</a>
				</div>
				
				<div id="information" class="vtabs-content" style="">
					<table class="form">
						<h2>Footer Information Module</h2>
						<tr>
							<td>Enable Module:</td>
							<td>
								<select name="newa[infoEnable]">
									<option value="disable" <?php if(isset($newa['infoEnable']) && $newa['infoEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['infoEnable']) && $newa['infoEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Module Title:<br /><span class="help">Example: Abous Us</span></td>
							<td>
							   <input type="text" name="newa[aboutTitle]" value="<?php if(isset($newa['aboutTitle'])){ echo $newa['aboutTitle']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>
								Footer About Text: <br />
								<span class="help">You can use images</span>
							</td>
							<td><textarea name="newa[footer_information]" cols="52" rows="5"><?php if(isset($newa['footer_information'])){ echo $newa['footer_information']; } ?></textarea>
							</td>
						</tr>
					</table>
				</div>
				
				<div id="facebook" class="vtabs-content" style="">
					<table class="form">
						<h2>Facebook Module</h2>
						<tr>
							<td>Enable Module:</td>
							<td>
								<select name="newa[facebookEnable]">
									<option value="disable" <?php if(isset($newa['facebookEnable']) && $newa['facebookEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['facebookEnable']) && $newa['facebookEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Module Title:<br /><span class="help">Example: Find Us On Facebook</span></td>
							<td>
							   <input type="text" name="newa[facebookTitle]" value="<?php if(isset($newa['facebookTitle'])){ echo $newa['facebookTitle']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Facebook Page Link:<br /><span class="help">Example: https://www.facebook.com/envato</span></td>
							<td>
							   <input type="text" name="newa[facebookAccount]" value="<?php if(isset($newa['facebookAccount'])){ echo $newa['facebookAccount']; } ?>" size="50" />
							</td>
						</tr>
					</table>
				</div>
				
				<div id="twitter" class="vtabs-content" style="">
					<table class="form">
						<h2>Twitter Module</h2>
						<tr>
							<td>Enable Module:</td>
							<td>
								<select name="newa[twitterEnable]">
									<option value="disable" <?php if(isset($newa['twitterEnable']) && $newa['twitterEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['twitterEnable']) && $newa['twitterEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Module Title:<br /><span class="help">Example: Follow Us On Twitter</span></td>
							<td>
							   <input type="text" name="newa[twitterTitle]" value="<?php if(isset($newa['twitterTitle'])){ echo $newa['twitterTitle']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Twitter Account:</td>
							<td>
							   <input type="text" name="newa[twitterAccount]" value="<?php if(isset($newa['twitterAccount'])){ echo $newa['twitterAccount']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Count:</td>
							<td>
								<select name="newa[twitterCount]">
									<option value="1" <?php if(isset($newa['twitterCount']) && $newa['twitterCount']=='1'){ echo 'selected="selected"'; }?>>1</option>
									<option value="2" <?php if(isset($newa['twitterCount']) && $newa['twitterCount']=='2'){ echo 'selected="selected"'; }?>>2</option>
									<option value="3" <?php if(isset($newa['twitterCount']) && $newa['twitterCount']=='3'){ echo 'selected="selected"'; }?>>3</option>
									<option value="4" <?php if(isset($newa['twitterCount']) && $newa['twitterCount']=='4'){ echo 'selected="selected"'; }?>>4</option>
									<option value="5" <?php if(isset($newa['twitterCount']) && $newa['twitterCount']=='5'){ echo 'selected="selected"'; }?>>5</option>
								</select>
							</td>
						</tr>
					</table>
				</div>
				
				<div id="custom" class="vtabs-content" style="">
					<table class="form">
						<h2>Custom Footer</h2>
						<tr>
							<td>Enable Module:</td>
							<td>
								<select name="newa[customEnable]">
									<option value="disable" <?php if(isset($newa['customEnable']) && $newa['customEnable']=='disable'){ echo 'selected="selected"'; }?>><?php echo $text_disabled; ?></option>
									<option value="enable" <?php if(isset($newa['customEnable']) && $newa['customEnable']=='enable'){ echo 'selected="selected"'; }?>><?php echo $text_enabled; ?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Module Title:<br /><span class="help">Example: Contact Info</span></td>
							<td>
							   <input type="text" name="newa[customTitle]" value="<?php if(isset($newa['customTitle'])){ echo $newa['customTitle']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Adress:</td>
							<td>
							   <input type="text" name="newa[adress]" value="<?php if(isset($newa['adress'])){ echo $newa['adress']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Telephone:</td>
							<td>
							   <input type="text" name="newa[telephone]" value="<?php if(isset($newa['telephone'])){ echo $newa['telephone']; } ?>" size="50" />
							</td>
						</tr>
						<tr>
							<td>Fax:</td>
							<td>
							   <input type="text" name="newa[fax]" value="<?php if(isset($newa['fax'])){ echo $newa['fax']; } ?>" size="50" />
							</td>
						</tr>
						
						<tr>
							<td>E-mail:</td>
							<td>
							   <input type="text" name="newa[email]" value="<?php if(isset($newa['email'])){ echo $newa['email']; } ?>" size="50" />
							</td>
						</tr>
					</table>
				</div>
				<div id="others" class="vtabs-content" style="">
					<table class="form">
						<h2>Others</h2>
						<tr>
							<td>
								Footer Copyright Text:
							</td>
							<td>
								<input type="text" name="newa[footer_copyright]" value="<?php if(isset($newa['footer_copyright'])){ echo $newa['footer_copyright']; } ?>" size="50" />
							</td>
						</tr>
					</table>
				</div>
			</div>
			
			<div id="social_links" class="divtab">
			
				<div id="social_settings_tabs" class="vtabs">
					<a href="#social_networks" class="selected">Social Networks</a>
				</div>
				
				<div id="information" class="vtabs-content" style="">
					<table class="form">
						<?php foreach ($s_links as $sv => $sc) { ?>				
						<tr>
							<td><?php echo $sc; ?> Link:</td>
							<td>
								<input type="text" name="newa[<?php echo $sv; ?>]" value="<?php if(isset($newa[$sv])){ echo $newa[$sv]; } ?>" size="50" />
							</td>
						</tr>
						<?php } ?>
					</table>
				</div>

			</div>

			<div id="custom_css_settings" class="divtab">
			
				<div id="custom_css_settings_tabs" class="vtabs">
					<a href="#custom_css_code" class="selected">Custom CSS Code</a>
				</div>
				
				<div id="information" class="vtabs-content" style="">
					<table class="form">
						<tr>
							<td colspan="4">
								<h3>Custom CSS</h3>
								<span class="pclass" style="width: 100%;">You can use here for your own CSS code.</span>
							</td>
						</tr>
						<tr>
							<td>Paste your own CSS:</td>
							<td>
								<textarea id="newa[customCss]" name="newa[customCss]" cols="52" rows="20" style="width:80%;"><?php if(isset($newa['customCss'])){ echo $newa['customCss']; } ?></textarea>
							</td>
						</tr>
					</table>
				</div>
			</div>

		</form>

	</div>

</div>

<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#pattern-preview').removeClass().addClass('<?php if(isset($newa['background_pattern']) && $newa['background_pattern']!=''){ echo $newa['background_pattern']; } else { echo 'pattern1'; }?>');
$('#scheme-preview').removeClass().addClass('<?php if(isset($newa['color_schemes']) && $newa['color_schemes']!=''){ echo 'col' . substr($newa['color_schemes'], 5); } else { echo 'theme1'; }?>');
$('#color-preview').removeClass().addClass('<?php if(isset($newa['background_color']) && $newa['background_color']!=''){ echo $newa['background_color']; } else { echo 'col1'; }?>');
//--></script> 

<script type="text/javascript">
	$('#tabs a').tabs();
	$('#general_settings_tabs a').tabs();
	$('#footer_settings a').tabs();
	$('#social_settings_tabs a').tabs();
	$('#custom_css_settings_tabs a').tabs();
</script>

<script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script> 

<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>

<script type="text/javascript">

	CKEDITOR.replace('newa[footer_information]', {
		filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
		filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
	});
</script>
<script type="text/javascript"><!--
function image_upload(field, preview) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
					dataType: 'text',
					success: function(data) {
						$('#' + preview).replaceWith('<img src="' + data + '" alt="" id="' + preview + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 700,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 