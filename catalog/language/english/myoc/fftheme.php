<?php

$_['text_all_categories'] = 'Menu';

$_['text_inside'] = 'Inside';

$_['text_special'] = 'Promotions';

$_['text_contact'] = 'Contact';

$_['text_sitemap'] = 'Sitemap';

$_['text_manufacturer'] = 'Brands';

$_['text_shop_manufacturer'] = 'Shop By Brands';

$_['text_all_manufacturer'] = 'All Brands';

$_['text_stay'] = 'Stay in touch:';

$_['text_buynow'] = 'Buy Now';

$_['text_more'] = 'More';

$_['text_more_special'] = 'View more Special Offers';



$_['tab_gallery'] = 'Gallery';



$_['text_extend_contact'] = 'Our Contact';

$_['text_extend_tweet'] = 'Recent Tweets';

$_['text_extend_fblike'] = 'Like Us';



$_['text_account_welcome'] = 'Hello, %s!';



$_['text_copyright'] = 'Ecommerce made and designed by <a href="http://oneweb.my/" target="_blank">OneWeb.my</a>.';



$_['text_success'] = 'Theme settings saved.';

$_['error_save'] = 'Error saving theme settings!';
?>