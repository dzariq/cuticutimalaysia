<?php
/*

This file is auto generated from a template file

*/

$_['heading_title'] 			= '{HJ_CLASS}';
$_['text_module']	 			= 'Modules';

$_['entry_layout'] 				= 'Layout';
$_['entry_limit'] 				= 'Limit';
$_['entry_image'] 				= 'Image (W x H)';
$_['entry_position'] 			= 'Position';
$_['entry_status'] 				= 'Status';
$_['entry_sort_order'] 			= 'Sort Order';
{HJ_CATEGORY_SELECTOR}$_['entry_category'] = 'Select Categories';{/HJ_CATEGORY_SELECTOR}
{HJ_MANUFACTURER_SELECTOR}$_['entry_manufacturer'] = 'Select Manufacturers';{/HJ_MANUFACTURER_SELECTOR}
//{HJ_PRODUCT_SELECTOR}$_['entry_product'] = 'Select Products';{/HJ_PRODUCT_SELECTOR}

$_['text_content_top']			= 'Content Top';
$_['text_content_bottom']		= 'Content Bottom';
$_['text_column_left']			= 'Column Left';
$_['text_column_right']			= 'Column Right';

$_['text_success']				= 'Success! You have modified {HJ_CLASS}';


{HJ_LANGUAGE_FULL}

?>