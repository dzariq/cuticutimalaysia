<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li class="active"><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
                                <h2 class="dash-content-title">My Profile</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h4>Profile Details</h4></div>
                                    <div class="panel-body">
                                        <div class="row">

                                            <div class="col-sm-12  user-detail">
                                                <ul class="list-unstyled">
                                                    <li><span>Name:</span> <?php echo $name ?></li>
                                                    <li><span>Email:</span> <?php print_r($email) ?></li>
                                                    <li><span>Phone:</span> <?php print_r($telephone) ?></li>
                                                    <li><span>Address:</span>  <?php echo $my_address['address_1'] ?>,<br>
                                                        <?php if($my_address['address_2'] != ''){echo $my_address['address_2'].','; } ?> <br>
                                                        <?php if($my_address['postcode'] != ''){echo $my_address['postcode'].','; } ?> <br>
                                                        <?php echo $my_address['city'] ?>,<br>
                                                        <?php echo $my_address['zone'] ?><br>
                                                    <li><span>Country:</span> <?php echo $my_address['country'] ?></li>
                                                </ul>
                                                <a href='index.php?route=account/edit'><button class="btn"  >Edit Profile</button></a>
                                                <a href='index.php?route=account/address'><button class="btn"  >Change Address</button></a>
                                                <a href='index.php?route=account/password'><button class="btn"  >Change Password</button></a>
                                            </div><!-- end columns -->

                                          
                                        </div><!-- end row -->

                                    </div><!-- end panel-body -->
                                </div><!-- end panel-detault -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 