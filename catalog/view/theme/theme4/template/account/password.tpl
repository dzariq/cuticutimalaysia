<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <h2><?php echo $heading_title; ?></h2>

            <div class="col-md-3">
                <?php echo $column_left ?>
            </div>
            <div id="content" class="col-md-9">
                <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="password">
                    <div class="form-group">
                        <label for="password" class="required"><?php echo $entry_password; ?></label>
                        <input class="form-control" type="password" id="password" name="password" value="<?php echo $password; ?>" autofocus />
                        <?php if ($error_password) { ?>
                        <span class="error"><?php echo $error_password; ?></span>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
                        <input class="form-control" type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                        <?php if ($error_confirm) { ?>
                        <span class="error"><?php echo $error_confirm; ?></span>
                        <?php } ?>
                    </div>
                    <div class="pull-left">
                        <a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a>
                    </div>
                    <div class="pull-right">
                        <a onclick="$('#password').submit();" class="btn btn-primary"><?php echo $button_continue; ?></a>
                    </div>
                </form>
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?>