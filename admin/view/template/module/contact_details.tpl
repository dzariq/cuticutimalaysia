<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error) { ?>
    <div class="warning"><?php echo $error; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">

            <div class="buttons"><a onclick="$('#form').submit();" ></a>
                <a onclick="updateContact()" class="button">Update</a>
                <a href="<?php echo $reply; ?>" class="button"><?php echo $button_reply; ?></a>
                <a onclick="$('#execute').val('delete');
                        $('#form').submit();" class="button"><?php echo $button_delete; ?> </a>
                <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
            </div>
            <div class="message-success"></div>
            <div class="message-error"></div>
            <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
        </div>
        <div class="content">
            <form action="<?php echo $execute; ?>" method="post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="execute" id="execute" />
                <?php if ($single_data) { ?>
                <?php foreach ($single_data as $contact) { ?>
                <p>
                    <label><?php echo $column_name; ?></label>
                    <span class="field"><input type="text" name="bannerTitle" id="name" value="<?php echo $contact['firstname']; ?>" class="input-large" /></span>
                </p>
                <p>
                    <label><?php echo $column_email; ?></label>
                    <span class="field"><input readonly="readonly" type="text" name="bannerTitle" id="email" value="<?php echo $contact['email'] ?>" class="input-large" /></span>
                </p>
                <p>
                    <label><?php echo $column_ip; ?></label>
                    <span class="field"><input type="text" name="bannerTitle" id="ip" value="<?php echo $contact['ipaddress'] ?>" class="input-large" /></span>
                </p>
                <p>
                    <label><?php echo $column_description; ?></label>
                    <span class="field">
                        <textarea id="enquiry" name="enquiry" class="input-large" style="width:50%;height:180px;"> <?php echo $contact['enquiry']; ?>  </textarea>
                    </span>
                    <?php foreach($details as $detail){   ?>
                <p>
                    <label><?php echo $detail['name']; ?></label> 
                    <span class="field">
                        <input type="<?php echo $detail['type'] ?>" name="<?php echo $detail['name'] ?>" id="<?php echo $detail['name'] ?>" value="<?php echo $detail['value'] ?>" class="input-large" />
                    </span>

                </p>
                <?php } ?>
                <?php if($contact['attachment'] != ''){   ?>
                <input type="hidden" id="attachment1_set" value="<?php echo $contact['attachment'] ?>" />
                <p>
                    <label>Attachment</label> 
                <div style="height: 38px;" class="field">
                    <a target='_blank' href="../<?php echo $contact['attachment'] ?>"><?php echo $contact['attachment'] ?></a>
                </div>
                </p>
                <?php }else{ ?>
                <input type="hidden" id="attachment1_set" value="" />
                <?php } ?>
                <?php if($contact['attachment2'] != ''){   ?>
                <input type="hidden" id="attachment2_set" value="<?php echo $contact['attachment2'] ?>" />
                <p>
                    <label>Attachment 2</label> 
                <div  style="height: 38px;" class="field">
                    <a target='_blank' href="../<?php echo $contact['attachment2'] ?>"><?php echo $contact['attachment2'] ?></a>
                </div>
                </p>
                <?php }else{ ?>
                <input type="hidden" id="attachment2_set" value="" />
                <?php } ?>
                <p style="height:30px">
                <label>Attachment 1</label>
                <input class="form-control" type="file" value="<?php echo $contact['attachment'] ?>" name="attachment1" />
                </p>
                <p style="height:30px">
                <label>Attachment 2</label>
                <input class="form-control" type="file" value="<?php echo $contact['attachment2'] ?>" name="attachment2" />
                </p>
                <p class="right">

                    <a  href="<?php $reply = $this->url->link('module/contact/contact_reply', 'token=' . $this->session->data['token'].'&id='.$contact['contact_id'], 'SSL') ?>">
                    </a> 

                </p>
                <?php } ?>


                <?php } else { ?>
                <?php echo $text_no_results; ?>
                <?php } ?>
            </form>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style type="text/css">

    .content p label {
        background: none repeat scroll 0 0 #F4F4F4;
        clear: both;
        display: inline-block;
        float: left;
        margin-bottom: 5px;
        margin-right: 10px;
        padding: 10px;
        text-align: left;
        width: 150px;
    }

    input[type="text"] {
        background: none repeat scroll 0 0 #F4F4F4;
        border: medium none;
        box-shadow: none;
        height: 30px;
        padding-left: 5px;
        width: 300px;
    }
    textarea{
        background: none repeat scroll 0 0 #F4F4F4;	
        border: medium none;
        box-shadow: none;
    }
    .right {
        display: block;
        margin-left: 751px;

    }
</style>

<script>
    function updateContact() {

        $('.message-error').hide()
        $('.message-success').hide()
        $('.message-error').html('')
        $('.message-success').html('')



        var data = new FormData();
        data.append('name', $('#name').val()),
                data.append('email', $('#email').val()),
                data.append('details_start_text', $('#start').val()),
                data.append('enquiry', 'Alumni Form'),
                data.append('category', 'alumni');
        data.append('attachment1_set', $('#attachment1_set').val()),
                data.append('attachment2_set', $('#attachment2_set').val()),
                data.append('attachment1', $('input[name=attachment1]')[0].files[0]);
        data.append('attachment2', $('input[name=attachment2]')[0].files[0]);
        data.append('json', 1);

        console.log(data)
        $.ajax({
            url: 'index.php?route=module/contact/update&token=<?php echo $this->session->data["token"] ?>',
            type: "POST",
            processData: false,
            contentType: false,
            dataType: 'json',
            data: data,
            success: function (data) {
                var html = '';
                html += data.remark;

                if (data.status == 1) {

                    window.location = 'index.php?route=module/contact&token=<?php echo $this->session->data["token"] ?>';
                } else {
                    $('.message-error').show()
                    $('.message-error').html(html)
                }
                $('#remark').focus()
                console.log(data);
            }
        });


    }
</script>