<?php
#########################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com  #
#########################################################################################
class ModelModuleAutoModule extends Model {

	public function createModule($settings) {
		//print_r($settings);exit();
		if ($settings['mod_name']) {
			$classname = ucwords(strtolower($settings['mod_name']));
			$classname = str_replace(' ', '', $classname); 
			$filename = str_replace(' ', '_', strtolower($settings['mod_name']));
		
			$paths = array(
				DIR_APPLICATION . 'view/template/module' => 'back',
				DIR_APPLICATION . 'controller/module' => 'back',
				DIR_APPLICATION . 'language/english/module' => 'back',
				DIR_APPLICATION . '../catalog/view/theme/default/template/module' => 'front',
				DIR_APPLICATION . '../catalog/controller/module' => 'front',
				DIR_APPLICATION . '../catalog/model/module' => 'front',
				DIR_APPLICATION . '../catalog/language/english/module' => 'front',				
			);
			if ($settings['backend_model']) {
				$files[DIR_APPLICATION . 'model/module'] = 'back';
			}
			if ($settings['frontend_model']) {
				$files[DIR_APPLICATION . '../catalog/model/module'] = 'front';
			}
			
			$replacements = $this->replacements($filename, $classname, $settings);
			$removals = $this->removals($settings);
			foreach ($paths as $path => $site) {
				$this->createFile($path, $filename, $site, $replacements, $removals);				
			}
		}
	}

	private function createFile($path, $filename, $site, $replacements, $removals) {
		$contents = file_get_contents($path . '/hj_template.txt');
		foreach ($removals as $remove) {
			$contents = preg_replace($remove, '', $contents);
		}
		foreach ($replacements as $replacement) {
			$contents = str_replace($replacement['text'], $replacement['replace'], $contents);
		}
		$filename = (!strstr($path, '/template/')) ? $path . '/' . $filename . '.php' : $path . '/' . $filename . '.tpl';
		$fh = fopen($filename, 'w');
		fwrite($fh, $contents);
	}
	
	private function replacements($filename, $classname, $settings) {
		$langs = array();
		$fields = array();
		$config = array();
		
		//create controller additions
		foreach ($settings['fields'] as $type) {
			if ($type != '--Select--') {
				$name = array_shift($settings['field_'.$type]);
				$internal_name = str_replace(' ', '', $name);
				$internal_name = $filename.'_'.strtolower($internal_name);
				if ($type == 'textarea') {
					$fields[] = '<tr><td><?php echo $entry_' . $internal_name . '; ?></td><td><textarea name="' . $internal_name . '"><?php echo $' . $internal_name . '; ?></textarea></td></tr>';
				} elseif ($type == 'text' || $type == 'file') {
					$fields[] = '<tr><td><?php echo $entry_' . $internal_name . '; ?></td><td><input type="' . $type . '" name="' . $internal_name . '" value="<?php echo $' . $internal_name . '; ?>"/></td></tr>';
				} elseif ($type == 'select') {
					$options = array_shift($settings['field_multi_val']);
					$new_field = '<tr><td><?php echo $entry_' . $internal_name . '; ?></td><td><select name="' . $internal_name . '">';
					foreach ($options as $option) {
						$internal_opt = str_replace(' ', '', $option);
						$internal_opt = strtolower($internal_opt);
						$new_field .= '<option value="' . $internal_opt . '" <?php if ($' . $internal_name . ' == "' . $internal_opt . '") echo \'selected="true"\'; ?>">' . $option . '</option>';
					}
					$new_field .= '</select></td></tr>';
					$fields[] = $new_field;
				} elseif ($type == 'radio') {
					$options = array_shift($settings['field_multi_val']);
					$new_field = '<tr><td><?php echo $entry_' . $internal_name . '; ?></td><td>';
					foreach ($options as $option) {
						$internal_opt = str_replace(' ', '', $option);
						$internal_opt = strtolower($internal_opt);
						$new_field .= $option . '<input name="' . $internal_name . '" type="' . $type . '" value="' . $internal_opt . '" <?php if ($' . $internal_name . ' == "' . $internal_opt . '") echo \'checked="true"\'; ?>><br/>';
					}
					$new_field .= '</td></tr>';
					$fields[] = $new_field;
				} elseif ($type == 'checkbox') {
					$options = array_shift($settings['field_multi_val']);
					$new_field = '<tr><td><?php echo $entry_' . $internal_name . '; ?></td><td>';
					foreach ($options as $option) {
						$internal_opt = str_replace(' ', '', $option);
						$internal_opt = strtolower($internal_opt);
						$new_field .= $option . '<input name="' . $internal_name . '[]" type="' . $type . '" value="' . $internal_opt . '" <?php if (is_array($' . $internal_name . ') && in_array("' . $internal_opt . '", $' . $internal_name . ')) echo \'checked="true"\'; ?>><br/>';
					}
					$new_field .= '</td></tr>';
					$fields[] = $new_field;
				}
				
				$langs['entry_' . $internal_name] = $name;
				$config[] = "'" . $internal_name . "'";
			}
		}		
		
		//create view additions
		//$product_select = '';
		$category_select = '';
		$manufacturer_select = '';
		
		$language_items = array();
		foreach ($langs as $name=>$text) {
			$language_items[$name] = '$_[\'' . $name . "'] = '" . $text . "';";
		}
		$language_full = implode("\n", array_values($language_items));
		$language = "'" . implode("',\n'", array_keys($langs)) . "'";
		
		
		//create replacements
		$replacements = array();
		$replacements[] = array(
			'text'		=> '{HJ_MOD_NAME}',
			'replace'	=> $filename
		);
		$replacements[] = array(
			'text' 		=> '{HJ_CLASS}',
			'replace' 	=> $classname
		);
		$replacements[] = array(
			'text'		=> '{HJ_CONFIG}',
			'replace' 	=> implode(",\n", $config)
		);
		$replacements[] = array(
			'text'		=> '{HJ_FIELDS}',
			'replace' 	=> implode("\n", $fields)
		);
		$replacements[] = array(
			'text'		=> '{HJ_LANGUAGE}',
			'replace' 	=> $language
		);
		$replacements[] = array(
			'text'		=> '{HJ_LANGUAGE_FULL}',
			'replace' 	=> $language_full
		);
		//$replacements[] = array(
		//	'text'		=> '{HJ_PRODUCT_SELECT}',
		//	'replace' 	=> $product_select
		//);
		$replacements[] = array(
			'text'		=> '{HJ_CATEGORY_SELECT}',
			'replace' 	=> $category_select
		);
		$replacements[] = array(
			'text'		=> '{HJ_MANUFACTURER_SELECT}',
			'replace' 	=> $manufacturer_select
		);
		return $replacements;
	}
	
	private function removals($settings) {
		//with smarter naming we could do this in one loop. two for now in case we do something special later.
		$removals = array(
                    			'limit'		=> '/{HJ_REMOVE_LIMIT}.*?{\/HJ_REMOVE_LIMIT}/s',
                    			'banner'		=> '/{HJ_REMOVE_BANNER}.*?{\/HJ_REMOVE_BANNER}/s',
			'title'		=> '/{HJ_REMOVE_TITLE}.*?{\/HJ_REMOVE_TITLE}/s',
			'description'		=> '/{HJ_REMOVE_DESCRIPTION}.*?{\/HJ_REMOVE_DESCRIPTION}/s',
			'link'		=> '/{HJ_REMOVE_LINK}.*?{\/HJ_REMOVE_LINK}/s',
			'linkname'		=> '/{HJ_REMOVE_LINKNAME}.*?{\/HJ_REMOVE_LINKNAME}/s',
			'layout'	=> '/{HJ_REMOVE_LAYOUT}.*?{\/HJ_REMOVE_LAYOUT}/s',
			'position'	=> '/{HJ_REMOVE_POSITION}.*?{\/HJ_REMOVE_POSITION}/s',
			'status'	=> '/{HJ_REMOVE_STATUS}.*?{\/HJ_REMOVE_STATUS}/s',
			'image'		=> '/{HJ_REMOVE_IMAGE}.*?{\/HJ_REMOVE_IMAGE}/s',
			'sort_order'=> '/{HJ_REMOVE_SORT_ORDER}.*?{\/HJ_REMOVE_SORT_ORDER}/s',
		);
		$opt_removals = array(
			'frontend_model'		=> '/{HJ_REMOVE_FRONTEND_MODEL}.*?{\/HJ_REMOVE_FRONTEND_MODEL}/s',
			//'product_selector'		=> '/{HJ_PRODUCT_SELECTOR}.*?{\/HJ_PRODUCT_SELECTOR}/s',
			'category_selector'		=> '/{HJ_CATEGORY_SELECTOR}.*?{\/HJ_CATEGORY_SELECTOR}/s',
			'manufacturer_selector'	=> '/{HJ_MANUFACTURER_SELECTOR}.*?{\/HJ_MANUFACTURER_SELECTOR}/s',
		);
		//half empty approach
		foreach ($settings['layout_data'] as $layout) {
			$removals[$layout] = '/{\/?HJ_REMOVE_' . strtoupper($layout) . '}/s';
		}
		//half full approach
		foreach ($opt_removals as $key => $regex) {
			if (!$settings[$key]) {
				$removals[$key] = $regex;
			} else {
				$removals[$key] = '/{\/?HJ_' . strtoupper($key) . '}/s'; 
			}
		}
		
		return array_values($removals);
	}
	
	
}
?>