<?php

class ControllerSubdomainSearch extends Controller {

    public function index() {
        if (isset($this->request->get['domain']) && $this->request->get['domain'] != '') {
            
            $status = 0;
           
            $this->ch = curl_init();

            $url = "http://freedomainapi.com/?key=fcsqcmu5m9&domain=" . $this->request->get['domain'];

            curl_setopt($this->ch, CURLOPT_URL, $url);
            curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

            $data = json_decode(curl_exec($this->ch));
            
            if($data->available == 1){
                $status = 1;
            }

            curl_close($this->ch);
            
            $result = array(
                'status' => $status
            );
            
            echo json_encode($result);
        }
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

