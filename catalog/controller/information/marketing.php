<?php

class ControllerInformationMarketing extends Controller {

    public function index() {

        $this->data['breadcrumbs'] = array();
        $this->data['button_send_code'] = $this->language->get('button_send_code');
        $this->data['button_resend_code'] = $this->language->get('button_resend_code');
        $this->data['seminar_link'] = $this->config->get('config_seminarlink');
        $this->data['seminar_text'] = $this->language->get('seminar_text');

        $this->load->model('account/customer');

        $email = $this->request->get['e'];

        if (!$email) {
            $email = $_COOKIE['marketing_email'];
        }

        $existing = $this->model_account_customer->getCustomerByEmail($email);

        if (!$email || !$existing) {
            $this->redirect($this->url->link('common/home', '', 'SSL'));
        }

        $this->data['phone'] = $existing['telephone'];

        $this->data['email'] = $email;

        $this->document->setTitle('Seminar');



        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/marketing_firststep.tpl')) {

            $this->template = $this->config->get('config_template') . '/template/information/marketing_firststep.tpl';
        } else {

            $this->template = 'default/template/information/marketing_firststep.tpl';
        }


        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );



        $this->response->setOutput($this->render());
    }

    public function storeEmailName() {

        $this->load->model('account/customer');

        $error = '';
        if (utf8_strlen($this->request->post['name']) < 3 || utf8_strlen($this->request->post['name']) > 50) {
            $error = $this->language->get('error_name');
        }
        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {

            $error = $this->language->get('error_email');
        }

        if ($error == '') {
            //check db
            $existing = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

            if ($existing) {
                $error = $this->language->get('existing_email');
            } else {
                $data = array(
                    'firstname' => $this->request->post['name'],
                    'email' => $this->request->post['email'],
                );
                $this->model_account_customer->addCustomer($data);
                $this->customer->login($data['email'], '', true);

                setcookie('marketing_email', $this->request->post['email'], time() + (86400 * 7));
            }
        }

        if ($error != '') {
            $data = array(
                'error' => $error,
            );
        } else {
            $data = array(
                'error' => $error,
                'redirect' => 1,
                'link' => 'index.php?route=information/marketing',
            );
        }


        echo json_encode($data);
    }

    public function updatePhone() {

        $this->load->model('account/customer');
        $this->load->language('account/register');

        $error = '';
        if ((utf8_strlen($this->request->post['phone']) < 3) || (utf8_strlen($this->request->post['phone']) > 32)) {

            $error = $this->language->get('error_telephone');
        }

        if ($error == '') {
            //check db
            $existing = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

            if ($existing) {
                $data = array(
                    'email' => $existing['email'],
                    'firstname' => $existing['firstname'],
                    'lastname' => $existing['lastname'],
                    'telephone' => $this->request->post['phone']
                );
                $this->model_account_customer->editCustomer($data);

                //email code

                $subject = $this->language->get('mail_seminar_code_subject');
                $message = sprintf($this->language->get('mail_seminar_code_message'),$this->config->get('config_seminarcouponcode'),$this->config->get('config_seminarlink'));

                $mail = new Mail();

                $mail->protocol = $this->config->get('config_mail_protocol');

                $mail->parameter = $this->config->get('config_mail_parameter');

                $mail->hostname = $this->config->get('config_smtp_host');

                $mail->username = $this->config->get('config_smtp_username');

                $mail->password = $this->config->get('config_smtp_password');

                $mail->port = $this->config->get('config_smtp_port');

                $mail->timeout = $this->config->get('config_smtp_timeout');

                $mail->setTo($existing['email']);

                $mail->setFrom('no-reply@'.$_SERVER['HTTP_HOST']);

                $mail->setSender($this->config->get('config_name'));

                $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));

                $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));

                $mail->send();



                // Send to main admin email if new account email is enabled

                if ($this->config->get('config_account_mail')) {

                    $mail->setTo($this->config->get('config_email'));

                    $mail->send();



                    // Send to additional alert emails if new account email is enabled

                    $emails = explode(',', $this->config->get('config_alert_emails'));



                    foreach ($emails as $email) {

                        if (strlen($email) > 0 && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {

                            $mail->setTo($email);

                            $mail->send();
                        }
                    }
                }
            } else {
                $error = $this->language->get('existing_email');
            }
        }


        if ($error != '') {
            $data = array(
                'error' => $error,
            );
        } else {
            $data = array(
                'error' => $error,
                'success' => $this->language->get('marketing_success'),
                'show' => 'seminar_link',
                'hide' => 'seminar_link1',
            );
        }


        echo json_encode($data);
    }

}

?>