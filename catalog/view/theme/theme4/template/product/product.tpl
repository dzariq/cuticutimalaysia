<?php echo $header ?>
<section>
    <div class="container">
        <div class=" product-info col-md-12">
            <h2><?php echo $heading_title ?> - 
                <?php if (!$special) { ?>
                <?php echo $price; ?>
                <?php }else{ ?>
                <span style="text-decoration:line-through">
                    <?php echo $price; ?>
                </span>&nbsp;&nbsp;
                <?php echo $special; ?>
                <?php } ?>
            </h2>
            <div class="row">
                <div class="single-item ">
                    <div style="padding:0px" class="col-sm-5 col-xs-12">
                        <?php  if ($thumb) { ?>
                            <div style="height:325px" class="col-xs-12" id="main-slider" >
                                <div class="carousel slide">
                                    <ol class="carousel-indicators">
                                        <li data-target="#main-slider" data-slide-to="0" class="active" ></li>
                                        <?php $i=1; foreach($images as $banner){ ?>
                                        <li data-target="#main-slider" data-slide-to="<?php echo $i ?>" ></li>
                                        <?php $i++; } ?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="item active" style="cursor:pointer;" >
                                            <a class="preview" href="<?php echo $popup ?>" rel="prettyPhoto[1]">
                                                <img src='<?php echo $thumb ?>' alt="<?php echo $heading_title; ?>" class='col-xs-12 sponsive' />
                                            </a>
                                        </div><!--/.item-->
                                        <?php $i=0; foreach($images as $banner){ ?>
                                        <div class="item " style="cursor:pointer;" >
                                            <a class="preview" href="<?php echo $banner[thumb] ?>" rel="prettyPhoto[1]">
                                                <img src='<?php echo $banner[thumb] ?>' class='col-xs-12 img-responsive' />
                                            </a>
                                        </div><!--/.item-->
                                        <?php $i++; } ?>
                                    </div><!--/.carousel-inner-->
                                </div><!--/.carousel-->
                                <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
                                    <i class="fa fa-chevron-left"></i>
                                </a>
                                <a class="next hidden-xs" href="#main-slider" data-slide="next">
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                            </div><!--/#main-slider-->

                            <div class="clearfix"></div>
                       
                            <div class="col-xs-12" id="review"></div>
                        <?php if ($products) { ?>
                        <div class="row tab-content">
                            <hr/>
                            <h5><?php echo $tab_related ?></h5>
                            <div class="box-product">
                                <?php foreach ($products as $product) { ?>
                                <div class="col-xs-4">
                                    <?php if ($product['thumb']) { ?>
                                    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
                                    <?php } ?>

                                    <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                                    <?php if ($product['price']) { ?>
                                    <div class="price">
                                        <?php if (!$product['special']) { ?>
                                        <span style="font-size:11px;"><?php echo $product['price']; ?></span>
                                        <?php } else { ?>
                                        <span style="font-size:10px;text-decoration:line-through"><?php echo $product['price']; ?></span> <span style="font-size:11px;"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>

                        <div class="clearfix"></div>


                        <!--                        section faci-->
                        <div style="margin-bottom:20px" class="col-xs-12">
                            <table style='background:#fafafa;width:100%' class='table-striped' >
                                <tr>
                                <thead>
                                    <tr>
                                        <td style="width:50%">
                                            Facilities
                                        </td>
                                        <td style="width:50%">
                                            Information
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($section2_images as $fac){ ?>
                                    <tr>
                                        <td style="width:50%">
                                            <?php if($fac['image'] != 'no_image.jpg'){ ?>
                                            <a href='<?php echo $fac[image] ?>' rel='prettyPhoto' >
                                                <img style='margin:0 auto' class='col-xs-12 img-responsive' src="<?php echo $fac['image'] ?>" />
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td style="width:50%">
                                            <h4><?php echo $fac['caption'] ?></h4>
                                            <p>
                                                <?php echo $fac['description'] ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                </tr>

                            </table>
                        </div>
                        <div class="col-xs-12">
                        <div style="padding:10px;border:1px solid #e0e0e0;background:#fafafa;">
                            <h2 id="review-title"><?php echo $text_write; ?></h2>
                            <br/>
                            <br/>
                            <b><?php echo $entry_name; ?></b><br />

                            <input type="text" name="name" value="" />

                            <br />

                            <br />

                            <b><?php echo $entry_review; ?></b>

                            <textarea  class="grey" name="text" cols="40" rows="8" style="padding:8px;background:#f4f4f4;width: 98%;"></textarea>

                            <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />

                            <br />

                            <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="1" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="2" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="3" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="4" />

                            &nbsp;

                            <input class="ratingbox" type="radio" name="rating" value="5" />

                            &nbsp;<span><?php echo $entry_good; ?></span><br />

                            <br />

                            <b><?php echo $entry_captcha; ?></b><br />

                            <input type="text" name="captcha" value="" />

                            <br />
                            <br />

                            <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />

                            <br />

                            <div class="buttons">

                                <div class="right"><a id="button-review" style="cursor:pointer" class="btn btn-primary"><?php echo $button_continue; ?></a></div>

                            </div>
                        </div>
                        </div>

                        <!--          section faci-->
                    </div>

                    <div  class="col-sm-7 col-xs-12">
                        <div class="tab-wrap">
                            <div class="media">
                                <div class="parrent pull-left">
                                    <ul class="nav nav-tabs nav-stacked">
                                        <li class="active"><a href="#tab1" data-toggle="tab" class="analistic-01">Information</a></li>
                                        <?php if ($attribute_groups) { ?>
                                        <li class=""><a href="#tab2" data-toggle="tab" class="analistic-02">Details</a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="parrent media-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active in" id="tab1">
                                            <?php echo $description; ?>
                                        </div>
                                        <?php if ($attribute_groups) { ?>
                                        <div class="tab-pane" id="tab2">
                                            <table class="table table-striped">
                                                <?php foreach ($attribute_groups as $attribute_group) { ?>
                                                <thead>

                                                </thead>
                                                <?php } ?>
                                                <tbody>
                                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                    <tr>
                                                        <td><strong><?php echo $attribute['name']; ?></strong></td>
                                                        <td><?php echo $attribute['text']; ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } ?>

                                    </div> <!--/.tab-content-->  
                                </div> <!--/.media-body--> 
                            </div> <!--/.media-->     
                        </div><!--/.tab-wrap-->   

                        <div  class="col-sm-12" style="padding:0px;">
                            <div class="book-sect">
                                <div class="col-xs-5">
                                    <span class="required">*</span> Night(s)
                                    <br/>
                                    <?php if($isbn == ''){ $isbn = 1; } ?>
                                    <select onchange="calculateCheckOut(); checkRoomAvailable()" style='margin-top: 12px;' class="form-control" name="room-duration" id="room-duration" >
                                        <?php for($i=1;$i <= 5 ;$i++){ ?>
                                        <option value="<?php echo $i ?>" ><?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-xs-7">
                                    <span class="required">*</span> <?php echo $text_quantity ?>
                                    <br/>
                                    <div style="margin-left:2px" id="resta1" class="number">-</div><div id="number1" class="number">1</div><div id="suma1" class="number">+</div>
                                </div>
                                <div class="col-xs-12">
                                    <?php include "catalog/view/theme/theme4/template/product/product_options.tpl" ?>
                                </div>
                                <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                                <input type='hidden' value='<?php echo $minimum  ?>' id="quantity" name="quantity"  />
                                <?php if($stockstatus == 'SOLD OUT'){ ?>
                                <button style="margin-top:-10px;margin-bottom:10px;color:white;width:60%;border:none;background:#a50025 !important" type="button" class="btn btn-primary" >SOLD OUT</button>
                                <?php }else{ ?>
                                <button style="width:50%;margin-top:0px;height:0px;visibility:hidden" type="button" id="button-cart" class="btn btn-primary" >Book Now</button>
                                <?php } ?>
                            </div>
                        </div>
                        <!--                        section rooms-->
                        <div class="col-sm-12" style="padding:0px;margin-bottom:20px">
                            <ul style="  margin-left: 50px;">
                                <li class="col-sm-2 col-xs-4">
                                    <div class="input-color">
                                        <input type="text" value="Normal" />
                                        <div class="color-box" style="background-color: grey;"></div>
                                        <!-- Replace "#FF850A" to change the color -->
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input type="text" value="Weekend" />
                                        <div class="color-box" style="background-color: #51a351;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input type="text" value="Public Holiday" />
                                        <div class="color-box" style="background-color: #F8BB00;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input type="text" value="Super Peak" />
                                        <div class="color-box" style="background-color: #EE5757;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                                <li class="col-sm-2  col-xs-4">
                                    <div class="input-color">
                                        <input type="text" value="School Break" />
                                        <div class="color-box" style="background-color: #FFD1D1;"></div>
                                        <!-- Replace "navy" to change the color -->
                                    </div>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            <table  class='table-responsive table-striped' style="margin-top:20px;width:100%" >
                                <tr>
                                <thead>
                                    <tr>
                                        <td  style="width:40%">
                                            Room
                                        </td>
                                        <td>
                                            Price/day (RM)
                                        </td>
                                        <td >
                                            Includes
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($section1_images as $room){ ?>
                                <tr>
                                    <td style='text-align: center;width:35%'>
                                        <?php if($room['image'] != 'no_image.jpg'){ ?>
                                        <a href='<?php echo $room[image] ?>' rel='prettyPhoto' >
                                            <img class='col-xs-12  img-responsive' src="<?php echo $room['image'] ?>" />
                                        </a>
                                        <?php } ?> 
                                    </td>
                                    <td style="text-align: center;font-size:16px;width:35%">
                                        <table class='table-striped' style="width:100%">
                                            <?php if($room['description2'] != 0){ ?>
                                            <tr>
                                                <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: grey' class='price-hi'><?php echo $room['description2'] ?></span></td>
                                            </tr>
                                            <?php } ?>
                                            <?php if($room['price2'] != 0){ ?>
                                            <tr>
                                                <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #51a351' class='price-hi'><?php echo $room['price2'] ?></span></td>
                                            </tr>
                                            <?php } ?>
                                            <?php if($room['price3'] != 0){ ?>
                                            <tr>
                                                <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #F8BB00' class='price-hi'><?php echo $room['price3'] ?></span></td>
                                            </tr>
                                            <?php } ?>
                                            <?php if($room['price4'] != 0){ ?>
                                            <tr>
                                                <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background: #EE5757' class='price-hi'><?php echo $room['price4'] ?></span></td>
                                            </tr>
                                            <?php } ?>
                                            <?php if($room['price5'] != 0){ ?>
                                            <tr>
                                                <td style="text-align: center;padding:0px;padding-bottom:10px;border:0px"><span style='background:  #FFD1D1' class='price-hi'><?php echo $room['price5'] ?></span></td>
                                            </tr>
                                            <?php } ?>
                                        </table>

                                        <!--  <br/>
                                          <b>Walk-in:</b><br/> <span class='price-hi'><?php echo $room['price4'] ?></span>
                                          <br/>-->
                                    </td>
                                    <td style="width:35%">
                                        <h4><?php echo $room['caption'] ?></h4>
                                        <p>
                                            <?php echo $room['description'] ?>
                                        </p>
                                        <br/>
                                        <button style=";" type="button" onclick="book('<?php echo $room[room_id] ?>','<?php echo $room[caption] ?>')" class="btn btn-primary" >Book Now</button>
                                        <span id='full-<?php echo $room[room_id] ?>' style='display:none' class="noroom">Fully Booked</span>
                                    </td>

                                </tr>
                                <?php } ?>
                            </tbody>
                            </tr>

                        </table>
                    </div>
                    <!--                        section rooms-->

                    <?php if (isset($pmLocations) && !empty($pmLocations)) { ?>
                    <div class=' col-xs-12'>
                        <div style="margin-top:20px;text-align: center" class="map-sect" >
                        <?php 
                        echo "<h2 style='margin-bottom:20px'>Direction Map</h2>";
                        echo "<p style='margin-bottom:20px'>{$pmLocations[0]['address']}</p>";
                        echo "<div id='pmMapDiv'  >";
                        echo "<div style='margin: 0 auto; width:350px; height:350px;' id=\"pmMap{$pmLocations[0]['id']}\"></div>";
                        echo "</div>";
                        echo "<div id='pmDirDiv' >";
                        echo "<input  id='pmTo' value='{$pmLocations[0]['address']}' type='hidden' />";
                        echo "<input style='margin-top:20px' class='form-control' id='pmFrom' value='Kuala Lumpur, Malaysia' type='text' />";
                        echo "<input id='pmDirectionsBtn' class='btn btn-primary' type='button' value='$button_get_directions'>";
                        echo "<div id='pmDirectionsList'></div>";
                        echo "</div>";

                        ?>

                        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

                        <script type="text/javascript">
                                    $(function() {

                                    <?php
                                            echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                            ?>
                                            function createMap(e, lat, lng, zoom, width, height) {
                                            var gmLatlng = new google.maps.LatLng(lat, lng);
                                                    var gmOpt = {
                                                    zoom: zoom,
                                                            center: gmLatlng,
                                                            mapTypeId: google.maps.MapTypeId.ROADMAP
                                                    }
                                            var map = new google.maps.Map(document.getElementById(e), gmOpt);
                                                    var marker = new google.maps.Marker({
                                                    position: gmLatlng,
                                                            map: map,
                                                            draggable:true
                                                    });
                                                    return map;
                                            }


                                    function generateRoute(map) {
                                    $("#pmDirWarning").remove();
                                            $("#pmDirectionsList").empty();
                                            var directionsService = new google.maps.DirectionsService();
                                            var directionsDisplay = new google.maps.DirectionsRenderer();
                                            var start = document.getElementById("pmFrom").value;
                                            var end = document.getElementById("pmTo").value;
                                            var request = {
                                            origin:start,
                                                    destination:end,
                                                    travelMode: google.maps.TravelMode.DRIVING
                                            };
                                            directionsService.route(request, function(result, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                            directionsDisplay.setMap(map);
                                                    directionsDisplay.setPanel(document.getElementById("pmDirectionsList"));
                                                    directionsDisplay.setDirections(result);
                                            } else if (status == google.maps.DirectionsStatus.ZERO_RESULTS) {
                                            $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_route_not_found; ?></div>');
                                            } else if (status == google.maps.DirectionsStatus.NOT_FOUND) {
                                            $("#pmFrom").before('<div class="warning" id="pmDirWarning"><?php echo $error_address_not_found; ?></div>');
                                            } else {
                                            $("#pmFrom").before('<div class="warning" id="pmDirWarning">' + status + '</div>');
                                            }
                                            });
                                    }

                                    $('#pmDirectionsBtn').bind('click', function(event, ui) {
                                    <?php
                                            echo "var map = createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                            ?>
                                            generateRoute(map);
                                    });
                                            $('#tab-maps-load').bind('click', function(event, ui) {
                                    <?php
                                            echo "createMap('pmMap{$pmLocations[0]['id']}', {$pmLocations[0]['lat']}, {$pmLocations[0]['lng']}, {$pmLocations[0]['zoom']}, {$pmLocations[0]['width']}, {$pmLocations[0]['height']});";
                                            ?>
                                    });
                                    });

                        </script>
                    </div>
                    </div>
                    <?php } ?>


                </div>
            </div>

        </div>
    </div>
    <?php include "catalog/view/theme/theme4/template/product/product_footer.tpl" ?>
</div>
</section>


<?php echo $footer ?>
