<?php echo $header; ?>
<div class="grid_12">
    <?php if ($success) { ?>
    <div class="alert alert-success">
        <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger">
        <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!-- Page content -->
    <section style="height:auto" class="grid_4 search sidebar">
        <form class="form-horizontal black" role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="login">
            <div class="half">
                <label ><?php echo $entry_email; ?></label>
                <input type="email" class="form-control" value="<?php echo $email ?>" name="email" id="email" placeholder="Email">
            </div>
            <div class="half">
                <label ><?php echo $entry_password; ?></label>
                <input type="password" value="<?php echo $password ?>" class="form-control" name="password" id="password" placeholder="Password">
            </div>
            <input type="button" onclick="$('#login').submit();" value="<?php echo $button_login ?>" />
            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
        </form>
    </section>
    <script type="text/javascript"><!--
    $('#login input').keydown(function (e) {
            if (e.keyCode == 13) {
                $('#login').submit();
            }
        });
    //--></script>  

</div>
<?php echo $footer; ?>