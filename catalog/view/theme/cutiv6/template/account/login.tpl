<?php echo $header; ?>

<!-- Page content -->
<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="login" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="flex-content">
                        <div class="custom-form custom-form-fields">
                            <h3><?php echo $heading_title; ?></h3>
                            <p>Check your booking history and get promos. Login Now</p>
                            <?php if ($success) { ?>
                            <div class="alert alert-success">
                                <?php echo $success; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                            <?php } ?>
                            <?php if ($error_warning) { ?>
                            <div class="alert alert-danger">
                                <?php echo $error_warning; ?>
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                            <?php } ?>
                            <form class="form-horizontal" role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="login">

                                <div class="form-group">
                                    <input type="text" value='<?php echo $email ?>' name='email' class="form-control" placeholder="<?php echo $entry_email; ?>"  required/>
                                    <span><i class="fa fa-user"></i></span>
                                </div>

                                <div class="form-group">
                                    <input type="password" value='<?php echo $password ?>' name='password' class="form-control" placeholder="<?php echo $entry_password; ?>"  required/>
                                    <span><i class="fa fa-lock"></i></span>
                                </div>
                                <button onclick="$('#login').submit();" class="btn btn-orange btn-block"><?php echo $button_login ?></button>
                            </form>

                            <div class="other-links">
                                <p class="link-line">New Here ? <a href="register">Signup</a></p>
                                <a class="simple-link" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                            </div><!-- end other-links -->
                        </div><!-- end custom-form -->

                        <div class="flex-content-img custom-form-img">
                            <img src="image/login_image.jpg" class="img-responsive" alt="registration-img" />
                        </div><!-- end custom-form-img -->
                    </div><!-- end form-content -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->         
    </div><!-- end login -->
</section><!-- end innerpage-wrapper -->
<script type="text/javascript"><!--
$('#login input').keydown(function (e) {
        if (e.keyCode == 13) {
            $('#login').submit();
        }
    });
//--></script>  
<?php echo $footer; ?>