<?php echo $header; ?>
<div class="<?php echo layout ?>">
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb hidden-xs">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ol>
    </div>
    <div id="content" class="col-md-12">
        <?php echo $content_top; ?>
        <section>
            <h1><?php echo $heading_title; ?></h1>
            <?php echo $text_error; ?>
        </section>
        <div class="pull-right">
            <a href="<?php echo $continue; ?>" class="btn btn-info"><?php echo $button_continue; ?></a>
        </div>
    	<?php echo $content_bottom; ?>
	</div>
</div>
</div>
<?php echo $footer; ?>