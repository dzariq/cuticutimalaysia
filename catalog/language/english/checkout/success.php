<?php
// Heading
$_['heading_title'] = 'Your Booking Has Been Processed!';
$_['heading_title_customer'] = 'Your Booking #%s Has Been Processed!';

// Text
$_['text_customer'] = '<h2>Booking Was Successful!</h2><p>Your booking has been successfully processed!</p><p>You can view your booking history by going to <a style="color:orange" href="%s">history</a>.</p><p>Booking details has been send to your email id. Kindly check your email in order to confirm the transaction.</p>';
$_['text_guest']    = '<p>Your booking #%s has been successfully processed!</p><p>Please direct any questions you have to the store owner.</p><p>Thanks for shopping with us online!</p>';
$_['text_basket']   = 'Basket';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Success';
?>