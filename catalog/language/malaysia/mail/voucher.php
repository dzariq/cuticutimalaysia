<?php
// Text
$_['text_subject']  = 'Anda telah menerima Voucher Hadiah yang dikirim oleh %s';
$_['text_greeting'] = 'Selamat, Anda telah menerima Voucher Hadiah senilai %s';
$_['text_from']     = 'Voucher Hadiah ini dikirim untuk Anda oleh %s';
$_['text_message']  = 'Dengan pesan';
$_['text_redeem']   = 'Untuk menggunakan Voucher Hadiah ini, tulis kode voucher berikut <b>%s</b> kemudian klik link dibawah ini dan beli produk yang Anda suka. Anda dapat memasukkan kod voucher pada Bakul Belanja sebelum mengklik butang checkout.';
$_['text_footer']   = 'Silahkan balas Email ini jika Anda memiliki pertanyaan.';
?>