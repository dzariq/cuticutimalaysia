<?php
//-----------------------------------------
// Author: Qphoria@gmail.com
// Web: http://www.OpenCartGuru.com/
//-----------------------------------------

// Heading
$_['heading_title']      = 'Webcash (Q)';

// Text
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified the account details!';
$_['text_development']   = '<span style="color: green;">Ready</span>';
$_['text_server']        = 'Server (offsite. SSL not required)';
$_['text_merchant']      = 'Merchant (onsite. requires SSL)';
$_['text_yes']      	 = 'Yes';
$_['text_no']      		 = 'No';

// Entry
$_['entry_status']       = 'Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_mid']          = 'Merchant ID:<br/><span class="help">Get this from the merchant provider</span>';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_test']         = 'Test Mode:';
$_['entry_ajax']         = 'Ajax Pre-Confirm';
$_['entry_debug']        = 'Debug Logging:<br/><span class="help">Logs transaction messages to "system/logs/'.basename(__FILE__, '.php').'_debug.txt" file (in ftp) for troubleshooting. Please send the logged messaging when contacting the developer for help.</span>';

// Help
$_['help_ajax']          = 'Pre-confirm the order upon clicking the confirmation button before sending to the gateway. This is only needed if there are server communication problems with the gateway and orders are being lost. By pre-confirming the order, the order starts in a pending state, then upon gateway return, the final order status is updated. (Recommended: DISABLED)';
$_['help_debug']         = 'Debug mode is used when there are payment or order update issues to help track down where the problem is. This usually includes saving logs or sending emails to the store email with extra information. (Recommended: DISABLED)';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify this payment module!';
$_['error_mid']          = 'Field required!';
$_['error_key']          = 'Field required!';
$_['error_desc']         = 'Field required!';

?>