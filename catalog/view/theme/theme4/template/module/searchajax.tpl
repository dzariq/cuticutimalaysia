<div id='searchengine' class="col-sm-12"   >
    <div class="inner-search col-sm-12"   >
        <h4 >Destinasi</h4>
        <div style="padding-left:0px"  class="col-md-12">
            <select class="form-control" style='width:100%' id="category" name='category' >
                <option value=''>Choose Destination</option>
                <?php  foreach($categories as $category){ ?>
                <option value='<?php echo $category[category_id] ?>'><?php echo $category['name'] ?></option>
                <?php } ?>
            </select>
        </div>
         <br/>
        <div class='clearfix'></div>
        <div style="padding-left:0px"  class="col-md-6">
            <h4 >Check in</h4>
            <input type='text' value='' id='maindate' class='date form-control' name='date' />
        </div>
        <div style="padding-left:0px"  class="col-md-6">
            <h4 >Nights(s)</h4>
            <select class="form-control" style='width:100%' id="nights" name='nights' >
                <?php for($i=1;$i<31;$i++){ ?>
                <option value='<?php echo $i ?>'><?php echo $i ?></option>
                <?php } ?>
            </select>
        </div>
        <br/>
        <div class='clearfix'></div>
        <h4 >Carian Hotel</h4>
        <div style="padding-left:0px" class="col-md-12 ">
            <input placeholder='Keyword' type="text" id="name-search" class="form-control" name="name" value="" placeholder="Search" />
        </div>
        <br/>
        <button id="searchengine_submit" style="margin-top:20px" class="btn btn-primary"  >Search</button>
    </div>
</div>