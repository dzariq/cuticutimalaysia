<!-- Auto Generated Module By HostJars http://opencart.hostjars.com -->

<h2><?php echo $title ?></h2>
<div class="clearfix"></div>
<div class="col-xs-12">
    <ul class="results">
        <?php if($banners){ ?>
        <?php $i=1; foreach($banners as $banner){ ?>
        <?php if($i<=$limit){ ?>
        <li style="background:transparent;box-shadow:none" class="short col-xs-4">
            <a href="<?php echo $banner['imageori'] ?>" class="fancybox"><img src="<?php echo $banner['image'] ?>" alt="" /></a>
            <div>
                <span><?php echo $banner['title'] ?></span>
            </div>
        </li>
        <?php  } $i++; } ?>
        <?php } ?>
    </ul>
</div>