<?php
// Heading 
$_['heading_title'] = 'Logout Akaun';

// Text
$_['text_message']  = '<p>Anda telah logout dari Akaun Afiliasi Anda.</p>';
$_['text_account']  = 'Akaun';
$_['text_logout']   = 'Logout';
?>