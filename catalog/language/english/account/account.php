<?php
// Heading 
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_account_date']       = '<b>Subscription Period: </b>';
$_['text_account_number']       = '<b>Agent Membership Number: </b>';

$_['text_my_orders']     = 'My Bookings';
$_['text_renew']     = 'Renew my Account';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Edit your address';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your booking history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points'; 
$_['text_return']        = 'View your return requests'; 
$_['text_transaction']   = 'Your Transactions'; 
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
?>