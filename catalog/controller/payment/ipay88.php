<?php

class ControllerPaymentIpay88 extends Controller {

    protected function index() {
        $this->language->load('payment/ipay88');

        $this->data['button_confirm'] = $this->language->get('button_confirm');
        $this->data['button_back'] = $this->language->get('button_back');

        $this->data['action'] = 'https://www.mobile88.com/epayment/entry.asp';

        $vendor = $this->config->get('ipay88_vendor');
        $password = $this->config->get('ipay88_password');
        $support_currency = $this->config->get('entry_currency');

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);


        // Lets start define iPay88 parameters here

        $this->data['products'] = array();
        if ($order_info['type'] == 1) {
            $total = $this->currency->format($order_info['total'], $order_info['currency_code'], false, false);

            if ($total > 0) {

                $this->data['products'][] = array(
                    'name' => $order_info['comment'],
                    'model' => '',
                    'price' => $total,
                    'quantity' => 1,
                    'option' => array(),
                    'weight' => 0
                );
            } else {

                $this->data['discount_amount_cart'] -= $total;
            }
        } else if ($order_info['type'] == 2) {
            $total = $this->currency->format($order_info['total'], $order_info['currency_code'], false, false);

            if ($total > 0) {

                $this->data['products'][] = array(
                    'name' => $order_info['comment'] . '(Renew)',
                    'model' => '',
                    'price' => $total,
                    'quantity' => 1,
                    'option' => array(),
                    'weight' => 0
                );
            } else {

                $this->data['discount_amount_cart'] -= $total;
            }
        } else {
            foreach ($this->cart->getProducts() as $product) {
                $option_data = array();

                foreach ($product['option'] as $option) {
                    $option_data[] = array(
                        'name' => $option['name'],
                        'value' => $option['option_value']
                    );
                }

                $this->data['products'][] = array(
                    'name' => $product['name'],
                    'model' => $product['model'],
                    'price' => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'], false),
                    'quantity' => $product['quantity'],
                    'option' => $option_data,
                    'weight' => $product['weight']
                );
            }
        }

        // Let's Generate Digital Signature

        $ipaySignature = '';

        $merId = $this->config->get('ipay88_vendor');
        $ikey = $this->config->get('ipay88_password');
        $tmpAmount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
        $ordAmount = number_format($tmpAmount, 2, ".", "");

        $ipaySignature = '';
        $HashAmount = str_replace(".", "", str_replace(",", "", $ordAmount));
//            echo $HashAmount;die;
        $str = sha1($ikey . $merId . $this->session->data['order_id'] . $HashAmount . $order_info['currency_code']);
        for ($i = 0; $i < strlen($str); $i = $i + 2) {
            $ipaySignature .= chr(hexdec(substr($str, $i, 2)));
        }

        $ipaySignature = base64_encode($ipaySignature);

        // Signature generating done !
        // Assign values for form post

        $this->data['MerchantCode'] = $this->config->get('ipay88_vendor');
        $this->data['PaymentId'] = '';
        $this->data['RefNo'] = $this->session->data['order_id'];
//		$this->data['Amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
        $this->data['Amount'] = $ordAmount;
        $this->data['Currency'] = $order_info['currency_code'];
        //$this->data['ProdDesc'] = $product['name'];
        $this->data['ProdDesc'] = $this->config->get('config_name') . ' - #' . $order_info['order_id'];
        $this->data['UserName'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
        $this->data['UserEmail'] = $order_info['email'];
        $this->data['UserContact'] = $order_info['telephone'];
        $this->data['Remark'] = sprintf($this->language->get('text_description'), date($this->language->get('date_format_short')), $this->session->data['order_id']);
        $this->data['Lang'] = "UTF-8";
        $this->data['Signature'] = $ipaySignature;


        $this->data['ResponseURL'] = $this->url->link('payment/ipay88/callback', '', 'SSL');
        $this->data['BackendURL'] = $this->url->link('payment/ipay88/backendcallback', '', 'SSL');



        $this->data['back'] = $this->url->link('checkout/payment', '', 'SSL');

        $this->id = 'payment';

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ipay88.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/ipay88.tpl';
        } else {
            $this->template = 'default/template/payment/ipay88.tpl';
        }

        $this->render();
    }

    public function callback() {
        $this->language->load('payment/ipay88');

        $this->load->model('checkout/order');


        $this->data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

        if (!isset($this->request->server['HTTPS']) || ($this->request->server['HTTPS'] != 'on')) {
            $this->data['base'] = HTTP_SERVER;
        } else {
            $this->data['base'] = HTTPS_SERVER;
        }

        $order_info = $this->model_checkout_order->getOrder($_POST['RefNo']);


        $this->data['charset'] = $this->language->get('charset');
        $this->data['language'] = $this->language->get('code');
        $this->data['direction'] = $this->language->get('direction');

        $this->data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

        $this->data['text_response'] = $this->language->get('text_response');
        $this->data['text_success'] = $this->language->get('text_success');
        $this->data['text_success_wait'] = sprintf($this->language->get('text_success_wait'), $this->url->link('checkout/success', '', 'SSL'));
        $this->data['text_failure'] = $this->language->get('text_failure');
        if ($order_info['type'] == 1) {
            $this->data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('account/register'));
        } else if ($order_info['type'] == 2) {
            $this->data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('account/account'));
        } else {
            $this->data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('checkout/cart'));
        }

        $expected_sign = $_POST['Signature'];
        $merId = $this->config->get('ipay88_vendor');
        $ikey = $this->config->get('ipay88_password');

        $check_sign = "";
        $ipaySignature = "";
        $str = "";
        $HashAmount = "";

        $HashAmount = str_replace(array(',', '.'), "", $_POST['Amount']);
        $str = $ikey . $merId . $_POST['PaymentId'] . trim(stripslashes($_POST['RefNo'])) . $HashAmount . $_POST['Currency'] . $_POST['Status'];


        $str = sha1($str);

        for ($i = 0; $i < strlen($str); $i = $i + 2) {
            $ipaySignature .= chr(hexdec(substr($str, $i, 2)));
        }

        $check_sign = base64_encode($ipaySignature);


        if ($_POST['Status'] == "1" && $check_sign == $expected_sign) {


            $this->model_checkout_order->confirm($_POST['RefNo'], $this->config->get('ipay88_order_status_id'), "Transaction ID : " . $_POST['TransId'], TRUE);

            $this->data['continue'] = $this->url->link('checkout/success', '', 'SSL');

            //insert new membership if type = 1
            $order_id = $_POST['RefNo'];
            $order_info = $this->model_checkout_order->getOrder($order_id);

            if ($order_info['type'] == 1) {
                $this->load->model('account/customer');
                $order_info['status'] = 1;
                $order_info['address_1'] = $order_info['payment_address_1'];
                $order_info['city'] = $order_info['payment_city'];
                $order_info['postcode'] = $order_info['payment_postcode'];
                $order_info['zone_id'] = $order_info['payment_zone_id'];
                $order_info['country_id'] = $order_info['payment_country_id'];
                $order_info['customer_group_id'] = $order_info['customer_group_id'];

                $count_customer = count($this->model_account_customer->getCustomerByEmail($order_info['email']));

                if ($count_customer == 0) {
                    $customer_id = $this->model_account_customer->addCustomer($order_info, 1);
                    //update customer_order
                    $this->model_checkout_order->updateMembership($order_id, $this->config->get('ipay88_order_status_id'), $customer_id);
                }
                //update customer_order
                $this->model_checkout_order->updateMembership($order_id, $this->config->get('ipay88_order_status_id'), $customer_id);
            }
            if ($order_info['type'] == 2) {
                $this->load->model('account/customer');
                $customer_id = $this->model_account_customer->updateCustomerDateExpired($order_info);
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ipay88_success.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/payment/ipay88_success.tpl';
            } else {
                $this->template = 'default/template/payment/ipay88_success.tpl';
            }

            $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        } else {

            if ($order_info['type'] == 1) {
                $this->data['continue'] = $this->url->link('account/register');
            } else if ($order_info['type'] == 2) {
                $this->data['continue'] = $this->url->link('account/account');
            } else {
                $this->data['continue'] = $this->url->link('checkout/cart');
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ipay88_failure.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/payment/ipay88_failure.tpl';
            } else {
                $this->template = 'default/template/payment/ipay88_failure.tpl';
            }

            $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        }
    }

    public function backendcallback() {
        $this->language->load('payment/ipay88');

        $this->data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

        if (!isset($this->request->server['HTTPS']) || ($this->request->server['HTTPS'] != 'on')) {
            $this->data['base'] = HTTP_SERVER;
        } else {
            $this->data['base'] = HTTPS_SERVER;
        }

        // $this->data['charset'] = $this->language->get('charset');
        // $this->data['language'] = $this->language->get('code');
        // $this->data['direction'] = $this->language->get('direction');
        // $this->data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));
        // $this->data['text_response'] = $this->language->get('text_response');
        // $this->data['text_success'] = $this->language->get('text_success');
        // $this->data['text_success_wait'] = sprintf($this->language->get('text_success_wait'), $this->url->link('checkout/success', '', 'SSL'));
        // $this->data['text_failure'] = $this->language->get('text_failure');
        // $this->data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('checkout/cart'));


        $expected_sign = $_POST['Signature'];
        $merId = $this->config->get('ipay88_vendor');
        $ikey = $this->config->get('ipay88_password');

        $check_sign = "";
        $ipaySignature = "";
        $str = "";
        $HashAmount = "";

        $HashAmount = str_replace(array(',', '.'), "", $_POST['Amount']);
        $str = $ikey . $merId . $_POST['PaymentId'] . trim(stripslashes($_POST['RefNo'])) . $HashAmount . $_POST['Currency'] . $_POST['Status'];


        $str = sha1($str);

        for ($i = 0; $i < strlen($str); $i = $i + 2) {
            $ipaySignature .= chr(hexdec(substr($str, $i, 2)));
        }

        $check_sign = base64_encode($ipaySignature);


        if ($_POST['Status'] == "1" && $check_sign == $expected_sign) {

            $this->load->model('checkout/order');

            $this->model_checkout_order->confirm($_POST['RefNo'], $this->config->get('ipay88_order_status_id'), "Transaction ID : " . $_POST['TransId'], TRUE);

            $this->data['continue'] = $this->url->link('checkout/success', '', 'SSL');

            //insert new membership if type = 1
            $order_id = $_POST['RefNo'];
            $order_info = $this->model_checkout_order->getOrder($order_id);

            if ($order_info['type'] == 1) {
                $this->load->model('account/customer');
                $order_info['status'] = 1;
                $order_info['address_1'] = $order_info['payment_address_1'];
                $order_info['city'] = $order_info['payment_city'];
                $order_info['postcode'] = $order_info['payment_postcode'];
                $order_info['zone_id'] = $order_info['payment_zone_id'];
                $order_info['country_id'] = $order_info['payment_country_id'];
                $order_info['customer_group_id'] = $order_info['customer_group_id'];

                $count_customer = count($this->model_account_customer->getCustomerByEmail($order_info['email']));

                if ($count_customer == 0) {
                    $customer_id = $this->model_account_customer->addCustomer($order_info, 1);
                    //update customer_order
                    $this->model_checkout_order->updateMembership($order_id, $this->config->get('ipay88_order_status_id'), $customer_id);
                }
            }

            if ($order_info['type'] == 2) {
                $this->load->model('account/customer');
                $customer_id = $this->model_account_customer->updateCustomerDateExpired($order_info);
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ipay88_success_backend.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/payment/ipay88_success_backend.tpl';
            } else {
                $this->template = 'default/template/payment/ipay88_success_backend.tpl';
            }

            $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        } else {
            if ($order_info['type'] == 1) {
                $this->data['continue'] = $this->url->link('account/register');
            } else if ($order_info['type'] == 2) {
                $this->data['continue'] = $this->url->link('account/account');
            } else {
                $this->data['continue'] = $this->url->link('checkout/cart');
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/ipay88_failure.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/payment/ipay88_failure.tpl';
            } else {
                $this->template = 'default/template/payment/ipay88_failure.tpl';
            }

            $this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        }
    }

}

?>