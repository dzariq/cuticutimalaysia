<?php
// Heading 
$_['heading_title']        = 'Daftar Akaun';

// Text
$_['text_account']         = 'Akaun';
$_['text_register']        = 'Daftar';
$_['text_account_already'] = 'Jika Anda telah memiliki Akaun, silahkan login di <a href="%s">halaman login</a>.';
$_['text_your_details']    = 'Maklumat Anda';
$_['text_your_address']    = 'Alamat Anda';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Password Anda';
$_['text_agree']           = 'Saya telah membaca dan setuju dengan <br/><a class="colorbox" href="%s" alt="%s"><b style="font-size: 20px;
color: orange;">%s</b></a>';
$_['text_payment_method']   = 'Cara Bayaran';

// Entry
$_['entry_firstname']      = 'Nama:';
$_['entry_lastname']       = 'Nama Belakang:';
$_['entry_email']          = 'Email:';
$_['entry_telephone']      = 'No. Telefon:';
$_['entry_fax']            = 'Fax:';
$_['entry_account']        = 'Akaun:';
$_['entry_company']        = 'Nama Syarikat:';
$_['entry_company_id']     = 'ID Syarikat:';
$_['entry_tax_id']         = 'Tax ID:';
$_['entry_address_1']      = 'Alamat:';
$_['entry_address_2']      = 'Alamat 2:';
$_['entry_postcode']       = 'Poskod:';
$_['entry_city']           = 'Bandar:';
$_['entry_country']        = 'Negara:';
$_['entry_zone']           = 'Negeri:';
$_['entry_newsletter']     = 'Langganan Promosi Terkini:';
$_['entry_password']       = 'Password:';
$_['entry_confirm']        = 'Pengesahan Password:';

// Error
$_['error_exists']         = 'Peringatan: Alamat Email telah terdaftar!';
$_['error_firstname']      = 'Nama Hadapan harus terdiri dari 1 hingga 32 karakter!';
$_['error_lastname']       = 'Nama Belakang harus terdiri dari 1 hingga 32 karakter!';
$_['error_email']          = 'Alamat Email tidak valid!';
$_['error_telephone']      = 'No Telefon harus terdiri dari 3 hingga 32 karakter!';
$_['error_password']       = 'Password harus terdiri dari 4 hingga 20 karakter!';
$_['error_confirm']        = 'Pengesahan Password tidak sesuai password!';
$_['error_company_id']     = 'ID Syarikat diperlukan!';
$_['error_tax_id']         = 'Tax ID diperlukan!';
$_['error_vat']            = 'Nomor VAT tidak valid!';
$_['error_address_1']      = 'Alamat 1 harus terdiri dari 3 hingga 128 karakter!';
$_['error_city']           = 'Bandar harus terdiri dari 2 hingga 128 karakter!';
$_['error_postcode']       = 'Poskod harus terdiri dari 2 hingga 10 karakter!';
$_['error_country']        = 'Sila pilih Negara!';
$_['error_zone']           = 'Sila pilih Negeri!';
$_['error_agree']          = 'Peringatan: Anda harus setuju dengan %s!';
?>