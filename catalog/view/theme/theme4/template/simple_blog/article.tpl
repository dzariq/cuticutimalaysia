<?php echo $header; ?>
<section id="blog" class="container">
    <div class="center">
        <h2>Blogs</h2>
    </div>
    <div class="blog">
        <div class="row">
            <div class='col-md-8 col-xs-12' >
                <?php if($articles) { ?>
                <?php  foreach($articles as $article) { ?>
                <div class="blog-item">
                    <div class="row">
                        <div class="col-xs-12 col-sm-2 text-center">
                            <div class="entry-meta">
                                <span id="publish_date"><?php echo $article['date_added']; ?></span>
                                <span><i class="fa fa-user"></i> <a href="<?php echo $article[author_href]; ?>"><?php echo $article[author_name]; ?></a></span>
                                <span><i class="fa fa-comment"></i> <a href="<?php echo $article['comment_href']; ?>#comment-section"><?php echo $article['total_comment']; ?> Comments</a></span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-10 blog-content">
                            <a href="<?php echo $article['href']; ?>">
                                <img style='margin-bottom:45px !Important' class="img-responsive img-blog" src="<?php echo $article['image']; ?>" width="100%" alt="" />
                            </a>
                            <h2><a href="<?php echo $article['href']; ?>"><?php echo $article['article_title']; ?></a></h2>
                            <h3>
                                <?php echo $article['description']; ?>
                            </h3>
                            <a class="btn btn-primary readmore" href="<?php echo $article['href']; ?>">
                                Read More <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>    
                </div><!--/.blog-item-->
                <?php } ?>
                
                <div class="pagination"><?php echo $pagination; ?></div>

                <?php }else { ?>
                <div class="center">
                    <?php echo $text_no_found; ?>
                </div>
                <?php } ?>


            </div>
            <div class='col-md-4 col-xs-12'>
                <h2><?php echo $column_right; ?></h2>
            </div>
        </div>
    </div>
</section>


<?php echo $footer; ?>	