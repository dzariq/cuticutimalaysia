<?php

################################################################################################
#  Auto Module Builder for Opencart 1.5.1.x From HostJars http://opencart.hostjars.com    	   #
################################################################################################

class ControllerModuleGallery extends Controller {

    protected function index($setting) {

        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $this->data['limit'] = $setting['limit'];
        $this->data['scroll'] = $setting['scroll'];
        $this->data['title'] = $setting['title'];
        $this->data['description'] = $setting['description'];
        $this->data['link'] = $setting['link'];
        $this->data['linkname'] = $setting['linkname'];

        $this->data['banners'] = array();

        $results = $this->model_design_banner->getBanner($setting['banner_id']);

        foreach ($results as $result) {

            $tagArray = Array();

            if ($result['tagging'] != '') {
                $temp = explode(',', $result['tagging']);
                if (count($temp) != 0) {
                    foreach ($temp as $val) {
                        $taggingArray[] = $val;
                        $tagArray[] = $val;
                    }
                }
            }


            if (file_exists(DIR_IMAGE . $result['image'])) {
                $this->data['banners'][] = array(
                    'title' => $result['title'],
                    'link' => $result['link'],
                    'description' => nl2br($result['description']),
                    'link_name' => $result['link_name'],
                    'tagging' => $tagArray,
                    'image' => $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']),
                    'imagepopup' => 'image/'.$result['image'],
                    'imagelarge' => $this->model_tool_image->resize($result['imagelarge'], $setting['image_width'] * 1.5, $setting['image_height'] * 1.5)
                );
            }
        }

        $this->data['taggingArray'] = array_unique($taggingArray);

        $this->data['module'] = $module++;

        $this->language->load('module/gallery');

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('module/gallery');


        if ($setting['position'] == 'footer') {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/gallery_footer.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/module/gallery_footer.tpl';
            } else {
                $this->template = 'default/template/module/gallery_footer.tpl';
            }
        } else {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/gallery.tpl')) {
                $this->template = $this->config->get('config_template') . '/template/module/gallery.tpl';
            } else {
                $this->template = 'default/template/module/gallery.tpl';
            }
        }

        $this->render();
    }

}

?>