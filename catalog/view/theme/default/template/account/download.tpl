<?php echo $header; ?>
<div class="<?php echo layout ?>">
<div class="row">
	<div class="col-md-12">
        <ol class="breadcrumb hidden-xs">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ol>
    </div>
         <div class="col-md-3">
                  <?php echo $column_left ?>
               </div>
               <div class="col-md-9">
        <?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
        <?php foreach ($downloads as $download) { ?>
            <article class="download-list">
                <div class="download-id">
                    <strong><?php echo $text_order; ?></strong> 
                    <?php echo $download['order_id']; ?>
                </div>
                <div class="download-status">
                    <strong><?php echo $text_size; ?></strong> 
                    <?php echo $download['size']; ?>
                </div>
                <div class="download-content">
                    <div>
                        <strong><?php echo $text_name; ?></strong> 
                        <?php echo $download['name']; ?>
                        <br />
                        <strong><?php echo $text_date_added; ?></strong> 
                        <?php echo $download['date_added']; ?>
                    </div>
                    <div>
                        <strong><?php echo $text_remaining; ?></strong> 
                        <?php echo $download['remaining']; ?>
                    </div>
                    <div class="download-info">
                        <a href="<?php echo $download['href']; ?>" class="tooltip-item" data-toggle="tooltip" title="<?php echo $button_download; ?>"><span class="glyphicon glyphicon-download-alt"></span></a>
                    </div>
                </div>
            </article>
        <?php } ?>
        <div class="pull-right clearfix"><?php echo $pagination; ?></div>
        <div class="pull-right">
            <a href="<?php echo $continue; ?>" class="btn btn-info"><?php echo $button_continue; ?></a>
        </div>
        <?php echo $content_bottom; ?>
    </div>
</div>
</div>
<?php echo $footer; ?>