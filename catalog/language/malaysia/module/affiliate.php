<?php
// Heading 
$_['heading_title']    = 'Afiliasi';

// Text
$_['text_register']    = 'Daftar';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Lupa Password';
$_['text_account']     = 'Akaun Saya';
$_['text_edit']        = 'Ubah Akaun';
$_['text_password']    = 'Password';
$_['text_payment']     = 'Option Pembayaran';
$_['text_tracking']    = 'Tracking Afiliasi';
$_['text_transaction'] = 'Transaksi';
?>
