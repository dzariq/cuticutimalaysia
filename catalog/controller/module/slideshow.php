<?php

class ControllerModuleSlideshow extends Controller {

    protected function index($setting) {
        static $module = 0;

        $this->load->model('design/banner');
        $this->load->model('tool/image');

        $this->document->addScript('catalog/view/javascript/jquery/nivo-slider/jquery.nivo.slider.pack.js');

        if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/slideshow.css')) {
            $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/slideshow.css');
        } else {
            $this->document->addStyle('catalog/view/theme/default/stylesheet/slideshow.css');
        }

        if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
//            $this->data['logo'] = $server . $this->config->get('config_logo');
            $this->data['logo'] = 'image/' . $this->config->get('config_logo');
        } else {
            $this->data['logo'] = '';
        }

        $this->data['width'] = $setting['width'];
        $this->data['height'] = $setting['height'];

        $this->data['banners'] = array();


        $this->load->model('catalog/product');



        $this->load->model('tool/image');



        $this->data['products'] = array();



        $products = explode(',', $this->config->get('featured_product'));



        if (empty($setting['limit'])) {

            $setting['limit'] = 15;
        }



        $products = array_slice($products, 0, (int) $setting['limit']);



        foreach ($products as $product_id) {

            $product_info = $this->model_catalog_product->getProduct($product_id);



            if ($product_info) {

                if ($product_info['image']) {

                    $image = $this->model_tool_image->resize($product_info['image'], 400, 530);
                    $image2 = $this->model_tool_image->resize($product_info['image'], '398', '398');
                } else {

                    $image = false;
                    $image2 = false;
                }



                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {

                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $price = false;
                }



                if ((float) $product_info['special']) {

                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {

                    $special = false;
                }



                if ($this->config->get('config_review_status')) {

                    $rating = $product_info['rating'];
                } else {

                    $rating = false;
                }



                $this->data['featured'][] = array(
                    'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'product_id' => $product_info['product_id'],
                    'thumb' => $image,
                    'thumb_large' => $image2,
                    'name' => $product_info['name'],
                    'viewed' => $product_info['viewed'],
                    'price' => $price,
                    'special' => $special,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int) $product_info['reviews']),
                    'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                );
            }
        }

        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $Linux = stripos($_SERVER['HTTP_USER_AGENT'], "Linux");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");


        if (isset($setting['banner_id'])) {
            $results = $this->model_design_banner->getBanner($setting['banner_id']);

            foreach ($results as $result) {
                if (file_exists(DIR_IMAGE . $result['image'])) {

                  
                        $this->data['banners'][] = array(
                            'title' => $result['title'],
                            'link' => $result['link'],
                            'description' => $result['description'],
                            'link_name' => $result['link_name'],
                            'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
                            'imageraw' => 'image/' . $result['image']
                        );
                }
            }
        }

        $this->data['module'] = $module++;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/slideshow.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/slideshow.tpl';
        } else {
            $this->template = 'default/template/module/slideshow.tpl';
        }



        $this->render();
    }

}

?>