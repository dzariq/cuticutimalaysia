

<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<section class="innerpage-wrapper">
    <div id="hotel-details" class="product-info innerpage-section-padding">
        <div class="container">
            <div class="row">        	
                <div class="col-xs-12" >
                    <div class="booking-form">
                        <h3><?php echo $content_top; ?></h3>
                        <h1><?php echo $text_payment_title; ?></h1>
                        <div class="content"><div style="color: <?php echo $color; ?>"><?php echo $text_payment_status; ?></div></div>
                        <br/>
                        <div class="buttons">
                            <div class="right"><a href="<?php echo $continue; ?>" class="btn btn-orange"><?php echo $button_continue; ?></a></div>
                        </div>
                        <?php echo $content_bottom; ?>
                    </div>
                </div>
            </div>
            </section>
            <?php echo $footer; ?>