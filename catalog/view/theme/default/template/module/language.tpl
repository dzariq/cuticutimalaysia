<?php if (count($languages) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div id="language" style='margin-top:12px;color:black'>
    <?php foreach ($languages as $language) { ?>
        <?php if($this->session->data['language'] == $language['code']){ ?>
            <div  class="metromenu col-xs-6">
                <?php echo strtoupper($language['code']); ?>
            </div>
        <?php }else{ ?>
            <div onclick="$('input[name=\'language_code\']').attr('value', '<?php echo $language[code]; ?>'); $(this).parent().parent().submit();" class="metromenu-inactive col-xs-6">
                <?php echo strtoupper($language['code']); ?>
            </div>
        <?php } ?>
    <?php } ?>
    <input type="hidden" name="language_code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>
