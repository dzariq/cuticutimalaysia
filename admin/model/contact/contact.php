<?php

Class ModelContactContact extends Model {

    public function getContactinfo($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "contact ";
// renamed filter_category_id to filter_category
        if (!empty($data['filter_category'])) {
            $sql .= " WHERE category = '" . $this->db->escape($data['filter_category']) . "'";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getContactCategories() {
        $query = $this->db->query("SELECT DISTINCT(category) FROM " . DB_PREFIX . "contact");

        return $query->rows;
    }

    public function getSingledata($id = '') {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact where contact_id='$id' ");

        return $query->rows;
    }

    public function csvdata() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact");
        return $query->rows;
    }

    public function insertvalue($view_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "contact SET is_read='$view_id' where contact_id ='$view_id'");
    }

    public function getTotalContactsByEmail($email) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "contact where email='$email' ");

        return $query->num_rows;
    }

    public function get_client_ip() {

        $ipaddress = '';

        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');

        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');

        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');

        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');

        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');

        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';



        return $ipaddress;
    }

    public function updateContact($data) {

        $this->db->query("DELETE  FROM " . DB_PREFIX . "contact where email = '" . $data['email'] . "' ");

        $ip = $this->get_client_ip();

        if (!isset($data['category']) || $data['category'] == '') {

            $data['category'] = 'contact';
        }



        $details = array();

        $i = 0;



        //check if there are details content

        foreach ($data as $key => $item) {

            $temp = explode('_', $key);

            if ($temp[0] == 'details') {

                $details[$i]['name'] = $temp[1];

                $details[$i]['value'] = $item;

                $details[$i]['type'] = $temp[2];
            }

            $i++;
        }



        $details = serialize($details);

        

        $this->db->query("INSERT INTO " . DB_PREFIX . "contact SET attachment2 = '" . $this->db->escape($data['attachment_2']) . "',attachment = '" . $this->db->escape($data['attachment']) . "', details = '" . $details . "', firstname = '" . $this->db->escape($data['name']) . "', category =  '" . $this->db->escape($data['category']) . "', email = '" . $this->db->escape($data['email']) . "', enquiry = '" . $this->db->escape($data['enquiry']) . "', ipaddress = '$ip'");
    }

    public function deletecontact($contact_id) {
        $query = $this->db->query("DELETE  FROM " . DB_PREFIX . "contact where contact_id = '" . $contact_id . "' ");

        return $query;
    }

}

?>