<?php
/**
 * @author Qphoria@gmail.com
 * @web http://www.opencartguru.com/
 *
 * @usage
 *		$params = array(
 *			'xxx' => 'value1',
 *			'yyy' => 'value2',
 *			'zzz' => 'value3',
 *		);
 *
 *		$payclass = New PayClass();
 *		$payclass->sendPayment($params);
 */

class webcash {
	
	private $_url = 'https://webcash.com.my/wcgatewayinit.php';
	private $_testurl = 'https://webcash.com.my/testwebcash/wcgatewayinit.php';
	private $_log = '';

	public function __construct($logpath = '') {
		if ($logpath && is_dir($logpath) && is_writable($logpath)) {	$this->_log = $logpath .  basename(__FILE__, '.php') . '.log'; }
	}
	
	public function buildOutput($params) {
		
		$url = $this->_url;
		if (isset($params['test'])) {
			unset($params['test']);
			$url = $this->_testurl;
		}	
				
		$data  = 'Redirecting...';
		$data .= '<form action="'.$url.'" id="payform" method="post">';
		foreach ($params as $key => $value) {
			$data .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />';
		}
		$data .= '<input type="submit" value="-->" />';
		$data .= '</form>';
		$data .= '<script type="text/javascript">';
		$data .= 'document.forms["payform"].submit();';
		$data .= '</script>';

		$this->writeLog($data);
		return $data;
	}

	private function writeLog($msg) {
		if ($this->_log) {
			$msg = (str_repeat('-', 70) . "\r\n" . $msg . "\r\n" . str_repeat('-', 70) . "\r\n");
			file_put_contents($this->_log, $msg, FILE_APPEND);
		}
	}
}
?>