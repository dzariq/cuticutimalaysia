</div> <!-- Close <?php echo layout ?> div from header -->
<!-- Footer starts -->
<?php
$layout = 'container';
if($this->config->get('config_package') == 1){
$layout = 'container-full';
}




?>

<footer style='box-shadow: none;'>
    <div class="<?php echo $layout ?>">

        <style>
            @media (max-width: 768px) {
                .col-xs-12 {
                    margin-top:20px;
                }
            }
            ul {
                list-style: none;
                line-height: 1.3;
            }
            @media (max-width:315px){
                footer h3{
                    font-size:12px !Important;
                }
                footer span, footer a, footer p{
                    font-size:10px !Important;
                }
                footer img{
                    width:8% !Important;
                    height:8% !Important;
                    margin-top:6px !Important
                }
            }


            footer{
                padding-top:20px;
                letter-spacing: 2;
            }

            footer span, footer a, footer p{
                font-size:12px ;
                border:none !important;
            }


        </style>
        <div style='box-shadow: none;' class=" row ">
            <div style="line-height:12" class="text-center col-md-2 col-sm-12 col-xs-12 ">
                <?php if($this->config->get('config_logo') != ''){ ?>
                <img style="margin:110px auto;" class="img-responsive" src='image/<?php echo $this->config->get("config_logo") ?>' />
                <?php }else{ ?>
                <div style="line-height:1.5" class="logo" >
                    <?php echo $this->config->get("config_name") ?>
                </div>
                <?php } ?>
            </div>
            <div class=" col-md-3 col-sm-12 col-sm-12 ">
                <h4><?php echo $text_about ?></h4>
                <div class="fwidget">
                    <p ><?php echo $this->config->get('config_meta_description') ?></p>
                    <br/>
                    <?php if($this->config->get('config_twitter') != ''){ ?>
                        <a style="color:white !important;padding-right:7px;background:#55ACEE" target="_blank" href="<?php echo $this->config->get('config_twitter') ?>" class="btn-sm btn-social-icon btn-twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                    &nbsp;&nbsp;
                    <?php } ?>
                    <?php if($this->config->get('config_fb') != ''){ ?>
                    <a style="color:white !important;background:#3B5998" target="_blank" href="<?php echo $this->config->get('config_fb') ?>" class="btn-sm btn-social-icon btn-facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                    &nbsp;&nbsp;
                    <?php } ?>
                    <?php if($this->config->get('config_insta') != ''){ ?>
                        <a style="color:white !important;background:#3F729B" target="_blank" href="<?php echo $this->config->get('config_insta') ?>" class="btn-sm btn-social-icon btn-instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 ">
                <h4><?php echo $text_information ?></h4>
                <?php if($informations){ ?>
                <ul>
                    <?php foreach($informations as $information){ ?>
                    <li><a style='margin-left:-13px;' href="<?php echo $information['href'] ?>" ><?php echo $information['title'] ?></a>
                    </li>
                    <?php } ?>
                    <li><a  style='margin-left:-13px;' href="index.php?route=information/contact" ><?php echo $text_contact ?></a>
                    </li>
                    <li><a  style='margin-left:-13px;' href="index.php?route=information/sitemap" >Site Map</a>
                    </li>
                </ul>
                <?php } ?>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 ">
                <?php if($this->config->get('config_fb') != ''){ ?>
                <iframe src="//www.facebook.com/plugins/likebox.php?href=<?php echo urlencode($this->config->get('config_fb')) ?>&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=800733466639218" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;" allowTransparency="true"></iframe>
                <?php }else{ ?>
                <?php } ?>

            </div>
        </div>
        <hr/>
        <div class="row">
            <div   class="col-md-12 copy text-center">
                <?php echo $this->config->get('config_name') ?> <?php echo date('Y') ?> &copy; - <a   target="_blank" href="http://www.creativeparttime.com">Designed by Creativeparttime.com</a>
            </div>
        </div>
    </div>
</footer>
<!-- Footer ends -->
<!-- Javascript files -->

<!-- Compulsory JS -->
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen, projection" />

<!-- Add-on JS -->
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/jquery.navgoco.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/filter.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/respond.min.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/html5shiv.js"></script>
<script type="text/javascript" src="catalog/view/theme/<?php echo $this->config->get('config_template');?>/js/custom.js"></script>
</body>
</html>

