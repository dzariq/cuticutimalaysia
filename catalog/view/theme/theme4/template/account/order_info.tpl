<?php echo $header; ?>
<section>
    <div class="container">
        <div class="row">
            <h2><?php echo $heading_title; ?></h2>
            <style>
                table td{
                    border:1px solid #e0e0e0;

                }
                table{
                    width:100%
                }
            </style>
            <div class="col-md-3">
                <?php echo $column_left ?>
            </div>
            <div id="content" class="col-md-9">
                <?php echo $content_top; ?>


                <table class="table table-striped tcart">
                    <thead>
                        <tr>
                            <td class="left" colspan="2"><?php echo $text_order_detail; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="left" style="width: 50%;">
                                <?php if ($invoice_no) { ?>
                                <strong><?php echo $text_invoice_no; ?></strong>
                                <?php echo $invoice_no; ?>
                                <br />
                                <?php } ?>
                                <strong><?php echo $text_order_id; ?></strong> 
                                #<?php echo $order_id; ?>
                                <br />
                                <strong><?php echo $text_date_added; ?></strong> 
                                <?php echo $date_added; ?>
                            </td>
                            <td class="left" style="width: 50%;">
                                <?php if ($payment_method) { ?>
                                <strong><?php echo $text_payment_method; ?></strong> 
                                <?php echo $payment_method; ?>
                                <br />
                                <?php } ?>
                              
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="list">
                    <thead>
                        <tr>
                            <td class="left"><?php echo $text_payment_address; ?></td>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="left"><?php echo $payment_address; ?></td>
                          
                        </tr>
                    </tbody>
                </table>
                <br/>
                <table class="list">
                    <thead>
                        <tr>
                            <td class="left"><?php echo $column_name; ?></td>
                            <td class="right"><?php echo $column_quantity; ?></td>
                            <td class="right"><?php echo $column_price; ?></td>
                            <td class="right"><?php echo $column_total; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($products as $product) { ?>
                        <tr>
                            <td class="left">
                                <?php echo $product['name']; ?>
                                <?php foreach ($product['option'] as $option) { ?>
                                <br />
                                &nbsp;
                                <small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                <?php } ?>
                            </td>
                            <td class="right"><?php echo $product['quantity']; ?></td>
                            <td class="right"><?php echo $product['price']; ?></td>
                            <td class="right"><?php echo $product['total']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php foreach ($vouchers as $voucher) { ?>
                        <tr>
                            <td class="left"><?php echo $voucher['description']; ?></td>
                            <td class="left"></td>
                            <td class="right">1</td>
                            <td class="right"><?php echo $voucher['amount']; ?></td>
                            <td class="right"><?php echo $voucher['amount']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td colspan="2"></td>
                            <td class="right"><strong><?php echo $total['title']; ?>:</strong></td>
                            <td class="right"><?php echo $total['text']; ?></td>
                        </tr>
                        <?php } ?>
                    </tfoot>
                </table>
                <?php if ($comment) { ?>
                <table class="list">
                    <thead>
                        <tr>
                            <td class="left"><?php echo $text_comment; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="left"><?php echo $comment; ?></td>
                        </tr>
                    </tbody>
                </table>
                <?php } ?>
                <?php if ($histories) { ?>
                <h2><?php echo $text_history; ?></h2>
                <table class="list">
                    <thead>
                        <tr>
                            <td class="left"><?php echo $column_date_added; ?></td>
                            <td class="left"><?php echo $column_status; ?></td>
                            <td class="left"><?php echo $column_comment; ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($histories as $history) { ?>
                        <tr>
                            <td class="left"><?php echo $history['date_added']; ?></td>
                            <td class="left"><?php echo $history['status']; ?></td>
                            <td class="left"><?php echo $history['comment']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php } ?>
                <div class="pull-right">
                    <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                </div>
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
</section>
<?php echo $footer; ?> 