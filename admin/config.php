<?php

//setting
$url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";


$parse = parse_url($url);


//CONFIG HERE

$domain_name = $parse['host'];

if ($domain_name == 'staging.cuticuti-malaysia.com')
{
    $db_host = 'localhost';
    $db_username = 'cuticuti_manager';
    $db_password = 'amkay9793';
    $db_name = 'cuticuti_v6';
    $domain_name = 'staging.cuticuti-malaysia.com';
    define('SERVERURL', '/home/cuticuti/staging.cuticuti-malaysia.com/');
}
else
{
    $db_host = 'localhost';
    $db_username = 'root';
    $db_password = '';
    $db_name = 'cuti';
    $domain_name = 'localhost/cuti2019';
    define('SERVERURL', 'C://xampp/htdocs/cuti2019/');
}

//CONFIG HERE



define('DOMAIN_NAME', $domain_name);



// HTTP
define('HTTP_SERVER', 'http://' . $domain_name . '/admin/');
define('HTTP_CATALOG', 'http://' . $domain_name . '/');
define('HTTP_IMAGE', 'http://' . $domain_name . '/image/');

// HTTPS
define('HTTPS_SERVER', 'https://' . $domain_name . '/admin/');
define('HTTPS_CATALOG', 'https://' . $domain_name);
define('HTTPS_IMAGE', 'https://' . $domain_name . '/image/');

// DIR
define('DIR_APPLICATION', SERVERURL . 'admin/');
define('DIR_SYSTEM', SERVERURL . 'system/');
define('DIR_DATABASE', SERVERURL . 'system/database/');
define('DIR_LANGUAGE', SERVERURL . 'admin/language/');
define('DIR_TEMPLATE', SERVERURL . 'admin/view/template/');
define('DIR_CONFIG', SERVERURL . 'system/config/');
define('DIR_IMAGE', SERVERURL . 'image/');
define('DIR_CACHE', SERVERURL . '/system/cache/');
define('DIR_DOWNLOAD', SERVERURL . 'download/');
define('DIR_LOGS', SERVERURL . 'system/logs/');
define('DIR_CATALOG', SERVERURL . 'catalog/');



define('DIR_IMAGECLIENT', 'cuticuti');

// DB
define('DB_DRIVER', 'mysqliz');

define('DB_HOSTNAME', $db_host);
define('DB_USERNAME', $db_username);
define('DB_PASSWORD', $db_password);
define('DB_DATABASE', $db_name);
define('DB_NAME', $db_name);

define('DB_PREFIX', '');
?>