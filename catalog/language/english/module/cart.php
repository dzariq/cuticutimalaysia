<?php
// Heading 
$_['heading_title'] = 'Booking';

// Text
$_['text_items']    = '%s item(s) - %s';
$_['text_empty']    = 'Your booking is empty!';
$_['text_cart']     = 'View Booking';
$_['text_checkout'] = 'Checkout';
?>