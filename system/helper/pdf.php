<?php
function pdf($data, $name) {
    if (count($name) > 1) {
		$name = "CutiCutiMalaysia_Booking_ID";
	}else{
		$name = 'CutiCutiMalaysia_Booking_ID_'.$name[0]['booking_voucher'];
	}
    $pdf = new DOMPDF;
    $pdf->load_html($data);
    $pdf->render();
    $pdf->stream($name.".pdf");
}
?>