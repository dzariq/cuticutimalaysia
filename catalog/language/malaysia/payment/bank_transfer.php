<?php
// Text
$_['text_title']       = 'Pindahan Bank Manual';
$_['text_instruction'] = 'Pindahan Bank';
$_['text_description'] = 'Sila pindah jumlah harga pembayaran ke nombor akaun berikut.';
$_['text_payment']     = 'Pesanan akan dikirim setelah kami menerima & mengesahkan pembayaran dari Anda.';
?>