<?php
// Heading
$_['heading_title']					 = 'Pesanan Anda telah diproses!';
$_['heading_title_customer'] = 'Pesanan Anda #%s telah di proses!';

// Text
$_['text_customer'] = '<p>Pesanan Anda telah Berjaya diproses!</p><p>Anda dapat melihat sejarah pesanan di <a  style="color:black" href="%s">Sejarah Pesanan</a>.</p><p>Jika ada pertanyaan sila <a href="%s">hubungi kami</a>.</p><p>Terima kasih telah membeli-belah di kedai kami!</p>';
$_['text_guest']    = '<p>Pesanan Anda telah Berjaya diproses!</p><p>Jika ada pertanyaan sila hubungi kami.</p><p>Terima kasih telah membeli-belah di kedai kami!</p>';
$_['text_basket']   = 'Bakul';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Berjaya';
?>