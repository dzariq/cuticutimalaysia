<?php
// Heading 
$_['heading_title']   = 'Lupa Password Anda?';

// Text
$_['text_account']    = 'Akaun';
$_['text_forgotten']  = 'Lupa Password';
$_['text_your_email'] = 'Alamat Email Anda';
$_['text_email']      = 'Masukkan alamat Email yang Anda gunakan sewaktu mendaftar Akaun dulu. Kemudian klik butang "Teruskan" untuk mereset password Anda.';
$_['text_success']    = 'Berjaya: Password baru Berjaya dikirim ke alamat Email Anda.';

// Entry
$_['entry_email']     = 'Alamat Email:';

// Error
$_['error_email']     = 'Peringatan: Alamat Email tidak ditemukan dalam sistem kami, silahkan cuba lagi!';
?>