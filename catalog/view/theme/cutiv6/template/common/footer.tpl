<!--======================= FOOTER =======================-->
<section id="footer" class="ftr-heading-w ftr-heading-mgn-2">

    <div id="footer-top" class="banner-padding ftr-top-grey ftr-text-grey">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 footer-widget ftr-about ftr-our-company">
                    <h3 class="footer-heading">OUR COMPANY</h3>
                    <p>Our very own online Travel Agent which offers you the best rates, travel packages from hotels and resorts in Malaysia.</p>
                    <ul class="social-links list-inline list-unstyled">
                        <li><a href="https://www.facebook.com/cuticutimalaysia2010/"><span><i class="fa fa-facebook"></i></span></a></li>
                        <li><a href="https://www.instagram.com/cuticutimalaysia.official/"><span><i class="fa fa-instagram"></i></span></a></li>
                        <li><a href="https://twitter.com/cuticutimalays1"><span><i class="fa fa-twitter"></i></span></a></li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 footer-widget ftr-map">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.810586187923!2d101.47918831410644!3d3.144639997710239!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc518fea3f4f4b%3A0xc58c444fa93ce0a2!2sCuti-Cuti+Malaysia+(CCM)!5e0!3m2!1sen!2smy!4v1545982787490"  allowfullscreen></iframe>
                    </div>
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-top -->

    <div id="footer-bottom" class="ftr-bot-black">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
                    <p>© 2019 <a href="#">Cuti Cuti Malaysia</a>. All rights reserved.</p>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
                    <ul class="list-unstyled list-inline">
                        <li><a href="index.php?route=information/information&information_id=13">Terms & Condition</a></li>
                        <li><a href="index.php?route=information/information&information_id=14">Privacy Policy</a></li>
                    </ul>
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-bottom -->

</section><!-- end footer -->


<!-- Page Scripts Starts -->

<script type="text/javascript" src="catalog/view/javascript/blog_search.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script src="<?php echo $theme_path ?>js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $theme_path ?>js/bootstrap.min.js"></script>
<script src="<?php echo $theme_path ?>js/jquery.flexslider.js"></script>
<script src="<?php echo $theme_path ?>js/owl.carousel.min.js"></script>
<script src="<?php echo $theme_path ?>js/custom-navigation.js"></script>
<script src="<?php echo $theme_path ?>js/slick.min.js"></script>
<script src="<?php echo $theme_path ?>js/custom-flex.js"></script>
<script src="<?php echo $theme_path ?>js/custom-owl.js"></script>
<script src="<?php echo $theme_path ?>js/custom-gallery.js"></script>
<script src="<?php echo $theme_path ?>js/custom-slick.js"></script>
<script>
    $('.dpd3').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: "<?php echo date('d/m/Y', strtotime(' +2 day')) ?>",
    });
</script>
<!-- Page Scripts Ends -->

</body>
</html>