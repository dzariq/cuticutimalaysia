<?php echo $header; ?>
<?php if ($success) { ?>
<div class="alert alert-success">
    <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<section class="innerpage-wrapper">
    <div id="dashboard" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="dashboard-heading">
                        <h2>Cuti Cuti <span>Profile</span></h2>
                        <p>Hi <?php echo $this->customer->getFirstName() ?>, Welcome to Cuti Cuti Malaysia!</p>
                        <p>All your trips booked with us will appear here and you'll be able to manage everything!</p>
                    </div><!-- end dashboard-heading -->

                    <div class="dashboard-wrapper">
                        <div class="row">

                            <div class="col-xs-12 col-sm-2 col-md-2 dashboard-nav">
                                <ul class="nav nav-tabs nav-stacked text-center">
                                    <li class="active"><a href="index.php?route=account/account"><span><i class="fa fa-user"></i></span>Profile</a></li>
                                    <li><a href="index.php?route=account/order"><span><i class="fa fa-briefcase"></i></span>Booking</a></li>
                                </ul>
                            </div><!-- end columns -->

                            <div class="col-xs-12 col-sm-10 col-md-10 dashboard-content user-profile">
                                <h2 class="dash-content-title">My Profile</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading"><h4>Change Password</h4></div>
                                    <div class="panel-body">
                                        <div class="row">

                                            <form role="form" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="password">
                                                <div class="form-group">
                                                    <label for="password" class="required"><?php echo $entry_password; ?></label>
                                                    <input class="form-control" type="password" id="password" name="password" value="<?php echo $password; ?>" autofocus />
                                                    <?php if ($error_password) { ?>
                                                    <span class="error"><?php echo $error_password; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="form-group">
                                                    <label for="confirm" class="required"><?php echo $entry_confirm; ?></label>
                                                    <input class="form-control" type="password" id="confirm" name="confirm" value="<?php echo $confirm; ?>" />
                                                    <?php if ($error_confirm) { ?>
                                                    <span class="error"><?php echo $error_confirm; ?></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="pull-left">
                                                    <a href="<?php echo $back; ?>" ><button class="btn btn-orange" >BACK</button></a>
                                                </div>
                                                <div class="pull-right">
                                                    <a onclick="$('#password').submit();" ><button class="btn btn-orange" >SAVE</button></a>
                                                </div>
                                            </form>

                                        </div><!-- end row -->

                                    </div><!-- end panel-body -->
                                </div><!-- end panel-detault -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end dashboard-wrapper -->
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->          
    </div><!-- end dashboard -->
</section><!-- end innerpage-wrapper -->


<?php echo $footer; ?> 