
<div class="col-sm-2 col-xs-4">
    <div id="tile9" class="tile">
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-book bigicon"></span>
                    </div>
                    <div class="icontext">
                        Menu items
                    </div>
                    <div class="icontext">
                        120
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="col-sm-4 col-xs-8">
    <div id="tile10" class="tile">
        <div class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active text-center">
                    <div>
                        <span class="fa fa-phone bigicon"></span>
                    </div>
                    <div class="icontext">
                        917-2233-4433
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>